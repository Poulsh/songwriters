<p align="center">
    <a href="https://songwriters.pro/assets/img/songwriters_logo_white.svg" target="_blank">
        <img src="https://songwriters.pro/assets/img/songwriters_logo_white.svg" height="100px">
    </a>
    <h1 align="center">Songwriters</h1>
    <br>
</p>

http://cp.songwriters.local/  
http://songwriters.local

Dev environment config:   
MAMP NGINX, php 7.1 (7.4 ?)  
Nginx port 8888  (Apache 80)  
MySQL port 8889

make build-dev
  
(если не проходит CORS - еще раз перезапустить MAMP)

Project maintenance by makefile

http://songwriters.local   
######traslation process

---

*make locale - create locale json file for translation "client/src/locale/locale.json"*
- (compose the markup)  
- make locale  
- copy to locale.ru.json / locale.en.json  (translate to locale.en.json)   
- make build-dev   
- (check at http://songwriters.local)



---
```
<span  
     i18n="@@common_song_nameLabel"  
     class="label">Название</span>

<button                        
    i18n-matTooltip="@@aHome_deleteTrackFromRadio"
    matTooltip="Удалить из радио секции"
    ><mat-icon
            i18n-aria-label="@@aHome_deleteTrackFromRadio"
            aria-label="Удалить из радио секции">delete</mat-icon>
</button>
```
---

translation done: 
-------------------------
pipes/status  
artist-my-song-view  
artist-premium-view  
artist-radio-filling-out  
artist-radio-filling-out-window  
artist-radio-index   
artist-radio-new  
artist-radio-view  
artist-sidebar-menu  
hello   
login  
password-reset +letter +link  
password-reset-form   
signup  +letter 
onefield-form
notification 
user language db table field -> change on switch
messages admin to artist about song
radio-all-tracks
radio-selected-tracks
radio-artist-view
radio-edit-personal-info-block

? "date:"
? "this.metaTitle"
при создании если не указать название плейлиста прилетает непереведенная ошибка
 

# Multilang #
--- run ---  
make buil-dev  
http://songwriters.local/

### Frontend ###

```
i18n-matTooltip="@@aHome_deleteTrackFromRadio"
```

change locale:  
language > language-switch

change switch doesn't work for run dev!

Getting locale in project
-------------------------

in .ts:
```
import { LOCALE_ID, Inject } from '@angular/core';

constructor(
   @Inject(LOCALE_ID) public locale: string,
){}
```

in template:  
```
{{locale=='ru'? option.name_ru:option.name_eng}}

[buttonName]="locale=='ru'?'Сохранить':'Save'"
```

sets header App-Language in HeaderInterceptor

### Backend ##
```

  $lang = User::getLanguageByUserID($id);
 
  Yii::$app->request->headers->get('App-Language'),  
  
  Yii::$app->request->headers->get('App-Language')=='ru'?'Сброс пароля':'Password reset'  
  
  \Yii::$app->language=='ru-RU'?'Сброс пароля':'Password reset'
  
  \Yii::t('api', 'Пользователь с email {email} не найден', [
                  'email' => $email,
              ])
              
  \Yii::t('api', 'LINK_TO_PASS_SENT', [
                      'email' => $email,
                  ])
```


DIRECTORY STRUCTURE
-------------------

```
client                   frontend - Angular  typescript
    dist                 built files for production
    e2e
    node modules
    src                  sorce files
        app              project
        assets
        environments
        locale           contains files for multi languages
server                   backend - Yii2 php
    api
        assets/              contains application assets such as JavaScript and CSS
        config/              contains api configurations
        controllers/         contains Web controller classes
        models/              contains frontend-specific model classes
        runtime/             contains files generated during runtime
        tests/               contains tests for frontend application
        views/               contains view files for the Web application
        web/                 contains the entry script and Web resources
        widgets/             contains frontend widgets
    common
        components/
        config/              contains shared configurations  
        img/                 contains progect images
        mail/                contains view files for e-mails
        models/              contains model classes used in both backend and frontend
        tests/               contains tests for common classes    
        uploadedsongs/       contains songs, audio files
            /listen          contains mp3 files
            /source          contains source files
    console
        config/              contains console configurations
        controllers/         contains console controllers (commands)
        migrations/          contains database migrations
        models/              contains console-specific model classes
        runtime/             contains files generated during runtime
    backend
        assets/              contains application assets such as JavaScript and CSS
        config/              contains backend configurations
        controllers/         contains Web controller classes
        models/              contains backend-specific model classes
        runtime/             contains files generated during runtime
        tests/               contains tests for backend application    
        views/               contains view files for the Web application
        web/                 contains the entry script and Web resources
  
    vendor/                  contains dependent 3rd-party packages
    environments/            contains environment-based overrides
```
