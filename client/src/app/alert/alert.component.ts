
import {DOCUMENT} from '@angular/common';
import {Component,OnInit, Inject, TemplateRef, ViewChild} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent  {

  config = {
    disableClose: false,
    panelClass: 'custom-overlay-pane-class',
    hasBackdrop: true,
    backdropClass: '',
    width: '',
    height: '',
    minWidth: '',
    minHeight: '',
    maxWidth: '',
    maxHeight: '',
    position: {
      top: '',
      bottom: '',
      left: '',
      right: ''
    },
  };


  constructor(
      public dialog: MatDialog,
      public dialogRef: MatDialogRef<AlertComponent>,
      @Inject(DOCUMENT) doc: any,
      @Inject(MAT_DIALOG_DATA) public data: any
  ) {

    dialog.afterOpened.subscribe(() => {
      if (!doc.body.classList.contains('no-scroll')) {
        doc.body.classList.add('no-scroll');
      }
    });
    dialog.afterAllClosed.subscribe(() => {
      doc.body.classList.remove('no-scroll');
    });
  };

  closeAlert(): void {
    this.dialogRef.close();
  }

}
