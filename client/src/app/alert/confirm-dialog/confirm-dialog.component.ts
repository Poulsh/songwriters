import { Component, OnInit, Inject, Input, Output } from '@angular/core';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {environment} from "../../../environments/environment";


export interface ConfirmDialogData {
  message: string;
  settings: any;
  head: string,
  yesBtn: string,
  noBtn: string,
  value: any,
  noBtnValue: any,
  textInputPlaceholder: string,
  textInput: string,
}

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialog implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<ConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData
  ) {}
  onNoClick(): void {
    this.dialogRef.close();
  }

  closeForm(): void {
    this.dialogRef.close();
  }

  ngOnInit(){}
}
