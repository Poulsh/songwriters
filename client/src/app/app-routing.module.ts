import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { HelloComponent } from './auth/hello/hello.component';
import { ArtistHomeComponent } from './artist/artist-home/artist-home.component';

import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { RoleGuard } from './auth/role.guard';
import {VerificationComponent} from "./auth/verification/verification.component";
import {ArtistReviewComponent} from "./artist/artist-review/artist-review.component";
import {ArtistCommercialComponent} from "./artist/artist-commercial/artist-commercial.component";
import {ArtistMySongIndexComponent} from "./artist/artist-my-song-index/artist-my-song-index.component";
import {ArtistMySongViewComponent} from "./artist/artist-my-song-view/artist-my-song-view.component";
import {ArtistMySongEditComponent} from "./artist/artist-my-song-edit/artist-my-song-edit.component";
import {MediaHomeComponent} from "./media/media-home/media-home.component";
import {RadioPlaylistIndexComponent} from "./radio/radio-playlist-index/radio-playlist-index.component";
import {RadioPlaylistViewComponent} from "./radio/radio-playlist-view/radio-playlist-view.component";
import {UserAccountComponent} from "./user/user-account/user-account.component";
import {PasswordResetComponent} from "./auth/password-reset/password-reset.component";
import {PasswordResetFormComponent} from "./auth/password-reset-form/password-reset-form.component";
import {ArtistRadioViewComponent} from "./artist/artist-radio-view/artist-radio-view.component";
import {ArtistRadioIndexComponent} from "./artist/artist-radio-index/artist-radio-index.component";
import {ArtistRadioNewComponent} from "./artist/artist-radio-new/artist-radio-new.component";
import {ArtistRadioFillingOutComponent} from "./artist/artist-radio-filling-out/artist-radio-filling-out.component";
import {ArtistPremiumViewComponent} from "./artist/artist-premium-view/artist-premium-view.component";
import {RadioSongViewComponent} from "./radio/radio-song-view/radio-song-view.component";
import {RadioArtistViewComponent} from "./radio/radio-artist-view/radio-artist-view.component";
import {RadioAllTracksComponent} from "./radio/radio-all-tracks/radio-all-tracks.component";
import {RadioSelectedTracksComponent} from "./radio/radio-selected-tracks/radio-selected-tracks.component";
import {UnauthSongViewComponent} from "./unauth/unauth-song-view/unauth-song-view.component";
import {UnauthRadioMailingComponent} from "./unauth/unauth-radio-mailing/unauth-radio-mailing.component";
import {DocTermsComponent} from "./doc/doc-terms/doc-terms.component";
import {DocViolationComponent} from "./doc/doc-violation/doc-violation.component";
import {DocConfidentialityComponent} from "./doc/doc-confidentiality/doc-confidentiality.component";
import {UnauthContactsComponent} from "./unauth/unauth-contacts/unauth-contacts.component";



export const routes: Routes = [
  {
    path: '',
    redirectTo: 'hello',
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'signup/:key', component: SignupComponent },
  { path: 'hello', component: HelloComponent },
  { path: 'verification/:token', component: VerificationComponent },
  { path: 'account', component: UserAccountComponent},
  { path: 'password-reset', component: PasswordResetComponent },
  { path: 'password-reset-form/:token', component: PasswordResetFormComponent },
  { path: 'terms', component: DocTermsComponent },
  { path: 'violation', component: DocViolationComponent },
  { path: 'confidentiality', component: DocConfidentialityComponent },
  { path: 'contacts', component: UnauthContactsComponent },


  // artist
  { path: 'artist-home', component: ArtistHomeComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-radio', component: ArtistRadioIndexComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-radio-new', component: ArtistRadioNewComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-radio/:id', component: ArtistRadioViewComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-review', component: ArtistReviewComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-commercial', component: ArtistCommercialComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-my-song', component: ArtistMySongIndexComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-my-song-view/:hash', component: ArtistMySongViewComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  { path: 'artist-my-song-edit/:hash', component: ArtistMySongEditComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},


  { path: 'artist-selected/:id', component: ArtistPremiumViewComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},


  { path: 'artist-radio-fill-out/:service/:hash', component: ArtistRadioFillingOutComponent, canActivate:[RoleGuard],
    data: {expectedRole: 'artist'}},

  // media
  { path: 'media-home', component: MediaHomeComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'media'}},



  //radio
  { path: 'radio-home', component: RadioSelectedTracksComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},
  // { path: ':lang/radio-home', component: RadioSelectedTracksComponent,
  //   canActivate:[RoleGuard], data: {expectedRole: 'radio'}},

  { path: 'radio-playlist', component: RadioPlaylistIndexComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},

  { path: 'radio-playlist/:id', component: RadioPlaylistViewComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},

  // { path: 'radio-premium', component: RadioPremiumComponent, canActivate:[RoleGuard],
  //   data: {expectedRole: 'radio'}},
  { path: 'radio-all-tracks', component: RadioAllTracksComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},



  { path: 'radio-song-view/:hash', component: RadioSongViewComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},

  { path: 'radio-artist/:id', component: RadioArtistViewComponent,
    canActivate:[RoleGuard], data: {expectedRole: 'radio'}},



  { path: 'song/:hash', component: UnauthSongViewComponent},
  { path: 'radio-mailing', component: UnauthRadioMailingComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    relativeLinkResolution: 'legacy',
    useHash:true,
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
