import {OnInit, OnDestroy, AfterContentChecked, Component} from '@angular/core';
import { AuthService } from './auth/auth.service';
import {User} from "./models/user";
import {environment} from "../environments/environment";
import { ChangeDetectorRef } from '@angular/core';
import {IWsMessage, WebsocketService} from "./websocket";
import {fromEvent, Observable, Subscription} from 'rxjs';
import { MatSnackBar } from "@angular/material/snack-bar";
import {Router} from "@angular/router";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy,  AfterContentChecked{
  title = 'app';
  private apiUrl = environment.apiUrl;

  public onlineEvent: Observable<Event>;
  public offlineEvent: Observable<Event>;

  subscriptions: Subscription[] = [];

  connectionStatus: string;



  constructor(
    public authService: AuthService,
    private changeDetectionRef: ChangeDetectorRef,
    private websocketService: WebsocketService,
    private router: Router,
    public snackBar: MatSnackBar,
  ){
    //   запускаем websocket
    websocketService.webSocketEvent
        .subscribe(
            (result)=>{
              //     можно что-нибудь тут придумать
            }
        );
  }




  // testWs(){
  //   this.wsService.send('text', 'Test text from app.component through this.wsService.send()');
  // }


  ngAfterContentChecked(): void {
    this.changeDetectionRef.detectChanges();
  }

  ngOnInit(): void {
    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');

    this.subscriptions.push(this.onlineEvent.subscribe(e => {
      this.connectionStatus = 'online';
      console.log('Online...');
      this.snackBar.open('Подключение к интернету установлено', '', {
        duration: 5000,
      });
    }));

    this.subscriptions.push(this.offlineEvent.subscribe(e => {
      this.connectionStatus = 'offline';
      console.log('Offline...');
      this.snackBar.open('Нет подключения к интернету', '✕', {
      });
    }));



  }



  changeLanguage(lang: string) {
    // const langs = ['en', 'fr'];
    // this.languages = this.allLanguages.filter((language) => {
    //   return language.lang !== lang;
    // });
    // this.curentLanguage = this.allLanguages[langs.indexOf(lang)].name;

    localStorage.setItem('Language', lang);

    location.reload(true);

  }

  getCurrentRoute() {
    return this.router.url;
  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }
}
