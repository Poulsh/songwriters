import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderInterceptor } from './interceptors/header-interceptor';
import { ErrorInterceptor } from './interceptors/error-interceptor';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu'
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatRippleModule} from '@angular/material/core';
import { MatPaginatorModule, MatPaginatorIntl} from '@angular/material/paginator';
import { getRuPaginatorIntl } from './common/ru-paginator-intl';




import { ArtistHomeComponent } from './artist/artist-home/artist-home.component';
import { SongIndexComponent } from './song/song-index/song-index.component';

import { SongService } from "./song/song.service";
import { AlertComponent } from './alert/alert.component';

import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './auth/auth.guard';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HelloComponent } from './auth/hello/hello.component';
import { VerificationComponent } from './auth/verification/verification.component';
import { ArtistReviewComponent } from './artist/artist-review/artist-review.component';
import { ArtistCommercialComponent } from './artist/artist-commercial/artist-commercial.component';
import { ArtistService } from "./artist/artist.service";
import { ImgPipe } from "./pipes/img.pipe";
import {FileUploadComponent} from "./file-upload/file-upload.component";
import { ArtistMySongIndexComponent } from './artist/artist-my-song-index/artist-my-song-index.component';
import { ArtistMySongViewComponent } from './artist/artist-my-song-view/artist-my-song-view.component';
import { ArtistMySongEditComponent } from './artist/artist-my-song-edit/artist-my-song-edit.component';
import {WavesurferPlayerComponent} from "./wavesurfer-player/wavesurfer-player.component";
import { MediaHomeComponent } from './media/media-home/media-home.component';
import {ShowToRoleDirective} from "./directives/show-to-role.directive";
import { ArtistSidebarMenuComponent } from './artist/artist-sidebar-menu/artist-sidebar-menu.component';
import { RadioSidebarMenuComponent } from './radio/radio-sidebar-menu/radio-sidebar-menu.component';
import { MediaSidebarMenuComponent } from './media/media-sidebar-menu/media-sidebar-menu.component';
import {RadioService} from "./radio/radio.service";
import {MediaService} from "./media/media.service";
import { RadioSongFilterComponent } from './radio/radio-song-filter/radio-song-filter.component';
import {ConfirmDialog} from "./alert/confirm-dialog/confirm-dialog.component";
import { RadioPlaylistIndexComponent } from './radio/radio-playlist-index/radio-playlist-index.component';
import { RadioPlaylistViewComponent } from './radio/radio-playlist-view/radio-playlist-view.component';
import { UserAccountComponent } from './user/user-account/user-account.component';
import {WebsocketModule} from "./websocket";
import {environment} from "../environments/environment";
import { NotificationDropdownComponent } from './notification/notification-dropdown/notification-dropdown.component';
import {StatusPipe} from "./pipes/status.pipe";
import {SharedService} from "./shared/shared.service";
import {MessageService} from "./message/message.service";
import {MessageFormComponent} from "./message/message-form/message-form.component";
import {PasswordResetComponent} from "./auth/password-reset/password-reset.component";
import {PasswordResetFormComponent} from "./auth/password-reset-form/password-reset-form.component";
import { ArtistRadioViewComponent } from './artist/artist-radio-view/artist-radio-view.component';
import { FileDownloadComponent } from './file-download/file-download.component';
import {ArtistPremiumIndexComponent} from "./artist/artist-premium-index/artist-premium-index.component";
import {ArtistRadioIndexComponent} from "./artist/artist-radio-index/artist-radio-index.component";
import { ArtistRadioNewComponent } from './artist/artist-radio-new/artist-radio-new.component';
import { ArtistRadioFillingOutComponent } from './artist/artist-radio-filling-out/artist-radio-filling-out.component';
import { ArtistMySongFormComponent } from './artist/artist-my-song-form/artist-my-song-form.component';
import {ArtistPremiumViewComponent} from "./artist/artist-premium-view/artist-premium-view.component";
import {UnauthSidebarMenuComponent} from "./unauth/unauth-sidebar-menu/unauth-sidebar-menu.component";
import {ArtistRadioFillingOutWindowComponent} from "./artist/artist-radio-filling-out-window/artist-radio-filling-out-window.component";
import {ArtistMySongFormGreyComponent} from "./artist/artist-my-song-form-grey/artist-my-song-form-grey.component";
import {SongUploadComponent} from "./file-upload/song-upload/song-upload.component";
import {ImageUploadComponent} from "./file-upload/image-upload/image-upload.component";
import { RadioSongViewComponent } from './radio/radio-song-view/radio-song-view.component';
import { OnefieldFormComponent } from './common/onefield-form/onefield-form.component';
import { CommonCommentListComponent } from './common/common-comment-list/common-comment-list.component';
import { RadioArtistViewComponent } from './radio/radio-artist-view/radio-artist-view.component';
import {RadioEditPersonalInfoBlockComponent} from "./radio/radio-edit-personal-info-block/radio-edit-personal-info-block.component";
import {RadioAllTracksComponent} from "./radio/radio-all-tracks/radio-all-tracks.component";
import {RadioSelectedTracksComponent} from "./radio/radio-selected-tracks/radio-selected-tracks.component";
import {UnauthSongViewComponent} from "./unauth/unauth-song-view/unauth-song-view.component";
import {UnauthService} from "./unauth/unauth.service";
import { UnauthRadioMailingComponent } from './unauth/unauth-radio-mailing/unauth-radio-mailing.component';
import {SeoService} from "./seo/seo.service";
import { DocConfidentialityComponent } from './doc/doc-confidentiality/doc-confidentiality.component';
import { DocTermsComponent } from './doc/doc-terms/doc-terms.component';
import { DocViolationComponent } from './doc/doc-violation/doc-violation.component';
import { UnauthContactsComponent } from './unauth/unauth-contacts/unauth-contacts.component';
import {LoadingBlockComponent} from "./common/loading-block/loading-block.component";
import { LanguageSwitchComponent } from './language/language-switch/language-switch.component';



export const MY_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};


@NgModule({
  declarations: [
    AppComponent,
    ArtistHomeComponent,
    SongIndexComponent,
    AlertComponent,
    LoginComponent,
    SignupComponent,
    HelloComponent,
    VerificationComponent,
    ArtistRadioIndexComponent,
    ArtistReviewComponent,
    ArtistCommercialComponent,
    ImgPipe,
    StatusPipe,
    FileUploadComponent,
    SongUploadComponent,
    ImageUploadComponent,
    ArtistMySongIndexComponent,
    ArtistMySongViewComponent,
    ArtistMySongEditComponent,
    WavesurferPlayerComponent,
    MediaHomeComponent,
    ShowToRoleDirective,
    ArtistSidebarMenuComponent,
    RadioSidebarMenuComponent,
    MediaSidebarMenuComponent,
    UnauthSidebarMenuComponent,
    RadioSongFilterComponent,
    ConfirmDialog,
    RadioPlaylistIndexComponent,
    RadioPlaylistViewComponent,
    UserAccountComponent,
    NotificationDropdownComponent,
    MessageFormComponent,
    ArtistPremiumIndexComponent,
    RadioSelectedTracksComponent,
    PasswordResetComponent,
    PasswordResetFormComponent,
    ArtistRadioViewComponent,
    ArtistPremiumViewComponent,
    FileDownloadComponent,
    ArtistRadioNewComponent,
    ArtistRadioFillingOutComponent,
    ArtistMySongFormComponent,
    ArtistRadioFillingOutWindowComponent,
    ArtistMySongFormGreyComponent,
    RadioSongViewComponent,
    OnefieldFormComponent,
    CommonCommentListComponent,
    RadioArtistViewComponent,
    RadioEditPersonalInfoBlockComponent,
    RadioAllTracksComponent,
    UnauthSongViewComponent,
    UnauthRadioMailingComponent,
    DocConfidentialityComponent,
    DocTermsComponent,
    DocViolationComponent,
    UnauthContactsComponent,
    LoadingBlockComponent,
    LanguageSwitchComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatTabsModule,
    MatInputModule,
    MatProgressBarModule,
    MatButtonModule,
    MatIconModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTooltipModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    WebsocketModule.config({
      url: environment.ws
    }),
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatRippleModule,
    MatPaginatorModule,


  ],
  entryComponents: [
    AlertComponent,
    ConfirmDialog
  ],
  providers: [
    ArtistService,
    RadioService,
    MediaService,
    SongService,
    AuthService,
    AuthGuard,
    { provide:HTTP_INTERCEPTORS, useClass:HeaderInterceptor, multi:true},
    { provide:HTTP_INTERCEPTORS, useClass:ErrorInterceptor, multi:true},
    SharedService,
    MessageService,
    { provide: MAT_DATE_LOCALE, useValue: 'ru' },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    UnauthService,
    SeoService,
    // { provide: MatPaginatorIntl, useValue: getRuPaginatorIntl() },


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
