import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistCommercialComponent } from './artist-commercial.component';

describe('ArtistCommercialComponent', () => {
  let component: ArtistCommercialComponent;
  let fixture: ComponentFixture<ArtistCommercialComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistCommercialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistCommercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
