import { LOCALE_ID, Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AlertComponent } from '../../alert/alert.component';
import { Song } from '../../models/song';
import { SongService } from '../../song/song.service';

import {
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Artist } from "../../models/artist";
import { ArtistService } from "../artist.service";
import { environment } from '../../../environments/environment';
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {Playlist} from "../../models/playlist";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-artist-home',
  templateUrl: './artist-home.component.html',
  styleUrls: ['./artist-home.component.scss']
})
export class ArtistHomeComponent implements OnInit {

  artist: Artist;
  public apiUrl = environment.apiUrl;
  public artistInfoForm: FormGroup;

  public radioSection: Playlist;
  public premiumSection: Playlist;
  public undone: Song[];

  public showArtistInfoForm:boolean = false;
  public showEditButtons:boolean = false;

  public statusUndone = Song.STATUS_UNDONE;
  public serviceRadioAllTracks = Artist.SERVICE_RADIO_ALL_TRACKS;
  public serviceRadioSelectedRu = Artist.SERVICE_RADIO_SELECTED_RU;
  public serviceRadioSelectedWorld = Artist.SERVICE_RADIO_SELECTED_WORLD;




  constructor(
    private route: ActivatedRoute,
    private artistService: ArtistService,
    private authService: AuthService,
    private location: Location,
    private fb: FormBuilder,
    public dialog: MatDialog,
    @Inject(LOCALE_ID) public locale: string,
  ) {}

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.initForm();
    this.getArtist();
  }

  initForm(){
    this.artistInfoForm = this.fb.group({
      name: ['', [
        Validators.required,
      ]],
      country: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      city: ['', []],
      main_info: ['', []]
    });
  }

  get aif() {return this.artistInfoForm.controls;}

  getArtist(): void {
    this.artistService.getArtist()
      .subscribe( response => {
        this.processResponse(response);
      });
  }

  constructUserInfoForm(){
    this.showArtistInfoForm = true;
  }

  saveInfo(){
    let artist = {
      name:this.aif.name.value,
      country:this.aif.country.value,
      city:this.aif.city.value,
      main_info:this.aif.main_info.value,
    };
    this.artistService.updateArtist(artist)
        .subscribe( response => {
          this.processResponse(response);
        });
  }

  processResponse(response):void {
    let timeStamp = (new Date()).getTime();

    if (response.artist) {
      this.artist = response.artist;
      this.artist.image = response.artist.image?response.artist.image+'?'+timeStamp:null;
      this.aif.name.setValue(this.artist.name);
      this.aif.country.setValue(this.artist.country);
      this.aif.city.setValue(this.artist.city);
      this.aif.main_info.setValue(this.artist.main_info);
      if (response.success){
        this.showArtistInfoForm = false;
      }
    }
    if(response.radioSection){
      this.radioSection = response.radioSection;
    }
    if(response.premiumSection){
      this.premiumSection = response.premiumSection;
    }

    if (response.success){
      this.showArtistInfoForm = false;
    }
    this.undone = response.undone;
  }



  // triggered by complete avatar file-upload
  onAvatarUploadComplete(data: any) {
    this.getArtist();
  }



  removeItemFromRadio(itemId){
    let settings = {};
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: this.locale=='ru'?'Обратите внимание!':'',
        message: this.locale=='ru'?'Точно удалить?':'Do you want to delete the file?',
        yesBtn: this.locale=='ru'?'Удалить':'Yes',
        noBtn: this.locale=='ru'?'Не удалять':'No',
        value: true,
        settings:settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.artistService.removeItemFromRadioSection(itemId)
          .subscribe( response => {
            if(response.success){
              this.getArtist();
            }
          });
      }
    });
  }

  removeSongFromUndone(hash){
    let settings = {};
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: this.locale=='ru'?'Обратите внимание!':'',
        message: this.locale=='ru'?'Точно удалить?':'Do you want to delete the file?',
        yesBtn: this.locale=='ru'?'Удалить':'Yes',
        noBtn: this.locale=='ru'?'Не удалять':'No',
        value: true,
        settings:settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.artistService.deleteMySong(hash)
          .subscribe( response => {
            if(response.success){
              this.getArtist();
            }
          });
      }
    });
  }

  // removeItemFromPremium(itemId){
  //   let settings = {};
  //   const dialogRef = this.dialog.open(ConfirmDialog, {
  //     data: {
  //       head: 'Обратите внимание!',
  //       message: 'Точно удалить?',
  //       yesBtn: 'Удалить',
  //       noBtn: 'Не удалять',
  //       value: true,
  //       settings:settings
  //     }
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.artistService.removeItemFromPremiumSection(itemId)
  //         .subscribe( response => {
  //           if(response.success){
  //             this.getArtist();
  //           }
  //         });
  //     }
  //   });
  // }

}
