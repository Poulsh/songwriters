import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistMySongEditComponent } from './artist-my-song-edit.component';

describe('ArtistMySongEditComponent', () => {
  let component: ArtistMySongEditComponent;
  let fixture: ComponentFixture<ArtistMySongEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistMySongEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistMySongEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
