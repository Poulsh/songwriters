import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {
  FormGroup,
  FormBuilder, Validators
} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {ArtistService} from "../artist.service";
import {Song} from "../../models/song";
import {environment} from "../../../environments/environment";
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";
import {Location} from '@angular/common';


@Component({
  selector: 'app-artist-my-song-edit',
  templateUrl: './artist-my-song-edit.component.html',
  styleUrls: ['./artist-my-song-edit.component.scss']
})
export class ArtistMySongEditComponent implements OnInit {

  public song: Song;
  private apiUrl = environment.apiUrl;
  public form: FormGroup;

  public stylesOptions: MusicStyle[] = [];
  public languagesOptions: Language[] = [];

  public head: string = 'Редактирование трека';
  public buttonName: string = 'Сохранить';

  constructor(
      private route: ActivatedRoute,
      private fb: FormBuilder,
      public dialog: MatDialog,
      private artistService: ArtistService,
      private router: Router,
      private location: Location,
      @Inject(LOCALE_ID) public locale: string
  ) { }

  ngOnInit() {
    if (this.locale!='ru'){
      this.head = 'Edit';
      this.buttonName = 'Save';
    }

    this.getSong();
  }



  getSong(): void {
    const hash = this.route.snapshot.paramMap.get('hash');
    this.artistService.getMySongEditData(hash)
        .subscribe( response => {
          this.song = response.song;
          this.stylesOptions = response.styles;
          this.languagesOptions = response.languages;
          // this.songToForm();
        });
  }

  onImageUploadComplete(data:any){
    this.getSong();
  }


  // onUpdate(event){
  //   this.artistService.updateSongSilently(event)
  //     .subscribe( response => {
  //       if (response.song ) {
  //         this.song = response.song;
  //       }
  //     });
  // }

  goPreviousRoute() {
    this.location.back();
  }

  onSubmit(event){

    // console.log('ArtistMySongEditComponent onSubmit(event)');console.log(event);

    this.artistService.updateSong(event)
      .subscribe( response => {
        if (response.success ) {
          this.goPreviousRoute();
        }
      });

  }

  // songToForm():void {
  //   this.f.name.setValue(this.song.name);
  //   this.f.artist_name.setValue(this.song.artist_name);
  //   this.f.rightholders.setValue(this.song.rightholders);
  //   this.f.music_style1_id.setValue(this.song.music_style1);
  //   this.f.music_style2_id.setValue(this.song.music_style2);
  //   this.f.music_style3_id.setValue(this.song.music_style3);
  //   this.f.vocal.setValue(this.song.vocal);
  //   this.f.language.setValue(this.song.lang_obj);
  //   this.f.label.setValue(this.song.label);
  //   this.f.isrc.setValue(this.song.isrc);
  //   this.f.release_date.setValue(this.song.release_date?new Date(this.song.release_date*1000): null);
  //   this.f.comments.setValue(this.song.comments);
  //   this.f.hide_reviews.setValue(this.song.hide_reviews);
  //   this.f.adult_text.setValue(this.song.adult_text);
  //   this.f.info.setValue(this.song.info);
  //   this.f.image_alt.setValue(this.song.image_alt);
  //   this.f.status.setValue(this.song.status);
  // }

  // saveForm(){
  //
  //     let arr = [];
  //     if (this.f.language.value){
  //         for (let l in this.f.language.value) {
  //             arr[l]=this.f.language.value[l].id;
  //         }
  //     }
  //   let song = {
  //     id:this.song.id,
  //     name:this.f.name.value,
  //     artist_name:this.f.artist_name.value,
  //     rightholders:this.f.rightholders.value,
  //     music_style1_id:this.f.music_style1_id.value ? this.f.music_style1_id.value.id : null,
  //     music_style2_id:this.f.music_style2_id.value ? this.f.music_style2_id.value.id : null,
  //     music_style3_id:this.f.music_style3_id.value ? this.f.music_style3_id.value.id : null,
  //     vocal:this.f.vocal.value,
  //     language:JSON.stringify(arr),
  //     label:this.f.label.value,
  //     isrc:this.f.isrc.value,
  //     release_date: Math.round( new Date(this.f.release_date.value).getTime()/1000),
  //     comments:this.f.comments.value,
  //     hide_reviews:this.f.hide_reviews.value,
  //     adult_text:this.f.adult_text.value,
  //     info:this.f.info.value,
  //     image_alt:this.f.image_alt.value,
  //     status:this.f.status.value,
  //   };
  //
  //   // console.log(arr);
  //   this.artistService.updateSong(song)
  //       .subscribe( response => {
  //         if (response.song && response.success) {
  //             this.router.navigate(['/artist-my-song-view/'+response.song.name_hash]);
  //           // this.song = response.song;
  //           //   this.songToForm();
  //         }
  //       });
  // }

    // displayName(model:any) : string | undefined {
    //     return model ? model.name : undefined;
    // }
    // compareId(x: any, y: any): boolean {
    //     return x && y ? x.id === y.id : x === y;
    // }


}
