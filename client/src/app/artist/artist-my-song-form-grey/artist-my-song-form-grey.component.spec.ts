import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistMySongFormGreyComponent } from './artist-my-song-form-grey.component';

describe('ArtistMySongFormGreyComponent', () => {
  let component: ArtistMySongFormGreyComponent;
  let fixture: ComponentFixture<ArtistMySongFormGreyComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistMySongFormGreyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistMySongFormGreyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
