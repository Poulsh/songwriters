import {Component, OnInit, Input, Output, EventEmitter, Inject, LOCALE_ID} from '@angular/core';
import {Song} from "../../models/song";
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Artist} from "../../models/artist";
import {ArtistService} from "../artist.service";
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
  selector: 'app-artist-my-song-form-grey',
  templateUrl: './artist-my-song-form-grey.component.html',
  styleUrls: ['./artist-my-song-form-grey.component.scss']
})
export class ArtistMySongFormGreyComponent implements OnInit {

  public songData:Song;

  @Input() set song(song){
    if (song){
      this.songData = song;
      if (!this.form){
        this.initForm();
      }
      this.songToForm(this.songData);
      if (song.image){
        let timeStamp = (new Date()).getTime();
        this.songData.image = song.image+'?'+timeStamp;
      }
    }
  };

  @Input() set resetForm(num){
    if (num && this.form){
      this.form.reset();
      if (!this.song){
        this.songData=null;
      }
    }
  };

  @Input() songOptions: Song[] = [];

  @Input() stylesOptions: MusicStyle[] = [];
  @Input() languagesOptions: Language[] = [];
  @Input() buttonName: string = 'Далее';
  @Input() buttonIcon: string = 'save';
  @Input() serviceType: string;

  @Output() onSubmit = new EventEmitter<any>();
  @Output() onUpdate = new EventEmitter<any>();
  @Output() onImageUpload = new EventEmitter<boolean>();

  public form: FormGroup;
  public statusUndone = Song.STATUS_UNDONE;
  public statusActive = Song.STATUS_ACTIVE;
  public serviceRadioAllTracks = Artist.SERVICE_RADIO_ALL_TRACKS;
  public serviceRadioSelectedRu = Artist.SERVICE_RADIO_SELECTED_RU;
  public serviceRadioSelectedWorld = Artist.SERVICE_RADIO_SELECTED_WORLD;
  public apiUrl = environment.apiUrl;

  public songOptionsForm: FormGroup;
  public showExistedSongOptions: boolean = false;

  public songLoading:boolean = false;

  constructor(
    private fb: FormBuilder,
    private artistService: ArtistService,
    private router: Router,
    public dialog: MatDialog,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.initForm();
    this.songToForm(this.songData);
  }

  initForm() {
    this.form = this.fb.group({
      name: ['', [
        Validators.required,
        Validators.maxLength(255)]],
      artist_name: ['',[
        Validators.required,
        Validators.maxLength(255)]],
      rightholders: ['', [Validators.required]],
      music_style1_id: ['', [ Validators.required,]],
      music_style2_id: ['', []],
      music_style3_id: ['', []],
      vocal: ['', []],
      language: ['', []],
      label: ['', []],
      isrc: ['', []],
      release_date: ['', []],
      author_comment: ['', [Validators.maxLength(1000)]],
      hide_reviews: ['', []],
      adult_text: ['', []],
      info: ['', []],
      publication_agreed: ['', []],
      // image_alt: ['', []],
      // status: ['', []],
      // view: ['', []],
    });
    this.songOptionsForm = this.fb.group({
      song: ['', [Validators.required]],
    });
  }

  get f() {return this.form.controls;}

  songToForm(song):void {
    this.f.name.setValue(song?song.name:null);
    this.f.artist_name.setValue(song?song.artist_name:null);
    this.f.rightholders.setValue(song?song.rightholders:null);
    this.f.music_style1_id.setValue(song?song.music_style1:null);
    this.f.music_style2_id.setValue(song?song.music_style2:null);
    this.f.music_style3_id.setValue(song?song.music_style3:null);
    this.f.vocal.setValue(song?song.vocal:null);
    this.f.language.setValue(song?song.lang_obj:null);
    this.f.label.setValue(song?song.label:null);
    this.f.isrc.setValue(song?song.isrc:null);
    this.f.release_date.setValue(song && song.release_date?new Date(song.release_date*1000): null);
    this.f.author_comment.setValue(song?song.author_comment:null);
    this.f.hide_reviews.setValue(song?song.hide_reviews:null);
    this.f.adult_text.setValue(song?song.adult_text:null);
    this.f.info.setValue(song?song.info:null);
    this.f.publication_agreed.setValue(song?song.publication_agreed:null);

  }


  displayName(model:any) : string | undefined {
    return model ? model.name : undefined;
  }
  compareId(x: any, y: any): boolean {
    return x && y ? x.id === y.id : x === y;
  }


  update():void{
    if(this.songData){
      let langArr = [];
      if (this.f.language.value){
        for (let l in this.f.language.value) {
          if ( this.f.language.value.hasOwnProperty(l)) {
            langArr[l]=this.f.language.value[l].id;
          }
        }
      }

      let song = {
        id:this.songData.id,
        name:this.f.name.value,
        artist_name:this.f.artist_name.value,
        rightholders:this.f.rightholders.value,
        music_style1_id:this.f.music_style1_id.value ? this.f.music_style1_id.value.id : null,
        music_style2_id:this.f.music_style2_id.value ? this.f.music_style2_id.value.id : null,
        music_style3_id:this.f.music_style3_id.value ? this.f.music_style3_id.value.id : null,
        vocal:this.f.vocal.value,
        language:JSON.stringify(langArr),
        label:this.f.label.value,
        isrc:this.f.isrc.value,
        release_date: Math.round( new Date(this.f.release_date.value).getTime()/1000),
        author_comment:this.f.author_comment.value,
        hide_reviews:this.f.hide_reviews.value==true?1:0,
        adult_text:this.f.adult_text.value==true?1:0,
        publication_agreed:this.f.publication_agreed.value==true?1:0,
      };
      this.onUpdate.emit(song);
    }

  }


  submitForm(serviceType):void{

    let langArr = [];
    if (this.f.language.value){
      for (let l in this.f.language.value) {
        if ( this.f.language.value.hasOwnProperty(l)) {
          langArr[l]=this.f.language.value[l].id;
        }
      }
    }

    let song = {
      serviceType:serviceType,
      id:this.songData.id,
      name:this.f.name.value,
      artist_name:this.f.artist_name.value,
      rightholders:this.f.rightholders.value,
      music_style1_id:this.f.music_style1_id.value ? this.f.music_style1_id.value.id : null,
      music_style2_id:this.f.music_style2_id.value ? this.f.music_style2_id.value.id : null,
      music_style3_id:this.f.music_style3_id.value ? this.f.music_style3_id.value.id : null,
      vocal:this.f.vocal.value,
      language:JSON.stringify(langArr),
      label:this.f.label.value,
      isrc:this.f.isrc.value,
      release_date: Math.round( new Date(this.f.release_date.value).getTime()/1000),
      author_comment:this.f.author_comment.value,
      hide_reviews:this.f.hide_reviews.value==true?1:0,
      adult_text:this.f.adult_text.value==true?1:0,
      publication_agreed:this.f.publication_agreed.value==true?1:0,

    };
    this.onSubmit.emit(song);
  }



  onSongUploadComplete(data: any) {
    this.artistService.process(data);
    if (data.success && data.song) {
      if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS ||
        this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU ||
        this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD) {
        this.router.navigate(['/artist-radio-fill-out/'+this.serviceType+'/'+data.song.name_hash]);
      }
      this.songData = data.song;
      this.songToForm(this.songData);
    }

  }

  selectSong() {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate(['/artist-radio-fill-out/'+this.serviceType+'/'+this.songOptionsForm.controls.song.value.name_hash]));
  }

  onImageUploadComplete(){
    if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS ||
      this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU ||
      this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD
    ) {
      this.update();
    } else {
      this.onImageUpload.emit(true);
    }
  }



  deleteSong(hash){
    let settings = {};
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        message: this.locale=='ru'?'Точно удалить?':'Delete track',
        yesBtn: this.locale=='ru'?'Удалить':'yes',
        noBtn: this.locale=='ru'?'Не удалять':'no',
        value: true,
        settings:settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.artistService.deleteMySong(hash)
          .subscribe( response => {
            if(response.success){
              this.router.navigate(['/artist-home'])
            }
          });
      }
    });
  }
}
