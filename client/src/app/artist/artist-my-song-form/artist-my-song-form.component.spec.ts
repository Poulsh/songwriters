import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistMySongFormComponent } from './artist-my-song-form.component';

describe('ArtistMySongFormComponent', () => {
  let component: ArtistMySongFormComponent;
  let fixture: ComponentFixture<ArtistMySongFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistMySongFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistMySongFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
