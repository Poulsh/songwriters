import {Component, OnInit, Input, Output, EventEmitter, Inject, LOCALE_ID} from '@angular/core';
import {Song} from "../../models/song";
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Artist} from "../../models/artist";

@Component({
  selector: 'app-artist-my-song-form',
  templateUrl: './artist-my-song-form.component.html',
  styleUrls: ['./artist-my-song-form.component.scss']
})
export class ArtistMySongFormComponent implements OnInit {

  @Input() song:Song;
  @Input() stylesOptions: MusicStyle[] = [];
  @Input() languagesOptions: Language[] = [];
  @Input() buttonName: string = 'Далее';
  @Input() buttonIcon: string = 'save';
  @Input() serviceType: string;

  @Output() onSubmit = new EventEmitter<any>();
  @Output() onUpdate = new EventEmitter<any>();

  public form: FormGroup;
  public statusUndone = Song.STATUS_UNDONE;
  public serviceRadioAllTracks = Artist.SERVICE_RADIO_ALL_TRACKS;
  public serviceRadioSelectedRu = Artist.SERVICE_RADIO_SELECTED_RU;
  public serviceRadioSelectedWorld = Artist.SERVICE_RADIO_SELECTED_WORLD;


  constructor(
    private fb: FormBuilder,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.initForm();
    this.songToForm();
  }

  initForm() {
    this.form = this.fb.group({
      name: ['', [
        Validators.required,
        Validators.maxLength(255)]],
      artist_name: ['',[
        Validators.required,
        Validators.maxLength(255)]],
      rightholders: ['', []],
      music_style1_id: ['', [ Validators.required,]],
      music_style2_id: ['', []],
      music_style3_id: ['', []],
      vocal: ['', []],
      language: ['', []],
      label: ['', []],
      isrc: ['', []],
      release_date: ['', []],
      author_comment: ['', []],
      hide_reviews: ['', []],
      adult_text: ['', []],
      info: ['', []],
      // image_alt: ['', []],
      // status: ['', []],
      // view: ['', []],
    });
  }

  get f() {return this.form.controls;}

  songToForm():void {
    this.f.name.setValue(this.song?this.song.name:null);
    this.f.artist_name.setValue(this.song?this.song.artist_name:null);
    this.f.rightholders.setValue(this.song?this.song.rightholders:null);
    this.f.music_style1_id.setValue(this.song?this.song.music_style1:null);
    this.f.music_style2_id.setValue(this.song?this.song.music_style2:null);
    this.f.music_style3_id.setValue(this.song?this.song.music_style3:null);
    this.f.vocal.setValue(this.song?this.song.vocal:null);
    this.f.language.setValue(this.song?this.song.lang_obj:null);
    this.f.label.setValue(this.song?this.song.label:null);
    this.f.isrc.setValue(this.song?this.song.isrc:null);
    this.f.release_date.setValue(this.song && this.song.release_date?new Date(this.song.release_date*1000): null);
    this.f.author_comment.setValue(this.song?this.song.author_comment:null);
    this.f.hide_reviews.setValue(this.song?this.song.hide_reviews:null);
    this.f.adult_text.setValue(this.song?this.song.adult_text:null);
    this.f.info.setValue(this.song?this.song.info:null);
    // this.f.image_alt.setValue(this.song.image_alt);
    // this.f.status.setValue(this.song.status);
  }


  displayName(model:any) : string | undefined {
    return model ? model.name : undefined;
  }
  compareId(x: any, y: any): boolean {
    return x && y ? x.id === y.id : x === y;
  }


  update():void{
    let arr = [];
    if (this.f.language.value){
      for (let l in this.f.language.value) {
        if ( this.f.language.value.hasOwnProperty(l)) {
          arr[l]=this.f.language.value[l].id;
        }
      }
    }

    let song = {
      id:this.song.id,
      name:this.f.name.value,
      artist_name:this.f.artist_name.value,
      rightholders:this.f.rightholders.value,
      music_style1_id:this.f.music_style1_id.value ? this.f.music_style1_id.value.id : null,
      music_style2_id:this.f.music_style2_id.value ? this.f.music_style2_id.value.id : null,
      music_style3_id:this.f.music_style3_id.value ? this.f.music_style3_id.value.id : null,
      vocal:this.f.vocal.value,
      language:JSON.stringify(arr),
      label:this.f.label.value,
      isrc:this.f.isrc.value,
      release_date: Math.round( new Date(this.f.release_date.value).getTime()/1000),
      author_comment:this.f.author_comment.value,
      hide_reviews:this.f.hide_reviews.value==true?1:0,
      adult_text:this.f.adult_text.value==true?1:0,
    };
    this.onUpdate.emit(song);
  }


  submitForm(serviceType):void{

    let arr = [];
    if (this.f.language.value){
      for (let l in this.f.language.value) {
        if ( this.f.language.value.hasOwnProperty(l)) {
          arr[l]=this.f.language.value[l].id;
        }
      }
    }

    let song = {
      serviceType:serviceType,
      id:this.song.id,
      name:this.f.name.value,
      artist_name:this.f.artist_name.value,
      rightholders:this.f.rightholders.value,
      music_style1_id:this.f.music_style1_id.value ? this.f.music_style1_id.value.id : null,
      music_style2_id:this.f.music_style2_id.value ? this.f.music_style2_id.value.id : null,
      music_style3_id:this.f.music_style3_id.value ? this.f.music_style3_id.value.id : null,
      vocal:this.f.vocal.value,
      language:JSON.stringify(arr),
      label:this.f.label.value,
      isrc:this.f.isrc.value,
      release_date: Math.round( new Date(this.f.release_date.value).getTime()/1000),
      author_comment:this.f.author_comment.value,
      hide_reviews:this.f.hide_reviews.value==true?1:0,
      adult_text:this.f.adult_text.value==true?1:0,
      // info:this.f.info.value,
      // image_alt:this.f.image_alt.value,
      // status:this.f.status.value,
    };
    this.onSubmit.emit(song);
  }
}
