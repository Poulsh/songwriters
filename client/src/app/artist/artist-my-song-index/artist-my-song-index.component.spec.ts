import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistMySongIndexComponent } from './artist-my-song-index.component';

describe('ArtistMySongIndexComponent', () => {
  let component: ArtistMySongIndexComponent;
  let fixture: ComponentFixture<ArtistMySongIndexComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistMySongIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistMySongIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
