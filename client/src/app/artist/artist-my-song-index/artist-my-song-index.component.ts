import { Component, OnInit } from '@angular/core';
import {ArtistService} from "../artist.service";
import {Location} from "@angular/common";
import {FormBuilder} from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import {Song} from "../../models/song";
import {Router} from "@angular/router";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-artist-my-song-index',
  templateUrl: './artist-my-song-index.component.html',
  styleUrls: ['./artist-my-song-index.component.scss']
})
export class ArtistMySongIndexComponent implements OnInit {

  public songs: Song[];


  constructor(
      private artistService: ArtistService,
      private authService: AuthService,
      private fb: FormBuilder,
      public dialog: MatDialog,
      private router: Router
  ) { }

  ngOnInit() {
      this.authService.activeRole = User.TYPE_ARTIST;
      this.getSongs();
  }


  getSongs(): void {
    this.artistService.getMySongs()
        .subscribe( response => {
          if (response.songs){
            this.songs = response.songs;
          }
        });
  }




  // triggered by complete song file-upload
  onSongUploadComplete(data: any) {
    this.artistService.process(data);

    if (data.success && data.song) {
      this.router.navigate(['/artist-my-song-view/'+data.song.name_hash]);
    }
    // this.getSongs();
  }

  deleteSong(hash:string){
    this.artistService.deleteMySong(hash)
        .subscribe( response => {
          if (response.success){
              this.getSongs();
          }
        });
  }

  toRadio(hash:string){
      this.artistService.addToRadio(hash)
          .subscribe( response => {
              if (response.success){
                  this.getSongs();
              }
          });
  }
}
