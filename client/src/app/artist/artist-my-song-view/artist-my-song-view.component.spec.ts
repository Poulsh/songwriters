import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistMySongViewComponent } from './artist-my-song-view.component';

describe('ArtistMySongViewComponent', () => {
  let component: ArtistMySongViewComponent;
  let fixture: ComponentFixture<ArtistMySongViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistMySongViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistMySongViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
