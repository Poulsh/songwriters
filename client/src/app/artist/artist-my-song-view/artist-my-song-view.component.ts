import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {
  FormGroup,
  FormBuilder} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {ArtistService} from "../artist.service";
import {Song} from "../../models/song";
import {environment} from "../../../environments/environment";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";
import {Comment} from "../../models/comment";



@Component({
  selector: 'app-artist-my-song-view',
  templateUrl: './artist-my-song-view.component.html',
  styleUrls: ['./artist-my-song-view.component.scss']
})
export class ArtistMySongViewComponent implements OnInit {

  public song: Song;
  private apiUrl = environment.apiUrl;
  public comments: Comment[];

  constructor(
      private route: ActivatedRoute,
      private fb: FormBuilder,
      public dialog: MatDialog,
      private artistService: ArtistService,
      private authService: AuthService,
  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSong();
  }


  getSong(): void {
    const hash = this.route.snapshot.paramMap.get('hash');
    this.artistService.getMySong(hash)
        .subscribe( response => {
          this.song = response.song;
          this.comments = response.comments;
        });
  }

  onImageUploadComplete(e){
    if (!e.error){
      this.getSong();
    } else {
      this.artistService.process(e);
    }
  }
}
