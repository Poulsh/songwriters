import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistPremiumIndexComponent } from './artist-premium-index.component';

describe('ArtistPremiumIndexComponent', () => {
  let component: ArtistPremiumIndexComponent;
  let fixture: ComponentFixture<ArtistPremiumIndexComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistPremiumIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistPremiumIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
