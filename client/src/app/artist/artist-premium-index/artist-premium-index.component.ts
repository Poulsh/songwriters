import {Component, OnDestroy, OnInit} from '@angular/core';
import {Playlist} from "../../models/playlist";
import {MessageDialog} from "../../models/messageDialog";
import {ActivatedRoute} from "@angular/router";
import {SongService} from "../../song/song.service";
import {ArtistService} from "../artist.service";
import {AuthService} from "../../auth/auth.service";
import {Location} from "@angular/common";
import { MatDialog } from "@angular/material/dialog";
import {WebsocketService} from "../../websocket";
import {User} from "../../models/user";
import {Notification} from "../../models/notification";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import { LOCALE_ID, Inject } from '@angular/core';

@Component({
  selector: 'app-artist-premium-index',
  templateUrl: './artist-premium-index.component.html',
  styleUrls: ['./artist-premium-index.component.scss']
})
export class ArtistPremiumIndexComponent implements OnInit, OnDestroy {

  public playlist: Playlist;

  public messageSubjectTypeItemInPremiumSection = MessageDialog.SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION;
  public userTypeArtistInPremiumSection = MessageDialog.USER_TYPE_ARTIST_IN_PREMIUM_SECTION;

  private unSubscribe = new Subject();


  constructor(
    private route: ActivatedRoute,
    private songService: SongService,
    private artistService: ArtistService,
    private authService: AuthService,
    private location: Location,
    public dialog: MatDialog,
    private websocketService: WebsocketService,
    @Inject(LOCALE_ID) public locale: string,

  ) {
    websocketService.webSocketEvent
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
        (message)=>{
          if (message.event == Notification.EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE ) {
            this.getSongs();
          }
          if (message.event == Notification.EVENT_PREMIUM_SECTION_ITEM_NEW_MESSAGE) {
            this.getSongs();
          }
        }
      );
  }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSongs();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  getSongs(): void {
    this.artistService.getPremiumSection()
      .subscribe( response => {
        if (response.playlist){
          this.playlist = response.playlist;

        }
      });
  }
}
