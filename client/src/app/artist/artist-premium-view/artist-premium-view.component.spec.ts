import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistPremiumViewComponent } from './artist-premium-view.component';

describe('ArtistPremiumViewComponent', () => {
  let component: ArtistPremiumViewComponent;
  let fixture: ComponentFixture<ArtistPremiumViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistPremiumViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistPremiumViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
