import {Component, OnDestroy, OnInit} from '@angular/core';
import {Song} from "../../models/song";
import {ActivatedRoute} from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import {ArtistService} from "../artist.service";
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {PlaylistItem} from "../../models/playlistItem";
import {MessageDialog} from "../../models/messageDialog";
import {WebsocketService} from "../../websocket";
import {Notification} from "../../models/notification";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import { LOCALE_ID, Inject } from '@angular/core';

@Component({
  selector: 'app-artist-premium-view',
  templateUrl: './artist-premium-view.component.html',
  styleUrls: ['./artist-premium-view.component.scss']
})
export class ArtistPremiumViewComponent implements OnInit, OnDestroy {

  public item: PlaylistItem;

  public messageSubjectTypeItemInPremiumSection = MessageDialog.SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION;
  public userTypeArtistInPremiumSection = MessageDialog.USER_TYPE_ARTIST_IN_PREMIUM_SECTION;

  private unSubscribe = new Subject();

  public statusInit = PlaylistItem.STATUS_INIT;
  public statusApproved = PlaylistItem.STATUS_APPROVED;
  public statusActive = PlaylistItem.STATUS_ACTIVE;
  public paymentInfo = PlaylistItem.PAYMENT_INFO;
  public priceRadioSelectedRu = PlaylistItem.PRICE_RADIO_SELECTED_RU;

  public serviceTypeRadioSelectedWorld = PlaylistItem.SERVICE_TYPE_RADIO_SELECTED_WORLD;
  public serviceTypeRadioSelectedRu = PlaylistItem.SERVICE_TYPE_RADIO_SELECTED_RU;


  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private artistService: ArtistService,
    private authService: AuthService,
    private websocketService: WebsocketService,
    @Inject(LOCALE_ID) public locale: string,
  ) {
    websocketService.webSocketEvent
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
        (message)=>{
          if (message.event == Notification.EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE && message.data.itemId==this.item.id) {
            this.getSong();
          }
          if (message.event == Notification.EVENT_PREMIUM_SECTION_ITEM_NEW_MESSAGE && message.data.itemId==this.item.id) {
            this.getSong();
          }
        }
      );
  }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSong();
    this.subscribeRouteChange();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  subscribeRouteChange() {
    this.route.params
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(params => {
      this.getSong();
    });
  }

  getSong(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.artistService.getPremiumItem(id)
      .subscribe( response => {
        this.item = response.item;
      });
  }

}
