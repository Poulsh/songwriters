import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistRadioFillingOutWindowComponent } from './artist-radio-filling-out-window.component';

describe('ArtistRadioFillingOutWindowComponent', () => {
  let component: ArtistRadioFillingOutWindowComponent;
  let fixture: ComponentFixture<ArtistRadioFillingOutWindowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistRadioFillingOutWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistRadioFillingOutWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
