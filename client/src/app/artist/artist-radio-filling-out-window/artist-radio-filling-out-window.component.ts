import {Component, Input, OnInit} from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import {ArtistService} from "../artist.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Song} from "../../models/song";
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";
import {Artist} from "../../models/artist";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";
import { LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'app-artist-radio-filling-out-window',
  templateUrl: './artist-radio-filling-out-window.component.html',
  styleUrls: ['./artist-radio-filling-out-window.component.scss']
})
export class ArtistRadioFillingOutWindowComponent implements OnInit {


  @Input() song: Song;
  @Input() serviceType: string;
  @Input() stylesOptions: MusicStyle[] = [];
  @Input() languagesOptions: Language[] = [];
  @Input() getOptions: boolean = true;

  // public head: string;
  public buttonName: string;

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private artistService: ArtistService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getData();
  }


  getData(): void {
    const hash = this.route.snapshot.paramMap.get('hash');
    // this.serviceType = this.route.snapshot.paramMap.get('service');
    // if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS){
    //   this.head='Новый трек в директорию "All Tracks"'
    // } else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU){
    //   this.head='Новый трек в директорию "Selected Tracks (СНГ)"'
    // } else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD){
    //   this.head='Новый трек в директорию "Selected Tracks (World)"'
    // }
    if (this.getOptions){
      this.artistService.getRadioNewData()
        .subscribe( response => {
          this.stylesOptions = response.styles;
          this.languagesOptions = response.languages;
          this.buttonName = this.locale=='ru'?'Отправить':'Send';
        });
    }


  }


  onImageUploadComplete(event){
    if (event.success){
      this.artistService.getMySong(this.song.name_hash)
        .subscribe( response => {
          let timeStamp = (new Date()).getTime();
          if (response.song.image) {
            response.song.image = response.song.image+'?'+timeStamp;
          }
          this.song = response.song;
        });
    }
  }


  onUpdate(event){
    this.artistService.updateSongSilently(event)
      .subscribe( response => {
        if (response.song ) {
          this.song = response.song;
        }
      });
  }

  processResponseError(response):void{
    if (response.error && response.redirect) {
      this.router.navigate(['/artist-home']);
    }
  }

  onSubmit(event){
    if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS){
      this.artistService.submitRadioAllTracks(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-radio/'+response.id]);
          }
          this.processResponseError(response);
        });
    }
    else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU){
      this.artistService.submitRadioSelectedRu(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-selected/'+response.id]);
          }
          this.processResponseError(response);
        });
    }
    else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD){
      this.artistService.submitRadioSelectedWorld(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-selected/'+response.id]);
          }
          this.processResponseError(response);
        });
    }

  }
}
