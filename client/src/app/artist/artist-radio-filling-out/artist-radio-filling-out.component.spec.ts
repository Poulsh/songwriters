import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistRadioFillingOutComponent } from './artist-radio-filling-out.component';

describe('ArtistRadioFillingOutComponent', () => {
  let component: ArtistRadioFillingOutComponent;
  let fixture: ComponentFixture<ArtistRadioFillingOutComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistRadioFillingOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistRadioFillingOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
