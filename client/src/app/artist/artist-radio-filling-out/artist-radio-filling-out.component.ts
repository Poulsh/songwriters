import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import {ArtistService} from "../artist.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Song} from "../../models/song";
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";
import {Artist} from "../../models/artist";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import {Subject} from "rxjs";
import { LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'app-artist-radio-filling-out',
  templateUrl: './artist-radio-filling-out.component.html',
  styleUrls: ['./artist-radio-filling-out.component.scss']
})
export class ArtistRadioFillingOutComponent implements OnInit {

  public song: Song;
  public serviceType: string;
  private hash: string;
  public stylesOptions: MusicStyle[] = [];
  public languagesOptions: Language[] = [];
  public songOptions: Song[] = [];

  public head: string;
  public buttonName: string;

  private unSubscribe = new Subject();

  public resetForm:number=0;

  constructor(
    public dialog: MatDialog,
    private authService: AuthService,
    private artistService: ArtistService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.route.paramMap
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(urlParams => {
        if (
          (urlParams.get('service') && this.serviceType && this.serviceType != urlParams.get('service')) ||
          (urlParams.get('hash') && this.hash && this.hash != urlParams.get('hash'))
        ){
          this.getSong();
        }
      });
    this.getSong();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  getSong(): void {
    this.hash = this.route.snapshot.paramMap.get('hash');
    this.serviceType = this.route.snapshot.paramMap.get('service');
    let serviceName = null;
    let serviceUrlCode = null;
    if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS){
      this.head=this.locale=='ru'? 'Новый трек в директорию "All Tracks"':'New track in "All Tracks"';
      serviceName = '"All Tracks"';
      serviceUrlCode = Song.SERVICE_URL_CODE_ALL_TRACKS;
    } else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU){
      this.head=this.locale=='ru'? 'Новый трек в директорию "Selected Tracks (СНГ)"':'New track in "Selected Tracks (Russia)"';
      serviceName = this.locale=='ru' ? '"Selected Tracks (СНГ)"':'"Selected Tracks (Russia)"';
      serviceUrlCode = Song.SERVICE_URL_CODE_SELECTED_RU;
    } else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD){
      this.head = this.locale=='ru' ? 'Новый трек в директорию "Selected Tracks (World)"' : 'New track in "Selected Tracks (World)"';
      serviceName = '"Selected Tracks (World)"';
      serviceUrlCode = Song.SERVICE_URL_CODE_SELECTED_WORLD;
    }

    if (this.hash == 'new'){

      this.resetForm++;

      this.artistService.getNewSongFormData(serviceUrlCode)
        .subscribe( response => {
          this.stylesOptions = response.styles;
          this.languagesOptions = response.languages;
          this.songOptions = response.songOptions;
          this.buttonName = this.locale=='ru' ? 'далее' : 'further';
        });
    } else {
      this.artistService.getMySongEditData(this.hash)
        .subscribe( response => {
          if(response.song){
            this.song = response.song;
            this.head = serviceName+' '+this.song.name;
          }
          this.stylesOptions = response.styles;
          this.languagesOptions = response.languages;
          this.buttonName = this.locale=='ru' ? 'далее' : 'further';
        });
    }
  }


  onImageUploadComplete(event){
    if (event.success){
      this.artistService.getMySong(this.song.name_hash)
        .subscribe( response => {
          let timeStamp = (new Date()).getTime();
          if (response.song.image) {
            response.song.image = response.song.image+'?'+timeStamp;
          }
          this.song = response.song;
        });
    }
  }


  onUpdate(event){
    this.artistService.updateSongSilently(event)
      .subscribe( response => {
        if (response.song ) {
          this.song = response.song;
        }
      });
  }

  processResponseError(response):void{
    if (response.error && response.redirect) {
      this.router.navigate(['/artist-home']);
    }
  }

  onSubmit(event){
    if (this.serviceType == Artist.SERVICE_RADIO_ALL_TRACKS){
      this.artistService.submitRadioAllTracks(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-radio/'+response.id]);
          }
          this.processResponseError(response);
        });
    }
    else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_RU){
      this.artistService.submitRadioSelectedRu(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-selected/'+response.id]);
          }
          this.processResponseError(response);
        });
    }
    else if (this.serviceType == Artist.SERVICE_RADIO_SELECTED_WORLD){
      this.artistService.submitRadioSelectedWorld(event)
        .subscribe( response => {
          if (response.success) {
            this.router.navigate(['/artist-selected/'+response.id]);
          }
          this.processResponseError(response);
        });
    }

  }




}
