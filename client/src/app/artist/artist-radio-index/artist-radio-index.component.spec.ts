import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistRadioIndexComponent } from './artist-radio-index.component';

describe('ArtistRadioIndexComponent', () => {
  let component: ArtistRadioIndexComponent;
  let fixture: ComponentFixture<ArtistRadioIndexComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistRadioIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistRadioIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
