import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { AlertComponent } from '../../alert/alert.component';
import { Song } from '../../models/song';
import { SongService } from '../../song/song.service';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {ArtistService} from "../artist.service";
import {AuthService} from "../../auth/auth.service";
import {Playlist} from "../../models/playlist";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import {User} from "../../models/user";
import {WebsocketService} from "../../websocket";
import {MessageDialog} from "../../models/messageDialog";
import {Notification} from "../../models/notification";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import { LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'app-artist-radio-index',
  templateUrl: './artist-radio-index.component.html',
  styleUrls: ['./artist-radio-index.component.scss']
})
export class ArtistRadioIndexComponent implements OnInit, OnDestroy {

  public playlist: Playlist;

  public messageSubjectTypeItemInRadioSection = MessageDialog.SUBJECT_TYPE_ITEM_IN_RADIO_SECTION;
  public userTypeArtistInRadioSection = MessageDialog.USER_TYPE_ARTIST_IN_RADIO_SECTION;

  private unSubscribe = new Subject();


  constructor(
    private route: ActivatedRoute,
    private songService: SongService,
    private artistService: ArtistService,
    private authService: AuthService,
    private location: Location,
    public dialog: MatDialog,
    private websocketService: WebsocketService,
    @Inject(LOCALE_ID) public locale: string,
  ) {
    websocketService.webSocketEvent
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
            (message)=>{
              if (message.event == Notification.EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE ) {
                this.getSongs();
              }
              if (message.event == Notification.EVENT_RADIO_SECTION_ITEM_NEW_MESSAGE) {
                this.getSongs();
              }
            }
        );
  }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSongs();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  getSongs(): void {
    this.artistService.getRadioSection()
      .subscribe( response => {
        this.playlist = response.playlist;

        // let activeItems = {};
        // for (let item of response.playlist.items){
        //   activeItems[item.id] = {
        //     id:item.id,
        //     messagesQnt:item.messages_qnt?item.messages_qnt.qnt:null,
        //     messagesQntNotSeen:item.messages_qnt?item.messages_qnt.not_seen:null,
        //   };
        // }
        // this.notificationService.userCheckSum.activeItems = activeItems;

      });
  }



  removeItem(itemId){
    let settings = {};
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: this.locale=='ru'?'Обратите внимание!':'',
        message:  this.locale=='ru'?'Точно удалить?':'Delete exactly?',
        yesBtn:  this.locale=='ru'?'Удалить':'Delete',
        noBtn:  this.locale=='ru'?'Не удалять':"Don't delete",
        value: true,
        settings:settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.artistService.removeItemFromRadioSection(itemId)
            .subscribe( response => {
              if(response.success){
                this.getSongs();
              }
            });
      }
    });
  }
}
