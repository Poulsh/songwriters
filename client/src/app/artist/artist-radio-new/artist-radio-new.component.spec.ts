import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistRadioNewComponent } from './artist-radio-new.component';

describe('ArtistRadioNewComponent', () => {
  let component: ArtistRadioNewComponent;
  let fixture: ComponentFixture<ArtistRadioNewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistRadioNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistRadioNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
