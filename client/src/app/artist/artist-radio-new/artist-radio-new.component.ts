import { Component, OnInit } from '@angular/core';
import {ArtistService} from "../artist.service";
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import { MatDialog } from "@angular/material/dialog";
import {Router} from "@angular/router";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import {Song} from "../../models/song";
import {
  FormGroup,
  FormBuilder,
  Validators } from '@angular/forms';
import {Artist} from "../../models/artist";
import {PlaylistItem} from "../../models/playlistItem";


@Component({
  selector: 'app-artist-radio-new',
  templateUrl: './artist-radio-new.component.html',
  styleUrls: ['./artist-radio-new.component.scss']
})
export class ArtistRadioNewComponent implements OnInit {

  public showAddToAllTracksBox:boolean = false; //   todo убрать после перепила на редирект
  public showSelectSelectedBox:boolean = false;
  public showSelectedRuBox:boolean = false;
  public showSelectedWorldBox:boolean = false;
  public allTracksOptionForm: FormGroup;
  public allTracksOptions: Song[];

  public selectedRuOptionForm: FormGroup;
  public selectedRuOptions: Song[];

  public selectedWorldOptionForm: FormGroup;
  public selectedWorldOptions: Song[];


  public serviceRadioAllTracks = Artist.SERVICE_RADIO_ALL_TRACKS;

  public urlNewAllTracks = '/artist-radio-fill-out/'+Artist.SERVICE_RADIO_ALL_TRACKS+'/new';
  public urlNewSelectedRu = '/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_RU+'/new';
  public urlNewSelectedWorld = '/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_WORLD+'/new';


  public songs: Song[];

  public priceRadioAllTracks = PlaylistItem.PRICE_RADIO_ALL_TRACKS;
  public priceRadioSelectedRu = PlaylistItem.PRICE_RADIO_SELECTED_RU;
  public priceRadioSelectedWorld = PlaylistItem.PRICE_RADIO_SELECTED_WORLD;

  // public stylesOptions: MusicStyle[] = [];
  // public languagesOptions: Language[] = [];

  constructor(
    private artistService: ArtistService,
    private router: Router,
    private authService: AuthService,
    public dialog: MatDialog,
    private fb: FormBuilder,

  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getData();
    this.initForm();
  }

  initForm() {
    this.allTracksOptionForm = this.fb.group({
      song: ['', [Validators.required,]],
    });
    this.selectedRuOptionForm = this.fb.group({
      song: ['', [Validators.required,]],
    });
    this.selectedWorldOptionForm = this.fb.group({
      song: ['', [Validators.required,]],
    });

  }

  getData(): void {
    this.artistService.getRadioNewData()
      .subscribe( response => {
        this.songs = response.songs;
        // this.stylesOptions = response.styles;
        // this.languagesOptions = response.languages;
        let allTracksOptions = [];
        let selectedRuOptions = [];
        let selectedWorldOptions = [];
        for (let song of this.songs){
          if (
            song.radio_status == Song.STATUS_UNDONE ||
            song.premium_ru_status== Song.STATUS_UNDONE ||
            song.premium_world_status== Song.STATUS_UNDONE

          ){
            allTracksOptions.push(song);
            selectedRuOptions.push(song);
            selectedWorldOptions.push(song);
          }
        }
        this.allTracksOptions = allTracksOptions;
        this.selectedRuOptions = selectedRuOptions;
        this.selectedWorldOptions = selectedWorldOptions;
      });
  }
  get atf() {return this.allTracksOptionForm.controls;}
  get srf() {return this.selectedRuOptionForm.controls;}
  get swf() {return this.selectedWorldOptionForm.controls;}


  onRadioAllTracksUploadComplete(data: any) {
    this.artistService.process(data);
    if (data.success && data.song) {
      this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_ALL_TRACKS+'/'+data.song.name_hash]);
    }
  }

  onRadioSelectedRuUploadComplete(data: any) {
    this.artistService.process(data);
    if (data.success && data.song) {
      this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_RU+'/'+data.song.name_hash]);
    }
  }

  onRadioSelectedWorldUploadComplete(data: any) {
    this.artistService.process(data);
    if (data.success && data.song) {
      this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_WORLD+'/'+data.song.name_hash]);
    }
  }


  submitAllTracksForm(song){
    this.artistService.markToRadioAllTracks(song.id)
      .subscribe( response => {
        this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_ALL_TRACKS+'/'+song.name_hash]);
      });
  }

  submitSelectedRuForm(song){
    this.artistService.markToRadioSelectedRu(song.id)
      .subscribe( response => {
        this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_RU+'/'+song.name_hash]);
      });
  }

  submitSelectedWorldForm(song){
    this.artistService.markToRadioSelectedWorld(song.id)
      .subscribe( response => {
        this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_SELECTED_WORLD+'/'+song.name_hash]);
      });
  }







  // submitAllTracksForm(song){
  //   this.artistService.markToRadioAllTracks(song.id)
  //     .subscribe( response => {
  //       this.router.navigate(['/artist-radio-fill-out/'+Artist.SERVICE_RADIO_ALL_TRACKS+'/'+song.name_hash]);
  //     });
  //
  // }


}
