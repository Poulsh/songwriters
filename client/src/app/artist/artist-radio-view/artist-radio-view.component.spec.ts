import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistRadioViewComponent } from './artist-radio-view.component';

describe('ArtistRadioViewComponent', () => {
  let component: ArtistRadioViewComponent;
  let fixture: ComponentFixture<ArtistRadioViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistRadioViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistRadioViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
