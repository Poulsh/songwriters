import {Component, OnDestroy, OnInit} from '@angular/core';
import {Song} from "../../models/song";
import {ActivatedRoute} from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
import {ArtistService} from "../artist.service";
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {PlaylistItem} from "../../models/playlistItem";
import {MessageDialog} from "../../models/messageDialog";
import {WebsocketService} from "../../websocket";
import {Notification} from "../../models/notification";
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import { LOCALE_ID, Inject } from '@angular/core';

@Component({
  selector: 'app-artist-radio-view',
  templateUrl: './artist-radio-view.component.html',
  styleUrls: ['./artist-radio-view.component.scss']
})
export class ArtistRadioViewComponent implements OnInit, OnDestroy {

  public item: PlaylistItem;
  public messageSubjectTypeItemInRadioSection = MessageDialog.SUBJECT_TYPE_ITEM_IN_RADIO_SECTION;
  public userTypeArtistInRadioSection = MessageDialog.USER_TYPE_ARTIST_IN_RADIO_SECTION;

  public statusInit = PlaylistItem.STATUS_INIT;
  public statusActive = PlaylistItem.STATUS_ACTIVE;
  public paymentInfo = PlaylistItem.PAYMENT_INFO;
  public priceRadioAllTracks = PlaylistItem.PRICE_RADIO_ALL_TRACKS;


  private unSubscribe = new Subject();

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private artistService: ArtistService,
    private authService: AuthService,
    private websocketService: WebsocketService,
    @Inject(LOCALE_ID) public locale: string,

  ) {
    websocketService.webSocketEvent
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
        (message)=>{
          if (message.event == Notification.EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE && message.data.itemId==this.item.id) {
            this.getSong();
          }
          if (message.event == Notification.EVENT_RADIO_SECTION_ITEM_NEW_MESSAGE && message.data.itemId==this.item.id) {
            this.getSong();
          }
        }
      );
  }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSong();
    this.subscribeRouteChange();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  subscribeRouteChange() {
    this.route.params
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(params => {

        if (this.item && this.item.song_id && +this.route.snapshot.paramMap.get('id')!=this.item.song_id){
          this.getSong();
        }
      });
  }

  getSong(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.artistService.getRadioItem(id)
      .subscribe( response => {
        if (response.item){
          this.item = response.item;
        }
      });
  }
}
