import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistReviewComponent } from './artist-review.component';

describe('ArtistReviewComponent', () => {
  let component: ArtistReviewComponent;
  let fixture: ComponentFixture<ArtistReviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
