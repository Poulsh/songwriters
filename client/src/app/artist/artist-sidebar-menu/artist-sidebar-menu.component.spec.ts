import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ArtistSidebarMenuComponent } from './artist-sidebar-menu.component';

describe('ArtistSidebarMenuComponent', () => {
  let component: ArtistSidebarMenuComponent;
  let fixture: ComponentFixture<ArtistSidebarMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistSidebarMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistSidebarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
