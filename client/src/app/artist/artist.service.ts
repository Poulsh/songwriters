import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {Artist} from "../models/artist";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class ArtistService {


  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }


  public getArtist(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/artist-home', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public updateArtist(artist): Observable<any> {
    const body = JSON.stringify({artist});
    return this.http.post<Artist>(environment.apiUrl+'artist/update-my-info', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public getMySongs(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-my-songs', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getMySong(hash:string): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-my-song/'+hash, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public getMySongEditData(hash:string): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-my-song-edit-data/'+hash, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getNewSongFormData(serviceType:number): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-new-song-form-data/'+serviceType, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  public updateSong(song:any): Observable<any> {
    const body = JSON.stringify({song:song});
    return this.http.post<any>(environment.apiUrl+'artist/update-my-song-info/'+song.id, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public updateSongSilently(song:any): Observable<any> {
    const body = JSON.stringify({song:song});
    return this.http.post<any>(environment.apiUrl+'artist/update-my-song-info/'+song.id, body, httpOptions)
      .pipe(map(data => this.processErrors(data)));
  }


  public deleteMySong(hash:string): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/delete-my-song/'+hash, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public addToRadio(hash:string): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/add-song-to-radio-section/'+hash, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getRadioSection(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-radio-section', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getRadioItem(id:string): Observable<any> {

    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-radio-item/'+id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public getPremiumSection(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-premium-section', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public getPremiumItem(id:string): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-premium-item/'+id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }
  //
  // public getPremiumWorldItem(id:string): Observable<any> {
  //   const body = JSON.stringify({});
  //   return this.http.post<any>(environment.apiUrl+'artist/get-premium-world-item/'+id, body, httpOptions)
  //     .pipe(map(data => this.process(data)));
  // }


  public removeItemFromRadioSection(itemId): Observable<any> {
    const body = JSON.stringify({item_id:itemId});
    return this.http.post<any>(environment.apiUrl+'artist/remove-item-from-radio-section', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public getRadioNewData(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'artist/get-radio-new-data', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public submitRadioAllTracks(song:any): Observable<any> {
    const body = JSON.stringify({song:song});
    return this.http.post<any>(environment.apiUrl+'artist/submit-radio-all-tracks/'+song.id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  public markToRadioAllTracks(id): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'artist/mark-song-to-radio-all-tracks/'+id, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  public markToRadioSelectedRu(id): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'artist/mark-song-to-radio-selected-ru/'+id, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public markToRadioSelectedWorld(id): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'artist/mark-song-to-radio-selected-world/'+id, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public submitRadioSelectedRu(song:any): Observable<any> {
    const body = JSON.stringify({song:song});
    return this.http.post<any>(environment.apiUrl+'artist/submit-radio-selected-ru/'+song.id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public submitRadioSelectedWorld(song:any): Observable<any> {
    const body = JSON.stringify({song:song});
    return this.http.post<any>(environment.apiUrl+'artist/submit-radio-selected-world/'+song.id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }








  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }
    return response;
  }

  public processErrors(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }

    return response;
  }
}
