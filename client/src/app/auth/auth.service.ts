import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user';
import { Observable ,  of } from 'rxjs';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';


import { BehaviorSubject } from 'rxjs-compat';
import {WebsocketService} from "../websocket";
import {Artist} from "../models/artist";
import {Radio} from "../models/radio";
import {Media} from "../models/media";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authUrl = environment.apiUrl+'auth/';
  public user: User;
  public artist: Artist;
  public radio: Radio;
  public media: Media;
  public isMultipleRole = false;
  public activeRole;

  private loginStatus = new BehaviorSubject<any>(false);

  constructor(
      private http: HttpClient,
      private router: Router,
      private route: ActivatedRoute,
      public dialog: MatDialog,
      public snackBar: MatSnackBar,
      private websocketService: WebsocketService

  ) { }

  ngOnInit(): void {
    this.getLocalUser();
  }

  private getLocalUser(): void {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
      if (this.user.roles){
        this.parseIfMultiple(this.user.roles);
      }
      this.loginStatus.next(true);
    } else {
      this.loginStatus.next(false);
    }

    if (localStorage.getItem('artist')) {
      this.artist = JSON.parse(localStorage.getItem('artist'));
    }
    if (localStorage.getItem('radio')) {
      this.radio = JSON.parse(localStorage.getItem('radio'));
    }
    if (localStorage.getItem('media')) {
      this.media = JSON.parse(localStorage.getItem('media'));
    }

  }


  getUserRoles(){
    this.getLocalUser();
    if (this.user) {
      return this.user.roles;
    } else {
      return '';
    }
  }

  parseIfMultiple(roles):void {
    let rolesQnt = 0;
    let singleRole = '';
    roles.forEach(function(role) {
      if (role == User.TYPE_ARTIST || role == User.TYPE_RADIO || role == User.TYPE_MEDIA){
        rolesQnt++;
        singleRole = role;
      }
    });
    if (rolesQnt>1) {
      this.isMultipleRole = true;
      if (!this.activeRole) {
        this.activeRole = singleRole;
      }
    } else if(rolesQnt == 1)  {
      this.isMultipleRole = false;
      this.activeRole = singleRole;
    } else {
      this.isMultipleRole = false;
    }
  }



  public login(email: string, password: string) : Observable<any> {
    const body = JSON.stringify({email:email, password:password});
    return this.http.post<User>(environment.apiUrl+'auth/login', body, httpOptions)
        .pipe(map(data => this.process(data)),);
  }

  getSignupData(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.get<any>(environment.apiUrl+'auth/signup-data',  httpOptions)
      .pipe();
  }

  public signup(form) : Observable<any> {
    const body = JSON.stringify(form);
    return this.http.post<any>(environment.apiUrl+'auth/signup', body, httpOptions)
      .pipe(map(data => this.processSuccessAsError(data)),);
  }

  public verification(token:string) : Observable<any> {
    return this.http.get<User>(environment.apiUrl+'auth/verification?token='+token, httpOptions)
      .pipe(map(data => this.processSuccessAsError(data)),);
  }

  public logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    if (localStorage.getItem('artist')) {
      localStorage.removeItem('artist');
    }
    if (localStorage.getItem('radio')) {
      localStorage.removeItem('radio');
    }
    if (localStorage.getItem('media')) {
      localStorage.removeItem('media');
    }


    delete(this.user);
    delete(this.artist);
    delete(this.radio);
    delete(this.media);
    this.sendLoginStatus(false);
    this.router.navigate(['/login']);
  }

  public isLoggedIn():Observable<boolean> {
    this.getLocalUser();
    return this.getLoginStatus();
  }

  public sendLoginStatus(status: boolean): void {
    this.loginStatus.next(status); //all subscribers get the new value
  }

  public getLoginStatus() {
    return this.loginStatus.asObservable();
  }

  public resetPassword(email: string) : Observable<any> {
    const body = JSON.stringify({email:email});
    return this.http.post<User>(environment.apiUrl+'auth/password-reset', body, httpOptions)
      .pipe(map(data => this.processSuccessAsError(data)),);
  }

  public changePasswordByToken(token: string,password:string) : Observable<any> {
    const body = JSON.stringify({token:token,password:password});
    return this.http.post<User>(environment.apiUrl+'auth/change-password', body, httpOptions)
      .pipe(map(data => this.processSuccessAsError(data)),);
  }










  private process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    else if (response.success) {
      if (response.token) {
        localStorage.setItem('token', response.token);
      }
      if (response.user) {
        this.user = response.user;
        localStorage.setItem('user', JSON.stringify(this.user));
        if (this.user.roles){
          this.parseIfMultiple(this.user.roles);
        }
      }
      if (response.artist) {
        this.artist = response.artist;
        localStorage.setItem('artist', JSON.stringify(this.artist));
      }
      if (response.radio) {
        this.radio = response.radio;
        localStorage.setItem('radio', JSON.stringify(this.radio));
      }
      if (response.media) {
        this.media = response.media;
        localStorage.setItem('media', JSON.stringify(this.media));
      }
      if (response.ws_ticket) {
        this.websocketService.connect(response.ws_ticket);
      }

      this.snackBar.open(response.success, '', {
        duration: 5000,
      });

    }

    return response;
  }

  private processSuccessAsError(response): any {
    if (response.token) {
      localStorage.setItem('token', response.token);
    }
    if (response.user) {
      this.user = response.user;
      localStorage.setItem('user', JSON.stringify(this.user));
      if (this.user.roles){
        this.parseIfMultiple(this.user.roles);
      }
    }
    if (response.ws_ticket) {
      this.websocketService.connect(response.ws_ticket);
    }

    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }

    if (response.success) {
      this.dialog.open(AlertComponent, {
        data: { message: response.success }
      });
    }

    if (response.redirect_to_login) {
      this.router.navigate(['/login']);
    }



    return response;
  }


  // private handleError<T> (operation = 'operation', result?:T){
  //   return (error:any): Observable<T> =>{
  //     console.error(error);
  //     console.log(`${operation} failed: ${error.message}`);
  //     return of(result as T);
  //   }
  // }

}
