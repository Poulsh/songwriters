import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {Router} from "@angular/router";
import {User} from "../../models/user";
import {SeoService} from "../../seo/seo.service";

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss']
})
export class HelloComponent implements OnInit {

  private metaTitle: string = 'Songwriters ';
  private metaDescription: string = 'Сервисы для компзиторов и радиостанций';


  constructor(
      public authService: AuthService,
      private router: Router,
      private seoService: SeoService,
  ) { }

  ngOnInit() {
    this.redirectIfAuth();
    this.seoService.set(this.metaTitle,this.metaDescription);
  }

  redirectIfAuth(){
    let user = this.authService.user;
    if (user && user.roles){
      this.authService.parseIfMultiple(user.roles);
      if (this.authService.activeRole == User.TYPE_ARTIST){
        this.router.navigate(['artist-home']);
      } else if(this.authService.activeRole == User.TYPE_RADIO){
        this.router.navigate(['radio-home']);
      } else if(this.authService.activeRole == User.TYPE_MEDIA){
        this.router.navigate(['media-home']);
      }
    }
  }

}
