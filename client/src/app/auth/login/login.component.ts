import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../auth.service'

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User } from '../../models/user';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: User;
  loginForm: FormGroup;
  loading = false;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      private location: Location,
      private fb: FormBuilder,
      public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.initForm();
  }

  get f() {return this.loginForm.controls;}

  initForm(){
    this.loginForm = this.fb.group({
      email: ['', [
        Validators.required,
      ]],
      password: ['', [
        Validators.required,
      ]]
    });
  }

  onSubmit(){
    const controls = this.loginForm.controls;
    if (this.loginForm.invalid) {
      Object.keys(controls)
          .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    this.login(this.loginForm.controls['email'].value,this.loginForm.controls['password'].value);
  }

  login(email:string, password:string){
    this.loading = true;
    this.authService.login(email, password)
        .subscribe( response => {
            this.loading = false;
            // console.log(response);
            if (response.user && response.user.roles){
              this.authService.parseIfMultiple(response.user.roles);
              if (this.authService.activeRole == User.TYPE_ARTIST){
                this.router.navigate(['artist-home']);
              } else if(this.authService.activeRole == User.TYPE_RADIO){
                this.router.navigate(['radio-home']);
              } else if(this.authService.activeRole == User.TYPE_MEDIA){
                this.router.navigate(['media-home']);
              }
            }
        });
  }


}
