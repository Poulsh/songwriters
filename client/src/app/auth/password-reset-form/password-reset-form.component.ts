import {Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import {Location } from '@angular/common';
import {AuthService } from '../auth.service'
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-password-reset-form',
  templateUrl: './password-reset-form.component.html',
  styleUrls: ['./password-reset-form.component.scss']
})
export class PasswordResetFormComponent implements OnInit {

  form: FormGroup;

  public loading;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
      private location: Location,
      private fb: FormBuilder,
      public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.form = this.fb.group({
      password: ['', [Validators.required]],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });

  }

  get f() {return this.form.controls;}

  onSubmit(){
    const controls = this.form.controls;
    if (this.form.invalid) {
      Object.keys(controls)
          .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    this.send();
  }

  checkPasswords(group: FormGroup) { // validation of passwordForm
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }

  send(){
    let token = this.route.snapshot.paramMap.get('token');
    this.loading = true;
    this.authService.changePasswordByToken(token, this.f.password.value)
        .subscribe( response => {
          this.loading = false;
          this.router.navigate(['login']);
        });

  }
}
