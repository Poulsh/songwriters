import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../auth.service'

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { User } from '../../models/user';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {

  public user: User;
  public form: FormGroup;
  public loading:boolean = false;

  constructor(
      private router: Router,
      private authService: AuthService,
      private location: Location,
      private fb: FormBuilder,
      private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.initForm();
  }

  get f() {return this.form.controls;}

  initForm(){
    this.form = this.fb.group({
      email: ['', [
        Validators.required,
        Validators.email,
      ]],
    });
  }

  onSubmit(){
    const controls = this.f.controls;
    if (this.form.invalid) {
      Object.keys(controls)
          .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    this.send(this.form.controls['email'].value);
  }

  send(email:string){
    this.loading = true;
    this.authService.resetPassword(email)
        .subscribe( response => {
          this.loading = false;
          if (response.user) {
            this.router.navigate(['login']);
          }
        });
  }
}
