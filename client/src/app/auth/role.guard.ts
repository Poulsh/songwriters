import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  constructor(public auth: AuthService, public router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const expectedRole = route.data.expectedRole;
    const token = localStorage.getItem('token');
    let user = JSON.parse(localStorage.getItem('user'));

    if (this.auth.isLoggedIn() && user && user.roles.indexOf(expectedRole) > -1 ) {
      return true;
    } else {
      this.router.navigate(['/hello']);
      return false;
    }
  }
}
