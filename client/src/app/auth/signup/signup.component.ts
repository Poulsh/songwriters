import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../auth.service'

import { Component, OnInit, Inject } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators} from '@angular/forms';
import { User } from '../../models/user';

import { AlertComponent } from '../../alert/alert.component';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Artist} from "../../models/artist";
import {Media} from "../../models/media";
import {Radio} from "../../models/radio";
import {MusicStyle} from "../../models/musicStyle";


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

   public user: User;
   public artist: Artist;
   public media: Media;
   public radio: Radio;
   public signupForm: FormGroup;
   public passwordFormGroup: FormGroup;
   public loading = false;


  public isUserArtist:boolean = false;
  public isUserMedia:boolean = false;
  public isUserRadio:boolean = false;

  public musicStyleOptions: MusicStyle[] = [];


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private location: Location,
    private fb: FormBuilder,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.initForm();
    this.getData();
  }

  initForm(){
    this.signupForm = this.fb.group({
      email: ['', [Validators.required,]],
      firstName: [''],
      lastName: [''],

      isArtist:[''],
      isRadio: [''],
      isMedia: [''],
      artistName: [''],
      mediaName: [''],
      radioName: [''],

      country: [''],
      city: [''],
      musicStyles: [''],
      passwordFormGroup: [''],

    });
    this.passwordFormGroup = this.fb.group({
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    }, {
      validator: RegistrationValidator.validate.bind(this)
    });
  }


  get f() {return this.signupForm.controls;}
  get pass() {return this.passwordFormGroup.controls;}

  getData() {
    const key = this.route.snapshot.paramMap.get('key');
    if (key == 'radio'){
      this.f.isRadio.setValue(true);
      this.isUserRadio = true;
    }
    if (key == 'artist'){
      this.f.isArtist.setValue(true);
      this.isUserArtist = true;
    }

    this.authService.getSignupData()
      .subscribe( response => {
        this.musicStyleOptions = response.musicStyles;
      });
  }

  onArtistCheckboxChanged(event) {
    console.log('onArtistCheckboxChanged event',event);
    if (event.checked) {
      this.isUserArtist = true;
      this.f.isRadio.setValue(false);
      this.isUserRadio = false;
    } else {
      this.isUserArtist = false;
    }
  }

  onMediaCheckboxChanged(event) {
    if (event.checked) {
      this.isUserMedia = true;
      this.f.isRadio.setValue(false);
      this.isUserRadio = false;
    } else {
      this.isUserMedia = false;
    }
  }

  onRadioCheckboxChanged(event) {
    if (event.checked) {
      this.isUserRadio = true;

      this.f.isArtist.setValue(false);
      this.isUserArtist = false;

      this.f.isMedia.setValue(false);
      this.isUserMedia = false;

    } else {
      this.isUserRadio = false;
    }
  }

  onSubmit(event){
    event.preventDefault();
    const controls = this.signupForm.controls;
    if (this.signupForm.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }
    this.signup();
  }


  signup():void {

    let form = {
      user:{
        email:this.f.email.value,
        first_name:this.f.firstName.value,
        last_name:this.f.lastName.value,
        password:this.pass.password.value,
      },

      isUserArtist:this.isUserArtist?true:null,
      isUserMedia:this.isUserMedia?true:null,
      isUserRadio:this.isUserRadio?true:null,

      country:this.f.country.value,
      city:this.f.city.value,
      music_styles:this.f.musicStyles.value,

      artist:{
        name:this.f.artistName.value,
      },
      radio:{
        name:this.f.radioName.value,
      },
      media:{
        name:this.f.mediaName.value,
      }
    };


    if (!this.isUserArtist && !this.isUserMedia &&!this.isUserRadio) {
      let message = 'Уточните тип аккаунта (один из верхних чекбоксов)';
      this.dialog.open(AlertComponent, {
        data: { message: message }
      });
    } else if (this.isUserRadio && !this.f.radioName.value) {
      let message = 'Уточните название радиостанции';
      this.dialog.open(AlertComponent, {
        data: { message: message }
      });
    } else {
      this.loading = true;
      this.authService.signup(form)
        .subscribe( response => {
          this.loading = false;
        });
    }
  }
}


export class RegistrationValidator {
  static validate(registrationFormGroup: FormGroup) {
    let password = registrationFormGroup.controls.password.value;
    let repeatPassword = registrationFormGroup.controls.repeatPassword.value;
    if (repeatPassword.length <= 0) {
      return null;
    }
    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }
    return null;
  }
}