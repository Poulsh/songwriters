import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../auth.service'

import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../../models/user';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {

  user: User;
  loading = false;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private location: Location,
    public dialog: MatDialog
  ) {

  }

  ngOnInit() {
    this.verify();
  }


  verify():void {

    let token = this.route.snapshot.paramMap.get('token');
    this.loading = true;


    this.authService.verification(token)
      .subscribe( response => {
        this.loading = false;
        this.router.navigate(['/hello']);
        //console.log('verify response'); console.log(response);
        //if (response.user) {
        //  this.router.navigate(['item']);
        //}
      });
    // get verification code
    // this.route.queryParams.subscribe(params => {
    //   let token = params['token'];
    //   console.log(token); // Print the parameter to the console.
    //   this.authService.verification(token)
    //     .subscribe( response => {
    //       this.loading = false;
    //
    //       console.log('response' + response);
    //       this.router.navigate(['/login'])
    //     });
    // });
  }

}
