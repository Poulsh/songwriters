import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CommonCommentListComponent } from './common-comment-list.component';

describe('CommonCommentListComponent', () => {
  let component: CommonCommentListComponent;
  let fixture: ComponentFixture<CommonCommentListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonCommentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonCommentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
