import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Comment} from "../../models/comment";
import {environment} from "../../../environments/environment";
import {Imagefile} from "../../models/imagefile";

@Component({
  selector: 'app-common-comment-list',
  templateUrl: './common-comment-list.component.html',
  styleUrls: ['./common-comment-list.component.scss']
})
export class CommonCommentListComponent implements OnInit {

  @Input() comments: Comment[];
  @Output() onUpdate = new EventEmitter<any>();

  public authorTypeRadio = Comment.AUTHOR_TYPE_RADIO;
  public authorTypeArtist = Comment.AUTHOR_TYPE_ARTIST;

  public replaceNoimageRadio = Imagefile.REPLACE_NOIMAGE_RADIO;
  public replaceNoimageArtist = Imagefile.REPLACE_NOIMAGE_ARTIST;



  public apiUrl = environment.apiUrl;


  constructor() { }

  ngOnInit() {
  }

}
