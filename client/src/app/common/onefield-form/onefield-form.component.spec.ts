import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OnefieldFormComponent } from './onefield-form.component';

describe('OnefieldFormComponent', () => {
  let component: OnefieldFormComponent;
  let fixture: ComponentFixture<OnefieldFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OnefieldFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnefieldFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
