import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators } from '@angular/forms';

@Component({
  selector: 'app-onefield-form',
  templateUrl: './onefield-form.component.html',
  styleUrls: ['./onefield-form.component.scss']
})
export class OnefieldFormComponent implements OnInit {

  public form:FormGroup;
  @Input() buttonName: string = 'отправить';
  @Input() placeholder: string;

  @Output() onSubmit = new EventEmitter<any>();


  constructor(
    private fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.initForm();
  }

  get f() {return this.form.controls;}


  initForm() {
    this.form = this.fb.group({
      text: ['',[Validators.maxLength(510)]],
    });
    // if (this.toBeEdited){
    //   this.commentToForm(this.toBeEdited);
    // }
  }

  // textToForm(text:string):void {
  //   this.f.text.setValue(text);
  // }


  submitForm(){
    if (this.f.text.value && this.f.text.value!=''){
      this.onSubmit.emit(this.f.text.value);
      // this.f.text.reset();
    }


  }
}
