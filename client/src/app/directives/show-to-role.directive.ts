import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import {AuthService} from "../auth/auth.service";

@Directive({
  selector: '[showToRole]',
  inputs: ['showToRole']
})
export class ShowToRoleDirective {

  constructor(private _templateRef: TemplateRef<any>,
              private _viewContainer: ViewContainerRef,
              private authService: AuthService) {}

  @Input() set showToRole(allowedRoles:Array<string>){

    let shouldShow: boolean = false;
    let userRoles = this.authService.getUserRoles();


    for (let role of userRoles){

      if (role.toLowerCase() == 'admin') {
        shouldShow = true;
        break;
      }
      for(let allowedRole of allowedRoles){
        if (allowedRole.toLowerCase() == role.toLowerCase()) {
          shouldShow = true;
          break;
        }
      }
    }
    if (shouldShow) {
      this._viewContainer.createEmbeddedView(this._templateRef);
    } else {
      this._viewContainer.clear();
    }
  }
}
