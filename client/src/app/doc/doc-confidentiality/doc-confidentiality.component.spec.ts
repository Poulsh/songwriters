import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DocConfidentialityComponent } from './doc-confidentiality.component';

describe('DocConfidentialityComponent', () => {
  let component: DocConfidentialityComponent;
  let fixture: ComponentFixture<DocConfidentialityComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DocConfidentialityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocConfidentialityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
