import { Component, OnInit } from '@angular/core';
import {SeoService} from "../../seo/seo.service";
import { LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'app-doc-confidentiality',
  templateUrl: './doc-confidentiality.component.html',
  styleUrls: ['./doc-confidentiality.component.scss']
})
export class DocConfidentialityComponent implements OnInit {

  private metaTitle: string = 'Songwriters - политика конфиденциальности';
  private metaDescription: string = 'Конфиденциальность персональных данных';

  constructor(
    private seoService: SeoService,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit(): void {
    this.seoService.set(this.metaTitle,this.metaDescription);

  }

}
