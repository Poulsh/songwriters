import { Component, OnInit } from '@angular/core';
import {SeoService} from "../../seo/seo.service";
import { LOCALE_ID, Inject } from '@angular/core';

@Component({
  selector: 'app-doc-terms',
  templateUrl: './doc-terms.component.html',
  styleUrls: ['./doc-terms.component.scss']
})
export class DocTermsComponent implements OnInit {

  private metaTitle: string = 'Songwriters - условия использования';
  private metaDescription: string = 'Соглашение о предоставлении услуг';

  constructor(
    private seoService: SeoService,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit(): void {
    this.seoService.set(this.metaTitle,this.metaDescription);
  }

}
