import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DocViolationComponent } from './doc-violation.component';

describe('DocViolationComponent', () => {
  let component: DocViolationComponent;
  let fixture: ComponentFixture<DocViolationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DocViolationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocViolationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
