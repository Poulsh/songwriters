import { Component, OnInit } from '@angular/core';
import {SeoService} from "../../seo/seo.service";

@Component({
  selector: 'app-doc-violation',
  templateUrl: './doc-violation.component.html',
  styleUrls: ['./doc-violation.component.scss']
})
export class DocViolationComponent implements OnInit {

  private metaTitle: string = 'Songwriters - что делать при нарушении авторских прав';
  private metaDescription: string = 'Сообщить о нарушении авторских прав';

  constructor(
    private seoService: SeoService,
  ) { }

  ngOnInit(): void {
    this.seoService.set(this.metaTitle,this.metaDescription);
  }

}
