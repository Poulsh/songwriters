import {Component, Inject, Input, LOCALE_ID, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
// import { saveAs } from 'file-saver/dist/FileSaver';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-file-download',
  templateUrl: './file-download.component.html',
  styleUrls: ['./file-download.component.scss']
})
export class FileDownloadComponent implements OnInit {

  @Input() url : string;
  @Input() type: string;   // 'application/json'
  @Input() fileName: string;
  @Input() buttonType: string = 'default';
  @Input() buttonName: string = this.locale=='ru'?'Скачать':'Download';
  @Input() buttonColor: string = 'warn';

  @Input() isUlrReady: boolean = false;


  constructor(
    private http: HttpClient,
    @Inject(LOCALE_ID) public locale: string,

  ) { }

  ngOnInit() {
  }




  downloadFile(): void{

    let regExp = /(?:\.([^.]+))?$/;
    let extension = regExp.exec(this.url)[1];

    let fileNameNoProb = this.fileName.replace(new RegExp(' ', 'g'), "_");
    let fileNameNoDots = fileNameNoProb.replace(new RegExp(/\./g, 'g'), "_");



    let filename = fileNameNoDots+'.'+extension;

    let link = environment.apiUrl+'uploadedsongs/source/'+this.url;
    if (this.isUlrReady){
      link = this.url;
    }

    const headers = new HttpHeaders({'Content-Type': this.type });   // 'application/json'
    this.http.get(link,{headers, responseType: 'blob' as 'json'}).subscribe(
      (response: any) =>{
        let dataType = response.type;
        let binaryData = [];
        binaryData.push(response);
        let downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));
        if (filename)
          downloadLink.setAttribute('download', filename);
        document.body.appendChild(downloadLink);
        downloadLink.click();
      }
    )
  }
}
