import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { environment } from '../../../environments/environment';


import {
  HttpClient,
  HttpRequest,
  HttpEventType,
  HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';
import { FileUpload } from "../../models/fileUpload";
import {ArtistService} from "../../artist/artist.service";



@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})


export class ImageUploadComponent implements OnInit {

  private _url = environment.apiUrl;

  /** Link text */
  @Input() text = 'Загрузить';

  @Input() buttonType = 'mat-button';
  @Input() buttonColor = 'primary';
  @Input() noIcon:boolean = false;
  @Input() icon='file_upload';

  /** Name used in form which will be sent in HTTP request. */
  @Input() param = 'image';

  /** Target URL for file uploading. */
  @Input() target = '';

  /** Object id */
  @Input() id:string|number = '';

  /** mode for chose url */
  @Input() mode: string;

  @Input() newSongFormData: any;




  /** File extension that accepted, same as 'accept' of <input type="file" />.
   By the default, it's set to 'image/*'. */
  @Input() accept = 'image/*';

  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();

  //private files: Array<FileUploadModel> = [];
  public files: Array<FileUpload> = [];




  constructor(
    private http: HttpClient,
    private artistService: ArtistService
  ) { }

  ngOnInit() {
  }

  onClick() {

    const imageUpload = document.getElementById('imageUpload') as HTMLInputElement;
    imageUpload.onchange = () => {
      for (let index = 0; index < imageUpload.files.length; index++) {
        const file = imageUpload.files[index];
        this.files.push({ data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true });
      }
      this.uploadFiles();
    };
    imageUpload.click();

  }

  private uploadFiles() {
    const imageUpload = document.getElementById('imageUpload') as HTMLInputElement;
    imageUpload.value = '';

    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  private uploadFile(file: FileUpload) {
    if (this.mode == 'artistAvatar') {
      this.target = this._url+'artist/upload-avatar';
    }
    else if (this.mode == 'radioAvatar') {
      this.target = this._url+'radio/upload-avatar';
    }
    else if (this.mode == 'mediaAvatar') {
      this.target = this._url+'media/upload-avatar';
    }
    else if (this.mode == 'songImage') {
      this.target = this._url+'artist/upload-image-to-song/'+this.id;
    }



    const fd = new FormData();
    fd.append(this.param, file.data);



    const req = new HttpRequest('POST', this.target, fd, {
      reportProgress: true
    });

    file.inProgress = true;
    file.sub = this.http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (this.mode == 'avatar' ) {
          this.artistService.process(event.body);
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        } else {
          if (typeof (event) === 'object') {
            this.removeFileFromArray(file);
            this.complete.emit(event.body);
          }
        }

      }
    );
  }

  cancelFile(file: FileUpload) {
    file.sub.unsubscribe();
    this.removeFileFromArray(file);
  }

  retryFile(file: FileUpload) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private removeFileFromArray(file: FileUpload) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }

}
