import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { environment } from '../../../environments/environment';


import {
  HttpClient,
  HttpRequest,
  HttpEventType,
  HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { catchError, last, map, tap } from 'rxjs/operators';
import { FileUpload } from "../../models/fileUpload";
import {ArtistService} from "../../artist/artist.service";
import {Song} from "../../models/song";
import {Artist} from "../../models/artist";



@Component({
  selector: 'app-song-upload',
  templateUrl: './song-upload.component.html',
  styleUrls: ['./song-upload.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})


export class SongUploadComponent implements OnInit {

  private _url = environment.apiUrl;
  //private uploadAvatarUrl = environment.apiUrl+'user/upload-avatar';
  //private uploadProductItemImageUrl = environment.apiUrl+'product-item/upload-image';
  //private uploadShopProductItemImageUrl = environment.apiUrl+'shop/shop-product-item-upload-image';


  /** Link text */
  @Input() text = 'Загрузить';

  @Input() buttonType = 'mat-button';
  @Input() buttonColor = 'primary';
  @Input() noIcon:boolean = false;
  @Input() icon='file_upload';

  /** Name used in form which will be sent in HTTP request. */
  @Input() param = 'song';

  /** Target URL for file uploading. */
  @Input() target = '';

  /** Object id */
  @Input() id:string|number = '';

  /** mode for chose url */
  @Input() mode: string;

  @Input() newSongFormData: any;




  /** File extension that accepted, same as 'accept' of <input type="file" />.
   By the default, it's set to 'image/*'. */
  @Input() accept = '.mp3,.wav';

  /** Allow you to add handler after its completion. Bubble up response text from remote. */
  @Output() complete = new EventEmitter<string>();
  @Output() loading = new EventEmitter<boolean>();

  //private files: Array<FileUploadModel> = [];
  public files: Array<FileUpload> = [];




  constructor(
    private http: HttpClient,
    private artistService: ArtistService
  ) { }

  ngOnInit() {
  }

  onClick() {

    const songUpload = document.getElementById('songUpload') as HTMLInputElement;
    songUpload.onchange = () => {
      for (let index = 0; index < songUpload.files.length; index++) {
        const file = songUpload.files[index];
        this.files.push({ data: file, state: 'in',
          inProgress: false, progress: 0, canRetry: false, canCancel: true });
      }
      this.uploadFiles();
    };
    songUpload.click();

  }

  private uploadFiles() {
    const songUpload = document.getElementById('songUpload') as HTMLInputElement;
    songUpload.value = '';

    this.files.forEach(file => {
      this.uploadFile(file);
    });
  }

  private uploadFile(file: FileUpload) {
    this.loading.emit(true);

    if (this.mode == Artist.SERVICE_RADIO_ALL_TRACKS) {
      this.target = this._url+'artist/upload-radio-song/'+Song.SERVICE_URL_CODE_ALL_TRACKS;
    }
    else if (this.mode ==  Artist.SERVICE_RADIO_SELECTED_RU) {
      this.target = this._url+'artist/upload-radio-song/'+Song.SERVICE_URL_CODE_SELECTED_RU;
    }
    else if (this.mode ==  Artist.SERVICE_RADIO_SELECTED_WORLD) {
      this.target = this._url+'artist/upload-radio-song/'+Song.SERVICE_URL_CODE_SELECTED_WORLD;
    }
    else if (this.mode ==  Artist.SERVICE_EDIT_OWN_SONG && this.id) {
      this.target = this._url+'artist/upload-new-file-of-song/'+this.id;
    }

    const fd = new FormData();
    fd.append(this.param, file.data);

    if (this.param == 'song' && this.newSongFormData){
      let songData = this.newSongFormData;
      songData.music_style1_id = this.newSongFormData.music_style1_id?this.newSongFormData.music_style1_id.id:null;
      songData.music_style2_id = this.newSongFormData.music_style2_id?this.newSongFormData.music_style2_id.id:null;
      songData.music_style3_id = this.newSongFormData.music_style3_id?this.newSongFormData.music_style3_id.id:null;
      songData.release_date = Math.round( new Date(this.newSongFormData.release_date).getTime()/1000);
      if (this.newSongFormData.language && this.newSongFormData.language.length>0){
        let langArr = [];
        for (let l in this.newSongFormData.language) {
          if ( this.newSongFormData.language.hasOwnProperty(l)) {
            langArr[l]=this.newSongFormData.language[l].id;
          }
        }
        songData.language = JSON.stringify(langArr);
      }
      fd.append('song_data',  JSON.stringify(songData));
    }


    const req = new HttpRequest('POST', this.target, fd, {
      reportProgress: true
    });

    file.inProgress = true;
    file.sub = this.http.request(req).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            file.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      tap(message => { }),
      last(),
      catchError((error: HttpErrorResponse) => {
        file.inProgress = false;
        file.canRetry = true;
        return of(`${file.data.name} upload failed.`);
      })
    ).subscribe(
      (event: any) => {
        if (this.mode == 'avatar' ) {
          this.artistService.process(event.body);
          this.removeFileFromArray(file);
          this.complete.emit(event.body);
        } else {
          if (typeof (event) === 'object') {
            this.removeFileFromArray(file);
            this.loading.emit(false);
            this.complete.emit(event.body);
          }
        }

      }
    );
  }

  cancelFile(file: FileUpload) {
    file.sub.unsubscribe();
    this.removeFileFromArray(file);
  }

  retryFile(file: FileUpload) {
    this.uploadFile(file);
    file.canRetry = false;
  }

  private removeFileFromArray(file: FileUpload) {
    const index = this.files.indexOf(file);
    if (index > -1) {
      this.files.splice(index, 1);
    }
  }

}
