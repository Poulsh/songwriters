import { Injectable } from '@angular/core';

import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertComponent } from '../alert/alert.component';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) { }

    intercept(request:HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{


      //    retry(1),
        return next.handle(request)
          .pipe(
            catchError((error:HttpErrorResponse)=>{
                let errorMessage = '';
                let notShowDialog = false;
                if (error.error instanceof ErrorEvent) {
                    // client error
                  errorMessage = `<span class="text-danger">Ошибка:</span><br> ${error.error.message}`;
                } else {
                  // server error
                  if (!navigator.onLine) {
                    // offline error
                    errorMessage = `Нет подключения к серверу`;
                    notShowDialog = true;
                    this.snackBar.open('Нет подключения к серверу', '✕', {
                    });
                  } else {
                    if (error.status == 404) {
                      errorMessage = `<span class="text-danger">Операция не выполнена.</span><br> Ошибка 404 - не найдено.`;
                    }
                    else if (error.status == 400) {
                      errorMessage = '<span class="text-danger">Ошибка <sup>400</sup> '+error.statusText +'.</span><br> '+error.error.message;
                    }
                    else if (error.status == 500) {
                      errorMessage = '<span class="text-danger">Ошибка <sup>500</sup> '+error.statusText +'.</span><br> '+error.error.message;
                    }
                    else if (error.status === 0) {
                      if (error.name =='HttpErrorResponse' && error.ok == false){
                        notShowDialog = true;
                        this.snackBar.open('Нет подключения к серверу', '✕', {
                        });
                      } else {
                        errorMessage = 'Неизвестная ошибка, проверьте соединение с интернетом';
                      }
                    }
                    else {
                      errorMessage = '<span class="text-danger">Ошибка <sup>'+error.status+'</sup> '+error.statusText +'.</span><br> '+error.error.message+'<br><small>'+error.message+'</small>';
                    }
                  }

                }

                if (!notShowDialog){
                  this.dialog.open(AlertComponent, {
                    data: { message: errorMessage }
                  });
                }
                return throwError(errorMessage);
            })
          );



    }
}
