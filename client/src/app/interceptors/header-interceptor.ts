import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse  } from '@angular/common/http';
import { Observable } from 'rxjs';



@Injectable()
export class HeaderInterceptor implements HttpInterceptor {

    currentLocale: string;

    constructor(@Inject(LOCALE_ID) public locale: string) {
        this.currentLocale = this.locale.split('-', 1)[0];
    }

    intercept(req:HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{

        const authReq = req.clone({ headers:
              (req.headers)
                .set("Authorization", "Bearer " + localStorage.getItem('token'))
                .set("App-Language", this.currentLocale )
        });

        return next.handle(authReq);

    }
}
