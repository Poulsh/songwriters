import { Component, OnInit, isDevMode } from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {UserService} from "../../user/user.service";

@Component({
  selector: 'app-language-switch',
  templateUrl: './language-switch.component.html',
  styleUrls: ['./language-switch.component.scss']
})
export class LanguageSwitchComponent implements OnInit {

  isDev = isDevMode();
  siteLanguage: string;
  siteLocale: string;
  languageList = [
    { code: 'en', label: 'English' },
    { code: 'ru', label: 'Russian' },
  ];

  constructor(
    private authService: AuthService,
    private userService: UserService,
  ) { }


  ngOnInit() {
    this.siteLocale = window.location.pathname.split('/')[1]

    if (this.authService.user){
      this.getUserLang()
    } else {
      this.siteLanguage = this.languageList.find(
        (f) => f.code === this.siteLocale
      )?.label;
      if (!this.siteLanguage) {
        this.changeTo(this.languageList[0].code)
      }
    }
  }

  getUserLang(){
    this.userService.getUser()
      .subscribe( response => {
        if(response.user && response.user.language !=  this.siteLocale ){
          this.gotoNewLangUrl(response.user.language)
        }
      });
  }

  changeTo(selectedLangCode: string) {
    if(this.authService.user){
      this.userService.changeLanguage(selectedLangCode)
        .subscribe(
          response => {
            this.gotoNewLangUrl(selectedLangCode)
          },
          (error: Event) => {
            this.gotoNewLangUrl(selectedLangCode)
          }
        );
    } else {
      this.gotoNewLangUrl(selectedLangCode)
    }
  }

  gotoNewLangUrl(langCode){
    let url =  window.location.pathname
    let urlArr = url.split('/')
    urlArr[1] = langCode
    window.location.href = urlArr.join('/')
  }

}
