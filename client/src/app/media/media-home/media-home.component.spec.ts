import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MediaHomeComponent } from './media-home.component';

describe('MediaHomeComponent', () => {
  let component: MediaHomeComponent;
  let fixture: ComponentFixture<MediaHomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
