import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {ActivatedRoute} from "@angular/router";
import {RadioService} from "../../radio/radio.service";
import {Location} from "@angular/common";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import {MediaService} from "../media.service";
import {Radio} from "../../models/radio";
import {environment} from "../../../environments/environment";
import {Media} from "../../models/media";

@Component({
  selector: 'app-media-home',
  templateUrl: './media-home.component.html',
  styleUrls: ['./media-home.component.scss']
})
export class MediaHomeComponent implements OnInit {

  media: Media;
  public apiUrl = environment.apiUrl;
  public mediaInfoForm: FormGroup;

  showInfoForm:boolean = false;
  showEditButtons:boolean = false;

  constructor(
      private route: ActivatedRoute,
      private mediaService: MediaService,
      private location: Location,
      private fb: FormBuilder,
      private authService: AuthService,
      public dialog: MatDialog

  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_MEDIA;
    this.initForm();
    this.getMedia();
  }

  initForm(){
    this.mediaInfoForm = this.fb.group({
      name: ['', [
        Validators.required,
      ]],
      country: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      city: ['', []],
      main_info: ['', []]
    });
  }

  get mif() {return this.mediaInfoForm.controls;}

  getMedia(): void {
    this.mediaService.getMedia()
        .subscribe( response => {
          this.processResponse(response);
        });
  }

  constructInfoForm(){
    this.showInfoForm = true;
    this.showEditButtons = false;
  }

  processResponse(response):void {
    let timeStamp = (new Date()).getTime();
    if (response.media) {
      this.media = response.media;
      this.media.image = response.media.image?response.media.image+'?'+timeStamp:null;
      this.mif.name.setValue(this.media.name);
      this.mif.country.setValue(this.media.country);
      this.mif.city.setValue(this.media.city);
      this.mif.main_info.setValue(this.media.main_info);
    }
    if (response.success){
      this.showInfoForm = false;
      this.showEditButtons = false;
    }
  }

  saveInfo():void {
    let info = {
      name:this.mif.name.value,
      country:this.mif.country.value,
      city:this.mif.city.value,
      main_info:this.mif.main_info.value,
    };
    this.mediaService.updateInfo(info)
        .subscribe( response => {
          this.processResponse(response);
        });
  }

  // triggered by complete avatar file-upload
  onAvatarUploadComplete(data: any) {
    this.getMedia();
    this.showEditButtons = false;
  }
}
