import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MediaSidebarMenuComponent } from './media-sidebar-menu.component';

describe('MediaSidebarMenuComponent', () => {
  let component: MediaSidebarMenuComponent;
  let fixture: ComponentFixture<MediaSidebarMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSidebarMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSidebarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
