import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {Artist} from "../models/artist";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class MediaService {

  private _url = environment.apiUrl;

  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }


  public getMedia(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(this._url+'media/media-home', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public updateInfo(media): Observable<any> {
    const body = JSON.stringify({media});
    return this.http.post<Artist>(this._url+'media/update-my-info', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }








  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }
    return response;
  }
}
