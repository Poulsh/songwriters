import { Component, OnInit, Input, Output, EventEmitter,
  DoCheck, AfterViewChecked, ViewChild, ElementRef, IterableDiffers, OnDestroy } from '@angular/core';
import { environment } from '../../../environments/environment';
import { MessageService } from "../message.service";

import {
  FormGroup,
  FormBuilder} from '@angular/forms';


import {
  HttpClient,
} from '@angular/common/http';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MessageDialog} from "../../models/messageDialog";
import {AuthService} from "../../auth/auth.service";
import {NotificationService} from "../../notification/notification.service";
import {Subject} from "rxjs/index";
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import {Message} from "../../models/message";



@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss'],
})
export class MessageFormComponent implements OnInit, OnDestroy {

  public apiUrl = environment.apiUrl;

  public showMessageForm:boolean  = false;


  @Input() subject_type;
  @Input() subject_id;
  @Input() buttonName;
  @Input() userType;
  @Input() inQnt;
  @Input() inQntNotSeen;

  @Input() dialogId:number;

  public messageForm:FormGroup;
  public messageDialog: MessageDialog=null;

  @ViewChild('scrollable') private scrollable: ElementRef;
  private shouldScrollDown: boolean;
  private iterableDiffer;
  private unSubscribe = new Subject();

  public notSeenQnt:number;
  public allQnt:number;


  public statusSeen:string = Message.STATUS_SEEN;
  public statusNotSeen:string = Message.STATUS_NOT_SEEN;

  public fromToSystemToArtist:string = Message.FROM_TO_SYSTEM_TO_ARTIST;





  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    public dialog: MatDialog,
    private messageService: MessageService,
    private authService: AuthService,
    private iterableDiffers: IterableDiffers,
    private notificationService: NotificationService
  ) {

    this.iterableDiffer = this.iterableDiffers.find([]).create(null);
    notificationService.notifyUserQntEvent
      .pipe(takeUntil(this.unSubscribe))
      .subscribe(
        (result)=>{

        }
      );
  }

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  get f() {return this.messageForm.controls;}

  ngDoCheck(): void {
    if (this.showMessageForm && this.messageDialog && this.iterableDiffer.diff(this.messageDialog.messages)) {
      this.shouldScrollDown = true;
    }
  }
  ngAfterViewChecked(): void {
    if (this.showMessageForm && this.shouldScrollDown) {
      this.scrollToBottom();
      this.shouldScrollDown = false;
    }
  }
  scrollToBottom() {
    try {
      this.scrollable.nativeElement.scrollTop = this.scrollable.nativeElement.scrollHeight;
    } catch (e) {
      console.error(e);
    }
  }

  initForm() {
    this.messageForm = this.fb.group({
      text: [''],
    });
  }

  getArtistRadioDialogBySubject():void {
    let subject = {
      subject_type:this.subject_type,
      subject_id:this.subject_id,
    };
    this.messageService.getArtistRadioDialogBySubject(subject)
      .subscribe( response => {
        if (response.dialog) {
          this.messageDialog = response.dialog;
          this.notSeenQnt = null;
          this.inQntNotSeen = null;
          // this.addDialog2Active(response.dialog);
        }
      });
  }

  getArtistPremiumDialogBySubject():void {
    let subject = {
      subject_type:this.subject_type,
      subject_id:this.subject_id,
    };
    this.messageService.getArtistPremiumDialogBySubject(subject)
      .subscribe( response => {
        if (response.dialog) {
          this.messageDialog = response.dialog;
          this.notSeenQnt = null;
          this.inQntNotSeen = null;
          // this.addDialog2Active(response.dialog);
        }
      });
  }

  getArtistDialogById(id:number):void {
    this.messageService.getArtistDialogById(id)
      .subscribe( response => {
        if (response.dialog) {
          this.messageDialog = response.dialog;
          this.notSeenQnt = null;
          this.inQntNotSeen = null;
        }
      });
  }



  showMessages(){
    this.showMessageForm=true;
    this.getDialog();
  }

  getDialog(){
    if (this.dialogId){
      if (this.userType == MessageDialog.USER_TYPE_ARTIST_IN_RADIO_SECTION||
        this.userType == MessageDialog.USER_TYPE_ARTIST_IN_PREMIUM_SECTION) {
        this.getArtistDialogById(this.dialogId);
      }

    } else {
      if (this.userType == MessageDialog.USER_TYPE_ARTIST_IN_RADIO_SECTION) {
        this.getArtistRadioDialogBySubject();
      } else if (this.userType == MessageDialog.USER_TYPE_ARTIST_IN_PREMIUM_SECTION){
        this.getArtistPremiumDialogBySubject();
      }
    }
  }


  sendMessage():void {
    if (!this.f.text.value||this.f.text.value==' ') return;

    let message = {
      text:this.f.text.value,
      from_to_type:Message.FROM_TO_ARTIST_TO_SYSTEM,
      subject_type:this.subject_type,
      subject_id:this.subject_id,

    };
    if (!this.messageDialog) {      // новый диалог
      this.messageService.send(message)
        .subscribe( response => {
          if (response.dialog) {
            this.messageDialog = response.dialog;
            this.dialogId = this.messageDialog.id;
            this.f.text.reset();
            // this.addDialog2Active(response.dialog);
          }
        });
    } else {              //  есть диалог
      this.messageService.reply(message,this.messageDialog.id)
        .subscribe( response => {
          if (response.success) {
            this.f.text.setValue('');
            this.getDialog();
          }
        });
    }
  }



  // addDialog2Active(dialog:MessageDialog){
  //   let activeDialogs = this.notificationService.userCheckSum.activeDialogs?
  //     this.notificationService.userCheckSum.activeDialogs:{};
  //   // let priceOffers = {};
  //   // for (var key in dialog.price_offers){
  //   //   console.log(`key: ${key}, value: ${dialog.price_offers[key]}`);
  //   //   priceOffers[key] = {
  //   //     id:dialog.price_offers[key].id,
  //   //     status:dialog.price_offers[key].status,
  //   //   }
  //   // }
  //
  //
  //   activeDialogs[dialog.id] = {
  //     id:dialog.id,
  //     messagesQnt:dialog.messagesQnt?dialog.messagesQnt:null,
  //     priceOffers:dialog.price_offers?dialog.price_offers:null,
  //   };
  //   this.notificationService.userCheckSum.activeDialogs = activeDialogs;
  // }



}
