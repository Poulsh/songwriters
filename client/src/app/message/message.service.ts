import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {NotificationService} from "../notification/notification.service";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class MessageService {


  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private notificationService: NotificationService

  ) { }

  send(message): Observable<any> {
    const body = JSON.stringify({Message:message});
    return this.http.post<any>(environment.apiUrl+'message/send', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  getArtistRadioDialogBySubject(subject): Observable<any> {
    const body = JSON.stringify({subject:subject});
    return this.http.post<any>(environment.apiUrl+'message/artist-radiosec-dialog-by-subject', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  getArtistPremiumDialogBySubject(subject): Observable<any> {
    const body = JSON.stringify({subject:subject});
    return this.http.post<any>(environment.apiUrl+'message/artist-premiumsec-dialog-by-subject', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  getArtistDialogById(id): Observable<any> {
    const body = JSON.stringify({id:id});
    return this.http.post<any>(environment.apiUrl+'message/artist-dialog/'+id, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  reply(message,dialogId): Observable<any> {
    const body = JSON.stringify({Message:message});
    return this.http.post<any>(environment.apiUrl+'message/reply/'+dialogId, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }




  public process(response): any {

    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 3000,
      });
    }
    if (response.userCheckSum) {
      // if (response.userCheckSum.totalMessagesQnt) {
      //   this.notificationService.userCheckSum.totalMessagesQnt = response.userCheckSum.totalMessagesQnt;
      // }
      // if (response.userCheckSum.messagesNotSeen) {
      //   this.notificationService.userCheckSum.messagesNotSeen = response.userCheckSum.messagesNotSeen;
      // }
      // if (response.userCheckSum.cartQnt) {
      //   this.notificationService.userCheckSum.cartQnt = response.userCheckSum.cartQnt;
      // }
      // if (response.userCheckSum.priceOffers) {
      //   this.notificationService.userCheckSum.priceOffers = response.userCheckSum.priceOffers;
      // }
    }
    return response;
  }
}
