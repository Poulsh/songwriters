import {User} from "./user";
export class Artist {

    public static SERVICE_RADIO_ALL_TRACKS = 'radio_all_tracks';
    public static SERVICE_RADIO_SELECTED_RU = 'radio_selected_ru';
    public static SERVICE_RADIO_SELECTED_WORLD = 'radio_selected_world';

    public static SERVICE_EDIT_OWN_SONG = 'editSong';


    id: number;
    user_id: number;
    user: User;
    page_id: number;
    name: string;
    country: string;
    city: string;
    hrurl: string;
    main_info: string;
    comment: string;
    image: string;
    image_alt: string;
    status: string;

}