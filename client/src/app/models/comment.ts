import {Radio} from "./radio";
import {Artist} from "./artist";
import {Media} from "./media";

export class Comment {

    public static OBJECT_TYPE_SONG_COMMENT = 100;

    public static AUTHOR_TYPE_RADIO = 100;
    public static AUTHOR_TYPE_ARTIST = 101;


    public static STATUS_ACTIVE = 100;



    id: number;
    text:string;
    object_type:number;
    object_id:number;
    author_type:number;
    author_id:number;
    status:number;
    created_at:number;
    updated_at:number;

    author: Radio | Artist | Media;
}