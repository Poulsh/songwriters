import { Subscription } from 'rxjs/Subscription';

export class FileUpload {
    data: File;
    state: string;
    inProgress: boolean;
    progress: number;
    canRetry: boolean;
    canCancel: boolean;
    sub?: Subscription;
}