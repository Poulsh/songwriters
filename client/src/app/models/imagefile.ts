export class Imagefile {

    public static REPLACE_NOIMAGE_RADIO = 'noimage_radio.jpg';
    public static  REPLACE_NOIMAGE_ARTIST = 'noimage_artist.jpg';
    public static  REPLACE_NOIMAGE_SONG = 'noimage_song.jpg';

    id: number;
    name: string;
}