export class Language {
    id: number;
    name_orig: string;
    name_ru: string;
    name_eng: string;
    name_fr: string;
}