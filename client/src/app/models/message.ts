import {User} from "./user";
export class Message {

    public static STATUS_NOT_SEEN = 'not_seen';
    public static STATUS_SEEN = 'seen';
    public static FROM_TO_ARTIST_TO_SYSTEM = 'artist_to_system';
    public static FROM_TO_SYSTEM_TO_ARTIST = 'system_to_artist';


    id: number;
    to_id: number;
    from_id: number;
    from_to_type: string;
    dialog_id: number;
    text: string;
    attachment_type: string;
    attachment_id: number;
    status: string;
    author_id: number;
    author: User;

    created_at: number;
    updated_at: number;



    //
    // public static  SYSTEM_MESSAGES  = {
    //     price_offer_sent_name: 'Предложение отправлено!',
    //     price_offer_sent_text: 'Предложение цены отправлено продавцу. <br> Решение продавца будет получено в диалоге <br> по данному товару.',
    // };



}