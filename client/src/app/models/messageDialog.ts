
import {User} from "./user";
import {Message} from "./message";
export class MessageDialog {

    public static SUBJECT_TYPE_ITEM_IN_RADIO_SECTION = 'item_in_radio_section';
    public static USER_TYPE_ARTIST_IN_RADIO_SECTION = 'artist_in_radio_section';

    public static SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION = 'item_in_premium_section';
    public static USER_TYPE_ARTIST_IN_PREMIUM_SECTION = 'artist_in_premium_section';



    id: number;
    type: string;
    artist_id: number;
    radio_id: number;
    media_id: number;
    subject_type: string;
    subject_id: string;
    name: string;
    status: string;
    author_id: number;


    messages: Message[];
    created_at: number;
    updated_at: number;


}