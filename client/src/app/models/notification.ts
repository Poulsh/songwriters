import {User} from "./user";
export class Notification {


    public static EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE = 'radioSectionItemStatusChange';
    public static EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE = 'premiumSectionItemStatusChange';

    public static EVENT_RADIO_SECTION_ITEM_NEW_MESSAGE = 'radio_section_item_new_message';
    public static EVENT_PREMIUM_SECTION_ITEM_NEW_MESSAGE = 'premium_section_item_new_message';



    public static STATUS_NOT_SEEN = 'not_seen';
    public static STATUS_CLOSED = 'closed';

    public static CALL_2_ACTION_NO_BUTTON = 'click_on_item';



    id: number;


}