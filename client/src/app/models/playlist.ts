import {Song} from "./song";
import {PlaylistItem} from "./playlistItem";

export class Playlist {





    id: number;
    type: string;
    name: string;
    object_type: string;
    object_id: number;
    author_id: number;
    items: PlaylistItem [];
    items_artist_home: PlaylistItem [];
    // items_cut: [{
    //     id:number,
    //     status:string,
    //     song:{
    //         id:number,
    //         name:string,
    //         radio_status:string,
    //         premium_ru_status:string,
    //         premium_world_status:string,
    //     },
    //     dialog:{
    //         id:number,
    //         messages:any[],   // unseen to user
    //     }
    // }];


    items_qnt:number;
    status: string;
    view: string;
    created_at: number;
    updated_at: number;
}