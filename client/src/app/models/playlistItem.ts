import {Song} from "./song";


export class PlaylistItem {

    public static STATUS_TYPE_SONG_IN_RADIO_SECTION = 'song_in_radio_section';
    public static STATUS_TYPE_SONG_IN_PREMIUM_SECTION = 'song_in_premium_section';
    public static STATUS_TYPE_SERVICE_TYPE_IN_PREMIUM_SECTION = 'service_type_in_premium_section';


    public static STATUS_ACTIVE = 'active';
    public static STATUS_INIT = 'init';
    public static STATUS_APPROVED = 'approved';
    public static STATUS_DECLINED = 'declined';
    public static STATUS_EXPIRED = 'expired';


    public static PRICE_RADIO_ALL_TRACKS = '1490';
    public static PRICE_RADIO_SELECTED_RU = '3990';
    public static PRICE_RADIO_SELECTED_WORLD = '5990';

    public static SERVICE_TYPE_RADIO_SELECTED_RU = 'radio_selected_ru';
    public static SERVICE_TYPE_RADIO_SELECTED_WORLD = 'radio_selected_world';
    public static SERVICE_TYPE_RADIO = 'radio';





        id: number;
        order_num: number;
        status: string;
        service_type: string;
        song_id: number;
        rating: number;
        plays: number;
        downloads: number;
        song: Song;
        song_name: string;
        dialog_id: number;
        till_to: number;



        messages_qnt:{
            qnt:number;
            not_seen:number;
        }

        playIter:number;  // итератор проигрывания для запуска трека в wavesurfer-player, живет только во фронте

    public static PAYMENT_INFO = 'Карта хххх хххх хххх хххх , платеж необходимо отправить с комментарием #';
}