import {User} from "./user";
export class Radio {
    id: number;
    user_id: number;
    user: User;
    page_id: number;
    name: string;
    country: string;
    city: string;
    hrurl: string;
    main_info: string;
    comment: string;
    image: string;
    image_alt: string;
    status: string;

}