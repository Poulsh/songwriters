import {environment} from "../../environments/environment";

export class Song {

    public static STATUS_UNDONE = 'undone';
    public static STATUS_INIT = 'init';
    public static STATUS_ACTIVE = 'active';
    public static STATUS_EXPIRED = 'expired';

    public static SERVICE_URL_CODE_ALL_TRACKS = 100;
    public static SERVICE_URL_CODE_SELECTED_RU = 101;
    public static SERVICE_URL_CODE_SELECTED_WORLD = 102;

    id: number;
    name: string;
    name_hash: string;
    hrurl: string;
    artist_name: string;
    artist_id: number;
    rightholders: string;
    music_style1_id: number;
    music_style2_id: number;
    music_style3_id: number;
    music_style1: {
        id:number,
        name:string,
    };
    music_style2: {
        id:number,
        name:string,
    };
    music_style3: {
        id:number,
        name:string,
    };
    page_id: number;
    vocal: boolean;
    language: any[];
    languages: string;
    lang_obj: [{
            id:number,
            name:string,
        }];
    label: string;
    isrc: string;
    release_date: number;
    author_comment: string;
    hide_reviews: boolean;
    adult_text: boolean;
    path_to_file: string;
    info: string;
    image: string;
    image_alt: string;
    background_image: string;
    peaks: string;
    status: string;
    view: string;
    radio_status: string;
    premium_ru_status: string;
    premium_world_status: string;
    created_at: number;
    updated_at: number;
    publication_agreed: boolean;


}