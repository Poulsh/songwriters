export class User {
    public static TYPE_RADIO = 'radio';
    public static TYPE_ARTIST = 'artist';
    public static TYPE_MEDIA = 'media';

    id: number;
    email: string;
    first_name: string;
    last_name: string;
    access_token:string;
    avatar:string;
    roles: string[];
    password: string;

}