import { Component, OnInit, OnDestroy  } from '@angular/core';
import { NotificationService } from "../notification.service";
// import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import {takeUntil} from "rxjs/internal/operators/takeUntil";
import {Subject} from "rxjs";
import {UserCheckSum} from "../../models/userCheckSum";
import {IWsMessage, WebsocketService} from "../../websocket";
import {SharedService} from "../../shared/shared.service";
import {Notification} from "../../models/notification";

@Component({
  selector: 'app-notification-dropdown',
  templateUrl: './notification-dropdown.component.html',
  styleUrls: ['./notification-dropdown.component.scss']
})
export class NotificationDropdownComponent implements OnInit {

  public notifications;
  public userCheckSum:UserCheckSum;
  public cartQnt;
  public loading:boolean = false;
  private unSubscribe = new Subject();
  public call2ActionNoButton = Notification.CALL_2_ACTION_NO_BUTTON;

  constructor(
      private notificationService: NotificationService,
      // public ngbDropdownConfig: NgbDropdownConfig,
      private wsService: WebsocketService,
      private sharedService: SharedService,

  ) {


    sharedService.userCheckSumFromWebSocketEvent
      .subscribe(
          (checkSum)=>{
              if (this.userCheckSum.notifyQnt != checkSum.notifyQnt) {
                  // console.log('ща прям getUpdates()');
                  this.getFeed();
              }
          }
      );

    notificationService.notifyUserQntEvent
        .pipe(takeUntil(this.unSubscribe))
        .subscribe(
            (result)=>{
              if (result) {
                  this.userCheckSum = result;
                // console.log('NotificationDropdownComponent получил обновление от notificationService.notifyUserQntEvent');
                // console.log(result);
              }
            }
        );


  }

  ngOnInit() {
    this.getFeed();
  }

  getFeed():void {

    this.notificationService.getUpdates()
        .subscribe( response => {
          if (response && !response.error) {
            this.process(response);
          }
        });
  }

  process(response):void{
    if (response.notifications) {
      this.notifications = response.notifications;
    }
    if (response.userCheckSum ) {
      this.userCheckSum = response.userCheckSum
    }

  }

  markAsSeen(id){
    this.notificationService.markAsSeen(id)
        .subscribe( response => {
          if (!response.error) {
            this.notifications.forEach( (item, index) => {
              if(item.id == id) this.notifications.splice(index,1);
            });
          }

        });
  }

  markAsSeenAll(){
    this.notificationService.markAsSeenAll()
        .subscribe( response => {
          if (!response.error) {
            this.notifications = [];
          }
        });
  }

  ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }
}
