import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AlertComponent } from '../alert/alert.component';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserCheckSum} from "../models/userCheckSum";
import { Router } from '@angular/router';
import { AuthService} from "../auth/auth.service";
import { Observable } from 'rxjs';
import {SharedService} from "../shared/shared.service";





const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  public notifyUserQntEvent: EventEmitter<UserCheckSum> = new EventEmitter();

  public userCheckSum: UserCheckSum = new UserCheckSum;


  constructor(
      private http: HttpClient,
      private router: Router,
      public dialog: MatDialog,
      public snackBar: MatSnackBar,
      private authService: AuthService,
  ) {

  }



  getUpdates(): Observable<any> {
    if (!this.authService.user ){
      return Observable.of(false);
    } else {
      let body:{[key: string]: any}={};
      body.userCheckSum = this.userCheckSum;
      return this.http.post<any>(environment.apiUrl+'notification/index', body, httpOptions)
          .pipe(
              map(data => this.process(data))
          );
    }
  }


  markAsSeen(id): Observable<any> {
    const body = JSON.stringify({id:id});
    return this.http.post<any>(environment.apiUrl+'notification/mark-as-seen/'+id, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  markAsSeenAll(): Observable<any> {
    const body = {};
    return this.http.post<any>(environment.apiUrl+'notification/mark-as-seen-all', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }

    this.emitNotifications(response);
    return response;
  }

  private  emitNotifications(res):void {
    if (res.userCheckSum) {
      this.userCheckSum = res.userCheckSum;
      this.notifyUserQntEvent.emit(res.userCheckSum);
    }
  }





}
