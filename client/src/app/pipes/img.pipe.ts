import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'img'
})
export class ImgPipe implements PipeTransform {
  // sm lg
  transform(name: string, key: string): string {

    let $nameParts = [];
    $nameParts = name.split('.');
    if (key == 'sm') {
      return $nameParts[0]+'_sm.'+$nameParts[1];
    }
    if (key == 'md') {
      return $nameParts[0]+'_md.'+$nameParts[1];
    }
    if (key == 'lg') {
      return $nameParts[0]+'_lg.'+$nameParts[1];
    }
    return null;
  }

}
