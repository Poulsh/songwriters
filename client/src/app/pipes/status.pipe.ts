import {Inject, LOCALE_ID, Pipe, PipeTransform} from '@angular/core';
import {Playlist} from "../models/playlist";
import {PlaylistItem} from "../models/playlistItem";


@Pipe({
  name: 'status'
})

export class StatusPipe implements PipeTransform  {

  constructor(
    @Inject(LOCALE_ID) private locale: string,
  ){}

  transform(name: string, type:string): string {

    if (type == PlaylistItem.STATUS_TYPE_SONG_IN_RADIO_SECTION) {
      if (name == PlaylistItem.STATUS_INIT) return this.locale=='ru'? 'Ожидает оплаты':'Awaiting payment';
      if (name == PlaylistItem.STATUS_ACTIVE) return this.locale=='ru'? 'Активирован':'Active';
      if (name == PlaylistItem.STATUS_DECLINED) return this.locale=='ru'?'Отклонен':'Rejected';
      if (name == PlaylistItem.STATUS_EXPIRED) return this.locale=='ru'?'Истек период действия':'Expired';

    }

    else if (type == PlaylistItem.STATUS_TYPE_SONG_IN_PREMIUM_SECTION) {
      if (name == PlaylistItem.STATUS_INIT) return this.locale=='ru'?'На рассмотрении':'On approval';
      if (name == PlaylistItem.STATUS_ACTIVE) return this.locale=='ru'?'Активирован':'Active';
      if (name == PlaylistItem.STATUS_DECLINED) return this.locale=='ru'?'Отклонен':'Rejected';
      if (name == PlaylistItem.STATUS_EXPIRED) return  this.locale=='ru'?'Истек период действия':'Expired';
      if (name == PlaylistItem.STATUS_APPROVED) return this.locale=='ru'? 'Ожидает оплаты':'Awaiting payment';
    }

    else if (type == PlaylistItem.STATUS_TYPE_SERVICE_TYPE_IN_PREMIUM_SECTION) {
      if (name == PlaylistItem.SERVICE_TYPE_RADIO_SELECTED_RU) return this.locale=='ru'? 'СНГ':'Russia';
      if (name == PlaylistItem.SERVICE_TYPE_RADIO_SELECTED_WORLD) return 'World';
    }


    return 'тип статуса неопределен';

  }

}
