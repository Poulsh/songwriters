import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioAllTracksComponent } from './radio-all-tracks.component';

describe('RadioAllTracksComponent', () => {
  let component: RadioAllTracksComponent;
  let fixture: ComponentFixture<RadioAllTracksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioAllTracksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioAllTracksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
