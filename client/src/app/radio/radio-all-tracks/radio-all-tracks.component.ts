import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import { MatDialog } from "@angular/material/dialog";
import {RadioService} from "../radio.service";
import {environment} from "../../../environments/environment";
import {Radio} from "../../models/radio";
import {Language} from "../../models/language";
import {MusicStyle} from "../../models/musicStyle";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import {Playlist} from "../../models/playlist";
import {PlaylistItem} from "../../models/playlistItem";


@Component({
  selector: 'app-radio-all-tracks',
  templateUrl: './radio-all-tracks.component.html',
  styleUrls: ['./radio-all-tracks.component.scss']
})
export class RadioAllTracksComponent implements OnInit {

  public radio: Radio;
  public apiUrl = environment.apiUrl;


  public isFiltered: boolean;
  public filterParams:{[key: string]: any};
  public filterLanguages: Language[];
  public filterVocal:boolean;
  public filterAdultText:boolean;
  public filterMusicStyles:MusicStyle[];

  public playlists:Playlist[] = [];


  public items: PlaylistItem[];

  public nowPlaying: string;

  public totalItems:number;
  public pageSize:number;
  public currentPage:number;

  constructor(
      private route: ActivatedRoute,
      private radioService: RadioService,
      private location: Location,
      private authService: AuthService,
      public dialog: MatDialog,
      @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_RADIO;
    this.getRadio();
    this.getMyPlaylists();
    this.getFeed({});

  }


  getRadio(force=false): void {
    if (!force && this.radioService.radio){
      this.radio = this.radioService.radio;
    } else {
      this.radioService.getRadio()
        .subscribe( response => {
          this.radio = response.radio;
        });
    }
  }


  getFeed(params): void {
    if (!params) {
      params = {};
    }
    if (!params.page) {
      params.page = 1;
    }


    this.radioService.getAllTracksFeed(params)
        .subscribe( response => {
          if (response.feed){
            this.items = response.feed;
          }
          if (response.filter ) {
            this.populateFilterData(response);
          }
          if (response.pagination) {
            this.totalItems = response.pagination.total;
            this.pageSize = response.pagination.pageSize;
            this.currentPage = response.pagination.currentPage;
          }
        });
  }





  populateFilterData(response){
    this.isFiltered = response.filter.isFiltered;
    this.filterLanguages = response.filter.languages;
    this.filterMusicStyles = response.filter.music_styles;
    this.filterVocal = response.filter.vocal;
    this.filterAdultText = response.filter.adult_text;
  }

  filterChange(params){
    this.filterParams = params;
    this.getFeed(this.filterParams);
  }

  resetFilter(){
    this.getFeed({page:1});
  }

  createPlaylist(){
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: this.locale=='ru'?'Создание плейлиста':'Create playlist',
        message: this.locale=='ru'?'Введите название нового плейлиста':'Enter name of new playlist',
        yesBtn: this.locale=='ru'?'Создать':'Create',
        value: true,
        textInputPlaceholder:this.locale=='ru'?'Название':'Name',
        settings:{}
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.radioService.createPlaylist(result)
            .subscribe( response => {
              if (response.success && response.playlist) {
                this.playlists.push(response.playlist);
              }
            });
      }
    });
  }

  addIoPlaylist(songId,playlistId){
    this.radioService.addSongToPlaylist(songId,playlistId)
        .subscribe( response => {
        });
  }

  getMyPlaylists(){
    this.radioService.getMyPlaylists()
        .subscribe( response => {
          if (response.playlists) {
            this.playlists=response.playlists;
          }
        });
  }


  onTrackPlayFinish(index){
    if (this.items[index+1]){
      if (!this.items[index+1].playIter) {
        this.items[index+1].playIter = 1;
      } else {
        this.items[index+1].playIter++;
      }
    }
  }



  paginate(data):void {
    let params:{[key: string]: any}={};
    if (this.filterParams) {
      params = this.filterParams;
    }
    params.page = data.pageIndex+1;
    params.pageSize = data.pageSize;

    this.currentPage = params.page;
    this.pageSize = params.pageSize;
    this.getFeed(params);
  }

}
