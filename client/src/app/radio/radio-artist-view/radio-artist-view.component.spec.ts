import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioArtistViewComponent } from './radio-artist-view.component';

describe('RadioArtistViewComponent', () => {
  let component: RadioArtistViewComponent;
  let fixture: ComponentFixture<RadioArtistViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioArtistViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioArtistViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
