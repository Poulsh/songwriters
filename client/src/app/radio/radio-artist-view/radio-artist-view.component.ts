import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {RadioService} from "../radio.service";
import {AuthService} from "../../auth/auth.service";
import {User} from "../../models/user";
import {Artist} from "../../models/artist";
import {environment} from "../../../environments/environment";
import {ImgPipe} from "../../pipes/img.pipe";

@Component({
  selector: 'app-radio-artist-view',
  templateUrl: './radio-artist-view.component.html',
  styleUrls: ['./radio-artist-view.component.scss'],
  providers: [ ImgPipe ]
})
export class RadioArtistViewComponent implements OnInit {

  public artist:Artist;
  public apiUrl = environment.apiUrl;
  public downloadImageName:string;


  constructor(
    private route: ActivatedRoute,
    private radioService: RadioService,
    private authService: AuthService,
    private imgPipe: ImgPipe,
  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_RADIO;
    this.getArtist();
  }

  getArtist(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.radioService.getArtist(id)
      .subscribe( response => {
        this.artist = response.artist;
        this.downloadImageName = this.imgPipe.transform(response.artist.image,'lg');

      });
  }

}
