import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioEditPersonalInfoBlockComponent } from './radio-edit-personal-info-block.component';

describe('RadioEditPersonalInfoBlockComponent', () => {
  let component: RadioEditPersonalInfoBlockComponent;
  let fixture: ComponentFixture<RadioEditPersonalInfoBlockComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioEditPersonalInfoBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioEditPersonalInfoBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
