import {Component, EventEmitter, Inject, Input, LOCALE_ID, OnInit, Output} from '@angular/core';
import {Radio} from "../../models/radio";
import {environment} from "../../../environments/environment";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RadioService} from "../radio.service";

@Component({
  selector: 'app-radio-edit-personal-info-block',
  templateUrl: './radio-edit-personal-info-block.component.html',
  styleUrls: ['./radio-edit-personal-info-block.component.scss']
})
export class RadioEditPersonalInfoBlockComponent implements OnInit {

  public _radio:Radio;

  @Input() set radio(event){
    if (event){
      this._radio = event;
      this.addTimestampToAvatar();
    }
  };

  @Output() onUpdate = new EventEmitter<any>();

  public radioInfoForm: FormGroup;

  public apiUrl = environment.apiUrl;

  public showInfoForm:boolean = false;
  public showEditButtons:boolean = false;

  constructor(
    private radioService: RadioService,
    private fb: FormBuilder,
    @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit(): void {
    this.initForm();
    this.infoToForm(this._radio);
  }


  initForm(){
    this.radioInfoForm = this.fb.group({
      name: ['', [
        Validators.required,
      ]],
      country: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      city: ['', []],
      main_info: ['', []]
    });
  }

  get rif() {return this.radioInfoForm.controls;}

  constructInfoForm(){
    this.showInfoForm = true;
    this.showEditButtons = false;
  }


  // triggered by complete avatar file-upload
  onAvatarUploadComplete(data: any) {
    this.onUpdate.emit(true);
    this.showEditButtons = false;
  }


  processResponse(response):void {

    if (response.radio) {
      this._radio = response.radio;
      this.addTimestampToAvatar();
      this.infoToForm(this._radio);
    }
    if (response.success){
      this.showInfoForm = false;
      this.showEditButtons = false;
    }
  }

  // для моментального обновления при загрузке новой картинки
  addTimestampToAvatar():void{
    let timeStamp = (new Date()).getTime();
    this._radio.image = this._radio.image?this._radio.image+'?'+timeStamp:null;
  }

  infoToForm(radio:Radio){
    this.rif.name.setValue(radio.name);
    this.rif.country.setValue(radio.country);
    this.rif.city.setValue(radio.city);
    this.rif.main_info.setValue(radio.main_info);
  }


  saveInfo():void {
    let info = {
      name:this.rif.name.value,
      country:this.rif.country.value,
      city:this.rif.city.value,
      main_info:this.rif.main_info.value,
    };
    this.radioService.updateInfo(info)
      .subscribe( response => {
        this.processResponse(response);
      });
  }
}
