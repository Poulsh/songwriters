import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioPlaylistIndexComponent } from './radio-playlist-index.component';

describe('RadioPlaylistIndexComponent', () => {
  let component: RadioPlaylistIndexComponent;
  let fixture: ComponentFixture<RadioPlaylistIndexComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioPlaylistIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioPlaylistIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
