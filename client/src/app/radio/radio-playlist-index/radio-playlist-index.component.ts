import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {RadioService} from "../radio.service";
import { MatDialog } from "@angular/material/dialog";
import {Playlist} from "../../models/playlist";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";

@Component({
  selector: 'app-radio-playlist-index',
  templateUrl: './radio-playlist-index.component.html',
  styleUrls: ['./radio-playlist-index.component.scss']
})
export class RadioPlaylistIndexComponent implements OnInit {

  public playlists:Playlist[] = [];


  constructor(
      private radioService: RadioService,
      public dialog: MatDialog,
      @Inject(LOCALE_ID) public locale: string,
  ) { }

  ngOnInit() {
    this.getMyPlaylists();
  }

  getMyPlaylists(){
    this.radioService.getMyPlaylists()
        .subscribe( response => {
          if (response.playlists) {
            this.playlists=response.playlists;
          }
        });
  }


  createPlaylist(){
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: this.locale=='ru'?'Создание плейлиста':'Create playlist',
        message: this.locale=='ru'?'Введите название нового плейлиста':'Enter name of a new playlist',
        yesBtn: this.locale=='ru'?'Создать':'Create',
        value: true,
        textInputPlaceholder:this.locale=='ru'?'Название':'Name',
        settings:{}
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.radioService.createPlaylist(result)
            .subscribe( response => {
              if (response.success && response.playlist) {
                this.playlists.push(response.playlist);
              }
            });
      }
    });
  }
}
