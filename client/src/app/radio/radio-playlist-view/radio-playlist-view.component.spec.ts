import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioPlaylistViewComponent } from './radio-playlist-view.component';

describe('RadioPlaylistViewComponent', () => {
  let component: RadioPlaylistViewComponent;
  let fixture: ComponentFixture<RadioPlaylistViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioPlaylistViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioPlaylistViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
