import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {Playlist} from "../../models/playlist";
import {ActivatedRoute, Router} from "@angular/router";
import {RadioService} from "../radio.service";
import { MatDialog } from "@angular/material/dialog";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";



@Component({
  selector: 'app-radio-playlist-view',
  templateUrl: './radio-playlist-view.component.html',
  styleUrls: ['./radio-playlist-view.component.scss']
})
export class RadioPlaylistViewComponent implements OnInit {

  public playlist: Playlist;
  public playlists:Playlist[] = [];

  public form: FormGroup;
  public showForm: boolean = false;

  public nowPlaying: string;


  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private radioService: RadioService,
      public dialog: MatDialog,
      private fb: FormBuilder,
      @Inject(LOCALE_ID) public locale: string,

  ) { }

  ngOnInit() {
    this.initForm();
    this.getPlaylist();
    this.getMyPlaylists();
  }

  initForm() {
    this.form = this.fb.group({
      name: ['', [Validators.required,]],
    });
  };

  get f() {return this.form.controls;}

  getPlaylist():void{
    const id = this.route.snapshot.paramMap.get('id');
    this.radioService.getMyPlaylist(id)
        .subscribe( response => {
          this.playlist = response.playlist;
          this.f.name.setValue(this.playlist.name);
        });

  }

  removeItem(itemId){
      let settings = {};
      const dialogRef = this.dialog.open(ConfirmDialog, {
          data: {
              head: this.locale=='ru'?'Обратите внимание!':'',
              message:  this.locale=='ru'?'Точно удалить?':'Delete item?',
              yesBtn: this.locale=='ru'?'Удалить':'Delete',
              noBtn: this.locale=='ru'?'Не удалять':"Close",
              value: true,
              settings:settings
          }
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result) {
              this.radioService.removeSongFromPlaylist(itemId,this.playlist.id)
                  .subscribe( response => {
                      if(response.success){
                          this.getPlaylist();
                      }
                  });
          }
      });
  }


    deletePlaylist(){
      let settings = {};
      const dialogRef = this.dialog.open(ConfirmDialog, {
          data: {
              head: this.locale=='ru'?'Обратите внимание!':'',
              message:  this.locale=='ru'?'Точно удалить плейлист?':'Delete playlist?',
              yesBtn:  this.locale=='ru'?'Удалить':'Delete',
              noBtn: this.locale=='ru'?'Не удалять':"Close",
              value: true,
              settings:settings
          }
      });

      dialogRef.afterClosed().subscribe(result => {
          if (result) {
              this.radioService.deletePlaylist(this.playlist.id)
                  .subscribe( response => {
                      if(response.success){
                          this.router.navigate(['/radio-playlist']);
                      }
                  });
          }
      });
  }



    addSongToPlaylist(songId,playlistId){
        this.radioService.addSongToPlaylist(songId,playlistId)
            .subscribe( response => {
            });
    }

    moveItemToPlaylist(songId,toPlaylistId){
        this.radioService.moveSongToPlaylist(songId,toPlaylistId,this.playlist.id)
            .subscribe( response => {
                if (response.success){
                    this.getPlaylist();
                }
            });
    }

    getMyPlaylists(){
        this.radioService.getMyPlaylists()
            .subscribe( response => {
                if (response.playlists) {
                    this.playlists=response.playlists;
                }
            });
    }


  editPlaylist(){
      let playlist = {
          name:this.f.name.value,
        };
        this.radioService.editPlaylist(playlist,this.playlist.id)
          .subscribe( response => {
            if (response.success) {
              this.playlist = response.playlist;
              this.showForm = false;
            }
          });
  }


  onTrackPlayFinish(index){
    if (this.playlist.items[index+1]){
      if (!this.playlist.items[index+1].playIter) {
        this.playlist.items[index+1].playIter = 1;
      } else {
        this.playlist.items[index+1].playIter++;
      }
    }
  }

}
