import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioSelectedTracksComponent } from './radio-selected-tracks.component';

describe('RadioSelectedTracksComponent', () => {
  let component: RadioSelectedTracksComponent;
  let fixture: ComponentFixture<RadioSelectedTracksComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioSelectedTracksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioSelectedTracksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
