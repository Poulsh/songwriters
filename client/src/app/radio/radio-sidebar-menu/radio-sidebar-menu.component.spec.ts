import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioSidebarMenuComponent } from './radio-sidebar-menu.component';

describe('RadioSidebarMenuComponent', () => {
  let component: RadioSidebarMenuComponent;
  let fixture: ComponentFixture<RadioSidebarMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioSidebarMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioSidebarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
