import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioSongFilterComponent } from './radio-song-filter.component';

describe('RadioSongFilterComponent', () => {
  let component: RadioSongFilterComponent;
  let fixture: ComponentFixture<RadioSongFilterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioSongFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioSongFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
