import { Component, OnInit, Input, Output } from '@angular/core';
import {
  FormsModule,
  FormGroup,
  FormBuilder,
  Validators,
  FormArray } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import {MusicStyle} from "../../models/musicStyle";
import {Language} from "../../models/language";


const STATUS_TO_BE_UPDATED = 10;
const STATUS_TO_RESET = 11;
const STATUS_DO_NOT_UPDATE = 12;
const STATUS_TO_BE_RECONSTRUCTED_SAVE_SELECTED = 14;

@Component({
  selector: 'app-radio-song-filter',
  templateUrl: './radio-song-filter.component.html',
  styleUrls: ['./radio-song-filter.component.scss']
})
export class RadioSongFilterComponent implements OnInit {

  @Input() isFiltered: boolean;
  @Output() resetFilterChange = new EventEmitter<boolean>();
  @Output() filterFormChange = new EventEmitter<any>();

  @Input()
  set languagesSetter( val ) {
    if (this.filterForm) {
      let _lf = this.f.languages as FormArray;
      // if (this.languagesStatus == STATUS_TO_BE_UPDATED) {
      //   this.updateFormNumbers(val,_bf);
      // }
      if (this.languagesStatus == STATUS_TO_RESET) {
        this.languages = val;
        this.allLanguages = true;
        this.initFalseForm(val,_lf);
        this.languagesStatus = STATUS_TO_BE_UPDATED;
      }
      if (this.languagesStatus == STATUS_TO_BE_RECONSTRUCTED_SAVE_SELECTED) {
        this.updateFormSaveSelected(val,_lf);
        this.languagesStatus = STATUS_TO_BE_UPDATED;
      }
    }
  }

  @Input()
  set musicStylesSetter( val ) {
    if (this.filterForm) {
      if (this.musicStylesStatus == STATUS_TO_BE_UPDATED) {
        let _options = [];
        for(let style of this.musicStyles){
          for(let newVal of val) {
            if (newVal.id == style.id){
              _options.push({
                  id:style.id,
                  name:style.name,
                  value:true,
                });
            }
          }
            // this.musicStyleOptions.push({
          //   id:style.id,
          //   name:style.name,
          //   value:false,
          // });

          // if (oVal && oVal.id == item.id ) {
          //   if (oVal.value == true) {
          //     formArray.push(this.createNameValIdForm(item.name,oVal.value,item.id));
          //     found=true;
          //   } else {
          //     formArray.push(this.createNameValIdForm(item.name,false,item.id));
          //     found=true;
          //   }
          // }
        }

        this.musicStyleOptions = _options;
        this.f.musicStyles.setValue(_options);
      }
      if (this.musicStylesStatus == STATUS_TO_RESET) {
        this.musicStyles = val;

        for(let style of this.musicStyles){
          this.musicStyleOptions.push({
            id:style.id,
            name:style.name,
            value:false,
          });
        }

        this.allMusicStyles = true;
        this.musicStylesStatus = STATUS_TO_BE_UPDATED;
      }

    }
  }



  public isFilterDirty:boolean = false;
  public filterForm:FormGroup;


  public allMusicStyles:boolean = true;
  public musicStyles: MusicStyle[];
  public musicStyleOptions: any[] = [];

  public allLanguages:boolean = true;
  public languages: Language[];

  public isVocal:boolean = false;
  public isNotVocal:boolean = false;
  public isAdult:boolean = false;
  public isNotAdult:boolean = false;

  public musicStylesStatus:number = STATUS_TO_RESET;
  public languagesStatus:number = STATUS_TO_RESET;
  public isVocalStatus:number = STATUS_TO_RESET;


  constructor(
      private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.filterForm = this.fb.group({
      musicStyles:[''],
      languages:this.fb.array([]),
      sort:[''],
    });
  }

  get f() {return this.filterForm.controls;}

  initFalseForm(values:any,formArray:FormArray):void {
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
    for (let item of values){
      formArray.push(this.createNameValIdForm(item.name,false,item.id));
    }
  }

  createNameValIdForm(name,value,id):FormGroup {
    return this.fb.group({
      name:name,
      value:value,
      id:id,
    })
  }

  updateFormSaveSelected(values:any,formArray:FormArray):void{
    let $oldValues = formArray.value;
    while (formArray.length !== 0) {
      formArray.removeAt(0)
    }
    for (let item of values){
      let found:boolean = false;
      for(let oVal of $oldValues){
        if (oVal && oVal.id == item.id ) {
          if (oVal.value == true) {
            formArray.push(this.createNameValIdForm(item.name,oVal.value,item.id));
            found=true;
          } else {
            formArray.push(this.createNameValIdForm(item.name,false,item.id));
            found=true;
          }
        }
      }
      if (!found) {
        formArray.push(this.createNameValIdForm(item.name,false,item.id));
      }
    }
  }

  resetFilter(){
    this.isFilterDirty=false;
    this.resetLanguagesForm();
    this.resetMusicStyleControl();
    this.isVocal = false;
    this.isNotVocal = false;
    this.isAdult = false;
    this.isNotAdult = false;

    this.resetFilterChange.emit(true);
  }

  resetLanguagesForm(){
    this.allLanguages = true;
    this.languagesStatus = STATUS_TO_RESET;
    let _f = this.filterForm.get('languages') as FormArray;
    for (var i = 0; i < _f.controls.length; i++) {
      let _control = _f.at(i);
      _control.patchValue({value:false},{emitEvent: false});
    }
  }

  resetMusicStyleControl(){
    this.allMusicStyles = true;
    this.musicStylesStatus = STATUS_TO_RESET;
    this.f.musicStyles.setValue([]);
  }


  updateFilter(type,index,event){
    if (type!='sort') {
      this.isFilterDirty=true;
    }

    if (type == 'allLanguages') {
      this.resetLanguagesForm();
    }
    if (type == 'language') {
      this.allLanguages = false;
      this.musicStylesStatus = STATUS_TO_BE_RECONSTRUCTED_SAVE_SELECTED;
      this.languagesStatus = STATUS_DO_NOT_UPDATE;
      let _lf = this.filterForm.get('languages') as FormArray;
      let _control = _lf.at(index);
      _control.patchValue({value:event.checked},{emitEvent: false});
    }




    if (type == 'musicStyle') {
      this.musicStylesStatus = STATUS_DO_NOT_UPDATE;
    }

    if (type == 'isVocal') {
      this.isVocal = event.checked;
      if (this.isVocal) {
        this.isNotVocal = false;
      }
      this.musicStylesStatus = STATUS_DO_NOT_UPDATE;
    }
    if (type == 'isNotVocal') {
      this.isNotVocal = event.checked;
      if (this.isNotVocal) {
        this.isVocal = false;
      }
      this.musicStylesStatus = STATUS_DO_NOT_UPDATE;
    }
    if (type == 'isAdult') {
      this.isAdult = event.checked;
      if (this.isAdult) {
        this.isNotAdult = false;
      }
      this.musicStylesStatus = STATUS_DO_NOT_UPDATE;
    }
    if (type == 'isNotAdult') {
      this.isNotAdult = event.checked;
      if (this.isNotAdult) {
        this.isAdult = false;
      }
      this.musicStylesStatus = STATUS_DO_NOT_UPDATE;
    }



    let data:{[key: string]: any} = {};



    if (this.f.sort.value) {
      data.sort = this.f.sort.value;
    }

    if (this.isVocal) {
      data.is_vocal = this.isVocal;
    }
    if (this.isNotVocal) {
      data.is_not_vocal = this.isNotVocal;
    }
    if (this.isAdult) {
      data.is_adult = this.isAdult;
    }
    if (this.isNotAdult) {
      data.is_not_adult = this.isNotAdult;
    }


    for (let style of this.f.musicStyles.value) {
      if (!data.music_style_id) {
        data.music_style_id = [];
      }
      data.music_style_id.push(style.id);
    }


    for (let lang of this.f.languages.value) {
      if (lang.value && lang.value == true) {
        if (!data.language_id) {
          data.language_id = [];
        }
        data.language_id.push(lang.id);
      }
    }

    // console.log(data);
    this.filterFormChange.emit(data);
  }

}
