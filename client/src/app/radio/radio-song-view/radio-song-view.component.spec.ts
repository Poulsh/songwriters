import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { RadioSongViewComponent } from './radio-song-view.component';

describe('RadioSongViewComponent', () => {
  let component: RadioSongViewComponent;
  let fixture: ComponentFixture<RadioSongViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioSongViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioSongViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
