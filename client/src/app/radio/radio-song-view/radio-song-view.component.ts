import { Component, OnInit,   ElementRef, ViewChild, LOCALE_ID, Inject } from '@angular/core';
import {Song} from "../../models/song";
import {ActivatedRoute} from "@angular/router";
import {RadioService} from "../radio.service";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";
import {environment} from "../../../environments/environment";
import {Comment} from "../../models/comment";
import {SeoService} from "../../seo/seo.service";

@Component({
  selector: 'app-radio-song-view',
  templateUrl: './radio-song-view.component.html',
  styleUrls: ['./radio-song-view.component.scss']
})
export class RadioSongViewComponent implements OnInit {

  public song:Song;
  public apiUrl = environment.apiUrl;
  public isCommentFormOpen:boolean = false;
  public comments:Comment[];

  @ViewChild('oneFieldForm') oneFieldForm: ElementRef;

  private metaTitle: string = 'Songwriters - страница трека';
  private metaDescription: string = 'Послушать и скачать трек';

  constructor(
    private route: ActivatedRoute,
    private radioService: RadioService,
    private authService: AuthService,
    @Inject(LOCALE_ID) public locale: string,
    private seoService: SeoService,

  ) { }

  ngOnInit() {
    this.authService.activeRole = User.TYPE_ARTIST;
    this.getSong();
  }

  getSong(): void {
    const hash = this.route.snapshot.paramMap.get('hash');
    this.radioService.getSong(hash)
      .subscribe( response => {
        this.song = response.song;
        this.comments = response.comments;

        this.metaTitle = this.song?.artist_name +' - '+this.song?.name ;
        this.metaDescription = 'Послушать и скачать трек '+this.song?.name +' артиста '+ this.song?.artist_name;
        if (this.locale!='ru'){
          this.metaDescription = 'Listen and download track '+this.song?.name +' by artist '+ this.song?.artist_name;;
        }
        this.seoService.set(this.metaTitle,this.metaDescription);
      });
  }

  sendComment(text){
    this.radioService.commentSong(this.song.name_hash,text)
      .subscribe( response => {
        if (!response.error){
          this.getSong();
          this.isCommentFormOpen = false;
        }
      });
  }

  openCommentForm(){
    this.isCommentFormOpen=true;
    const oneFieldForm: Element = this.oneFieldForm.nativeElement;
    oneFieldForm.scrollIntoView({ behavior: 'smooth' });
  }


}
