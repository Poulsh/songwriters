import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {Artist} from "../models/artist";
import {Radio} from "../models/radio";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})

export class RadioService {

  public radio:Radio;

  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }


  public getRadio(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'radio/radio-home', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }


  public updateInfo(radio): Observable<any> {
    const body = JSON.stringify({radio});
    return this.http.post<Artist>(environment.apiUrl+'radio/update-my-info', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }



  public createPlaylist(name:string): Observable<any> {
    const body = JSON.stringify({name:name});
    return this.http.post<any>(environment.apiUrl+'radio/create-playlist', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getMyPlaylists(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'radio/get-my-playlists', body, httpOptions);
  }

  public getMyPlaylist(id): Observable<any> {
    const body = JSON.stringify({id:id});
    return this.http.post<any>(environment.apiUrl+'radio/get-my-playlist/'+id, body, httpOptions);
  }

  public addSongToPlaylist(songId,playlistId): Observable<any> {
    const body = JSON.stringify({song_id:songId,playlist_id:playlistId});
    return this.http.post<any>(environment.apiUrl+'radio/add-song-to-playlist', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public removeSongFromPlaylist(itemId,playlistId): Observable<any> {
    const body = JSON.stringify({item_id:itemId,playlist_id:playlistId});
    return this.http.post<any>(environment.apiUrl+'radio/remove-item-from-playlist', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public editPlaylist(item,playlistId): Observable<any> {
    const body = JSON.stringify(item);
    return this.http.post<any>(environment.apiUrl+'radio/edit-playlist/'+playlistId, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public deletePlaylist(playlistId): Observable<any> {
    const body = JSON.stringify({playlist_id:playlistId});
    return this.http.post<any>(environment.apiUrl+'radio/delete-playlist/'+playlistId, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public moveSongToPlaylist(songId,toPlaylistId,fromPlaylistId): Observable<any> {
    const body = JSON.stringify({
      song_id:songId,
      to_playlist_id:toPlaylistId,
      from_playlist_id:fromPlaylistId,
    });
    return this.http.post<any>(environment.apiUrl+'radio/move-song-to-playlist', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public getAllTracksFeed(params): Observable<any> {
    const body = JSON.stringify({params:params});
    return this.http.post<any>(environment.apiUrl+'radio/get-all-tracks-feed', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }



  public getSelectedTracksFeed(params): Observable<any> {
    const body = JSON.stringify({params:params});
    return this.http.post<any>(environment.apiUrl+'radio/get-selected-tracks-feed', body, httpOptions)
      .pipe(map(data => this.process(data)));
  }




  public getSong(hash): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'radio/get-song/'+hash, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }

  public commentSong(hash,text): Observable<any> {
    const body = JSON.stringify({text:text});
    return this.http.post<any>(environment.apiUrl+'radio/comment-song/'+hash, body, httpOptions)
      .pipe(map(data => this.process(data)));
  }



  public getArtist(id): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'radio/get-artist/'+id, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }




  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }
    if (response.radio) {
      this.radio = response.radio;
    }
    return response;
  }
}
