import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SeoSitemapComponent } from './seo-sitemap.component';

describe('SeoSitemapComponent', () => {
  let component: SeoSitemapComponent;
  let fixture: ComponentFixture<SeoSitemapComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SeoSitemapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeoSitemapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
