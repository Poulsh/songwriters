import { Component, OnInit } from '@angular/core';
import {SeoService} from "../seo.service";


@Component({
  selector: 'app-seo-sitemap',
  templateUrl: './seo-sitemap.component.html',
  styleUrls: ['./seo-sitemap.component.scss']
})
export class SeoSitemapComponent implements OnInit {

  public sitemap;

  constructor(
    private seoService: SeoService,

  ) { }

  ngOnInit() {
    this.getData();

  }

  getData(): void {

    this.seoService.getSitemap()
      .subscribe( response => {
        this.sitemap = response;
        // console.log('this.seoService.getSitemap()',response);
      });
  }
}
