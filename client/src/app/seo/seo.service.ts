import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";


const headers = new HttpHeaders({ 'Content-Type': 'text/xml' }).set('Accept', 'text/xml');



@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(
    public meta: Meta,
    public title: Title,
    private http: HttpClient
  ) { }

  set(title,description):void{
    this.meta.updateTag({ name: 'description', content: description });
    this.title.setTitle(title);
  }

  getSitemap(): Observable<any> {
    return this.http.get(environment.apiUrl+'sitemap/sitemap', { headers: headers, responseType: 'text' });
  }


  // getSitemap(): Observable<any> {
  //   return this.http.get(environment.apiUrl+'sitemap/sitemap-json')
  //     // .flatMap(res => {console.log(res)})
  //     // .pipe(map((res:Response)=>res))
  //
  //     ;
  // }
}
