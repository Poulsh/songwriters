import { Injectable, EventEmitter } from '@angular/core';
import {UserCheckSum} from "../models/userCheckSum";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public userCheckSumFromWebSocketEvent: EventEmitter<UserCheckSum> = new EventEmitter();

  constructor() { }

  public  emitUserCheckSumFromWebSocket(data:UserCheckSum):void {
    // console.log('emitted userCheckSumFromWSEvent by SharedService'); console.log(data);
      this.userCheckSumFromWebSocketEvent.emit(data);
  }


}
