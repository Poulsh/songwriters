import {EventEmitter, Injectable} from '@angular/core';
import { Song } from '../models/song';
import { Observable ,  of } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap, finalize } from 'rxjs/operators';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {environment} from "../../environments/environment";
import {UserCheckSum} from "../models/userCheckSum";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class SongService {

  private songsUrl = environment.apiUrl+'song';


  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }



  getSongs(): Observable<any> {
    return this.http.get<any>(this.songsUrl)
      .pipe(
         map(data => this.process(data))
      );
  }

  getSong(id: number) : Observable<any> {
    const url = `${this.songsUrl}/view/${id}`;

    return this.http.get<any>(url).pipe(
    map(data => this.process(data))
    )
  }

  public addPeaks(hash:string, peaks:any): Observable<any> {
    const body = JSON.stringify({peaks:peaks});
    return this.http.post<any>(environment.apiUrl+'song/add-peaks/'+hash, body, httpOptions)
        .pipe(map(data => this.process(data)));
  }




  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 2000,
      });
    }
    return response;
  }
}
