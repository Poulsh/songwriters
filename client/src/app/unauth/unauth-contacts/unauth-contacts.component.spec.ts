import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnauthContactsComponent } from './unauth-contacts.component';

describe('UnauthContactsComponent', () => {
  let component: UnauthContactsComponent;
  let fixture: ComponentFixture<UnauthContactsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthContactsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
