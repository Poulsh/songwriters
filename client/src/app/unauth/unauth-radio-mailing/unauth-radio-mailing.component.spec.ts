import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnauthRadioMailingComponent } from './unauth-radio-mailing.component';

describe('UnauthRadioMailingComponent', () => {
  let component: UnauthRadioMailingComponent;
  let fixture: ComponentFixture<UnauthRadioMailingComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthRadioMailingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthRadioMailingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
