import { Component, OnInit,  LOCALE_ID, Inject } from '@angular/core';
import {PlaylistItem} from "../../models/playlistItem";
import {SeoService} from "../../seo/seo.service";

@Component({
  selector: 'app-unauth-radio-mailing',
  templateUrl: './unauth-radio-mailing.component.html',
  styleUrls: ['./unauth-radio-mailing.component.scss']
})
export class UnauthRadioMailingComponent implements OnInit {

  public priceRadioAllTracks = PlaylistItem.PRICE_RADIO_ALL_TRACKS;
  public priceRadioSelectedRu = PlaylistItem.PRICE_RADIO_SELECTED_RU;
  public priceRadioSelectedWorld = PlaylistItem.PRICE_RADIO_SELECTED_WORLD;

  private metaTitle: string = 'Songwriters - радио рассылка';
  private metaDescription: string = 'Рассылка треков по радиостанциям во всем мире';

  constructor(
    @Inject(LOCALE_ID) public locale: string,
    private seoService: SeoService,
  ) { }

  ngOnInit(): void {
    if (this.locale!='ru'){
      this.metaTitle = 'Songwriters - mailing';
      this.metaDescription = 'Distribution of tracks to radio stations around the world';
    }
    this.seoService.set(this.metaTitle,this.metaDescription);
  }

}
