import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnauthSidebarMenuComponent } from './unauth-sidebar-menu.component';

describe('UnauthSidebarMenuComponent', () => {
  let component: UnauthSidebarMenuComponent;
  let fixture: ComponentFixture<UnauthSidebarMenuComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthSidebarMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthSidebarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
