import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UnauthSongViewComponent } from './unauth-song-view.component';

describe('UnauthSongViewComponent', () => {
  let component: UnauthSongViewComponent;
  let fixture: ComponentFixture<UnauthSongViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthSongViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthSongViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
