import {Component, OnInit, ElementRef, ViewChild, Inject, LOCALE_ID} from '@angular/core';
import {Song} from "../../models/song";
import {ActivatedRoute, Router} from "@angular/router";
import {User} from "../../models/user";
import {AuthService} from "../../auth/auth.service";
import {environment} from "../../../environments/environment";
import {Comment} from "../../models/comment";
import {UnauthService} from "../unauth.service";
import {ConfirmDialog} from "../../alert/confirm-dialog/confirm-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {SeoService} from "../../seo/seo.service";

@Component({
  selector: 'app-unauth-song-view',
  templateUrl: './unauth-song-view.component.html',
  styleUrls: ['./unauth-song-view.component.scss']
})
export class UnauthSongViewComponent implements OnInit {

  public song:Song;
  public apiUrl = environment.apiUrl;
  public isCommentFormOpen:boolean = false;
  public comments:Comment[];

  @ViewChild('oneFieldForm') oneFieldForm: ElementRef;

  private metaTitle: string = 'Songwriters - страница трека';
  private metaDescription: string = 'Послушать и скачать трек';

  constructor(
    private route: ActivatedRoute,
    private unauthService: UnauthService,
    private authService: AuthService,
    public dialog: MatDialog,
    private router: Router,
    @Inject(LOCALE_ID) public locale: string,
    private seoService: SeoService,
  ) { }

  ngOnInit() {
    this.redirectIfAuth();
    this.getSong();
  }

  redirectIfAuth(){
    let user = this.authService.user;
    if (user && user.roles && user.roles.indexOf('radio')>-1){
      const hash = this.route.snapshot.paramMap.get('hash');
      this.router.navigate(['radio-song-view/'+hash]);
    }
  }

  getSong(): void {
    const hash = this.route.snapshot.paramMap.get('hash');
    this.unauthService.getSong(hash)
      .subscribe( response => {
        this.song = response.song;
        this.comments = response.comments;

        this.metaTitle = this.song?.artist_name +' - '+this.song?.name ;
        this.metaDescription = 'Послушать и скачать трек '+this.song?.name +' артиста '+ this.song?.artist_name;
        if (this.locale!='ru'){
          this.metaDescription = 'Listen and download track '+this.song?.name +' by artist '+ this.song?.artist_name;;
        }
        this.seoService.set(this.metaTitle,this.metaDescription);
      });
  }

  showAlertRegisterForComment(){
    let settings = {};
    const dialogRef = this.dialog.open(ConfirmDialog, {
      data: {
        head: '',
        message: 'Зарегистрируйтесь или войдите,<br> для оставления комментариев',
        yesBtn: 'Войти',
        noBtn: 'Зарегистрироваться',
        value: 'login',
        noBtnValue:'signup',
        settings:settings
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'signup') {
        this.router.navigate(['signup']);
      } else if (result == 'login') {
        this.router.navigate(['login']);
      }
    });
  }




}
