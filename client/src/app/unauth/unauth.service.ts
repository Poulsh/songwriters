import { Injectable } from '@angular/core';
import {AlertComponent} from "../alert/alert.component";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {map} from "rxjs/operators";

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};


@Injectable({
  providedIn: 'root'
})
export class UnauthService {

  constructor(
    private http: HttpClient,
    private router: Router,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) { }


  public getSong(hash): Observable<any> {
    return this.http.post<any>(environment.apiUrl+'unauth/get-song/'+hash, {}, httpOptions)
      .pipe(map(data => this.process(data)));
  }



  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }

    return response;
  }
}
