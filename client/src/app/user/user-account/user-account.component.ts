import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray} from '@angular/forms';
import {UserService} from "../user.service";
import {AuthService} from "../../auth/auth.service";
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import {environment} from '../../../environments/environment';
import {User} from "../../models/user";
import {Radio} from "../../models/radio";
import {Artist} from "../../models/artist";
import {Media} from "../../models/media";


@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {

  private apiUrl = environment.apiUrl;
  public user: User;
  public userForm: FormGroup;
  public passwordForm: FormGroup;

  public radio: Radio;
  public artist: Artist;
  public media: Media;


  constructor(
      private authService: AuthService,
      private userService: UserService,
      private fb: FormBuilder,
      public dialog: MatDialog
  ) { }

  ngOnInit() {
    // this.getActiveRoleIfMultiple();
    this.initForm();
    this.getUser();
  }


  get f() {return this.userForm.controls;}
  get pf() {return this.passwordForm;}

  initForm(){
    this.userForm = this.fb.group({
      firstName: ['', [
        Validators.required,
        Validators.pattern(/^[А-яA-z0-9]*$/),
        Validators.minLength(3)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.pattern(/^[А-яA-z0-9]*$/),
        Validators.minLength(3)
      ]],
      email: ['', [
        Validators.required,
        Validators.email,
      ]]
    });


    this.passwordForm = this.fb.group({
      password: ['', [Validators.required]],
      confirmPassword: ['']
    }, {validator: this.checkPasswords });


  }

  getUser(): void {
    this.userService.getUser()
        .subscribe( response => {
          this.user = response.user;
          this.userToForm(response.user);

          if (response.radio) {
            this.radio = response.radio;
          }
          if (response.artist) {
            this.artist = response.artist;
          }
          if (response.media) {
            this.media = response.media;
          }
        });
  }

  userToForm(user:User):void{
    this.f.firstName.setValue(user.first_name);
    this.f.lastName.setValue(user.last_name);
    this.f.email.setValue(user.email);

  }

  saveUser(): void {
    this.user.first_name = this.f.firstName.value;
    this.user.last_name = this.f.lastName.value;
    // this.user.email = this.f.email.value;
    this.userService.updateUser(this.user)
        .subscribe( response => {
          this.user = response.user;
          this.f.firstName.setValue(this.user.first_name);
          this.f.lastName.setValue(this.user.last_name);
          // this.f.email.setValue(this.user.email);
        });
  }

  changePassword(): void {
    this.userService.changePassword(this.passwordForm.controls.password.value)
        .subscribe( response => {
        });
  }

  checkPasswords(group: FormGroup) { // validation of passwordForm
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true }
  }
}
