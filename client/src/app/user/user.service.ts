import { Injectable } from '@angular/core';
import { Observable ,  of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { AlertComponent } from '../alert/alert.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {Router} from "@angular/router";
import {User} from "../models/user";


const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(
      private http: HttpClient,
      private router: Router,
      public dialog: MatDialog,
      public snackBar: MatSnackBar
  ) { }

  public getUser(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'user/index', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  public changeLanguage(lang:string): Observable<any> {
    const body = JSON.stringify({language:lang});
    return this.http.post<any>(environment.apiUrl+'user/change-language', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  updateUser(user): Observable<any> {
    const body = JSON.stringify({user});

    return this.http.post<User>(environment.apiUrl+'user/update', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  changePassword(password): Observable<any> {
    const body = JSON.stringify({password});
    return this.http.post<any>(environment.apiUrl+'user/passchange', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }

  getWsTicket(): Observable<any> {
    const body = JSON.stringify({});
    return this.http.post<any>(environment.apiUrl+'auth/ws-ticket', body, httpOptions)
        .pipe(map(data => this.process(data)));
  }


  public process(response): any {
    if(response.error){
      this.dialog.open(AlertComponent, {
        data: { message: response.error }
      });
    }
    if (response.success) {
      this.snackBar.open(response.success, '', {
        duration: 5000,
      });
    }
    return response;
  }
}
