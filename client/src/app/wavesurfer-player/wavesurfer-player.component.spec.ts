import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WavesurferPlayerComponent } from './wavesurfer-player.component';

describe('WavesurferPlayerComponent', () => {
  let component: WavesurferPlayerComponent;
  let fixture: ComponentFixture<WavesurferPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WavesurferPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WavesurferPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
