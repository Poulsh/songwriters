import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import * as WaveSurfer from 'wavesurfer.js';
import {Song} from "../models/song";
import {SongService} from "../song/song.service";
import {environment} from "../../environments/environment";
import { LOCALE_ID, Inject } from '@angular/core';


@Component({
  selector: 'wavesurfer-player',
  templateUrl: './wavesurfer-player.component.html',
  styleUrls: ['./wavesurfer-player.component.scss'],
})
export class WavesurferPlayerComponent implements OnInit, AfterViewInit {


  public wavesurfer;
  @ViewChild('wavesurfer', { read: ElementRef })
  public ws: ElementRef;


  @Input() song: Song;
  @Input() waveColor = '#929599'; // 0b5fa5
  @Input() progressColor = '#0b5fa5';    //FF4C00
  @Input() cursorColor = '#999999';

  @Input() link:string;
  @Input() artistLink:string;
  @Input() view:string = 'default';
  @Input() alwaysUpdateImage:boolean=false;


  @Input() set nowPlaying(hash){
    if (hash && this.song && hash!=this.song.name_hash) {
      if (this.playing && this.wavesurfer && this.wavesurfer.isReady) {
        this.wavesurfer.playPause();
      }
    }
  }


  private playIter:number=0;
  @Input() set startPlay(i){
    if (i &&  i!=this.playIter && !this.playing) {
      this.playPause();
      this.playIter = i;
    }
  }



  @Output() onStart = new EventEmitter<boolean>();
  @Output() onFinish = new EventEmitter<boolean>();



  progress = 0;
  public playing = false;
  public loaded = false;
  public apiUrl = environment.apiUrl;

  public timeStamp:number;



  constructor(
      private ref: ChangeDetectorRef,
      private songService:SongService,
      @Inject(LOCALE_ID) public locale: string,
  ) {}


  ngOnInit() {
    this.timeStamp = (new Date()).getTime();
  }
  ngAfterViewInit() {

     let _url = environment.apiUrl+'uploadedsongs/listen/'+this.song.name_hash+'.mp3'+'?'+this.timeStamp;

    requestAnimationFrame(() => {
      this.wavesurfer = WaveSurfer.create({
        container: this.ws.nativeElement,
        waveColor: this.waveColor,
        progressColor: this.progressColor,
        cursorColor: this.cursorColor,
        height: '66',
        backend: 'MediaElement',
        mediaType:'audio',
        normalize: true,

      });


      if (this.song.peaks){
        this.wavesurfer.backend.peaks = JSON.parse(this.song.peaks);
        this.wavesurfer.drawBuffer();
        this.loaded = false;

      } else {
        this.wavesurfer.load(_url);
        this.wavesurfer.on('waveform-ready', () => {

          let peaks = this.wavesurfer.backend.getPeaks(960);
          //  var dataImg = wavesurfer.exportImage();

          let jsonPeaks = JSON.stringify(peaks);

          this.songService.addPeaks(this.song.name_hash,jsonPeaks)
              .subscribe( response => {
                // this.song = response.song;
              });
        });
      }


      this.wavesurfer.on('play', () => {
        this.playing = true;
        this.onStart.emit(true);
      });
      this.wavesurfer.on('pause', () => {
        this.playing = false;
      });
      this.wavesurfer.on('finish', () => {
        this.playing = false;
        this.wavesurfer.seekTo(0);
        this.progress = 0;
        this.ref.markForCheck();
        this.onFinish.emit(true);
      });
      this.wavesurfer.on('audioprocess', () => {
        this.progress = this.wavesurfer.getCurrentTime() / this.wavesurfer.getDuration() * 100;
        this.ref.markForCheck();
      });
    });
  }


  playPause() {

    let _url = environment.apiUrl+'uploadedsongs/listen/'+this.song.name_hash+'.mp3'+'?'+this.timeStamp;

    if(!this.loaded) {
      this.loaded = true;
      this.wavesurfer.load(_url, this.wavesurfer.backend.peaks);
      this.wavesurfer.play();
    } else {
      if (this.wavesurfer && this.wavesurfer.isReady) {
        this.wavesurfer.playPause();
      }
    }
  }



}