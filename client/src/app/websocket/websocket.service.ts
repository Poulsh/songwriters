import {Injectable, OnDestroy, Inject, EventEmitter} from '@angular/core';
import { Observable, SubscriptionLike, Subject, Observer, interval } from 'rxjs';
import {filter, map, tap} from 'rxjs/operators';
import { WebSocketSubject, WebSocketSubjectConfig } from 'rxjs/webSocket';

import { share, distinctUntilChanged, takeWhile } from 'rxjs/operators';
import {  IWsMessage, WebSocketConfig } from './websocket.interfaces';
import { config } from './websocket.config';
import {environment} from "../../environments/environment";
import {UserService} from "../user/user.service";
import {SharedService} from "../shared/shared.service";
import {UserCheckSum} from "../models/userCheckSum";


@Injectable({
  providedIn: 'root'
})
export class WebsocketService implements OnDestroy {

  private config: WebSocketSubjectConfig<IWsMessage<any>>;
  private websocketSub: SubscriptionLike;
  private statusSub: SubscriptionLike;
  private socket: WebSocketSubject<IWsMessage<any>>;
  private reconnection: Observable<number>;   // Observable для реконнекта по interval
  private connectObserver: Observer<boolean>;  // сообщает, когда происходит коннект и реконнект
  private wsMessages$: Subject<IWsMessage<any>>;    // вспомогательный Observable для работы с сообщениями
  private reconnectInterval: number;  // пауза между попытками реконнекта в милисекундах
  private reconnectAttempts: number; // количество попыток реконнекта
  private isConnected: boolean;   // синхронный вспомогатель для статуса соединения
  public status: Observable<boolean>;     // статус соединения
  public webSocketEvent: EventEmitter<any> = new EventEmitter();

  constructor(@Inject(config)
              private wsConfig: WebSocketConfig,
              private userService: UserService,
              private sharedService: SharedService,
              ) {




    // this.wsMessages$ = new Subject<IWsMessage<any>>();

    // если в wsConfig не указано, задаем параметры реконнекта
    this.reconnectInterval = wsConfig.reconnectInterval || 5000;   // 5000
    this.reconnectAttempts = wsConfig.reconnectAttempts || 10;    // 10


    this.config = {
      url: wsConfig.url,
      closeObserver: {
        next: (event: CloseEvent) => {
          this.socket = null;
          this.connectObserver.next(false);
        }
      },
      openObserver: {
        next: (event: Event) => {
          this.connectObserver.next(true);
        }
      }
    };

    // connection status
    this.status = new Observable<boolean>((observer) => {
      this.connectObserver = observer;
    }).pipe(share(), distinctUntilChanged());

    // запускаем реконнект при отсутствии соединения
    this.statusSub = this.status
        .subscribe((isConnected) => {
          this.isConnected = isConnected;

          if (!this.reconnection && typeof(isConnected) === 'boolean' && !isConnected) {
            this.reconnect();
          }
        });

    // говорим, что что-то пошло не так
    // this.websocketSub = this.wsMessages$.subscribe(
    //     null, (error: ErrorEvent) => console.error('WebSocket error!', error)
    // );

    this.connect();

  }



  /*
  * connect to WebSocked
  * */
  public connect(token:string = null): void {

    if (token){
      this.config.url = environment.ws+'?ticket='+token;
      this.socket = new WebSocketSubject(this.config);
      this.socket.subscribe(
          // (message) => this.wsMessages$.next(message),
          (message) => this.process(message),
          (error: Event) => {
            if (!this.socket) {
              this.reconnect();
            }
          });
    } else {
      if (localStorage.getItem('user')) { // авторизован
        this.userService.getWsTicket()
          .subscribe( response => {
            if (response.ws_ticket) {
              this.config.url = environment.ws+'?ticket='+response.ws_ticket;


              // const ws = new WebSocket("ws://www.example.com");

              // const ws = new WebSocket(this.config.url );
              // ws.onmessage = (event) => {
              //   const data = JSON.parse(event.data);
              //
              //   console.log(event);
              //   console.log(data);
              //   this.process(event);
              //
              // };


              this.socket = new WebSocketSubject(this.config);
              this.socket.subscribe(
                // (message) => this.wsMessages$.next(message),
                (message) => this.process(message),
                (error: Event) => {
                  if (!this.socket) {
                    console.log('WebsocketService.connect() ->  !this.socket');
                    this.reconnect();
                  }
                });
            }
          });
      }


    }

  }


  /*
  * reconnect if not connecting or errors
  * */
  private reconnect(): void {
    // Пытаемся подключиться пока не подключимся, либо не упремся в ограничение попыток подключения
    this.reconnection = interval(this.reconnectInterval)
        .pipe(takeWhile((v, index) => index < this.reconnectAttempts && !this.socket));

    this.reconnection.subscribe(
        () => this.connect(),
        () => null,
        () => {
          this.reconnection = null;
          if (!this.socket) {
            // this.wsMessages$.complete();
            // this.connectObserver.complete();
          }
        });
  }


  /*
  * on message event
  * */
  // public on<T>(event: string): Observable<T> {
  //   if (event) {
  //     return this.wsMessages$.pipe(
  //         // filter((message: IWsMessage<T>) => message.event === event),
  //         map((message: IWsMessage<T>) => this.process(message)),
  //         // tap((message: IWsMessage<T>) => console.log(message))
  //
  //     );
  //   }
  // }





  ngOnDestroy() {
    this.websocketSub.unsubscribe();
    this.statusSub.unsubscribe();
  }




  /*
  * on message to server
  * */
  public send(event: string, data: any = {}): void {
    if (event && this.isConnected) {
      this.socket.next(<any>JSON.stringify({ event, data }));
    } else {

      this.userService.getWsTicket()
          .subscribe( response => {
            if (response.ws_ticket) {
              console.log('WebsocketService.send()->getWsTicket-> if (response.ws_ticket)');
              this.connect(response.ws_ticket);
              this.socket.next(<any>JSON.stringify({ event, data }));
            }
          });
      // console.error('WebsocketService.send() - Ошибка отправки!');
    }
  }




  public process(response): any {

    console.log('websocketService data in process'); console.log(response);

    this.webSocketEvent.emit(response);

    if (response.event == 'userCheckSum' &&  response.data.userCheckSum) {
      this.sharedService.emitUserCheckSumFromWebSocket(response.data.userCheckSum);

    }
  }
}