export const environment = {
  production: true,
  apiUrl:'https://api.songwriters.pro/',
  ws: 'wss://songwriters.pro:2346',
};
