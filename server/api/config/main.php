<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'name' => 'Songwriters API',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'i18n' => [
            'translations' => [
                'api*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@api/messages',
                    'sourceLanguage' => 'ru-RU',
                    'forceTranslation'=>true,
                    'fileMap' => [
                        'api' => 'api.php',
//                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'enableAutoLogin' => false,
//            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
//        'session' => [
            // this is the name of the session cookie used for login on the api
//            'name' => 'advanced-api',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['api'],
                    'logFile' => '@runtime/logs/api.log',
                    'logVars' => [],   // $_GET, $_POST, $_FILES, $_COOKIE, $_SESSION, $_SERVER
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 20,
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'controller' => 'unauth',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST get-song/<hash:[0-9a-zA-Z\-\_\/]+>'=>'get-song',
                        'POST get-artist/<id:[0-9a-zA-Z\-\_\/]+>'=>'get-artist',
                    ],
                ],
                [
                    'controller' => 'message',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST index' => 'index',
                        'POST send' => 'send',
                        'POST reply/<id:([0-9])+>' => 'reply',
                        'POST artist-radiosec-dialog-by-subject' => 'artist-radiosec-dialog-by-subject',
                        'POST artist-premiumsec-dialog-by-subject' => 'artist-premiumsec-dialog-by-subject',
                        'POST artist-dialog/<id:([0-9])+>' => 'artist-dialog',

                    ],
                ],
                [
                    'controller' => 'notification',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST index' => 'index',
                        'POST mark-as-seen/<id:([0-9])+>' => 'mark-as-seen',
                        'POST mark-as-seen-all' => 'mark-as-seen-all',

                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                    ],
                ],
                [
                    'controller' => 'user',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST index' => 'index',
                        'POST update' => 'update',
                        'POST passchange'=>'password-change',
                        'POST change-language'=>'change-language',


                    ],
                ],
                [
                    'controller' => 'media',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST media-home' => 'media-home',
                        'POST update-my-info' => 'update-my-info',
                        'POST upload-avatar'=>'upload-avatar',

                    ],
                ],
                [
                    'controller' => 'radio',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST radio-home' => 'radio-home',
                        'POST update-my-info' => 'update-my-info',
                        'POST upload-avatar'=>'upload-avatar',
                        'POST get-all-tracks-feed'=>'get-all-tracks-feed',
                        'POST get-selected-tracks-feed'=>'get-selected-tracks-feed',
                        'POST create-playlist'=>'create-playlist',
                        'POST get-my-playlists'=>'get-my-playlists',
                        'POST get-my-playlist/<id:[0-9a-zA-Z\-\_\/]+>'=>'get-my-playlist',
                        'POST add-song-to-playlist'=>'add-song-to-playlist',
                        'POST remove-item-from-playlist'=>'remove-item-from-playlist',
                        'POST delete-playlist/<id:[0-9a-zA-Z\-\_\/]+>'=>'delete-playlist',
                        'POST move-song-to-playlist'=>'move-song-to-playlist',
                        'POST edit-playlist/<id:[0-9a-zA-Z\-\_\/]+>'=>'edit-playlist',
                        'POST get-song/<hash:[0-9a-zA-Z\-\_\/]+>'=>'get-song',
                        'POST comment-song/<hash:[0-9a-zA-Z\-\_\/]+>'=>'comment-song',
                        'POST get-artist/<id:[0-9a-zA-Z\-\_\/]+>'=>'get-artist',



                    ],
                ],
                [
                    'controller' => 'artist',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'patterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'POST artist-home' => 'artist-home',
                        'POST update-my-info' => 'update-my-info',
                        'POST upload-avatar'=>'upload-avatar',
                        'POST get-my-songs'=>'get-my-songs',
                        'POST get-my-song/<hash:[0-9a-zA-Z\-\_\/]+>'=>'get-my-song',
                        'POST get-my-song-edit-data/<hash:[0-9a-zA-Z\-\_\/]+>'=>'get-my-song-edit-data',
                        'POST update-my-song-info/<id:[0-9a-zA-Z\-\_\/]+>'=>'update-my-song-info',
                        'POST upload-image-to-song/<id:[0-9a-zA-Z\-\_\/]+>'=>'upload-image-to-song',
                        'POST delete-my-song/<hash:[0-9a-zA-Z\-\_\/]+>'=>'delete-my-song',
                        'POST add-song-to-radio-section/<hash:[0-9a-zA-Z\-\_\/]+>'=>'add-song-to-radio-section',
                        'POST get-radio-section'=>'get-radio-section',
                        'POST get-premium-ru-section'=>'get-premium-ru-section',
                        'POST get-premium-world-section'=>'get-premium-world-section',
                        'POST remove-item-from-radio-section'=>'remove-item-from-radio-section',
                        'POST get-radio-item/<id:[0-9a-zA-Z\-\_\/]+>'=>'get-radio-item',
                        'POST get-premium-item/<id:[0-9a-zA-Z\-\_\/]+>'=>'get-premium-item',
                        'POST upload-radio-song/<type:[0-9]+>'=>'upload-radio-song',
                        'POST get-radio-new-data'=>'get-radio-new-data',
                        'POST submit-radio-all-tracks/<id:[0-9a-zA-Z\-\_\/]+>'=>'submit-radio-all-tracks',
                        'POST mark-song-to-radio-all-tracks/<id:[0-9a-zA-Z\-\_\/]+>'=>'mark-song-to-radio-all-tracks',
                        'POST upload-selected-ru-song'=>'upload-selected-ru-song',
                        'POST upload-selected-world-song'=>'upload-selected-world-song',
                        'POST submit-radio-selected-ru/<id:[0-9a-zA-Z\-\_\/]+>'=>'submit-radio-selected-ru',
                        'POST submit-radio-selected-world/<id:[0-9a-zA-Z\-\_\/]+>'=>'submit-radio-selected-world',
                        'POST mark-song-to-radio-selected-ru/<id:[0-9a-zA-Z\-\_\/]+>'=>'mark-song-to-radio-selected-ru',
                        'POST mark-song-to-radio-selected-world/<id:[0-9a-zA-Z\-\_\/]+>'=>'mark-song-to-radio-selected-world',
                        'POST get-new-song-form-data/<type:[0-9]+>'=>'get-new-song-form-data',
                        'POST upload-new-file-of-song/<id:[0-9a-zA-Z\-\_\/]+>'=>'upload-new-file-of-song',


                    ],
                ],
                [
                    'controller' => 'song',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'GET' => 'index',
                        'POST update/<id:([0-9])+>' => 'update',
                        'GET view/<id:([0-9])+>'=>'view',
                        'POST add-peaks/<hash:[0-9a-zA-Z\-\_\/]+>' => 'add-peaks',

                    ],
                ],
                [
                    'controller' => 'auth',
                    'class' => 'yii\rest\UrlRule',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action:[0-9a-zA-Z\-\/]+>' => 'options',
                        'GET signup' => 'signup',
                        'POST signup' => 'signup',
                        'POST login' => 'login',
                        'GET verification' => 'verification',
                        'GET signup-data' => 'signup-data',
                        'POST ws-ticket' => 'ws-ticket',
                        'POST password-reset' => 'password-reset',
                        'POST change-password' => 'change-password',
                    ],
                ],

            ],
        ],

    ],
    'params' => $params,
];
