<?php

namespace api\controllers;

use api\controllers\RestController;
use common\components\ErrorHelper;
use common\models\Artist;
use common\models\Imagefiles;
use common\models\Language;
use common\models\Menu;
use common\models\Playlist;
use common\models\PlaylistItem;
use common\models\Song;
use common\models\UploadSongForm;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class ArtistController extends RestController
{

    private $artistActions = [
        'artist-home',
        'update-my-info',
        'upload-avatar',
        'get-my-songs',
        'get-my-song',
        'get-my-song-edit-data',
        'update-my-song-info',
        'upload-image-to-song',
        'delete-my-song',
        'add-song-to-radio-section',
        'get-radio-section',
        'get-premium-ru-section',
        'get-premium-world-section',
        'remove-item-from-radio-section',
        'get-radio-item',
        'get-premium-item',
//        'get-premium-world-item',
        'upload-radio-song',
        'get-radio-new-data',
        'submit-radio-all-tracks',
        'mark-song-to-radio-all-tracks',
        'upload-selected-ru-song',
        'upload-selected-world-song',
        'submit-radio-selected-ru',
        'submit-radio-selected-world',
        'mark-song-to-radio-selected-ru',
        'mark-song-to-radio-selected-world',
        'get-new-song-form-data',
        'upload-new-file-of-song',


    ];


    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'except' => [ 'options'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->artistActions,
                        'roles' => [User::ROLE_ART],
                    ],
                ],
            ],
        ]);
    }



    /**
     * @return array
     */
    public function actionArtistHome()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $radioSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_RADIO_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artist->id,
        ])->one();
        if (!$radioSection) {
            $radioSection = new Playlist();
            $radioSection->name = 'Радио';
            $radioSection->type = Playlist::TYPE_RADIO_SECTION;
            $radioSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $radioSection->object_id = $artist->id;
            $radioSection->save();
        }

        $premiumSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_PREMIUM_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artist->id,
        ])->one();
        if (!$premiumSection) {
            $premiumSection = new Playlist();
            $premiumSection->name = 'Премиум';
            $premiumSection->type = Playlist::TYPE_PREMIUM_SECTION;
            $premiumSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $premiumSection->object_id = $artist->id;
            $premiumSection->save();
        }

        $undoneQuery = Song::find();
        $undoneQuery->where(
                ['OR',
                    ['AND',
                        ['artist_id'=>$artist->id],
                        ['radio_status'=> Song::STATUS_UNDONE]
                    ],
                    ['AND',
                        ['artist_id'=>$artist->id],
                        ['premium_ru_status'=> Song::STATUS_UNDONE],
                    ],
                    ['AND',
                        ['artist_id'=>$artist->id],
                        ['premium_world_status'=> Song::STATUS_UNDONE],
                    ],
                ]
            );
        $undone = $undoneQuery->select((new Song)->scenarios()[Song::SCENARIO_UNDONE_LIST])
            ->asArray()
            ->all();

        return [
            'artist'=>$artist,
            'radioSection'=>$radioSection->toArray($radioSection->scenarios()[Playlist::SCENARIO_ARTIST_HOME]),
            'premiumSection'=>$premiumSection->toArray($premiumSection->scenarios()[Playlist::SCENARIO_ARTIST_HOME]),
            'undone'=>$undone,
            'language'=>Yii::$app->request->headers->get('App-Language'),
        ];
    }


    /**
     * @return array
     */
    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateMyInfo()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) throw new NotFoundHttpException();
        $oModel =  Artist::edit($artist->id, \Yii::$app->request->post('artist'));
        if (!$oModel->errors) {
            return [
                'success'=>'Изменения сохранены',
                'artist'=>$oModel,
            ];
        } else {
            return [
                'error'=>'ошибка сохранения',
                'message'=>$oModel->errors,
            ];
        }
    }


    //  в restController отключена трансляция в json для работы с файлом
    public function actionUploadAvatar()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) throw new NotFoundHttpException();

        $newImage = Imagefiles::uploadImage(Imagefiles::TYPE_ARTIST_AVATAR,$artist->id);
        if ($newImage) {
            if (isset($newImage['error'])) {
                return Json::encode(['error'=>'ошибка загрузки - '.$newImage['error']]);
            }

            return Json::encode([
                'success'=>'Изображение загружено',
                'artist'=>$artist
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }




    /**
     * @return array
     */
    public function actionGetMySongs()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $songs = Song::find()
            ->where(['artist_id'=>$artist->id])
            ->all();
        return [
            'artist'=>$artist,
            'songs'=>$songs,
            ];
    }


    /**
     * @return array
     */
    public function actionGetMySong($hash)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $song = Song::find()
            ->where(['artist_id'=>$artist->id])
            ->andWhere(['name_hash'=>$hash])
            ->one();

        return [
            'artist'=>$artist,
            'song'=>$song,
            'comments'=>$song->comments,
            ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     *
     */
    public function actionGetMySongEditData($hash)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $styles = \common\models\Menu::tree(Menu::ROOT_MUSIC_STYLES_URL);
        $languages = Language::find()->all();

        $song = Song::find()
            ->where(['artist_id'=>$artist->id])
            ->andWhere(['name_hash'=>$hash])
            ->one();
        $songArr = $song?$song->toArray($song->scenarios()[Song::SCENARIO_VIEW]):null;

        return [
            'artist'=>$artist,
            'song'=>$songArr,
            'styles'=>$styles,
            'languages'=>$languages,
        ];
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     *
     */
    public function actionGetNewSongFormData($type)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $styles = \common\models\Menu::tree(Menu::ROOT_MUSIC_STYLES_URL);
        $languages = Language::find()->all();

        if ($type == Song::SERVICE_URL_CODE_ALL_TRACKS) {
            $songOptions = Song::findAllTracksOptionsToArtist($artist->id);
        } else if ($type == Song::SERVICE_URL_CODE_SELECTED_RU) {
            $songOptions = Song::findSelectedRuOptionsToArtist($artist->id);
        } else if ($type == Song::SERVICE_URL_CODE_SELECTED_WORLD) {
            $songOptions = Song::findSelectedWorldOptionsToArtist($artist->id);
        }


        return [
            'artist'=>$artist,
            'styles'=>$styles,
            'languages'=>$languages,
            'songOptions'=>$songOptions,
        ];
    }


    /**
     * @return array
     */
    public function actionUpdateMySongInfo($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $song = Song::find()
            ->where(['id'=>$id, 'artist_id'=>$artist->id,])
            ->one();
        if (!$song) {return ['error'=>'не найдено'];}
        $song->attributes = Yii::$app->request->post('song');

        if ($song->vocal === false) {
            $song->vocal = null;
        }
        if ($song->adult_text === false) {
            $song->adult_text = null;
        }

        if ($song->save()) {
            return [
                'success'=>'трек изменен',
                'song'=>$song,
                'errors'=>$song->errors
            ];
        } else {
            return ['error'=>'ошибка изменения - '.Json::encode($song->errors), 'message'=>$song->errors];
        }
    }


    /**
     * @return string
     *  в restController отключена трансляция в json для работы с файлом
     */
    public function actionUploadImageToSong($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) throw new NotFoundHttpException();



        $newImage = Imagefiles::uploadImage(Imagefiles::TYPE_SONG_ARTWORK,$id);
        if ($newImage) {
            if (isset($newImage['error'])) {
                return Json::encode(['error'=>'ошибка загрузки - '.$newImage['error']]);
            }

            return Json::encode([
                'success'=>'Изображение загружено',
                'artist'=>$artist
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }



    /**
     * @return array
     */
    public function actionDeleteMySong($hash)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $song = Song::findOne(['name_hash'=>$hash, 'artist_id'=>$artist->id]);
        if (!$song) {return ['error'=>'не найдено'];}

        if ($song->radio_status == Song::STATUS_ACTIVE ||
            $song->premium_ru_status == Song::STATUS_ACTIVE ||
            $song->premium_world_status == Song::STATUS_ACTIVE
        ) {
             // не удаляем активное
            $serviceName = null;
            if ($song->radio_status == Song::STATUS_ACTIVE) {
                $serviceName = 'All Tracks';
            } else if ($song->premium_ru_status == Song::STATUS_ACTIVE) {
                $serviceName = 'Selected Tracks (СНГ)';
            } else if ($song->premium_world_status == Song::STATUS_ACTIVE) {
                $serviceName = 'Selected Tracks (World)';
            }
            return [
                'error'=>'На трек предоставляется услуга '.$serviceName,
            ];

        } else {
            $song->delete();
            return [
                'success'=>'трек удален',
            ];
        }
    }




    /**
     * @return array
     */
    public function actionAddSongToRadioSection($hash)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $song = Song::findOne([
            'name_hash'=>$hash,
            'artist_id'=>$artist->id,
        ]);
        if (!$song) {return ['error'=>'не найдено'];}

        return $song->addToRadioSectionOfArtist($artist->id);
    }


    /**
     * @return array
     */
    public function actionGetRadioSection()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $radioSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_RADIO_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artist->id,
        ])->one();
        if (!$radioSection) {
            $radioSection = new Playlist();
            $radioSection->name = 'Радио';
            $radioSection->type = Playlist::TYPE_RADIO_SECTION;
            $radioSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $radioSection->object_id = $artist->id;
            $radioSection->save();
        }

        return ['playlist'=>$radioSection->toArray($radioSection->scenarios()[Playlist::SCENARIO_VIEW])];
    }

//    /**
//     * @return array
//     */
//    public function actionGetPremiumRuSection()
//    {
//        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
//
//        $section = Playlist::find()->where([
//            'type'=>Playlist::TYPE_PREMIUM_RU_SECTION,
//            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
//            'object_id'=>$artist->id,
//        ])->one();
//        if (!$section) {
//            $section = new Playlist();
//            $section->name = 'Премиум RU';
//            $section->type = Playlist::TYPE_PREMIUM_RU_SECTION;
//            $section->object_type = Playlist::OBJECT_TYPE_ARTIST;
//            $section->object_id = $artist->id;
//            $section->save();
//        }
//
//        return ['playlist'=>$section->toArray($section->scenarios()[Playlist::SCENARIO_VIEW])];
//
//    }
//    /**
//     * @return array
//     */
//    public function actionGetPremiumWorldSection()
//    {
//        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
//
//        $section = Playlist::find()->where([
//            'type'=>Playlist::TYPE_PREMIUM_WORLD_SECTION,
//            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
//            'object_id'=>$artist->id,
//        ])->one();
//        if (!$section) {
//            $section = new Playlist();
//            $section->name = 'Премиум world';
//            $section->type = Playlist::TYPE_PREMIUM_WORLD_SECTION;
//            $section->object_type = Playlist::OBJECT_TYPE_ARTIST;
//            $section->object_id = $artist->id;
//            $section->save();
//        }
//
//        return ['playlist'=>$section->toArray($section->scenarios()[Playlist::SCENARIO_VIEW])];
//
//    }



    public function actionRemoveItemFromRadioSection()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $post = Yii::$app->request->post();
        $radioSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_RADIO_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artist->id,
        ])->one();

        return $radioSection->removeFromRadioSection($post['item_id']);
    }


    /**
     * @return array
     */
    public function actionGetRadioItem($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $item = PlaylistItem::find()
            ->andWhere(['id'=>$id])
            ->one();

        $playlist = $item->playlist;
        if ($playlist->type!=Playlist::TYPE_RADIO_SECTION ||
            $playlist->object_type!=Playlist::OBJECT_TYPE_ARTIST ||
            $playlist->object_id!=$artist->id) {
            throw new BadRequestHttpException();
        }

        return [
            'item'=>$item,
        ];
    }

    /**
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionGetPremiumItem($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $item = PlaylistItem::findOne([
            'id'=>$id,
            'service_type'=>[PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU,PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD]
        ]);

        $playlist = $item->playlist;
        if ($playlist->type!=Playlist::TYPE_PREMIUM_SECTION||
            $playlist->object_type!=Playlist::OBJECT_TYPE_ARTIST ||
            $playlist->object_id!=$artist->id) {
            throw new BadRequestHttpException();
        }

        return [
            'item'=>$item,
        ];
    }








    /**
     *  в restController отключена трансляция в json для работы с файлом
     * @return string
     * @throws NotFoundHttpException
     *
     */
    public function actionUploadNewFileOfSong($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) throw new NotFoundHttpException('artist not found');

        $song = Song::findOne([
            'artist_id'=>$artist->id,
            'id'=>$id,
        ]);
        if (!$song) throw new NotFoundHttpException('song not found');


        $uploadModel = new UploadSongForm();
        $uploadModel->songFile = UploadedFile::getInstanceByName('song');;


        $isUploaded = $uploadModel->change($song->path_to_file,$song->name_hash);


        if ($isUploaded) {
            $song->setScenario('edit');
            $song->peaks = null;
            if ($song->save()) {
                return Json::encode([
                    'success'=>'Файл загружен',
                ]);
            } else {
                return Json::encode([
                    'error'=>ErrorHelper::toString($song->errors),
                ]);
            };

            if (isset($uploadModel['error']) && $isUploaded['error']!=[]) {
                return Json::encode(['error'=>'ошибка загрузки - '.$isUploaded['error']]);
            }
            if (isset($isUploaded['errors']) && $isUploaded['errors']!=[]) {
                return Json::encode(['error'=>'ошибка загрузки - '.Json::encode($isUploaded['errors'])]);
            }

            return Json::encode([
                'success'=>'Файл загружен',
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }






    /**
     *  в restController отключена трансляция в json для работы с файлом
     * @return string
     * @throws NotFoundHttpException
     *
     */
    public function actionUploadRadioSong($type)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) throw new NotFoundHttpException();

        $file = UploadedFile::getInstanceByName('song');
        $rawData = Yii::$app->request->getBodyParams('song_data');
        $songData = Json::decode($rawData['song_data']);

        $artistSongNum = count(Song::find()
            ->where(['artist_id'=>$artist->id])
            ->select('id')
            ->asArray()->all());
        if ($type == Song::SERVICE_URL_CODE_ALL_TRACKS) {
            $serviceType = Song::SERVICE_TYPE_RADIO;
        } else if ($type == Song::SERVICE_URL_CODE_SELECTED_RU){
            $serviceType = Song::SERVICE_TYPE_PREMIUM_RU;
        } else if ($type == Song::SERVICE_URL_CODE_SELECTED_WORLD){
            $serviceType = Song::SERVICE_TYPE_PREMIUM_WORLD;
        }

        $newSong = Song::uploadSongByApi(
            $file,
            $artist->id,
            isset($songData['artist_name']) && $songData['artist_name']? $songData['artist_name']: $artist->name,
            isset($songData['name']) && $songData['name']?
                $songData['name']:
                'uploaded-by-'.str_replace(' ', '_', $artist->name) .'_song_'.$artistSongNum++,
            isset($songData['rightholders']) && $songData['rightholders']? $songData['rightholders']:null,
            isset($songData['music_style1_id']) && $songData['music_style1_id'] ? $songData['music_style1_id']:null,
            $serviceType,
            isset($songData['music_style2_id']) && $songData['music_style2_id'] ? $songData['music_style2_id']:null,
            isset($songData['music_style3_id']) && $songData['music_style3_id'] ? $songData['music_style3_id']:null,
            isset($songData['vocal']) && $songData['vocal'] ? $songData['vocal']:null,
            isset($songData['language']) && $songData['language'] ? $songData['language']:null,
            isset($songData['label']) && $songData['label'] ? $songData['label']:null,
            isset($songData['isrc']) && $songData['isrc'] ? $songData['isrc']:null,
            isset($songData['release_date']) && $songData['release_date'] ? $songData['release_date']:null,
            isset($songData['comments']) && $songData['comments'] ? $songData['comments']:null,
            isset($songData['hide_reviews']) && $songData['hide_reviews'] ? $songData['hide_reviews']:null,
            isset($songData['adult_text']) && $songData['adult_text'] ? $songData['adult_text']:null,
            isset($songData['publication_agreed']) && $songData['publication_agreed'] ? $songData['publication_agreed']:null
        );


        if ($newSong) {
            if (isset($newSong['error']) && $newSong['error']!=[]) {
                return Json::encode(['error'=>'ошибка загрузки - '.$newSong['error']]);
            }
            if (isset($newSong['errors']) && $newSong['errors']!=[]) {
                return Json::encode(['error'=>'ошибка загрузки - '.Json::encode($newSong['errors'])]);
            }

            return Json::encode([
                'success'=>'Файл загружен',
                'song'=>$newSong,
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }



    /**
     * @return array
     */
    public function actionGetRadioNewData()
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $query = Song::find();
        $query->where(
            ['AND',
                ['artist_id'=>$artist->id],
                ['AND',
                    ['OR',
                        ['IS', 'radio_status', null],
                        ['radio_status' => 'undone'],
                    ],
                    ['OR',
                        ['AND',
                            ['IS', 'premium_ru_status', null],
                            ['IS', 'premium_world_status', null],
                        ],
                        ['AND',
                            ['IS', 'premium_ru_status', null],
                            ['not in', 'premium_world_status', [
                                Song::STATUS_ACTIVE,
                                Song::STATUS_EXPIRED
                            ]],
                        ],
                        ['AND',
                            ['not in', 'premium_ru_status', [
                                Song::STATUS_ACTIVE,
                                Song::STATUS_EXPIRED
                            ]],
                            ['IS', 'premium_world_status', null],
                        ],
                    ],
                ],
            ]
        );
        $songs = $query->select([
                'id',
                'name',
                'name_hash',
                'status',
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
            ])
            ->asArray()
            ->all();

        return [
            'songs'=>$songs,
//            'styles'=>Menu::tree(Menu::ROOT_MUSIC_STYLES_URL),
//            'languages'=> Language::find()->all(),
        ];
    }



    /**
     * @return array
     */
    public function actionSubmitRadioAllTracks($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

      //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}
        $song->attributes = Yii::$app->request->post('song');

        if ($song->vocal === false) {
            $song->vocal = null;
        }
        if ($song->adult_text === false) {
            $song->adult_text = null;
        }

        if ($song->save()) {
            return $song->submitToRadioAllTracks();
        } else {
            return ['error'=>'Ошибка: '.Json::encode($song->errors), 'message'=>$song->errors];
        }
    }



    /**
     * @return array | boolean
     */
    public function actionMarkSongToRadioAllTracks($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

      //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}

        $song->radio_status = Song::STATUS_UNDONE;
        $song->save();

        return true;
    }

    /**
     * @return array | boolean
     */
    public function actionMarkSongToRadioSelectedRu($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

      //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}

        $song->premium_ru_status = Song::STATUS_UNDONE;
        $song->save();

        return true;
    }

    /**
     * @return array | boolean
     */
    public function actionMarkSongToRadioSelectedWorld($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

      //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}

        $song->premium_world_status = Song::STATUS_UNDONE;
        $song->save();

        return true;
    }

    /**
     * @return array
     */
    public function actionSubmitRadioSelectedRu($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}
        $song->attributes = Yii::$app->request->post('song');

        if ($song->vocal === false) {
            $song->vocal = null;
        }
        if ($song->adult_text === false) {
            $song->adult_text = null;
        }

        if ($song->save()) {
            return $song->submitToRadioSelectedRu();
        } else {
            return ['error'=>'Ошибка: '.Json::encode($song->errors), 'message'=>$song->errors];
        }
    }

    /**
     * @return array
     */
    public function actionSubmitRadioSelectedWorld($id)
    {
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        //  с проверкой прав артиста
        $song = Song::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id
        ]);
        if (!$song) {return ['error'=>'не найден трек'];}
        $song->attributes = Yii::$app->request->post('song');

        if ($song->vocal === false) {
            $song->vocal = null;
        }
        if ($song->adult_text === false) {
            $song->adult_text = null;
        }

        if ($song->save()) {
            return $song->submitToRadioSelectedWorld();
        } else {
            return ['error'=>'Ошибка: '.Json::encode($song->errors), 'message'=>$song->errors];
        }
    }
}