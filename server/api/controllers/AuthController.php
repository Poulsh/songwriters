<?php
namespace api\controllers;

use common\models\Artist;
use common\models\LoginForm;
use common\models\Media;
use common\models\Menu;
use common\models\Radio;
use common\models\User;
use common\models\WebsocketTicket;
use yii\filters\AccessControl;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

class AuthController extends RestController
{

    private $authActions = [
        'logout',
        'ws-ticket',
    ];

    private $unauthActions = [
        'signup',
        'signup-data',
        'login',
        'options',
        'verification',
        'password-reset',
        'change-password',
    ];

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

//            'contentNegotiator' => [
//                'class' => ContentNegotiator::className(),
//                'formats' => [
//                    'application/json' => Response::FORMAT_JSON,
//                ],
//            ],
            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'only' =>  $this->authActions,
            ],

            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => $this->unauthActions,
                        'allow'   => true
                    ],
                    [
                        'actions' => $this->authActions,
                        'allow'   => '@'
                    ],

                ],
            ],

        ]);
    }


    public function actionLogin($isAdmin = false)
    {

        $oLoginModel = new LoginForm();
        $oLoginModel->attributes = Yii::$app->request->post();
        $isLogin = $oLoginModel->login();


        if ($isAdmin && $isLogin && !Yii::$app->user->can('Admin')) {
            $isLogin = false;
            $oLoginModel->addError('password','Недостаточно прав');
        }

        if ($isLogin) {
            $user = $oLoginModel->getUser();
            $authToken = $user['access_token'];
            $ticket = WebsocketTicket::create();
            $user['access_token'] = null;

            return [
                'success' => 'Вы успешно авторизовались',
                'user'=>$user,
                'ws_ticket'=>$ticket,
                'token'=>$authToken,
                'artist'=>Artist::find()
                    ->where(['user_id'=>$user->id])
                    ->select(['name','image'])
                    ->asArray()
                    ->one(),
                'radio'=> Radio::find()
                    ->where(['user_id'=>$user->id])
                    ->select(['name','image'])
                    ->asArray()
                    ->one(),
                'media'=> Media::find()
                    ->where(['user_id'=>$user->id])
                    ->select(['name','image'])
                    ->asArray()
                    ->one(),

            ];
        } else {
            if ($oLoginModel->errors) {
                $user = User::findOne(['email'=>$oLoginModel->email]);
                if ($user) {
                    if ($user->status==User::STATUS_EMAIL_VERIFICATION) {
                        return ['error'=>'Доступ на стадии подтвеждения email. <br>На адрес отправлено письмо с ссылкой на подтверждение.'];
                    }
                    if ($user->status==User::STATUS_ON_CHECK_BY_ADMIN) {
                        return ['error'=>'Доступ на стадии подтвеждения администрацией. <br>После согласования на указанный email придет оповещение.'];
                    }

                    if ($user->status==User::STATUS_DECLINED_RADIO) {
                        return ['error'=>'Пользователь найден, но доступ отклонен.'];
                    }

                    return ['error'=>'Пользователь найден, но доступ не активен.  <br>Обратитесь к администрации'];
                }
                return['error'=> 'Неправильный логин или пароль', 'sdf'=>$oLoginModel];
            }
        }


    }


    public function actionLogout()
    {
        return ['success' => 'Вы успешно вышли из аккаунта'];
    }


    public function actionSignup()
    {
        $postUser = Yii::$app->request->post('user');
        $exist = User::find()->where(['email'=>$postUser['email']])->one();

        if ($exist) {
            return ['error' => \Yii::$app->language=='ru-RU'?
                'пользователь с email '.$postUser['email'].' уже зарегистрирован':
                'user with email '.$postUser['email'].' already exist'];
        } else {
            $user = User::create(Yii::$app->request->post('user'));
            $styles = Yii::$app->request->post('music_styles');
            if ($user) {
                if (Yii::$app->request->post('isUserArtist')) {
                    $attr=Yii::$app->request->post('artist');
                    $attr['user_id'] = $user->id;
                    $attr['country'] = Yii::$app->request->post('country');
                    $attr['city'] = Yii::$app->request->post('city');
                    $artist = Artist::create($attr);
                    $artist->addMusicStyle($styles);
                }
                if (Yii::$app->request->post('isUserMedia')) {
                    $attr=Yii::$app->request->post('media');
                    $attr['user_id'] = $user->id;
                    $attr['country'] = Yii::$app->request->post('country');
                    $attr['city'] = Yii::$app->request->post('city');
                    $media = Media::create($attr);
                    $media->addMusicStyle($styles);
                }
                if (Yii::$app->request->post('isUserRadio')) {
                    $attr=Yii::$app->request->post('radio');
                    $attr['user_id'] = $user->id;
                    $attr['country'] = Yii::$app->request->post('country');
                    $attr['city'] = Yii::$app->request->post('city');
                    $radio = Radio::create($attr);
                    $radio->addMusicStyle($styles);
                }

                return [
                    'success' => \Yii::$app->language=='ru-RU'?
                        'На email '.$user['email'].' отправлен запрос на подтверждение. <br>Подтвердите email для авторизации на сайте':
                        'The confirmation request was sent to '.$user['email'].'. <br>Please confirm email for login',
                    'redirect_to_login'=>true,
                   ];
            } else {

                if (isset($user->errors['firstName'])) {
                    return ['error' => \Yii::$app->language=='ru-RU'?
                        'Имя пользователя '. $user['firstName'].' уже занято. Попробуйте другое':
                        'The name '. $user['firstName'].' already exists. Please try another'];
                } elseif (isset($user->errors['email'])){
                    return ['error' => \Yii::$app->language=='ru-RU'?
                        'пользователь с email '. $user['email'].' уже зарегистрирован. <br> Проверьте почту или зарегистриуйте другой email':
                        'user with email '. $user['email'].' already exists. <br> Please check email or sign in another email'];
                } else {
                    return ['error' => \Yii::$app->language=='ru-RU'?
                        'Ошибка добавления нового пользователя. Для регистрации свяжитесь с нами по телефону. Подробнее об ошибке:'.print_r($user->errors,true):
                        'Error found. Please connect us. Error details:'.print_r($user->errors,true)];
                }
            }
        }


    }

    /**
     * Верификация аккаунта
     *
     * @param $token
     *
     * @return array
     * @throws \yii\base\ErrorException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionVerification($token) {
        $user = User::verification($token);
        if ($user ) {
            if (isset($user['onRadioConfirmation']) ) { //  радио
                return $user;
            } else { // не радио
                $authToken = $user['access_token'];
                $user['access_token'] = null;
                return [
                    'success' => \Yii::$app->language=='ru-RU'?'email успешно подтвержден':'email successfully confirmed',
                    'user'=>$user,
                    'token'=>$authToken
                ];
            }

        } else {
            return [
                'error' => \Yii::$app->language=='ru-RU'?'Ошибка подтверждения':'Confirmation error',
//                'errors'=>$user->errors;
            ];
        }

    }

    public function actionSignupData()
    {
        return ['musicStyles'=>\common\models\Menu::tree(Menu::ROOT_MUSIC_STYLES_URL),];
    }

    public function actionOptions(){
        return ['code'=>200];
    }

    public function actionWsTicket()
    {
        return [
            'ws_ticket'=>WebsocketTicket::create(),
        ];
    }


    /**
     * Сброс пароля
     * @param $email
     * @return array
     */
    public function actionPasswordReset() {
        return User::passwordReset(Yii::$app->request->post('email'));
    }

    /**
     * Изменение пароля
     * @param $email
     * @return array
     */
    public function actionChangePassword() {
        return User::changePasswordByToken(Yii::$app->request->post('token'),Yii::$app->request->post('password'));
    }
}


