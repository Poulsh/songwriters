<?php

namespace api\controllers;

use api\controllers\RestController;
use common\models\Artist;
use common\models\Imagefiles;
use common\models\Language;
use common\models\Media;
use common\models\Menu;
use common\models\Radio;
use common\models\Song;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class MediaController extends RestController
{

    private $mediaActions = [
        'media-home',
        'update-my-info',
        'upload-avatar',
    ];


    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'except' => [ 'options'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->mediaActions,
                        'roles' => [User::ROLE_M],
                    ],
                ],
            ],




        ]);
    }



    /**
     * @return array
     */
    public function actionMediaHome()
    {
        $model = Media::findOne(['user_id'=>Yii::$app->user->id]);
        return ['media'=>$model];
    }


    /**
     * @return array
     */
    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateMyInfo()
    {
        $model = Media::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$model) throw new NotFoundHttpException();
        $oModel =  Media::edit($model->id, \Yii::$app->request->post('media'));
        if (!$oModel->errors) {
            return [
                'success'=>'Изменения сохранены',
                'media'=>$oModel,
            ];
        } else {
            return [
                'error'=>'ошибка сохранения',
                'message'=>$oModel->errors,
            ];
        }
    }


    //  в restController отключена трансляция в json для работы с файлом
    public function actionUploadAvatar()
    {
        $model = Media::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$model) throw new NotFoundHttpException();

        $newImage = Imagefiles::uploadImage(Imagefiles::TYPE_MEDIA_AVATAR,$model->id);
        if ($newImage) {
            if (isset($newImage['error'])) {
                return Json::encode(['error'=>'ошибка загрузки - '.$newImage['error']]);
            }

            return Json::encode([
                'success'=>'Изображение загружено',
                'media'=>$model
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }



}