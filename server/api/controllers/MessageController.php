<?php

namespace api\controllers;

use api\controllers\RestController;

use common\models\Menu;
use common\models\Message;
use common\models\MessageDialog;
use common\models\Notification;

use common\models\User;

use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class MessageController extends RestController
{

    private $authUserActions = [
        'index',
        'send',
        'artist-radiosec-dialog-by-subject',
        'artist-premiumsec-dialog-by-subject',
        'reply',
        'artist-dialog',

    ];



    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->authUserActions,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ]);
    }



    /**
     * @return array
     */
    public function actionIndex()
    {
        return ['success'=>'в контролере'];
    }


    public function actionSend()
    {
        return Message::create(\Yii::$app->request->post(),true);
    }

    public function actionArtistRadiosecDialogBySubject()
    {
        $post = Yii::$app->request->post('subject');
        $dialog = MessageDialog::getArtistDialogBySubjectAndMarkAsSeen(
            $post['subject_type'], $post['subject_id']);
        return [
            'dialog'=>$dialog,
        ];
    }


        public function actionArtistDialog($id)
    {
        $dialog = MessageDialog::getArtistDialogAndMarkAsSeen($id);
        return [
            'dialog'=>$dialog,
        ];
    }

        public function actionArtistPremiumsecDialogBySubject()
    {
        $post = Yii::$app->request->post('subject');
        $dialog = MessageDialog::getArtistDialogBySubjectAndMarkAsSeen(
            $post['subject_type'], $post['subject_id']);
        return [
            'dialog'=>$dialog,
        ];
    }


    /**
     * @throws
     */

    public function actionReply($id)
    {
        return Message::addReplyToDialog($id,\Yii::$app->request->post());
    }
//

//
//
//    /**
//     * @return array
//     */
//    public function actionDialogs()
//    {
//
//        $dialogs = MessageDialog::getMyDialogs();
//        $attr = [];
//        $attr['totalMessagesQnt']=true;
//        $attr['priceOffers']=true;
//
//        $checkSum = Notification::userCheckSum($attr);
//
//        return [
//            'dialogs'=>$dialogs,
//            'userCheckSum'=>$checkSum,
//        ];
//    }
//    /**
//     * @return array
//     */
//    public function actionMessagesOfDialog($id)
//    {
//        $dialog = MessageDialog::getAndMarkAsSeen($id);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(), // messagesNotSeen return by default
//        ];
//    }
//
//
//
//    /**
//     * @return array
//     */
//    public function actionMyDialogBySubject()
//    {
//        $dialog = MessageDialog::getMyBySubjectAndMarkAsSeen(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id']);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(), // messagesNotSeen return by default
//        ];
//    }
//
//    /**
//     * @return array
//     */
//    public function actionSellerDialogBySubject()
//    {
//        $dialog = MessageDialog::getSellerDialogBySubjectAndMarkAsSeen(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id'],
//            Yii::$app->user->id);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(), // messagesNotSeen return by default
//        ];
//    }
//
//    /**
//     * @return array
//     */
//    public function actionBuyerDialogBySubject()
//    {
//        $dialog = MessageDialog::getBuyerDialogBySubjectAndMarkAsSeen(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id'],
//            Yii::$app->user->id);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(), // messagesNotSeen return by default
//        ];
//    }
//
//
//    /**
//     * @return array
//     */
//    public function actionPersonalDialogByCompanionId($id)
//    {
//        $dialog = MessageDialog::getPersonalDialogByCompanionId($id);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(),
//        ];
//    }
//
//    /**
//     * @return array
//     */
//    public function actionMyDialogToShopByShopId($id)
//    {
//        $dialog = MessageDialog::getMyDialogToShopByShopId($id);
//        return [
//            'dialog'=>$dialog,
//            'userCheckSum'=>Notification::userCheckSum(),
//        ];
//    }
//
//
//
//
//    /**
//     * @return array
//     */
//    public function actionMyMessagesQntBySubject()
//    {
//        return MessageDialog::getMyMessagesQntBySubject(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id']);
//    }
//
//    /**
//     * @return array
//     */
//    public function actionSellerMessagesQntBySubject()
//    {
//        return MessageDialog::getSellerMessagesQntBySubject(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id'],
//            Yii::$app->user->id);
//    }
//
//
//    /**
//     * @return array
//     */
//    public function actionBuyerMessagesQntBySubject()
//    {
//        return MessageDialog::getBuyerMessagesQntBySubject(
//            Yii::$app->request->post('subject')['subject_type'],
//            Yii::$app->request->post('subject')['subject_id'],
//            Yii::$app->user->id);
//    }
//
//    /**
//     * @return array
//     */
//    public function actionMarkAsSeen($id)
//    {
//        $message = Message::findOne(['id'=>$id]);
//        if (!$message) throw new NotFoundHttpException('Сообщение не найдено');
//        if ($message->to_user_id != Yii::$app->user->id){
//            $dialog = $message->dialog;
//            if ($dialog->author_id != Yii::$app->user->id &&
//                $dialog->buyer_id != Yii::$app->user->id &&
//                $dialog->seller_id != Yii::$app->user->id ) {
//                throw new ForbiddenHttpException('Нет доступа');
//            }
//        }
//
//        $message->status = Message::STATUS_SEEN;
//        if ($message->save() && !$message->errors) {
//            return ['success'=> 'сообщение отмечено прочитанным'];
//        } else {
//            return ['error'=> 'ошибка изменения статуса сообщения','message'=>json_encode($message->errors)];
//        }
//    }
//
//
//    /**
//     * @return array
//     */
//    public function actionSendProductItemPriceOffer()
//    {
//        return Message::addProductItemPriceOffer( Yii::$app->request->post('Offer'));
//    }
//
//   /**
//     * @return array
//     */
//    public function actionSetProductItemPriceOfferStatus($id)
//    {
//        return Message::setProductItemPriceOfferStatus($id, Yii::$app->request->post('status'));
//    }
//


}


