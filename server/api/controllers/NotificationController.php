<?php

namespace api\controllers;

use api\controllers\RestController;
use common\models\Brand;
use common\models\Country;
use common\models\Follow;
use common\models\Image;
use common\models\Menu;
use common\models\Message;
use common\models\MessageDialog;
use common\models\Notification;
use common\models\NotificationSearch;
use common\models\Order;
use common\models\Price;
use common\models\Product;
use common\models\ProductCondition;
use common\models\ProductItem;
use common\models\ProductItemElasticSearch;
use common\models\Shop;
use common\models\User;
use common\models\Visit;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class NotificationController extends RestController
{

    private $authUserActions = [
        'index',
        'mark-as-seen',
        'mark-as-seen-all',
    ];



    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'only' => $this->authUserActions,
            ],

            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->authUserActions,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ]);
    }



    /**
     * @return array
     */
    public function actionIndex()
    {
//        return['success'=>'index of notify'];

        $res=[];

        $res['notifications'] =  NotificationSearch::getNotSeenToUser(\Yii::$app->user->id);
        $res['userCheckSum'] =  User::checkSum(\Yii::$app->user->id);

        return $res;
    }

    public function actionMarkAsSeen($id)
    {
        $aModel = Notification::findOne($id);
        $aModel->status = Notification::STATUS_CLOSED;
        $aModel->save();
        if (!$aModel->errors) {
            return ['success'=>'оповещение отмечено прочитанным'];
        } else {
            return ['error'=>'ошибка', 'message'=>$aModel->errors];
        }

    }

    public function actionMarkAsSeenAll()
    {
        return  Notification::markAsSeenAll(Yii::$app->user->id);

    }

}


