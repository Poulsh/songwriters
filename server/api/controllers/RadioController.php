<?php

namespace api\controllers;

use api\controllers\RestController;
use common\models\Artist;
use common\models\Comment;
use common\models\Imagefiles;
use common\models\Language;
use common\models\Menu;
use common\models\Playlist;
use common\models\PlaylistItem;
use common\models\PlaylistItemSearch;
use common\models\Radio;
use common\models\Song;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class RadioController extends RestController
{

    private $radioActions = [
        'radio-home',
        'update-my-info',
        'upload-avatar',
        'get-all-tracks-feed',
        'get-selected-tracks-feed',
        'create-playlist',
        'get-my-playlists',
        'get-my-playlist',
        'add-song-to-playlist',
        'remove-item-from-playlist',
        'delete-playlist',
        'move-song-to-playlist',
        'edit-playlist',
        'get-song',
        'comment-song',
        'get-artist',
    ];


    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'except' => [ 'options'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->radioActions,
                        'roles' => [User::ROLE_R],
                    ],
                ],
            ],




        ]);
    }



    /**
     * @return array
     */
    public function actionRadioHome()
    {
        $model = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        return ['radio'=>$model];
    }


    /**
     * @return array
     */
    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdateMyInfo()
    {
        $model = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$model) throw new NotFoundHttpException();
        $oModel =  Radio::edit($model->id, \Yii::$app->request->post('radio'));
        if (!$oModel->errors) {
            return [
                'success'=>'Изменения сохранены',
                'radio'=>$oModel,
            ];
        } else {
            return [
                'error'=>'ошибка сохранения',
                'message'=>$oModel->errors,
            ];
        }
    }


    //  в restController отключена трансляция в json для работы с файлом
    public function actionUploadAvatar()
    {
        $model = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$model) throw new NotFoundHttpException();

        $newImage = Imagefiles::uploadImage(Imagefiles::TYPE_RADIO_AVATAR,$model->id);
        if ($newImage) {
            if (isset($newImage['error'])) {
                return Json::encode(['error'=>'ошибка загрузки - '.$newImage['error']]);
            }

            return Json::encode([
                'success'=>'Изображение загружено',
                'radio'=>$model
            ]);
        } else {
            return Json::encode(['error'=>'ошибка загрузки']);
        }
    }



    public function actionCreatePlaylist()
    {
        return Playlist::createPlaylist(Yii::$app->request->post('name'));
    }

    public function actionGetMyPlaylists()
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $playlists =  Playlist::find()->where([
            'type'=>Playlist::TYPE_PLAYLIST,
            'object_type'=>Playlist::OBJECT_TYPE_RADIO,
            'object_id'=>$radio->id,
        ])->all();
        $plArr=[];
        foreach ($playlists as $playlist) {
            $plArr[] = $playlist->toArray($playlist->scenarios()[Playlist::SCENARIO_INDEX]);
        }

        return ['playlists'=>$plArr];
    }

    public function actionGetMyPlaylist($id)
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $playlist =  Playlist::find()->where([
            'id'=>$id,
            'type'=>Playlist::TYPE_PLAYLIST,
            'object_type'=>Playlist::OBJECT_TYPE_RADIO,
            'object_id'=>$radio->id,
        ])->one();
        if (!$playlist) return ['error'=>'Плейлист не найден'];
        return ['playlist'=>$playlist->toArray($playlist->scenarios()[Playlist::SCENARIO_VIEW])];
    }

    public function actionAddSongToPlaylist()
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $post = Yii::$app->request->post();
        $playlist = Playlist::find()
            ->where([
                'id'=>$post['playlist_id'],
                'object_type'=>Playlist::OBJECT_TYPE_RADIO,
                'object_id'=>$radio->id,
            ])->one();
        if (!$playlist) return ['error'=>'Плейлист не найден'];
        return $playlist->addSong($post['song_id']);
    }

    public function actionRemoveItemFromPlaylist()
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $post = Yii::$app->request->post();
        $playlist = Playlist::find()
            ->where([
                'id'=>$post['playlist_id'],
                'object_type'=>Playlist::OBJECT_TYPE_RADIO,
                'object_id'=>$radio->id,
                ])
            ->one();
        if (!$playlist) return ['error'=>'Плейлист не найден'];
        return $playlist->removeItem($post['item_id']);
    }


    public function actionDeletePlaylist($id)
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $playlist =  Playlist::find()->where([
            'id'=>$id,
            'type'=>Playlist::TYPE_PLAYLIST,
            'object_type'=>Playlist::OBJECT_TYPE_RADIO,
            'object_id'=>$radio->id,
        ])->one();
        if (!$playlist) return ['error'=>'Плейлист не найден'];
        $playlist->delete();
        return ['success'=>'Плейлист удален'];
    }

    public function actionMoveSongToPlaylist()
    {
        $post = Yii::$app->request->post();

        return Playlist::moveSongToPlaylist($post['song_id'],$post['to_playlist_id'],$post['from_playlist_id']);
    }


    /*
     * Выборка All Tracks для Radio
     * @return array
     * */
    public function actionGetAllTracksFeed()
    {
        $params =\Yii::$app->request->post('params');
        $params = self::setPage($params);
        return (new PlaylistItemSearch())->apiSearchInAllTracksPlaylist(['PlaylistItemSearch'=>$params]);
    }

    /*
     * Выборка Selected Tracks для Radio
     */
    public function actionGetSelectedTracksFeed()
    {
        $params =\Yii::$app->request->post('params');
        $params = self::setPage($params);
        return (new PlaylistItemSearch())->apiSearchInSelectedTracksPlaylist(['PlaylistItemSearch'=>$params]);
    }



    /**
     * @param $id
     * @return array
     */
    public function actionEditPlaylist($id)
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        $playlist =  Playlist::find()->where([
            'id'=>$id,
            'type'=>Playlist::TYPE_PLAYLIST,
            'object_type'=>Playlist::OBJECT_TYPE_RADIO,
            'object_id'=>$radio->id,
        ])->one();
        if (!$playlist) return ['error'=>'Плейлист не найден'];

        $post = Yii::$app->request->post();
        if (isset($post['name'])) {
            $playlist->name = $post['name'];
        }
        if ($playlist->save()) {
            return ['success'=>'плейлист изменен','playlist'=>$playlist];
        } else {
            if (isset($playlist['error']) && $playlist['error']!=[]) {
                return Json::encode(['error'=>'Ошибка изменения плейлиста - '.$playlist['error']]);
            }
            if (isset($playlist['errors']) && $playlist['errors']!=[]) {
                return Json::encode(['error'=>'Ошибка изменения плейлиста - '.Json::encode($playlist['errors'])]);
            }
            return ['error'=>'Ошибка изменения плейлиста '];
        }
    }

    /**
     * Один трек по хешу с комментариями
     * @return array
     */
    public function actionGetSong($hash)
    {

        $song = Song::find()
            ->where(['name_hash'=>$hash])
            ->one();
        return [
            'song'=>$song,
            'comments'=>$song->comments,
        ];
    }


    /**
     * Комментирование трека
     * @param $hash string  GET Song hash name
     * @param $text string  достаем из POST
     * @return array
     * @throws  NotFoundHttpException
     */
    public function actionCommentSong($hash)
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$radio) throw new NotFoundHttpException('Radio not found');

        $song = Song::find()
            ->where(['name_hash'=>$hash])
            ->one();
        if (!$song) throw new NotFoundHttpException('Song not found');
        $text = Yii::$app->request->post('text');

        return Comment::createSongCommentByRadio($radio->id,$song->id,$text);
    }


    /**
     * artist
     *
     * @param $id integer
     * @return array
     */
    public function actionGetArtist($id)
    {

        $artist = Artist::find()
            ->where(['id'=>$id])
            ->one();
        return [
            'artist'=>$artist,
        ];
    }
}