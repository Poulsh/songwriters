<?php
namespace api\controllers;

use Yii;
use yii\rest\Controller;

class RestController extends Controller
{

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'corsFilter'  => [
                'class' => \yii\filters\Cors::class,
                'cors'  => [
                    'Origin'                           => \Yii::$app->params['allowedDomains'],
                    'Access-Control-Request-Method'    => ['GET','POST','PATCH','PUT','DELETE'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,
                    'Access-Control-Request-Headers' => ['*'],

                ],
            ],
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'except' => [
                    //  для загрузки файлов
                    'upload-avatar',
                    'upload-song',
                    'upload-radio-song',
                    'upload-image',
                    'upload-image-to-song',
                    'upload-selected-ru-song',
                    'upload-selected-world-song',
                    'upload-new-file-of-song',
                ],
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
            'rateLimiter' => [
                'class' => \yii\filters\RateLimiter::class,
            ],
        ]);
    }


    /**
     * Метод переопределен в настройках сервера!!
     */
    public function actionOptions(){

        return ['code'=>200];
//        if (Yii::$app->getRequest()->getMethod() !== 'OPTIONS') {
//            Yii::$app->getResponse()->setStatusCode(405);
//        }
//
//        Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $this->_verbs));
    }

    /**
     * setPage() sets page to GET request for pagination
     * @param $params array
     * @return array
     */
    public static function setPage(array $params){
        if (isset($params ['page'])) {
            $_GET['page'] = (int) $params ['page'];
            if ($_GET['page'] < 1) {
                $_GET['page'] = 1;
            }
        }
        return $params;
    }


    public function beforeAction($action)
    {
//        \Yii::$app->response->headers->add('Access-Control-Allow-Headers', 'Authorization,Content-Type,App-Language');

        if (Yii::$app->request->headers->get('App-Language')=='ru') {
            \Yii::$app->language = 'ru-RU';
        } else {
            \Yii::$app->language = 'en-US';
        }

        return parent::beforeAction($action);
    }
}

