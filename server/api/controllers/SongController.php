<?php

namespace api\controllers;

use api\controllers\RestController;
use common\models\Song;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SongController extends RestController
{

    private $allowToAllActions = [
        'add-peaks',

    ];

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'except' => ['options'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => $this->allowToAllActions,
                    ],
                ],
            ],

        ]);
    }
//    public function behaviors()
//    {
//        return [
//            'corsFilter'  => [
//                'class' => \yii\filters\Cors::class,
//                'cors'  => [
//                    'Origin'                           => \Yii::$app->params['allowedDomains'],
//                    'Access-Control-Request-Method'    => ['POST','GET'],
//                    'Access-Control-Allow-Credentials' => true,
//                    'Access-Control-Max-Age'           => 3600,
//                    'Access-Control-Request-Headers' => ['*'],
//
//                ],
//            ],
//            'contentNegotiator' => [
//                'class' => ContentNegotiator::className(),
//                'formats' => [
//                    'application/json' => Response::FORMAT_JSON,
//                ],
//            ],
//            'rateLimiter' => [
//                'class' => RateLimiter::className(),
//            ],
//            'authenticator' => [
//                'class' => HttpBearerAuth::className(),
////                'class' => \app\components\auth\QueryParamAuth::className(),
//                'only' => [ 'create', 'update'],
//            ],
//
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['create', 'update'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'roles' => ['Admin'],
//                    ],
//                ],
//            ],
//
//
//        ];
//    }



    /**
     * @return \yii\data\ActiveDataProvider
     */
    public function actionIndex()
    {
        return Song::find()->all();
    }

    /**
     * @param $id
     *
     * @return Song
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        if (!($oModel = Song::findOne((int)$id))) {
            throw new NotFoundHttpException;
        } else {
            return $oModel;
        }
    }


    /**
     * @return array
     */
    public function actionCreate()
    {
        return ['error'=>'не запилено'];
//        return Song::create(\Yii::$app->request->post());
    }

    /**
     * @param $id
     *
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        return ['error'=>'не запилено'];
//        return Song::edit($id, \Yii::$app->request->post());
    }


    public function actionAddPeaks($hash)
    {

        return Song::addPeaks($hash, \Yii::$app->request->post('peaks'));
    }
}