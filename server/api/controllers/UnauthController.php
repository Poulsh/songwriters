<?php

namespace api\controllers;

use api\controllers\RestController;
use common\models\Artist;
use common\models\Comment;
use common\models\Imagefiles;
use common\models\Language;
use common\models\Menu;
use common\models\Playlist;
use common\models\PlaylistItem;
use common\models\PlaylistItemSearch;
use common\models\Radio;
use common\models\Song;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\rest\Controller;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

class UnauthController extends RestController
{



    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

//            'authenticator' => [
//                'class' => HttpBearerAuth::class,
//                'except' => ['options'],
//            ],
//
//            'access' => [
//                'class' => AccessControl::class,
//                'except' => [ 'options'],
//                'rules' => [
//                    [
//                        'allow' => true,
//                        'actions' => $this->radioActions,
//                        'roles' => [User::ROLE_R],
//                    ],
//                ],
//            ],




        ]);
    }



    /**
     * Один трек по хешу с комментариями
     * @return array
     */
    public function actionGetSong($hash)
    {

        $song = Song::find()
            ->where(['name_hash'=>$hash])
            ->one();
        return [
            'song'=>$song,
            'comments'=>$song->comments,
        ];
    }

    /**
     * artist
     *
     * @param $id integer
     * @return array
     */
    public function actionGetArtist($id)
    {

        $artist = Artist::find()
            ->where(['id'=>$id])
            ->one();
        return [
            'artist'=>$artist,
        ];
    }
}