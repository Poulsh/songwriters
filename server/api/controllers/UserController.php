<?php

namespace api\controllers;

use api\controllers\RestController;

use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\AccessControl;
use yii\helpers\Json;


class UserController extends RestController
{




    public function behaviors()
    {
        return array_merge(parent::behaviors(), [

            'authenticator' => [
                'class' => HttpBearerAuth::class,
                'except' => ['options'],
            ],

            'access' => [
                'class' => AccessControl::class,
                'except' => [ 'options'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],




        ]);
    }



    /**
     * @return array
     */
    public function actionIndex()
    {
        $model = User::findOne(['id'=>Yii::$app->user->id]);
        return ['user'=>$model];
    }


    public function actionUpdate()
    {
//        $user = Yii::$app->user->identity;
        $user = User::findOne(Yii::$app->user->id);
        $user->setScenario(User::SCENARIO_PROFILE);

        $request =  \Yii::$app->request->post('user');

        if (isset($request['first_name'])&&$request['first_name']) $user->first_name = $request['first_name'];
        if (isset($request['last_name'])&&$request['last_name']) $user->last_name = $request['last_name'];
//        if ($request['email']) $user->email = $request['email'];
//        if (isset($request['phone'])&&$request['phone']) $user->phone = $request['phone'];
//        if (isset($request['address'])&&$request['address']) $user->address = $request['address'];

//        $city_id = $request['city_id'];
//        $country_id = $request['country_id'];
        $customMessage = '';
//        if ($city_id) {
//            $city = City::findOne($city_id);
//            if (!$city) throw new NotFoundHttpException('Не найден город');
//            if ($country_id) {
//                if ($city->country_id != $country_id) {
//                    $country_id = $city->country_id;
//                    $customMessage .= 'Страна не соответствует, установлена по городу.';
//                }
//            } else {
//                $country_id = $city->country_id;
//            }
//        }
//
//        $user->city_id =  $city_id;
//        $user->country_id =  $country_id;


//        if (isset($request['telegram'])&&$request['telegram']) $user->telegram = $request['telegram'];
//        if (isset($request['whatsapp'])&&$request['whatsapp']) $user->whatsapp = $request['whatsapp'];
//        if (isset($request['skype'])&&$request['skype']) $user->skype = $request['skype'];

        $user->save();

        $user['access_token']=null;
        return [
            'success'=>$customMessage!=''?$customMessage:'Данные изменены',
            'user'=>$user,
        ];


    }


    public function actionPasswordChange()
    {
        $request =  \Yii::$app->request->post('password');
        $user =  User::findIdentityByAccessToken(Yii::$app->user->identity['access_token']);
        $user->setPassword($request);
        $user->scenario = 'profile';
        if ($user->save()) {
            return ['success'=>'Пароль изменен'];
        } else {
            return ['error'=>'Ошибка изменения пароля'];
        }
    }

    public function actionChangeLanguage()
    {
        $lang =  \Yii::$app->request->post('language');
        $user =  User::findIdentityByAccessToken(Yii::$app->user->identity['access_token']);
        $user->setLanguage($lang);
        $user->scenario = 'profile';
        if ($user->save()) {
            return ['success'=>$lang=='ru'?'Язык изменен':'The language is changed'];
        } else {
            return ['error'=>$lang=='ru'?'Ошибка изменения языка':'Error'];
        }
    }

}