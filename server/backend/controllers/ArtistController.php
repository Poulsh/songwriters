<?php

namespace backend\controllers;

use Yii;
use common\models\Artist;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Imagefiles;
use common\models\UploadImgForm;
use yii\web\UploadedFile;

use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;

/**
 * ArtistController implements the CRUD actions for Artist model.
 */
class ArtistController extends BackController
{


    /**
     * Lists all Artist models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $dataProvider = new ActiveDataProvider([
            'query' => Artist::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artist model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Artist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Artist();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous());
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Artist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Artist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Url::previous());
    }

    /**
     * Finds the Artist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Artist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Artist::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Upload images for  model with autofill corresponding model property
     */
    public function actionUploadImage()
    {
        $uploadmodel = new UploadImgForm();
        if (Yii::$app->request->isPost) {
            $uploadmodel->imageFile = UploadedFile::getInstance($uploadmodel, 'imageFile');
            $data=Yii::$app->request->post('UploadImgForm');
            $toModelProperty = $data['toModelProperty'];
            $model = Artist::find()->where(['id'=>$data['toModelId']])->one();
            $time = time();
            $filenameOrig = 'artist'.$model->id.'-'.$toModelProperty.'.'.$uploadmodel->imageFile->extension;
            if ($uploadmodel->upload($filenameOrig)) {


                // создаем нужные форматы картинок
                $webRoot = \Yii::getAlias('@webroot');

                $formats = Imagefiles::$avatarFormats;
                foreach ($formats as $key => $format) {
                    $width = $format;
                    $height = $format;
                    $filenameThumb = 'artist'.$model->id.'-'.$toModelProperty.'_'.$key.'.'.$uploadmodel->imageFile->extension;
                    Image::thumbnail( $webRoot.'/img/'.$filenameOrig, $width, $height, ManipulatorInterface::THUMBNAIL_OUTBOUND)
                        ->save( $webRoot.'/img/'.$filenameThumb, ['quality' => 85]);
                    // 'gaussian-blur'=>0.05
                    // 'sampling-factor'=> '4:2:0'
                    $imageFile = new Imagefiles();
                    $imageFile->addNew($filenameThumb);
                }

                //аргументу с названием $toModelProperty присваиваем значение имени файла
                $model->$toModelProperty = $filenameOrig;
                $model->save();
                Yii::$app->session->setFlash('success', 'Файл загружен успешно');
            } else {
                Yii::$app->session->setFlash('error', 'не получается');
            }
            return $this->redirect(Url::previous());
        }
    }
}
