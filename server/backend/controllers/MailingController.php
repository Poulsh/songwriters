<?php

namespace backend\controllers;

use common\models\Imagefiles;
use common\models\Mailing;
use common\models\UploadImgForm;
use common\models\UploadSongForm;
use Yii;
use common\models\Song;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;

use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;


/**
 * MailingController implements the mail sens actions
 */
class MailingController extends BackController
{



    /**
     * Index Page
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
//        $uploadModel = new UploadSongForm();
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => Song::find(),
//            'pagination'=> [
//                'pageSize' => 100,
//            ],
//            'sort' =>[
//                'defaultOrder'=> [
//                    'id' => SORT_DESC
//                ]
//            ]
//        ]);

        return $this->render('index', [
//            'dataProvider' => $dataProvider,
//            'uploadmodel' => $uploadModel,

        ]);


    }


    public function actionTest()
    {
        $post = Yii::$app->request->post('Mailing');
//        var_dump($post); die;

        if (Mailing::sendEmail($post['text'])) {
            Yii::$app->session->setFlash('success', 'Ога - тама');
        } else {
            Yii::$app->session->setFlash('error', 'не получилось');
        }
        return $this->redirect(Url::previous());

    }


}
