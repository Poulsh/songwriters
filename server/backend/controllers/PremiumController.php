<?php

namespace backend\controllers;

use common\models\Message;
use common\models\Playlist;
use common\models\PlaylistItem;
use common\models\PlaylistItemSearch;
use common\models\PremiumSearch;
use common\models\Song;
use Yii;
use common\models\Radio;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class PremiumController extends BackController
{


    /**
     * Lists all Radio models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $searchModel = new PlaylistItemSearch();
        $dataProvider = $searchModel->backendPremiumSearch(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionPlaylist()
    {
        Url::remember();
        $searchModel = new PlaylistItemSearch();
        $dataProvider = $searchModel->backendPremiumRuPlaylist(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['order_num' => SORT_DESC];


        return $this->render('premium_playlist', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
//    public function actionPlaylistWorld()
//    {
//        Url::remember();
//        $searchModel = new PlaylistItemSearch();
//        $dataProvider = $searchModel->backendPremiumWorldPlaylist(Yii::$app->request->queryParams);
//        $dataProvider->pagination->pageSize = 100;
//        $dataProvider->sort->defaultOrder = ['order_num' => SORT_DESC];
//
//
//        return $this->render('playlist-world', [
//            'dataProvider' => $dataProvider,
//            'searchModel' => $searchModel,
//        ]);
//    }
//    /**
//     * Displays a single Song model.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
    public function actionView($id)
    {

        Url::remember();
        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view_artist_section_item', [
            'model' => Song::findOne($item->song_id),
            'item' => $item,
        ]);
    }

    public function actionPlaylistItemView($id)
    {

        Url::remember();
        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view_common_playlist_item', [
            'model' => Song::findOne($item->song_id),
            'item' => $item,
        ]);
    }

//    public function actionAdd($id)
//    {
//        $item = PlaylistItem::findOne($id);
//        if (!$item) {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
//        $res = $item->addPremium();
//        if (isset($res['success'])) {
//            Yii::$app->session->addFlash('success', $res['success']);
//        } else {
//            Yii::$app->session->addFlash('error', $res['error']);
//        }
//        return $this->redirect(Url::previous());
//
//    }


    /**
     * Updates an existing Radio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
//        $item = PlaylistItem::findOne($id);
        $model = PlaylistItem::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * @throws BadRequestHttpException if from_to_type invalid
     */
    public function actionSendCommentOfItem($id)
    {
        $message = new Message();
        if ($message->load(Yii::$app->request->post()) && $message->sendSystemMessageToArtistForPremiumItem($id)) {
            return $this->redirect(Url::previous());
        }

        return $this->redirect(Url::previous());
    }


    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionSetExpired($id)
    {
        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $item->setPremiumExpired();

        return $this->redirect(Url::previous());


    }


    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        $item = PlaylistItem::findOne($id);
        if (!$item) throw new NotFoundHttpException('The requested item does not exist.');
       $item->activateForPremium();
        return $this->redirect(Url::previous());
    }


    /**
     * @throws NotFoundHttpException
     */
    public function actionApprove($id)
    {
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $item->approvePremium();
        return $this->redirect(Url::previous());
    }

    public function actionDecline($id)
    {
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $item->declinePremium();
        return $this->redirect(Url::previous());
    }
}
