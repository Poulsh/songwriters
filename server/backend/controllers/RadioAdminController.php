<?php

namespace backend\controllers;

use common\models\Message;
use common\models\Playlist;
use common\models\PlaylistItem;
use common\models\PlaylistItemSearch;
use common\models\RadioAdminSearch;
use common\models\Song;
use Yii;
use common\models\Radio;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RadioController implements the CRUD actions for Radio model.
 */
class RadioAdminController extends BackController
{


    /**
     * Lists all Radio models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $searchModel = new PlaylistItemSearch();
        $dataProvider = $searchModel->backendRadioSearch(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];


        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionPlaylist()
    {
        Url::remember();
        $searchModel = new PlaylistItemSearch();
        $dataProvider = $searchModel->backendRadioPlaylist(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['order_num' => SORT_DESC];


        return $this->render('playlist', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

//    /**
//     * Displays a single Song model.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
    public function actionView($id)
    {

        Url::remember();
        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view', [
            'model' => Song::findOne($item->song_id),
            'item' => $item,
        ]);
    }

    public function actionPlaylistItemView($id)
    {

        Url::remember();
        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('playlist-item-view', [
            'model' => Song::findOne($item->song_id),
            'item' => $item,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function actionActivate($id)
    {

//        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

       $item->activateForRadio();

        return $this->redirect(Url::previous());

    }

    public function actionDeactivate($id)
    {

        $this->layout = 'wavesurfer';
        $item = PlaylistItem::findOne($id);
        if (!$item) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $item->deactivateRadio();

        return $this->redirect(Url::previous());

    }
    /**
     * Updates an existing Radio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
//        $item = PlaylistItem::findOne($id);
        $model = PlaylistItem::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(Url::previous());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionMoveToTop($id)
    {
        $model = PlaylistItem::findOne($id);

        if ($model->radioMoveToTop()) {
            return $this->redirect(Url::previous());
        } else {
            throw new  BadRequestHttpException('Не получилось');
        }


    }



    public function actionSendCommentOfItem($id)
    {
        $message = Yii::$app->request->post()['Message'];

        $model = new Message();

        if ($model->load(Yii::$app->request->post()) && $model->sendSystemMessageToArtistForRadioItem($id)) {
            return $this->redirect(Url::previous());
        } else {
            throw new  BadRequestHttpException('Не получилось');
        }

    }

}
