<?php
namespace backend\controllers;

use common\models\Notification;
use common\models\User;
use common\models\WebsocketTicket;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $layout = 'login';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'web-socket','test','user-data','not-seen-notifications'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Url::remember();
        if (Yii::$app->user->can('adminPermission', [])) {
            $this->layout = 'main';
        }
        return $this->render('index');
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionWebSocket()
    {
        Url::remember();
        if (Yii::$app->user->can('adminPermission', [])) {
            $this->layout = 'main';
        }
        return $this->render('ws');
    }

//
//    public function actionTest()
//    {
//        $userId = 47;
//        $message = 'test';
//
//        $oldErrorReporting = error_reporting(); // save PHP error reporting level
//        error_reporting($oldErrorReporting ^ E_WARNING); // disable PHP warnings
//
//        $socket = stream_socket_client(Notification::TCP_PATH, $errNum, $errMessage, 30,STREAM_CLIENT_CONNECT);
//
//        error_reporting($oldErrorReporting); // restore PHP error reporting level
//
//        if (!$socket) {
//            Yii::$app->session->addFlash('error', "$errMessage ($errNum)");
//            return $this->redirect(Url::previous());
//        } else {
//            fwrite($socket, Json::encode(['user_id' => $userId, 'message' => $message])  . "\n");
//            fclose($socket);
//        }
//
//        return $this->redirect(Url::previous());
//    }




    public function actionUserData()
    {
        if (Yii::$app->user->can('adminPermission', [])) {
            $urlParts= explode(':',Yii::$app->params['wsPath']);
            $wsPath='ws:'.$urlParts[1].':'.$urlParts[2];
            return Json::encode([
                'user_id'=>Yii::$app->user->id,
                'ws_ticket'=>WebsocketTicket::create(),
                'ws_path'=> $wsPath,
                'adminCheckSum'=>User::adminCheckSum(),
            ]);
        }
    }

    public function actionNotSeenNotifications()
    {
        if (Yii::$app->user->can('adminPermission', [])) {
            return Json::encode([
                'notSeenNotesQnt'=> Notification::notSeenToAdminQnt(),
            ]);
        }
    }
}
