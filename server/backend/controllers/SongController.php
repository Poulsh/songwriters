<?php

namespace backend\controllers;

use common\models\Imagefiles;
use common\models\UploadImgForm;
use common\models\UploadSongForm;
use Yii;
use common\models\Song;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;

use Imagine\Image\ManipulatorInterface;
use yii\imagine\Image;


/**
 * SongController implements the CRUD actions for Song model.
 */
class SongController extends BackController
{
    public $layout = 'wavesurfer';


    /**
     * Lists all Song models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $uploadModel = new UploadSongForm();

        $dataProvider = new ActiveDataProvider([
            'query' => Song::find(),
            'pagination'=> [
                'pageSize' => 100,
            ],
            'sort' =>[
                'defaultOrder'=> [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'uploadmodel' => $uploadModel,

        ]);




    }

    /**
     * Displays a single Song model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Song model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Song();


        if ($model->load(Yii::$app->request->post()) ) {
            $model->language = json_encode($model->languages_array);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Song model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->languages_array = json_decode($model->language);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->language = json_encode($model->languages_array);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Song model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $prelisten = Yii::$app->basePath . '/web/uploadedsongs/listen/' . $model->name_hash .'.mp3';
        if (file_exists($prelisten)) {
            if(!unlink($prelisten)) {
                Yii::$app->session->setFlash('error', 'неполучается удалить файл предврительного прослушивания');
            }
        } else {
            Yii::$app->session->setFlash('error', 'файл предврительного прослушивания не найден');
        }

        $source =  Yii::$app->basePath . '/web/uploadedsongs/source/' . $model->path_to_file;
        if (file_exists($source)) {
            if(!unlink($source)) {
                Yii::$app->session->setFlash('error', 'неполучается удалить иходник');
            }
        } else {
            Yii::$app->session->setFlash('error', 'файл иходник не найден');
        }


        if(!$model->delete()) {
            Yii::$app->session->setFlash('error', 'неполучается удалить запись');
        } else {
            Yii::$app->session->setFlash('success', 'запись удалена');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Song model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Song the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Song::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрашиваемая информация (song '.$id.') не найдена');
    }

    /**
     * Upload images
     */
    public function actionUpload()
    {


        $uploadModel = new UploadSongForm();
        if (Yii::$app->request->isPost) {
            $postForm = Yii::$app->request->post('UploadSongForm');


//            $uploadModel->artistName = $postForm['artistName'];
//            $uploadModel->songName = $postForm['songName'];
//            $uploadModel->rightHolders = $postForm['rightHolders'];
//            $uploadModel->musicStyle1 = $postForm['musicStyle1'];
//            $uploadModel->songFile = UploadedFile::getInstance($uploadModel, 'songFile');
//            if ($uploadModel->upload()) {
//                Yii::$app->session->setFlash('success', 'аудиофайл загружен успешно');
//            }

            $file = UploadedFile::getInstance($uploadModel, 'songFile');
            Song::upload(
                $file,
                 isset($postForm['artistId'])?$postForm['artistId']:0,
                $postForm['artistName'],
                $postForm['songName'],
                $postForm['rightHolders'],
                $postForm['musicStyle1']
                );


            return $this->redirect(Url::previous());
        }
    }

    /**
     * Upload images for  model with autofill corresponding model property
     */
    public function actionUploadImage()
    {
        $uploadmodel = new UploadImgForm();
        if (Yii::$app->request->isPost) {
            $uploadmodel->imageFile = UploadedFile::getInstance($uploadmodel, 'imageFile');
            $data=Yii::$app->request->post('UploadImgForm');
            $toModelProperty = $data['toModelProperty'];
            $model = Song::find()->where(['id'=>$data['toModelId']])->one();
            $time = time();
            $filenameOrig = 'song'.$model->id.'-'.$toModelProperty.'.'.$uploadmodel->imageFile->extension;
            if ($uploadmodel->upload($filenameOrig)) {


                // создаем нужные форматы картинок
                $webRoot = \Yii::getAlias('@webroot');

                $formats = Imagefiles::$artworkFormats;
                foreach ($formats as $key =>  $format) {
                    $width = $format;
                    $height = $format;
                    $filenameThumb = 'song'.$model->id.'-'.$toModelProperty.'_'.$key.'.'.$uploadmodel->imageFile->extension;
                    Image::thumbnail( $webRoot.'/img/'.$filenameOrig, $width, $height, ManipulatorInterface::THUMBNAIL_OUTBOUND)
                        ->save( $webRoot.'/img/'.$filenameThumb, ['quality' => 100]);
                    $imageFile = new Imagefiles();
                    $imageFile->addNew($filenameThumb);
                }

                //аргументу с названием $toModelProperty присваиваем значение имени файла
                $model->$toModelProperty = $filenameOrig;
                $model->save();
                Yii::$app->session->setFlash('success', 'Файл загружен успешно');
            } else {
                Yii::$app->session->setFlash('error', 'не получается');
            }
            return $this->redirect(Url::previous());
        }
    }
}
