<?php

namespace backend\controllers;

use common\models\User;
use common\models\UserSearch;
use Yii;
use common\models\Notification;
use common\models\NotificationSearch;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements actions for User model.
 */
class UserController extends BackController
{


    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex()
    {
        Url::remember();
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        Url::remember();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Send activation link .
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSendactivationlink($id)
    {

        $user = User::findOne(['id' => $id]);
        $lang = User::getLanguageByUserID($id);
        if ($lang =='ru'){
            \Yii::$app->language = 'ru-RU';
        } else {
            \Yii::$app->language = 'en-US';
        }

        if ($user->sendEmailVerification()){
            Yii::$app->session->addFlash('success', 'Успешно отправлено');

        } else {
            Yii::$app->session->addFlash('error', 'Ошибка отправки');
        };

        return $this->redirect(Url::previous());

    }
//
//    /**
//     * Updates an existing Notification model.
//     * If update is successful, the browser will be redirected to the 'view' page.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(Url::previous());
//        }
//
//        return $this->render('update', [
//            'model' => $model,
//        ]);
//    }
//
//    /**
//     * Deletes an existing Notification model.
//     * If deletion is successful, the browser will be redirected to the 'index' page.
//     * @param integer $id
//     * @return mixed
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(Url::previous());
//    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

//      index of admin notifications
    public function actionAdminNotification()
    {
        Url::remember();
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->adminNotification(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('admin_notification', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Set status to model
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSetStatus($id,$status)
    {
        if (Notification::setStatus($id,$status)) {
            return $this->redirect(Url::previous());
        } else {
            return $this->redirect(Url::previous());
        }
    }



    public function actionSetStatusAsync($id,$status)
    {
        $res = Notification::setStatus($id,$status);
        if ($res) {
            if ($status == Notification::STATUS_NOT_SEEN) {
                $class = ' class="text-danger" ';
            } else {
                $class = ' class="text-secondary" ';
            }
            return    \yii\helpers\Json::encode([
                'status'=>$status,
                'htmlValue'=>'<span '.$class.'>'.$status.'</span>',
            ]);
//            return    true;
        } else {
            return false;
        }
    }


    public function actionGoToLink($id,$link)
    {
        $notification = Notification::findOne(['id'=>$id]);
        $notification->status = Notification::STATUS_CLOSED;
        $notification->save();
        return $this->redirect($link);
    }



    public function actionSetAllSeen()
    {
        Notification::setAllAdminSeen();
        return $this->redirect(Url::previous());
    }
}
