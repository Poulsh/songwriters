<?php

use common\models\Song;
use common\models\UploadSongForm;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Assign;
use \yii\widgets\ActiveForm;

$uploadmodel = new \common\models\UploadImgForm();

$uploadSongModel = new UploadSongForm();

/* @var $this yii\web\View */
/* @var $model common\models\Artist */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Artists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if ($model->image) {
        $nameParts = explode('.', $model->image);
        echo '<p>'.Html::img('/img/'. $nameParts[0].'_md.'.$nameParts[1], ['class'=>'img']).'</p>';
    }
    ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'page_id',
            'name',
            'country',
            'city',
            'hrurl',
            'main_info:ntext',
            'comment:ntext',
            'image',
//            [
//                'attribute'=>'image',
//                'value' => function($model)
//                {
//                    if ($model->image) {
//                        $nameParts = explode('.', $model->image);
//                        return Html::img('/img/'. $nameParts[0].'_md.'.$nameParts[1], ['class'=>'img']);
//                    }
//
//                },
//                'format'=> 'html',
//            ],
            'image_alt',
            'status',
            'view',
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>


<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <h4>Image Upload</h4>
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['/artist/upload-image'],
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($uploadmodel, 'toModelProperty')->dropDownList([
                    'image'=>'Image',
                ])->label(false) ?>
                <?= $form->field($uploadmodel, 'imageFile')->fileInput()->label(false) ?>
                <?= $form->field($uploadmodel, 'toModelId')->hiddenInput(['value'=>$model->id])->label(false) ?>
                <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>

</section>

<!-- стили -->
<section>
    <div class="row">
        <div class="col-sm-12">
            <h4> Стили </h4>
            <?php
            $styles = $model->styles;

            $stylesDataProvider = new \yii\data\ArrayDataProvider([
                'models'=>$styles,
            ]);
            echo yii\grid\GridView::widget([
                'dataProvider' => $stylesDataProvider,
                'emptyText' => '',
                'columns'=>[
                'name',
                    [
                        'class' => \yii\grid\ActionColumn::class,
                        'buttons' => [
                            'delete'=>function($url,$item ) use ($model)  {
                                $assign = Assign::find()->where([
                                    'type'=>Assign::TYPE_MUSIC_STYLE,
                                    'status'=>Assign::STATUS_ACTIVE,
                                    'other_id'=>$item['id'],
                                    'artist_id'=>$model['id'],
                                ])->one();
                                $newUrl = Yii::$app->getUrlManager()->createUrl(['/assign/delete','id'=>$assign['id']]);
                                return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $newUrl,
                                    [
                                        'title' => Yii::t('yii', 'Удалить'),
                                        'data-confirm' =>'Точно удалить?',
                                        'data-pjax' => '0',
                                        'data-method'=>'post'
                                    ]);
                            },
                            'view'=>function($url,$model){
                                return false;
                            },
                            'update'=>function($url,$model){
                                return false;
                            },
                        ]
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
</section>

<section>
    <div class="col-xs-12 text-left">
        <h4>Song Upload</h4>
    </div>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['/song/upload'],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="col-sm-3">
        <?= $form->field($uploadSongModel, 'songName')->textInput() ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($uploadSongModel, 'artistName')->textInput(['value'=>$model->name]) ?>
        <?= $form->field($uploadSongModel, 'artistId')->hiddenInput(['value'=>$model->id])->label(false) ?>
    </div>
    <div class="col-sm-3">
        <?= $form->field($uploadSongModel, 'rightHolders')->textInput() ?>
    </div>
    <!--        <input type="hidden" name="_csrf" value="--><?//=Yii::$app->request->getCsrfToken()?><!--" />-->
    <div class="col-sm-3">
        <?= $form->field($uploadSongModel, 'musicStyle1')
            ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()
                ->where(['tree'=>1])
                ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                ->all(), 'id',function($model) {return str_repeat('-', $model->depth).$model['name'];}
            ),['prompt'=>'Выберите стиль'])
            ->label('Стиль1') ?>
    </div>
    <div class="col-sm-4 col-sm-offset-4 text-center ">

        <?= $form->field($uploadSongModel, 'songFile')->fileInput()->label(false) ?>

        <?= Html::submitButton('Upload', ['class' => 'btn btn-success mb50']) ?>
        <?php ActiveForm::end() ?>
    </div>
    <div class="col-sm-3">

    </div>
</section>


<section>
    <div class="col-xs-12">


        <?php
        $dataProvider = new ActiveDataProvider([
            'query' => Song::find()->where(['artist_id'=>$model->id]),
            'pagination'=> [
                'pageSize' => 100,
            ],
            'sort' =>[
                'defaultOrder'=> [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'emptyText' => 'Не найдено загруженных треков',
            'columns' => [

                'id',
                'name',
                'artist_name',
                'name_hash',
                'rightholders:ntext',
                [
                    'attribute'=>'music_style1_id',
                    'value' => function($model)
                    {
                        $cat=\common\models\Menu::find()->where(['id'=>$model->music_style1_id])->one();
                        return $cat?$cat->name:null;
                    },
                    'format'=> 'html',
                ],
//                    'music_style2_id',
                [
                    'attribute'=>'music_style2_id',
                    'value' => function($model)
                    {
                        $cat=\common\models\Menu::find()->where(['id'=>$model->music_style2_id])->one();
                        return $cat?$cat->name:null;
                    },
                    'format'=> 'html',
                ],
//                    'music_style3_id',
                [
                    'attribute'=>'music_style3_id',
                    'value' => function($model)
                    {
                        $cat=\common\models\Menu::find()->where(['id'=>$model->music_style3_id])->one();
                        return $cat?$cat->name:null;
                    },
                    'format'=> 'html',
                ],
                'path_to_file',
                'status',

//                ['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => \yii\grid\ActionColumn::class,
                    'buttons' => [

                        'view'=>function($url,$model){
                            $newUrl = Yii::$app->getUrlManager()->createUrl([
                                '/song/view',
                                'id'=>$model['id'],
                            ]);
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $newUrl,
                                [
                                    'title' => Yii::t('yii', 'открыть'),
                                    'data-pjax' => '0',
                                    'data-method' => 'post'
                                ]);
                        },
                        'update'=>function($url,$model){
                            $newUrl = Yii::$app->getUrlManager()->createUrl([
                                '/song/update',
                                'id'=>$model['id'],
                            ]);
                            return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-pencil"></span>', $newUrl,
                                [
                                    'title' => Yii::t('yii', 'редактировать'),
                                    'data-pjax' => '0',
                                    'data-method' => 'post'
                                ]);
                        },
                        'delete'=>function($url,$model){
                            return false;
                        },
                    ]
                ],
            ],
        ]); ?>

    </div>
</section>