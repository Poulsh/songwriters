<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AssignSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="assign-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'artist_id') ?>

    <?= $form->field($model, 'song_id') ?>

    <?= $form->field($model, 'radio_id') ?>

    <?php // echo $form->field($model, 'media_id') ?>

    <?php // echo $form->field($model, 'playlist_id') ?>

    <?php // echo $form->field($model, 'tender_id') ?>

    <?php // echo $form->field($model, 'other_id') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
