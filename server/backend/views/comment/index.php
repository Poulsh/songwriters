<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Comment;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
<!--        --><?//= Html::a('Create Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="grid-scrollable">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

                'id',
                ['attribute'=>'text',
                    'format'=> 'html',
                    'contentOptions' => ['style' => 'width:25%;line-height:1.0; white-space: normal;'],
                    'value' => function($data) {
                        return '<small>'.nl2br($data->text).'</small>' ;
                    },
                ],
//            'object_type',
                [
                    'label'=> 'Object Type',
                    'attribute'=>'object_type',
                    'value' => function($model)
                    {
                        if ($model->object_type == Comment::OBJECT_TYPE_SONG) {
                            return 'song' ;
                        } else  {
                            $model->object_type ;
                        }
                    },
                    'format'=> 'html',
                    'filter' => [Comment::OBJECT_TYPE_SONG=>'song'],
                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
                ],
//            'object_id',
                [
                    'attribute'=>'object_id',
                    'value' => function($model)
                    {
                        if ($model->object_type == Comment::OBJECT_TYPE_SONG) {
                            return Html::a($model->object_id,'/song/view?id='.$model->object_id) ;
                        } else  {
                            $model->object_id ;
                        }
                    },
                    'format'=> 'html',
                ],
//            'author_type',
                [
                    'label'=> 'Author Type',
                    'attribute'=>'author_type',
                    'value' => function($model)
                    {
                        if ($model->author_type == Comment::AUTHOR_TYPE_RADIO) {
                            return 'radio' ;
                        } else if ($model->author_type == Comment::AUTHOR_TYPE_ARTIST) {
                            return 'artist' ;
                        } else  {
                            $model->object_type ;
                        }
                    },
                    'format'=> 'html',
                    'filter' => [
                        Comment::AUTHOR_TYPE_RADIO=>'radio',
                        Comment::AUTHOR_TYPE_ARTIST=>'artist',
                    ],
                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
                ],
//            'author_id',
                [
                    'attribute'=>'author_id',
                    'value' => function($model)
                    {
                        if ($model->author_type == Comment::AUTHOR_TYPE_RADIO) {
                            return Html::a($model->author_id,'/radio/view?id='.$model->author_id) ;
                        } else  if ($model->author_type == Comment::AUTHOR_TYPE_ARTIST) {
                            return Html::a($model->author_id,'/artist/view?id='.$model->author_id) ;
                        } else {
                            $model->author_id ;
                        }
                    },
                    'format'=> 'html',
                ],


                'status',
//            'created_at',
                [
                    'attribute'=>'created_at', 'format'=> 'html', 'filter'=>false,
                    'value' => function($data) {
                        return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');
                    },
                ],
//            'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>

</div>
