<?php

use common\models\Notification;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<?php

//    $notseenNotes = \common\models\Notification::notSeenYetToUser(Yii::$app->user->id);
//    if (count($notseenNotes)>0) {
//        $notSeenIcon = '<i class="glyphicon glyphicon-bell"></i> '.'<sup class=" label label-warning">'.count($notseenNotes).'</sup>';
//    } else {
//    }
$notSeenNotesQnt = Notification::notSeenToAdminQnt();
$notSeenIcon = '<i class="glyphicon glyphicon-bell"></i> '.'<sup
                    id="notSeenNotesQnt" 
                    data-not-seen-notes-qnt="'.$notSeenNotesQnt.'"
                    class=" label label-warning">'.$notSeenNotesQnt.'</sup>';

//if ($notseenNotesQnt>0) {
//
//} else {
//    $notSeenIcon = '<i class="glyphicon glyphicon-bell"></i> ';
//}




$radioIcon = '<i class="glyphicon glyphicon-bullhorn"></i> ';
$premiumIcon = '<i class="glyphicon glyphicon-star"></i> ';
$supportIcon = '<i class="glyphicon glyphicon-wrench"></i> ';
$mainIcon = '<i class="glyphicon glyphicon-music"></i> ';
$logo = '<svg version="1.1" id="songwriters_logo" 
	xmlns="http://www.w3.org/2000/svg" 
	xmlns:xlink="http://www.w3.org/1999/xlink" 
	x="0px" y="0px"
	viewBox="0 0 200 40" 
	style="enable-background:new 0 0 200 40;width: 200px; height: 40px;position:relative; top: -10px;" 
	xml:space="preserve">
<style type="text/css">
	.songwriters_logo_st0{fill:#9d9d9d;}
</style>
<g >
	<g >
		<path  class="songwriters_logo_st0" d="M20.2,29.7c-2.9,0-5.1-0.6-6.5-1.9c-1.4-1.3-2.2-2.9-2.3-5h2.6c0.2,1.5,0.8,2.7,1.9,3.4
			c1.1,0.7,2.5,1.1,4.1,1.1c1.5,0,2.7-0.3,3.7-1c0.9-0.6,1.4-1.5,1.4-2.5c0-1.1-0.5-1.9-1.4-2.5c-1-0.5-2.6-1-5-1.5
			c-2.3-0.5-4-1.2-5.1-2.1c-1-0.9-1.5-2.1-1.5-3.5c0-1.7,0.7-3,2-4.1s3.1-1.6,5.3-1.6c2.3,0,4.2,0.5,5.6,1.6
			c1.4,1.1,2.1,2.6,2.3,4.5h-2.6c-0.3-2.5-1.9-3.7-5.1-3.7c-1.5,0-2.7,0.3-3.5,0.8c-0.8,0.6-1.2,1.3-1.2,2.3c0,0.9,0.3,1.5,1,1.9
			c0.7,0.4,1.9,0.8,3.7,1.2c1.8,0.4,3.3,0.8,4.3,1.2c1.1,0.4,2,1,2.7,1.8c0.8,0.8,1.2,1.9,1.2,3.3c0,1.8-0.7,3.2-2.1,4.4
			C24.3,29.1,22.5,29.7,20.2,29.7z"/>
		<path class="songwriters_logo_st0" d="M35.8,14.9c2.1,0,3.8,0.6,5.1,1.9c1.3,1.3,1.9,3.1,1.9,5.5s-0.6,4.2-1.9,5.5
			c-1.3,1.3-2.9,1.9-5.1,1.9S32,29,30.8,27.7c-1.3-1.3-1.9-3.1-1.9-5.5s0.6-4.2,1.9-5.5C32.1,15.5,33.8,14.9,35.8,14.9z M35.8,27.7
			c1.3,0,2.3-0.4,3.1-1.4c0.8-0.9,1.2-2.3,1.2-4.1s-0.4-3.2-1.2-4.1c-0.8-0.9-1.8-1.4-3.1-1.4c-1.3,0-2.3,0.4-3.1,1.4
			c-0.8,0.9-1.2,2.3-1.2,4.1s0.4,3.1,1.2,4.1C33.6,27.2,34.6,27.7,35.8,27.7z"/>
		<path class="songwriters_logo_st0" d="M47,17.1c1.3-1.4,2.8-2.2,4.4-2.2c3.3,0,5,1.8,5,5.3v9.5h-2.6v-9.4c0-1.2-0.2-2-0.6-2.5
			S52,17,51,17c-1.4,0-2.7,0.8-4,2.3v10.4h-2.6V14.8H47L47,17.1z"/>
		<path class="songwriters_logo_st0" d="M70.8,28.3c0,4.6-2.1,6.8-6.4,6.8c-1.9,0-3.3-0.4-4.4-1.2c-1.1-0.8-1.6-1.8-1.6-3.1h2.7
			c0,1.6,1.1,2.3,3.3,2.3c1.2,0,2.2-0.3,2.9-1s1-1.9,1-3.8v-0.7c-1.2,1.4-2.6,2-4.3,2c-1.9,0-3.4-0.7-4.5-2
			c-1.1-1.3-1.6-3.1-1.6-5.3c0-2.3,0.6-4.2,1.8-5.5c1.2-1.4,2.7-2,4.5-2c1.6,0,2.9,0.5,4.1,1.4v-1.1l2.6-0.3L70.8,28.3z M68.2,25.3
			v-6.8c-1.2-1.2-2.4-1.7-3.9-1.7c-1.1,0-2,0.5-2.7,1.4c-0.7,0.9-1,2.3-1,4.2c0,1.8,0.3,3.1,1,4c0.6,0.9,1.5,1.4,2.7,1.4
			C65.7,27.7,67,26.9,68.2,25.3z"/>
		<polygon class="songwriters_logo_st0" points="92.3,14.9 87.9,29.7 85.3,29.7 82.1,18.8 78.9,29.7 76.4,29.7 72,14.9 74.8,14.9 
			77.6,26.2 80.9,14.9 83.3,14.9 86.7,26.2 89.4,14.9 		"/>
		<path id="XMLID_46_" class="songwriters_logo_st0" d="M101.1,17.5c-0.4-0.1-0.9-0.2-1.6-0.2c-1.4,0-2.5,0.7-3.5,2.1v10.3h-2.6V14.9H96l0,2.3
			c1.1-1.6,2.4-2.3,3.9-2.3c0.4,0,0.8,0,1.2,0.1L101.1,17.5L101.1,17.5z"/>
		<polygon class="songwriters_logo_st0" points="104.9,29.7 102.3,29.7 102.3,15.9 104.9,15.3 		"/>
		<path class="songwriters_logo_st0" d="M113.1,29.5c-0.6,0.1-1.2,0.2-1.9,0.2c-1.1,0-1.9-0.2-2.4-0.6c-0.5-0.4-0.8-0.9-1-1.5
			c-0.1-0.6-0.2-1.4-0.2-2.4v-8.3h-1.8v-2h1.8v-3.6l2.6,0.4v3.2h2.4v2h-2.4v8.4c0,1,0.1,1.6,0.4,1.9c0.2,0.3,0.6,0.4,1.1,0.4
			c0.4,0,0.9,0,1.3-0.1L113.1,29.5L113.1,29.5z"/>
		<path class="songwriters_logo_st0" d="M120.2,29.7c-2.2,0-3.9-0.6-5.3-1.9c-1.3-1.3-2-3.1-2-5.5c0-2.3,0.6-4.1,1.9-5.4
			c1.3-1.3,2.9-1.9,5-1.9c2.1,0,3.8,0.6,5.1,1.9c1.3,1.3,1.9,3.1,1.9,5.5v0.6h-11.2c0.1,1.6,0.6,2.9,1.4,3.6
			c0.8,0.8,1.9,1.2,3.1,1.2c0.9,0,1.8-0.2,2.5-0.7c0.8-0.4,1.1-1.1,1.2-1.9h2.7c-0.2,1.5-0.9,2.6-2.1,3.4
			C123.3,29.3,121.9,29.7,120.2,29.7z M124.1,20.9c-0.2-1.4-0.7-2.4-1.4-3.1c-0.8-0.7-1.7-1-2.7-1c-1,0-1.9,0.3-2.7,1
			c-0.8,0.7-1.2,1.7-1.4,3.1H124.1z"/>
		<path class="songwriters_logo_st0" d="M135.7,17.5c-0.4-0.1-0.9-0.2-1.6-0.2c-1.4,0-2.5,0.7-3.5,2.1v10.3h-2.6V14.9h2.6v2.3
			c1.1-1.6,2.4-2.3,3.9-2.3c0.4,0,0.8,0,1.2,0.1L135.7,17.5L135.7,17.5z"/>
		<path class="songwriters_logo_st0" d="M142.6,29.7c-4.1,0-6.2-1.5-6.4-4.6h2.6c0,1.7,1.2,2.6,3.7,2.6c1.1,0,1.9-0.2,2.5-0.6
			c0.6-0.4,0.9-1,0.9-1.6c0-0.6-0.3-1.1-0.8-1.4c-0.6-0.3-1.7-0.6-3.5-1s-3-0.9-3.8-1.4c-0.8-0.5-1.2-1.4-1.2-2.7
			c0-1.2,0.5-2.1,1.5-2.9c1-0.8,2.3-1.2,4-1.2c3.9,0,5.9,1.3,6,4h-2.6c-0.1-1.4-1.2-2-3.1-2c-0.9,0-1.6,0.2-2.2,0.5
			s-0.9,0.8-0.9,1.4c0,0.6,0.2,1,0.7,1.2c0.4,0.2,1.6,0.6,3.4,1c1.8,0.4,3.1,0.9,4,1.4c0.9,0.6,1.3,1.5,1.3,2.9
			c0,1.3-0.6,2.3-1.6,3.2C145.9,29.2,144.4,29.7,142.6,29.7z"/>
		<path class="songwriters_logo_st0" d="M156.6,16.9c1.2-1.4,2.6-2,4.3-2c1.9,0,3.4,0.6,4.5,1.9c1.1,1.3,1.6,3.1,1.6,5.3
			c0,2.3-0.6,4.1-1.8,5.5c-1.2,1.4-2.7,2-4.5,2c-1.6,0-2.9-0.5-4.1-1.4v6.5h-2.7V14.8l2.6,0.3L156.6,16.9L156.6,16.9z M156.6,25.9
			c1.1,1.2,2.4,1.7,3.9,1.7c1.2,0,2-0.5,2.7-1.4c0.7-0.9,1-2.3,1-4.1c0-1.8-0.3-3.1-1-3.9c-0.6-0.9-1.5-1.4-2.7-1.4
			c-1.5,0-2.8,0.8-4,2.4L156.6,25.9L156.6,25.9z"/>
		<path class="songwriters_logo_st0" d="M176.1,17.5c-0.4-0.1-0.9-0.2-1.6-0.2c-1.4,0-2.5,0.7-3.5,2.1v10.3h-2.6V14.9h2.7l0,2.3
			c1.1-1.6,2.4-2.3,3.9-2.3c0.4,0,0.8,0,1.2,0.1L176.1,17.5L176.1,17.5z"/>
		<path class="songwriters_logo_st0" d="M183.4,14.9c2.1,0,3.8,0.6,5.1,1.9c1.3,1.3,1.9,3.1,1.9,5.5s-0.6,4.2-1.9,5.5
			c-1.3,1.3-2.9,1.9-5.1,1.9s-3.8-0.6-5.1-1.9c-1.3-1.3-1.9-3.1-1.9-5.5s0.6-4.2,1.9-5.5C179.6,15.5,181.3,14.9,183.4,14.9z
			 M183.4,27.7c1.3,0,2.3-0.4,3.1-1.4c0.8-0.9,1.2-2.3,1.2-4.1s-0.4-3.2-1.2-4.1c-0.8-0.9-1.8-1.4-3.1-1.4c-1.3,0-2.3,0.4-3.1,1.4
			c-0.8,0.9-1.2,2.3-1.2,4.1s0.4,3.1,1.2,4.1C181.1,27.2,182.1,27.7,183.4,27.7z"/>
		<circle class="songwriters_logo_st0" cx="103.6" cy="12.7" r="1.5"/>
		<circle class="songwriters_logo_st0" cx="150.7" cy="28.2" r="1.5"/>
	</g>
	<g>
		<path class="songwriters_logo_st0" d="M80.9,14.2c11.8-5.6,27.7-9.1,45-9.1c13.6,0,26.3,2.1,36.8,5.7c2.7,0.9,5.4,2.1,7.8,3.3
			c0,0-17.1-7.7-44.6-8.1C102.1,5.7,80.9,14.2,80.9,14.2z"/>
	</g>
</g>
</svg>';

NavBar::begin([
    'brandLabel' => $logo,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);

$menuItems = [
    [
        'label' => $premiumIcon,
        'url' => ['/premium/index']
    ],
    [
        'label' => $radioIcon,
        'url' => ['/radio-admin/index']
    ],
    [
        'label' => $notSeenIcon,
        'url' => ['/notification/admin-notification']
    ],
    ['label' => 'WebSocket',
        'items' => [
            ['label' => 'test', 'url' => ['/site/web-socket']],
            ['label' => 'ticket', 'url' => ['/websocket-ticket']],

        ],
        'url' => ['/site/web-socket']
    ],
    [
        'label' => $supportIcon,
        'items' => [
            ['label' => 'Users', 'url' => ['/user']],
            ['label' => 'Assigns', 'url' => ['/assign']],
            ['label' => 'Menu', 'url' => ['/menu']],
            ['label' => 'Language', 'url' => ['/language']],
            ['label' => 'notification', 'url' => ['/notification']],
            ['label' => 'Message', 'url' => ['/message']],
            ['label' => 'Message Dialog', 'url' => ['/message-dialog']],
            ['label' => 'Comment', 'url' => ['/comment']],
            ['label' => 'mail test', 'url' => ['/mailing']],


        ],
    ],
    [
        'label' => $mainIcon,
        'items' => [
            ['label' => 'Артисты', 'url' => ['/artist']],
            ['label' => 'Радио', 'url' => ['/radio']],
            ['label' => 'Музыкальный редактор', 'url' => ['/media']],
            ['label' => 'Настроение музыки', 'url' => ['/music-mood']],
            ['label' => 'Стиль музыки', 'url' => ['/menu/view?id=1']],
            ['label' => 'Конкурсы', 'url' => ['/tender']],
            ['label' => 'Треки', 'url' => ['/song']],
            ['label' => 'Imagefiles', 'url' => ['/imagefiles']],
            ['label' => 'Playlist', 'url' => ['/playlist']],
            ['label' => 'PlaylistItem', 'url' => ['/playlist-item']],


        ],
    ],
];
if (Yii::$app->user->isGuest) {
    $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
} else {
    $menuItems[] = '<li>'
        . Html::beginForm(['/site/logout'], 'post')
        . Html::submitButton(
            'Logout (' . Yii::$app->user->identity->first_name . ')',
            ['class' => 'btn btn-link logout']
        )
        . Html::endForm()
        . '</li>';
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => $menuItems,
    'encodeLabels' => false,
]);
NavBar::end();
?>