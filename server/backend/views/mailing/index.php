<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mailing';
$this->params['breadcrumbs'][] = $this->title;
$model = new \common\models\Mailing();
?>
<div >

    <h1><?= Html::encode($this->title) ?></h1>




    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => ['/mailing/test'],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'text')->textInput(['maxlength' => true])->label('email') ?>
        </div>
        <div class="col-sm-6" style="padding-top: 25px;">
            <?= Html::submitButton('Test Mailing', ['class' => 'btn btn-success']) ?>
        </div>
    </div>




    <?php ActiveForm::end(); ?>




</div>
