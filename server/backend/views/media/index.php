<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Media';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Media', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'page_id',
            'name',
            'country',
            //'city',
            //'hrurl',
            //'main_info:ntext',
            //'comment:ntext',
            //'image',
            //'image_alt',
            'status',
            //'view',
            //'created_at',
            //'updated_at',

            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'delete'=>function($url,$model){
                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/notification/create','media_id'=>$model['id']]);
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-alert"></span>', $newUrl,
                            [
                                'title' => Yii::t('yii', 'Оповестить'),
                                'data-pjax' => '0',
                                'data-method'=>'post'
                            ]);
                    },
//                    'update'=>function($url,$model){
//                        return false;
//                    },
                ]
            ],
        ],
    ]); ?>
</div>
