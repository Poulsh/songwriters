<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Assign;
/* @var $this yii\web\View */
/* @var $model common\models\Media */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if ($model->image) {
        $nameParts = explode('.', $model->image);
        echo '<p>'.Html::img('/img/'. $nameParts[0].'_md.'.$nameParts[1], ['class'=>'img']).'</p>';
    }
    ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'page_id',
            'name',
            'country',
            'city',
            'hrurl',
            'main_info:ntext',
            'comment:ntext',
            'image',
            'image_alt',
            'status',
            'view',
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>

<section>
    <div class="row">
        <div class="col-sm-12">
            <h4> Стили </h4>
            <?php
            $styles = $model->styles;
            $stylesDataProvider = new \yii\data\ArrayDataProvider([
                'models'=>$styles,
            ]);
            echo yii\grid\GridView::widget([
                'dataProvider' => $stylesDataProvider,
                'emptyText' => '',
                'columns'=>[
                    'name',
                    [
                        'class' => \yii\grid\ActionColumn::class,
                        'buttons' => [
                            'delete'=>function($url,$item ) use ($model)  {
                                $assign = Assign::find()->where([
                                    'type'=> Assign::TYPE_MUSIC_STYLE,
                                    'status'=> Assign::STATUS_ACTIVE,
                                    'other_id'=>$item['id'],
                                    'media_id'=>$model['id'],
                                ])->one();
                                $newUrl = Yii::$app->getUrlManager()->createUrl(['/assign/delete','id'=>$assign['id']]);
                                return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $newUrl,
                                    [
                                        'title' => Yii::t('yii', 'Удалить'),
                                        'data-confirm' =>'Точно удалить?',
                                        'data-pjax' => '0',
                                        'data-method'=>'post'
                                    ]);
                            },
                            'view'=>function($url,$model){
                                return false;
                            },
                            'update'=>function($url,$model){
                                return false;
                            },
                        ]
                    ],
                ],
            ]);
            ?>

        </div>
    </div>



</section>