<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MessageDialog */

$this->title = 'Create Message Dialog';
$this->params['breadcrumbs'][] = ['label' => 'Message Dialogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-dialog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
