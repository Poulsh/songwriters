<?php

use common\models\Message;
use common\models\MessageDialog;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MessageDialog */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Message Dialogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="message-dialog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'artist_id',
            'radio_id',
            'media_id',
            'subject_type',
            'subject_id',
            'name',
            'status',
            'author_id',
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>


<section>
    <div class="col-sm-8 col-sm-offset-2">

        <h6 class="text-center">Сообщения</h6>

        <?php


        if ($model->messages) {
            $dataProvider = new ActiveDataProvider([
                'query' => Message::find()->where([
                    'dialog_id'=>$model->id,
                ]),
                'pagination'=> [
                    'pageSize' => 100,
                ],
                'sort' =>[
                    'defaultOrder'=> [
                        'id' => SORT_ASC
                    ]
                ]
            ]);

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'summary'=>false,
                'rowOptions'=>function($model){
                    if($model->from_to_type ==  Message::FROM_TO_SYSTEM_TO_ARTIST){
                        return ['class' => 'system'];
                    }
                },

                'tableOptions' => [
                    'class' => 'table  _table-bordered'
                ],
                'columns' => [
//                    'id',
                    [

                        'attribute'=>'text','label'=>false,
                        'contentOptions' => ['style'=>'border:none;'],
                        'headerOptions' => ['style'=>'border:none;'],
                        'value' => function($message) use ($model)
                        {
                            $authorName ='';
                            $created_at = \Yii::$app->formatter->asDatetime($message->created_at, 'HH:mm dd/MM/yy');

                            $res='';

                            if ($message->from_to_type==Message::FROM_TO_SYSTEM_TO_ARTIST){
                                $res .= '<div class="text-right" style="line-height:1;">';
                                $res .= '<span style="font-size:.8em;font-weight:bold;color: rgba(0,0,0,.4);">Songwriters</span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);">'.$created_at.'</span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);" >'.$message->status.'</span>';
                                $res .= '</div>';

                                $res .= '<div class="bubble my" style="padding-top: 10px;text-align: right;">'.$message->text.'</div>';
                            } else {

                                $res .= '<div class="text-left" style="line-height:1;">';
                                $res .= '<span style="font-size:.8em;font-weight:bold;color: rgba(0,0,0,.4);"> Пользователь '. $message->author_id .' </span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);">'.$created_at.'</span>';
                                $res .= '</div>';
                                $res.= '<div class="bubble other" style="padding-top: 10px;text-align: left;">'.$message->text.'</div>';
                            }




                            return $res;
                        },
                        'format'=> 'html',
                    ],



                ],
            ]);


        }

        ?>





        <?php

//        $form = ActiveForm::begin([
//            'options' => [
//                'id' => 'radio_comment_form',
//                'class' => 'comment-box',
//            ],
//            'action' => '/radio-admin/send-comment-of-item?id='.$item->id,
//            'validateOnChange' => false,
//            'validateOnBlur' => false,
//        ]);
//
//        $commentModel = new Message();
//
//        echo $form->field($commentModel, 'to_id')
//            ->hiddenInput(['value'=>$model->artist->id])
//            ->label(false);
//        echo $form->field($commentModel, 'dialog_id')
//            ->hiddenInput(['value'=>$dialog?$dialog->id:null])
//            ->label(false);
//        echo $form->field($commentModel, 'from_to_type')
//            ->hiddenInput(['value'=>Message::FROM_TO_SYSTEM_TO_ARTIST])
//            ->label(false);
//
//        echo $form->field($commentModel, 'text', ['template' => '{input}{error}'])
//            ->textarea(['placeholder' => 'Текст сообщения...', 'rows' => 4, 'data' => ['comment' => 'content']]);
//
//
//        echo '<div class="text-right">'. Html::submitButton('Отправить', [
//                'class' => 'btn btn-primary comment-submit',
//            ]) . '</div>';
//
//        $form->end();

        ?>

    </div>
</section>