<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MusicMood */

$this->title = 'Create Music Mood';
$this->params['breadcrumbs'][] = ['label' => 'Music Moods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-mood-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
