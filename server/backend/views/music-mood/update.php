<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MusicMood */

$this->title = 'Update Music Mood: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Music Moods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="music-mood-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
