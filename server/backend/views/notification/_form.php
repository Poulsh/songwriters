<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\Notification;

/* @var $this yii\web\View */
/* @var $model common\models\Notification */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="notification-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'artist_id')->textInput([
                    'value' => Yii::$app->request->get('artist_id'),
                    'readonly' => Yii::$app->request->get('artist_id')?true:false,
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'radio_id')->textInput([
                'value' => Yii::$app->request->get('radio_id'),
                'readonly' => Yii::$app->request->get('radio_id')?true:false,
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'media_id')->textInput([
                'value' => Yii::$app->request->get('media_id'),
                'readonly' => Yii::$app->request->get('media_id')?true:false,
            ]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'user_id')->textInput([
                'value' => Yii::$app->request->get('user_id'),
                'readonly' => Yii::$app->request->get('user_id')?true:false,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'type')->dropDownList(Yii::$app->helpers->value2KeyValue([
                    Notification::TYPE_SONG_IN_RADIO_SECTION_CHANGE_STATUS,
                    Notification::TYPE_SONG_IN_RADIO_SECTION_NEW_COMMENT,
            ]),['prompt'=>'Тип оповещения']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'subject_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'head')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'call2action_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'call2action_link')->textInput(['maxlength' => true]) ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
