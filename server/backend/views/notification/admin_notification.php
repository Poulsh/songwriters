<?php

use common\models\Notification;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Отметить просмотренными все', ['set-all-seen'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'type',
//            'subject_id',

            ['attribute'=>'head',
                'format'=> 'html',
                'contentOptions' => ['style' => 'width:100px;line-height:1.0; white-space: normal;'],
                'value' => function($data) {
                    return '<small>'.nl2br($data->head).'</small>' ;
                },

            ],
            ['attribute'=>'text',
                'format'=> 'html',
                'contentOptions' => ['style' => 'width:25%;line-height:1.0; white-space: normal;'],
                'value' => function($data) {
                    return '<small>'.nl2br($data->text).'</small>' ;
                },
            ],
            ['attribute'=>'call2action_name', 'format'=> 'html',
                'value' => function($data) {
                        if ($data->call2action_name) {
                            return Html::a($data->call2action_name,'go-to-link?id='.$data->id.'&link='. $data->call2action_link);
                        }
                    },
            ],

            ['attribute'=>'status',
                'format'=> 'html',
                'contentOptions' => ['class' => 'status'],
                'value' => function($data) {
                    if ($data->status == Notification::STATUS_NOT_SEEN) {
                       $class = ' class="text-danger" ';
                    } else {
                        $class = ' class="text-secondary" ';
                    }
                    return'<span '.$class.'>'.$data->status.'</span>' ;
                },
            ],

            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],

            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'view'=>function($url,$item) {
                        $_url = Yii::$app->getUrlManager()->createUrl([
                            '/notification/view',
                            'id'=>$item['id'],
                        ]);
                        $icon = 'glyphicon-share-alt';
                        $title = 'Открыть';
                        return \yii\helpers\Html::a( '<span class="glyphicon '.$icon.'"></span>', $_url,
                            ['title' => $title, 'data-method' => 'post']);

                    },
                    'update'=>function($url,$item) {
                        $_url = Yii::$app->getUrlManager()->createUrl([
                            '/notification/set-status-async',
                            'id'=>$item['id'],
                            'status'=>Notification::STATUS_SUBJECT_DONE,
                        ]);
                        $icon = 'glyphicon-check';
                        $title = 'Пoметить выполненной';



                        return Html::a('<span class="glyphicon '.$icon.'"></span>', '#', [
                            'title' => $title,
                            'aria-label' => $title,
                            'data-url' => $_url,
                            'onclick' => "
                               var obj = $(this);
                               var _url = obj.data('url');
                              
                               $.ajax({
                                    url    : _url,
                                    type   : 'POST',
                                    data   : {},
                                    success: function (response) {                          
                                            var res = JSON.parse(response);  
                                            console.log(res);                
                                            obj.closest('tr').find('.status').html(res.htmlValue);                                 
                                    },
                                    error  : function () {
                                        console.log('async - internal server error');
                                    }
                                });
                                return false;
                            ",
                        ]);

                    },

                    'delete'=>function($url,$item){
                        if ($item['status']==Notification::STATUS_NOT_SEEN) {
                            $status = Notification::STATUS_CLOSED;
                            $icon = 'glyphicon-sunglasses';
                            $title = 'Отметить просмотренной';
                        } else {
                            $status = Notification::STATUS_NOT_SEEN;
                            $icon = 'glyphicon-repeat';
                            $title = 'Вернуть в непросмотренные';
                        }
                        $_url = Yii::$app->getUrlManager()->createUrl([
                            '/notification/set-status-async',
                            'id'=>$item['id'],
                        ]);

                        return Html::a('<span class="glyphicon '.$icon.'"></span>', '#', [
                            'title' => $title,
                            'aria-label' => $title,
                            'data-status' => $status,
                            'data-url' => $_url,
                            'onclick' => "
                               var obj = $(this);
                               var _url = obj.data('url')+'&status='+obj.data('status');
                              
                               $.ajax({
                                    url    : _url,
                                    type   : 'POST',
                                    data   : {},
                                    success: function (response) {
                                       
                                                   
                                            var res = JSON.parse(response);  
                                            console.log(res);                
                                            obj.closest('tr').find('.status').html(res.htmlValue);                                              
                                             
                                            if(res.status=='not_seen'){
                                               obj.html('<span class=\"glyphicon glyphicon-sunglasses \"></span>');                                                             obj.attr('title','Отметить уже просмотренной');
                                             obj.data('status','closed');
                                               
                                            } else {
                                              obj.html('<span class=\"glyphicon glyphicon-repeat  \"></span>');                                                                 obj.attr('title','Вернуть в непросмотренные');
                                               obj.data('status','not_seen');
                                            }                                
                                      
                                 
                                    },
                                    error  : function () {
                                        console.log('async - internal server error');
                                    }
                                });                            
                                return false;
                            ",
                        ]);

                    },

                ]
            ],
        ],
    ]); ?>
</div>
