<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="grid-scrollable">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

                'id',
                'artist_id',
                [
                    'label'=> 'Артист',
                    'attribute'=>'artist_id',
                    'value' => function($model)
                    {
                        if ($model->artist) {
                            return Html::a($model->artist->name,'/artist/view?id='.$model->artist_id) ;

                        }
                    },
                    'format'=> 'html',
                    'filter' => \yii\helpers\ArrayHelper::map(
                        \common\models\Artist::find()->orderBy(['name'=>SORT_ASC])->all(), 'id','name'),
                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
                ],
                'radio_id',
                [
                    'label'=> 'Радио',
                    'attribute'=>'radio_id',
                    'value' => function($model)
                    {
                        if ($model->radio) {
                            return Html::a($model->radio->name,'/radio/view?id='.$model->radio_id) ;

                        }
                    },
                    'format'=> 'html',
                    'filter' => \yii\helpers\ArrayHelper::map(
                        \common\models\Radio::find()->orderBy(['name'=>SORT_ASC])->all(), 'id','name'),
                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
                ],
                'media_id',
                [
                    'label'=> 'Медиа',
                    'attribute'=>'media_id',
                    'value' => function($model)
                    {
                        if ($model->media) {
                            return Html::a($model->media->name,'/media/view?id='.$model->media_id) ;

                        }
                    },
                    'format'=> 'html',
                    'filter' => \yii\helpers\ArrayHelper::map(
                        \common\models\Media::find()->orderBy(['name'=>SORT_ASC])->all(), 'id','name'),
                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
                ],
                'user_id',
                'type',
                'subject_id',
                'head',
                ['attribute'=>'text',
                    'format'=> 'html',
                    'contentOptions' => ['style' => 'width:25%;line-height:1.0; white-space: normal;'],
                    'value' => function($data) {
                        return '<small>'.nl2br($data->text).'</small>' ;
                    },
                ],
                //'call2action_name',
                //'call2action_link',
                'status',
//            ['attribute'=>'created_at', 'format'=> 'html',
//                'value' => function($data) {
//                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
                ['attribute'=>'updated_at','format'=> 'html',
                    'value' => function($data) {
                        return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>


</div>
