<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PlaylistItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="playlist-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'playlist_id')->textInput(['readonly'=>true]) ?>
    <?= $form->field($model, 'order_num')->textInput() ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'song_id')->textInput() ?>

    <?= $form->field($model, 'rating')->textInput() ?>

    <?= $form->field($model, 'plays')->textInput() ?>

    <?= $form->field($model, 'downloads')->textInput() ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
