<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PlaylistItem */

$this->title = 'Create Playlist Item';
$this->params['breadcrumbs'][] = ['label' => 'Playlist Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
