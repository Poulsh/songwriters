<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PlaylistItem */

$this->title = 'Update Playlist Item: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Playlist Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="playlist-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
