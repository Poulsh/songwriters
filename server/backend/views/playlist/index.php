<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\Playlist;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PlaylistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Playlists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Playlist', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'type',
            'name',
            'object_type',
            'object_id',
            'author_id',
            'status',
            //'view',
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],


//            ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'delete'=>function($url,$model){
                        if ($model->type == Playlist::TYPE_PREMIUM_COMMON ||
                            $model->type == Playlist::TYPE_RADIO_COMMON ) {
                            return false;
                        } else {
                         $newUrl = Yii::$app->getUrlManager()->createUrl(['/playlist/delete','id'=>$model['id']]);
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $newUrl,
                            [
                                'title' => Yii::t('yii', 'Удалить'),
                                'data-pjax' => '0',
                                'data-confirm' => 'Точно?',
                                'data-method'=>'post'
                            ]);
                        }

                    },

                ]
            ],
        ],
    ]); ?>
</div>
