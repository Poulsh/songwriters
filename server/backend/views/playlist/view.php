<?php

use common\models\Message;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\PlaylistItem;

/* @var $this yii\web\View */
/* @var $model common\models\Playlist */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Playlists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playlist-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'name',
            'object_type',
            'object_id',
            'author_id',
            'status',
            'view',
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>


    <section>
        <div class="col-sm-8 col-sm-offset-2">

            <h6 class="text-center">Треки</h6>

<?php


if ($model->items) {
    $query = PlaylistItem::find();
    $query->joinWith(['song']);
    $query->where(['playlist_id'=>$model->id]);
    $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'pagination'=> [
            'pageSize' => 100,
        ],
        'sort' =>[
            'defaultOrder'=> [
                'id' => SORT_ASC
            ]
        ]
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>false,

        'tableOptions' => [
            'class' => 'table  _table-bordered'
        ],
        'columns' => [
            'order_num',
            'id',

            [
                'attribute'=>'song_id','label'=> 'song ID',
                'value' => function($item){
                    return Html::a($item->song_id,'/song/view?id='.$item->song_id);
                },
                'format'=>'html'
            ],
            [
                'attribute'=>'song_id','label'=> 'Название',
                'value' => 'song.name'
            ],
            [
                'attribute'=>'song_id','label'=> 'Артист название',
                'value' => 'song.artist_name'
            ],
            'status',

        ],
    ]);


}

?>