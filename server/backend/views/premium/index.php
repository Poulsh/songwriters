<?php

use common\models\PlaylistItem;
use \common\models\Playlist;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\RadioAdminSearch */


$this->title = 'Управление Премиум секцией';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-admin-index">

    <h1><i class="glyphicon glyphicon-star"></i> <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(  'Selected плейлист', ['playlist'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',

            [
                'attribute'=>'id','label'=> 'Artist item id',
                'value' => 'id'
            ],
            [
                'attribute'=>'premiumId','label'=> 'Premium item id',
                'value' => function($model){
                    return Html::a($model->rel_id,'/premium/playlist-item-view?id='.$model->rel_id) ;
                },
                'format'=>'html',
            ],


            [
                'attribute'=>'service_type','label'=> 'Тип',
                'value' => 'service_type',
                'filter' =>
                    [
                        PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU => PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU,
                        PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD => PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD,
                    ],
                'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control','id'=>null]
            ],
            [
                'attribute'=>'songName','label'=> 'Название',
                'value' => 'song.name'
            ],
            [
                'attribute'=>'songArtist','label'=> 'Артист название',
                'value' => 'song.artist_name'
            ],
            [
                'attribute'=>'artistId','label'=> 'Артист ID',
                'value' => 'song.artist_id',
            ],
//            'status',
            ['attribute'=>'status', 'format'=> 'html','label'=> 'Статус',
                'value' => function($data) {
                    return $data->status;
                },
                'filter' =>
                    [
                        PlaylistItem::STATUS_INIT=>PlaylistItem::STATUS_INIT,
                        PlaylistItem::STATUS_APPROVED=>PlaylistItem::STATUS_APPROVED,
                        PlaylistItem::STATUS_DECLINED=>PlaylistItem::STATUS_DECLINED,
                        PlaylistItem::STATUS_ACTIVE=>PlaylistItem::STATUS_ACTIVE,
                        PlaylistItem::STATUS_EXPIRED=>PlaylistItem::STATUS_EXPIRED,
                    ],
                'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control','id'=>null]
            ],
            'service_type',

            [
                'attribute'=>'till_to','label'=> 'до', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data->till_to, 'dd/MM/yyyy');
                },
            ],

            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'delete'=>function($url,$model){
                        return false;
//                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/notification/create','radio_id'=>$model['id']]);
//                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-alert"></span>', $newUrl,
//                            [
//                                'title' => Yii::t('yii', 'Оповестить'),
//                                'data-pjax' => '0',
//                                'data-method'=>'post'
//                            ]);
                    },
                    'update'=>function($url,$model){
                        return false;
                    },
                ]
            ],
        ],
    ]); ?>
</div>
