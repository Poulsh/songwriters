<?php

use common\models\PlaylistItem;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\RadioAdminSearch */


$this->title = 'Common Selected RU плейлист';
$this->params['breadcrumbs'][] =  ['label'=>'Управление премиум секцией','url'=>'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-admin-index">

    <h1><i class="glyphicon glyphicon-check"></i> <?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute'=>'order_num','label'=> 'Sort',
                'value' => 'order_num'
            ],
            [
                'attribute'=>'id','label'=> 'Playlist item id',
                'value' => 'id'
            ],
            [
                'attribute'=>'id','label'=> 'Radio section item id',
                'value' => function($model){
                    return Html::a($model->rel_id,'/premium/view?id='.$model->rel_id) ;
                },
                'format'=>'html',
            ],
            [
                'attribute'=>'service_type','label'=> 'Тип',
                'value' => 'service_type',
                'filter' =>
                    [
                        PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU => PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU,
                        PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD => PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD,
                    ],
                'filterInputOptions' => ['prompt' => 'Все', 'class' => 'form-control','id'=>null]
            ],

            [
                'attribute'=>'songName','label'=> 'Название',
                'value' => 'song.name'
            ],
            [
                'attribute'=>'songArtist','label'=> 'Артист название',
                'value' => 'song.artist_name'
            ],
            [
                'attribute'=>'artistId','label'=> 'Артист ID',
                'value' => 'song.artist_id',
            ],
            'status',
            [
                'attribute'=>'till_to','label'=> 'до', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data->till_to, 'dd/MM/yyyy');
                },
            ],


            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'view'=>function($url,$model){
                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/premium/playlist-item-view','id'=>$model['id']]);
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $newUrl,
                            [
                                'title' => Yii::t('yii', 'Перейти'),
                                'data-pjax' => '0',
                                'data-method'=>'post'
                            ]);
                    },
                    'delete'=>function($url,$model){

                        return false;
//                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/notification/create','radio_id'=>$model['id']]);
//                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-alert"></span>', $newUrl,
//                            [
//                                'title' => Yii::t('yii', 'Оповестить'),
//                                'data-pjax' => '0',
//                                'data-method'=>'post'
//                            ]);
                    },
                    'update'=>function($url,$model){
                        return false;
                    },
                ]
            ],
        ],
    ]); ?>
</div>
