<?php

use common\models\Playlist;
use common\models\Song;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\widgets\ActiveForm;
use \common\models\PlaylistItem;
use \common\models\MessageDialog;
use \common\models\Message;

$uploadmodel = new \common\models\UploadImgForm();


/* @var $this yii\web\View */
/* @var $model common\models\Song */
/* @var $item common\models\PlaylistItem */
$playlist = $item->playlist;

$this->title = $model->artist_name .' - '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Premium section', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-view">

    <span class="text-success" style="font-size: 2em;font-weight: bold;"><?php

        if ($item->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            echo 'Common - selected World';
        }
        else if ($item->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            echo 'Common - selected RU';
        }
        ?></span><br>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php if ($model->image) : ?>
        <div class="col-sm-12">
            <?php
            $nameParts = explode('.', $model->image);
            echo Html::img('/img/'. $nameParts[0].'_sm.'.$nameParts[1], ['class'=>'img']);
            ?>

        </div>
        <?php endif; ?>

        <div class="col-sm-6">
            <h4><?=   'Версия для предпрослушивания' ?></h4>
            <div id="waveForm_mp3" data-media="<?=  $model->name_hash ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_stop']) ?>
            </p>
        </div>
        <div class="col-sm-6">
            <h4><?=   'Оригинал' ?></h4>
            <div id="waveForm_orig" data-media="<?=  $model->path_to_file ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_orig_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_orig_stop']) ?>
            </p>
        </div>
    </div>




    <div class="mt50 mb50">
        <h5>Текущий статус: <?= $item->status ?>  </h5>
        <?php
        if ($item->till_to) {
            echo '<p class="text-secondary">Дата окончания оплаченного периода: '.\Yii::$app->formatter->asDatetime($item->till_to, 'dd/MM/yyyy').'</p>';
        }
        ?>
    </div>


    <h6 class="text-center">Song attributes</h6>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'artist_name',
            'artist_id',
            'rightholders:ntext',
            [
                'attribute'=>'music_style1_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style1_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style2_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style2_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style3_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style3_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            'vocal',
            [
                'attribute'=>'language',
                'value' => function($model)
                {
                    if ($model['language'] && $model['language'] != '""') {
                        $rawIds = $model['language'];
                        $out='';
                        $ids = json_decode($rawIds);
                        $i=0;
                        foreach ($ids as $id) {
                            $cat=\common\models\Language::find()->where(['id'=>$id])->one();
                            $out .=$cat->name_ru;
                            if (count($ids)-1>$i) {
                                $out .=', ';
                            }
                            $i++;
                        }
                        return $out;
                    }

                },
                'format'=> 'html',
                'label'=> 'Языки',
            ],
            'label',
            'isrc',
            'release_date',
//            'comments:ntext',
            'hide_reviews',
            'adult_text',
            'path_to_file',
            'info:ntext',
//            'peaks',
            'status',
            'radio_status',
            'view',
            [
                'attribute'=>'created_at',
                'value' => function($data)
                {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($data)
                {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
        ],
    ]) ?>

</div>

