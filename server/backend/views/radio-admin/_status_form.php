<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use  \common\models\PlaylistItem;

/* @var $this yii\web\View */
/* @var $model common\models\PlaylistItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="playlist-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->dropDownList(Yii::$app->helpers->value2KeyValue([
        PlaylistItem::RADIO_SECTION_STATUS_ACTIVE,
        PlaylistItem::RADIO_SECTION_STATUS_DECLINED,
        PlaylistItem::RADIO_SECTION_STATUS_INIT,

    ]),['prompt'=>'статус в радио секции']) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
