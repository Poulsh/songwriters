<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\RadioAdminSearch */


$this->title = 'Радио плейлист';
$this->params['breadcrumbs'][] =  ['label'=>'Управление радио секцией','url'=>'index'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-admin-index">

    <h1><i class="glyphicon glyphicon-check"></i> <?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],


            [
                'attribute'=>'order_num','label'=> 'Sort',
                'value' => 'order_num'
            ],
            [
                'attribute'=>'id','label'=> 'Playlist item id',
                'value' => 'id'
            ],
            [
                'attribute'=>'id','label'=> 'Radio section item id',
                'value' => function($model){
                    return Html::a($model->rel_id,'/radio-admin/view?id='.$model->rel_id) ;
                },
                'format'=>'html',
            ],
            'service_type',

            [
                'attribute'=>'songName','label'=> 'Название',
                'value' => 'song.name'
            ],
            [
                'attribute'=>'songArtist','label'=> 'Артист название',
                'value' => 'song.artist_name'
            ],
            [
                'attribute'=>'artistId','label'=> 'Артист ID',
                'value' => 'song.artist_id',
            ],
//            'status',
//            ['attribute'=>'status', 'format'=> 'html','label'=> 'Название',
//                'value' => function($data) {
//                    if ($data->song) {
//                        return Html::a($data->song->artist_name.' - '. $data->song->name,
//                            '/song/view?id='.$data->song_id);
//                    }
//                },
//            ],

            [
                'class' => \yii\grid\ActionColumn::class,
                'buttons' => [
                    'view'=>function($url,$model){
                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/radio-admin/playlist-item-view','id'=>$model['id']]);
                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-eye-open"></span>', $newUrl,
                            [
                                'title' => Yii::t('yii', 'Перейти'),
                                'data-pjax' => '0',
                                'data-method'=>'post'
                            ]);
                    },
                    'delete'=>function($url,$model){

                        return false;
//                        $newUrl = Yii::$app->getUrlManager()->createUrl(['/notification/create','radio_id'=>$model['id']]);
//                        return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-alert"></span>', $newUrl,
//                            [
//                                'title' => Yii::t('yii', 'Оповестить'),
//                                'data-pjax' => '0',
//                                'data-method'=>'post'
//                            ]);
                    },
                    'update'=>function($url,$model){
                        return false;
                    },
                ]
            ],
        ],
    ]); ?>
</div>
