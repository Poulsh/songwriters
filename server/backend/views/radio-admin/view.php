<?php

use common\models\Song;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\widgets\ActiveForm;
use \common\models\PlaylistItem;
use \common\models\MessageDialog;
use \common\models\Message;

$uploadmodel = new \common\models\UploadImgForm();


/* @var $this yii\web\View */
/* @var $model common\models\Song */
/* @var $item common\models\PlaylistItem */

$this->title = $model->artist_name .' - '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Radio section', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$activateButton = Html::a('Активировать', ['activate', 'id' => $item->id], [
    'class' => 'btn btn-primary',
    'data' => [
        'confirm' => 'Точно активировать?',
        'method' => 'post',
    ],
]);
$declineButton = Html::a('Деактивировать - вышел срок', ['deactivate', 'id' => $item->id], [
    'class' => 'btn btn-warning',
    'data' => [
        'confirm' => 'Точно деактивировать?',
        'method' => 'post',
    ],
]);


?>
<div class="song-view">

    <span style="font-size: 2em;font-weight: bold;"><i class="glyphicon glyphicon-bullhorn"></i> Artist radio section item</span><br>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <?php if ($model->image) : ?>
        <div class="col-sm-12">
            <?php
            $nameParts = explode('.', $model->image);
            echo Html::img('/img/'. $nameParts[0].'_sm.'.$nameParts[1], ['class'=>'img']);
            ?>

        </div>
        <?php endif; ?>

        <div class="col-sm-6">
            <h4><?=   'Версия для предпрослушивания' ?></h4>
            <div id="waveForm_mp3" data-media="<?=  $model->name_hash ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_stop']) ?>
            </p>
        </div>
        <div class="col-sm-6">
            <h4><?=   'Оригинал' ?></h4>
            <div id="waveForm_orig" data-media="<?=  $model->path_to_file ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_orig_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_orig_stop']) ?>
            </p>
        </div>
    </div>




    <div class="mt50 mb50">
        <h5>Текущий статус: <?= $item->status ?>  </h5>
        <?php
        if ($item->till_to) {
            echo '<p class="text-secondary">Дата окончания оплаченного периода: '.\Yii::$app->formatter->asDatetime($item->till_to, 'dd/MM/yyyy').'</p>';
        }
        ?>

        <?php
        if ($item->status!=PlaylistItem::STATUS_ACTIVE) {
            echo $activateButton;
        }
        if ($item->status==PlaylistItem::STATUS_ACTIVE) {
            echo $declineButton;
        }

        ?>



    </div>


    <h6 class="text-center">Song attributes</h6>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'artist_name',
            'artist_id',
            'rightholders:ntext',
            [
                'attribute'=>'music_style1_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style1_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style2_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style2_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style3_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style3_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            'vocal',
            [
                'attribute'=>'language',
                'value' => function($model) {
                    if ($model['language'] && $model['language'] != '""') {
                        $rawIds = $model['language'];
                        $out='';
                        $ids = json_decode($rawIds);
                        $i=0;
                        foreach ($ids as $id) {
                            $cat=\common\models\Language::find()->where(['id'=>$id])->one();
                            $out .=$cat->name_ru;
                            if (count($ids)-1>$i) {
                                $out .=', ';
                            }
                            $i++;
                        }
                        return $out;
                    }

                },
                'format'=> 'html',
                'label'=> 'Языки',
            ],
            'label',
            'isrc',
            'release_date',
            'hide_reviews',
            'adult_text',
            'path_to_file',
            'info:ntext',
////            'peaks',
            'status',
            'radio_status',
            'view',
            [
                'attribute'=>'created_at',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
        ],
    ]) ?>

</div>

<section>
    <div class="col-sm-8 col-sm-offset-2">

        <h6 class="text-center">Сообщения</h6>

        <?php
        $dialog = MessageDialog::findOne([
                'type'=>MessageDialog::TYPE_RADIO_ITEM,
                'subject_type'=>MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION,
                'subject_id'=>$item->id,
        ]);

        if ($dialog && $dialog->messages) {
            $dataProvider = new ActiveDataProvider([
                'query' => Message::find()->where([
                        'dialog_id'=>$dialog->id,
                ]),
                'pagination'=> [
                    'pageSize' => 100,
                ],
                'sort' =>[
                    'defaultOrder'=> [
                        'id' => SORT_ASC
                    ]
                ]
            ]);

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'summary'=>false,
                'rowOptions'=>function($model){
                    if($model->from_to_type ==  Message::FROM_TO_SYSTEM_TO_ARTIST){
                        return ['class' => 'system'];
                    }
                },

                'tableOptions' => [
                    'class' => 'table  _table-bordered'
                ],
                'columns' => [
//                    'id',
                    [

                        'attribute'=>'text','label'=>false,
                        'contentOptions' => ['style'=>'border:none;'],
                        'headerOptions' => ['style'=>'border:none;'],
                        'value' => function($message) use ($model)
                        {
                            $authorName ='';
                            $created_at = \Yii::$app->formatter->asDatetime($message->created_at, 'HH:mm dd/MM/yy');

                            $res='';


                            if ($message->from_to_type==Message::FROM_TO_ARTIST_TO_SYSTEM) {

                                $res .= '<div class="text-left" style="line-height:1;">';
                                $res .= '<span style="font-size:.8em;font-weight:bold;color: rgba(0,0,0,.4);">'. $model->artist->name .' </span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);">'.$created_at.'</span>';
                                $res .= '</div>';
                                $res.= '<div class="bubble other" style="padding-top: 10px;text-align: left;">'.$message->text.'</div>';
                            } else {
                                $res .= '<div class="text-right" style="line-height:1;">';
                                $res .= '<span style="font-size:.8em;font-weight:bold;color: rgba(0,0,0,.4);">Songwriters</span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);">'.$created_at.'</span><br>';
                                $res .= '<span style="font-size:.8em;color: rgba(0,0,0,.4);" >'.$message->status.'</span>';
                                $res .= '</div>';

                                $res .= '<div class="bubble my" style="padding-top: 10px;text-align: right;">'.$message->text.'</div>';
                            }



                            return $res;
                        },
                        'format'=> 'html',
                    ],



                ],
            ]);


        }

        ?>





        <?php

        $form = ActiveForm::begin([
            'options' => [
                'id' => 'radio_comment_form',
                'class' => 'comment-box',
            ],
            'action' => '/radio-admin/send-comment-of-item?id='.$item->id,
            'validateOnChange' => false,
            'validateOnBlur' => false,
        ]);

        $commentModel = new Message();

        echo $form->field($commentModel, 'to_id')
            ->hiddenInput(['value'=>$model->artist->id])
            ->label(false);
        echo $form->field($commentModel, 'dialog_id')
            ->hiddenInput(['value'=>$dialog?$dialog->id:null])
            ->label(false);
        echo $form->field($commentModel, 'from_to_type')
            ->hiddenInput(['value'=>Message::FROM_TO_SYSTEM_TO_ARTIST])
            ->label(false);

        echo $form->field($commentModel, 'text', ['template' => '{input}{error}'])
            ->textarea(['placeholder' => 'Текст сообщения...', 'rows' => 4, 'data' => ['comment' => 'content']]);


        echo '<div class="text-right">'. Html::submitButton('Отправить', [
            'class' => 'btn btn-primary comment-submit',
        ]) . '</div>';

        $form->end();

        ?>

    </div>
</section>
