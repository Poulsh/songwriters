<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Assign;
use \common\models\Radio;
/* @var $this yii\web\View */
/* @var $model common\models\Radio */


$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Radios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="radio-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p><?php
        $user = $model->user;
        if ($user) {
            echo 'Пользователь: '. $user->email.'  <b>'.$user->first_name.' '.$user->last_name.'</b>';
        }
        ?></p>
    <?php
        if ($model->image) {
            $nameParts = explode('.', $model->image);
            echo '<p>'.Html::img('/img/'. $nameParts[0].'_md.'.$nameParts[1], ['class'=>'img']).'</p>';
        }
    ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-xs btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <p>
        <?= $model->status != Radio::STATUS_ACTIVE ? Html::a('Согласовать Радио доступ', ['confirm-access', 'id' => $model->id], [
            'class' => 'btn  btn-primary',
            ]): null; ?>

        <?= $model->status != Radio::STATUS_DECLINED ?  Html::a('Отклонить Радио доступ', ['decline-access', 'id' => $model->id], [
            'class' => 'btn  btn-danger',
            'data' => [
                'confirm' => 'Точно отклонить?',
                'method' => 'post',
            ],
        ]): null; ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'page_id',
            'name',
            'country',
            'city',
            'hrurl',
            'main_info:ntext',
            'comment:ntext',
            'image',
            'image_alt',
            'status',
            'view',
            ['attribute'=>'created_at', 'format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
            ['attribute'=>'updated_at','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>

<section>
    <div class="row">
        <div class="col-sm-12">
            <h4> Стили </h4>
            <?php
            $styles = $model->styles;
            $stylesDataProvider = new \yii\data\ArrayDataProvider([
                'models'=>$styles,
            ]);
            echo yii\grid\GridView::widget([
                'dataProvider' => $stylesDataProvider,
                'emptyText' => '',
                'columns'=>[
                    'name',
                    [
                        'class' => \yii\grid\ActionColumn::class,
                        'buttons' => [
                            'delete'=>function($url,$item ) use ($model)  {
                                $assign = Assign::find()->where([
                                    'type'=> Assign::TYPE_MUSIC_STYLE,
                                    'status'=> Assign::STATUS_ACTIVE,
                                    'other_id'=>$item['id'],
                                    'radio_id'=>$model['id'],
                                ])->one();
                                $newUrl = Yii::$app->getUrlManager()->createUrl(['/assign/delete','id'=>$assign['id']]);
                                return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash"></span>', $newUrl,
                                    [
                                        'title' => Yii::t('yii', 'Удалить'),
                                        'data-confirm' =>'Точно удалить?',
                                        'data-pjax' => '0',
                                        'data-method'=>'post'
                                    ]);
                            },
                            'view'=>function($url,$model){
                                return false;
                            },
                            'update'=>function($url,$model){
                                return false;
                            },
                        ]
                    ],
                ],
            ]);
            ?>

        </div>
    </div>



</section>
