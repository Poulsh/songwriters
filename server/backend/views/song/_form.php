<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \common\models\Song;

/* @var $this yii\web\View */
/* @var $model common\models\Song */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="song-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'page_id')->textInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'hrurl')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'artist_id')->textInput() ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'uploader_user_id')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'artist_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'name_hash')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'rightholders')->textarea(['rows' => 1]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'music_style1_id')
                ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()
                    ->where(['tree'=>1])
                    ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                    ->all(), 'id',function($model) {return str_repeat('-', $model->depth).$model['name'];}
                ),['prompt'=>'Выберите стиль']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'music_style2_id')
                ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()
                    ->where(['tree'=>1])
                    ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                    ->all(), 'id',function($model) {return str_repeat('-', $model->depth).$model['name'];}
                ),['prompt'=>'Выберите стиль']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'music_style3_id')
                ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()
                    ->where(['tree'=>1])
                    ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                    ->all(), 'id',function($model) {return str_repeat('-', $model->depth).$model['name'];}
                ),['prompt'=>'Выберите стиль']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'isrc')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'release_date')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'info')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($model, 'author_comment')->textarea(['rows' => 2]) ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'vocal')->checkbox() ?>
        </div>
        <div class="col-sm-4">
<!--            --><?//= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'languages_array')
                ->listBox(\yii\helpers\ArrayHelper::map(\common\models\Language::find()
                    ->all(),'id','name_orig'),['multiple' => true])
                ->label('Языки вокала') ?>
        </div>

        <div class="col-sm-2">
            <?= $form->field($model, 'adult_text')->checkbox() ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'hide_reviews')->checkbox() ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'path_to_file')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'image_alt')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'image')->textarea(['rows' => 1]) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'background_image')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'radio_status')->dropDownList(Yii::$app->helpers->value2KeyValue([
                    Song::RADIO_STATUS_INIT,
                    Song::RADIO_STATUS_APPROVED,
                    Song::RADIO_STATUS_DECLINED,
            ]),['prompt'=>'статус в радио секции']) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'view')->textInput(['maxlength' => true]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
        <div class="col-sm-4"></div>
    </div>











    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
