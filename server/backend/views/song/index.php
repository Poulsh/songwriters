<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\widgets\ActiveForm;
use common\models\UploadSongForm;


$uploadModel = new UploadSongForm();

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Songs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-index">

    <div class="row">

        <div class="col-xs-12">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>



<!--        -->
<!--        <div class="col-xs-12 text-left">-->
<!--            <h4>Song Upload</h4>-->
<!--        </div>-->
<!--        --><?php //$form = ActiveForm::begin([
//            'method' => 'post',
//            'action' => ['/song/upload'],
//            'options' => ['enctype' => 'multipart/form-data'],
//        ]); ?>
<!--        <div class="col-sm-3">-->
<!--            --><?//= $form->field($uploadModel, 'songName')->textInput() ?>
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            --><?//= $form->field($uploadModel, 'artistName')->textInput() ?>
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            --><?//= $form->field($uploadModel, 'rightHolders')->textInput() ?>
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            --><?//= $form->field($uploadModel, 'musicStyle1')
//                ->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()
//                    ->where(['tree'=>1])
//                    ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
//                    ->all(), 'id',function($model) {return str_repeat('-', $model->depth).$model['name'];}
//                ),['prompt'=>'Выберите стиль'])
//                ->label('Стиль1') ?>
<!--        </div>-->
<!--        <div class="col-sm-4 col-sm-offset-4 text-center ">-->
<!--            --><?//= $form->field($uploadModel, 'songFile')->fileInput()->label(false) ?>
<!--            --><?//= Html::submitButton('Upload', ['class' => 'btn btn-success mb50']) ?>
<!--            --><?php //ActiveForm::end() ?>
<!--        </div>-->
<!--      -->





        <div class="col-xs-12 grid-scrollable">


<!--                --><?//= Html::a('Create Song', ['create'], ['class' => 'btn btn-success']) ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\ActionColumn'],

//                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'page_id',
//                    'hrurl',
                    'name',
                    'artist_name',
                    'name_hash',
                    'artist_id',
                    'uploader_user_id',
                    'rightholders:ntext',
//                    'music_style1_id',
                    [
                        'attribute'=>'music_style1_id',
                        'value' => function($model)
                        {
                                $cat=\common\models\Menu::find()->where(['id'=>$model->music_style1_id])->one();
                            return $cat?$cat->name:null;
                        },
                        'format'=> 'html',
                    ],
//                    'music_style2_id',
                    [
                        'attribute'=>'music_style2_id',
                        'value' => function($model)
                        {
                            $cat=\common\models\Menu::find()->where(['id'=>$model->music_style2_id])->one();
                            return $cat?$cat->name:null;
                        },
                        'format'=> 'html',
                    ],
//                    'music_style3_id',
                    [
                        'attribute'=>'music_style3_id',
                        'value' => function($model)
                        {
                            $cat=\common\models\Menu::find()->where(['id'=>$model->music_style3_id])->one();
                            return $cat?$cat->name:null;
                        },
                        'format'=> 'html',
                    ],
                    //'vocal',
                    //'language',
                    //'label',
                    //'isrc',
                    //'release_date',
                    //'author_comment:ntext',
                    //'hide_reviews',
                    //'adult_text',
                    'path_to_file',
                    //'info:ntext',
                    //'image:ntext',
                    //'image_alt',
                    //'background_image',
                    'status',
                    'radio_status',
                    'premium_ru_status',
                    'premium_world_status',
                    //'view',
                    //'created_at',
                    //'updated_at',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>

        </div>
    </div>






</div>
