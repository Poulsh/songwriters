<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \yii\widgets\ActiveForm;

$uploadmodel = new \common\models\UploadImgForm();


/* @var $this yii\web\View */
/* @var $model common\models\Song */

$this->title = $model->artist_name .' - '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Songs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="song-view  _grid-scrollable">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div class="col-sm-6">
            <h4><?=   'Версия для предпрослушивания' ?></h4>
            <div id="waveForm_mp3" data-media="<?=  $model->name_hash ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_stop']) ?>
            </p>
        </div>
        <div class="col-sm-6">
            <h4><?=   'Оригинал' ?></h4>
            <div id="waveForm_orig" data-media="<?=  $model->path_to_file ?>"></div>


            <p class="text-center">
                <?= Html::button('Play', ['class' => 'btn btn-success ','id'=>'wavesurfer_orig_play']) ?>
                <?= Html::button('Stop', ['class' => 'btn btn-success','id'=>'wavesurfer_orig_stop']) ?>
            </p>
        </div>
    </div>


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'image',
                'value' => function($model)
                {
                    if ($model->image) {
                        $nameParts = explode('.', $model->image);
                        return Html::img('/img/'. $nameParts[0].'_sm.'.$nameParts[1], ['class'=>'img']);
                    }

                },
                'format'=> 'html',
            ],
            'id',
            'page_id',
            'hrurl',
            'name',
            'name_hash',
            'artist_name',
            [
                'attribute'=>'artist_id',
                'value' => function($model) {
                    return Html::a($model->artist_id,'/artist/view?id='.$model->artist_id) ;
                },
                'format'=> 'html',
            ],
            'uploader_user_id',
            'rightholders:ntext',
            [
                'attribute'=>'music_style1_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style1_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style2_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style2_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'music_style3_id',
                'value' => function($model)
                {
                    $cat=\common\models\Menu::find()->where(['id'=>$model->music_style3_id])->one();
                    return $cat?$cat->name:null;
                },
                'format'=> 'html',
            ],
            'vocal',
//            'language',
            [
                'attribute'=>'language',
                'value' => function($model)
                {
                    if ($model['language'] && $model['language'] != '""') {
                        $rawIds = $model['language'];
                        $out='';
                        $ids = json_decode($rawIds);
                        $i=0;
                        foreach ($ids as $id) {
                            $cat=\common\models\Language::find()->where(['id'=>$id])->one();
                            $out .=$cat->name_ru;
                            if (count($ids)-1>$i) {
                                $out .=', ';
                            }
                            $i++;
                        }
                        return $out;
                    }

                },
                'format'=> 'html',
                'label'=> 'Языки',
            ],
            'label',
            'isrc',
            'release_date',
            'author_comment:ntext',
            'hide_reviews',
            'adult_text',
            'path_to_file',
            'info:ntext',
//            'peaks',
            [
                'attribute'=>'peaks',
                'value' => function($model)
                {
                    return '<p style="font-size: 0.3em; line-height: 0.3em; white-space: normal; word-break:break-all;">'.$model->peaks.'</p>';

                },
                'format'=> 'html',
                'contentOptions' => ['style' => 'white-space: normal;word-break:break-all;'],
            ],
            'image:ntext',
            'image_alt',
            'background_image',
            'status',
            'radio_status',
            'premium_ru_status',
            'premium_world_status',
            'review_status',
            'view',
            [
                'attribute'=>'created_at',
                'value' => function($data)
                {
                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
            [
                'attribute'=>'updated_at',
                'value' => function($data)
                {
                    return \Yii::$app->formatter->asDatetime($data['updated_at'], 'HH:mm dd/MM/yy');
                },
                'format'=> 'html',
            ],
        ],
    ]) ?>

</div>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3">
                <h4>Image Upload</h4>
                <?php $form = ActiveForm::begin([
                    'method' => 'post',
                    'action' => ['/song/upload-image'],
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= $form->field($uploadmodel, 'toModelProperty')->dropDownList([
                    'image'=>'Image',
                    'background_image'=>'Background Image',
                ])->label(false) ?>
                <?= $form->field($uploadmodel, 'imageFile')->fileInput()->label(false) ?>
                <?= $form->field($uploadmodel, 'toModelId')->hiddenInput(['value'=>$model->id])->label(false) ?>
                <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>

</section>
