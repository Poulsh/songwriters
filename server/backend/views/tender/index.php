<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Tender', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'hrurl',
            'author_user_id',
            'name',
            'description:ntext',
            //'conditions:ntext',
            //'music_mood',
            //'music_style1_id',
            //'music_style2_id',
            //'music_style3_id',
            //'language',
            //'only_vocal',
            //'end_date',
            //'reward:ntext',
            //'location',
            //'accept_demo',
            //'adult_text',
            //'tracks_from_author',
            //'example1',
            //'example2',
            //'example3',
            //'image:ntext',
            //'image_from_avatar',
            //'background_image:ntext',
            //'imagelink',
            //'imagelink_alt',
            //'promolink',
            //'promoname',
            //'stylekey',
            //'view',
            //'layout',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
