<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tender */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tenders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tender-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'hrurl',
            'author_user_id',
            'name',
            'description:ntext',
            'conditions:ntext',
            'music_mood',
            'music_style1_id',
            'music_style2_id',
            'music_style3_id',
            'language',
            'only_vocal',
            'end_date',
            'reward:ntext',
            'location',
            'accept_demo',
            'adult_text',
            'tracks_from_author',
            'example1',
            'example2',
            'example3',
            'image:ntext',
            'image_from_avatar',
            'background_image:ntext',
            'imagelink',
            'imagelink_alt',
            'promolink',
            'promoname',
            'stylekey',
            'view',
            'layout',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
