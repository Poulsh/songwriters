<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <div class="grid-scrollable">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

                'id',
                'email',
                'first_name',
                'last_name',

//                'radio_id',
//                [
//                    'label'=> 'Радио',
//                    'attribute'=>'radio_id',
//                    'value' => function($model)
//                    {
//                        if ($model->radio) {
//                            return Html::a($model->radio->name,'/radio/view?id='.$model->radio_id) ;
//
//                        }
//                    },
//                    'format'=> 'html',
//                    'filter' => \yii\helpers\ArrayHelper::map(
//                        \common\models\Radio::find()->orderBy(['name'=>SORT_ASC])->all(), 'id','name'),
//                    'filterInputOptions' => ['prompt' => 'Выбери', 'class' => 'form-control', 'id' => null]
//                ],

                //'call2action_name',
                //'call2action_link',
//                'status',
                ['attribute'=>'status','format'=> 'html',
                    'value' => function($data) {
                        switch ($data['status']) {
                            case \common\models\User::STATUS_ACTIVE:
                               return "active <sup>10</sup>"   ;
                                break;
                            case \common\models\User::STATUS_EMAIL_VERIFICATION:
                                return 'email verification <sup>1</sup>';
                                break;
                            case \common\models\User::STATUS_DELETED:
                                return 'deleted <sup>0</sup>';
                                break;
                            case \common\models\User::STATUS_ON_CHECK_BY_ADMIN:
                                return 'on check by admin <sup>2</sup>';
                                break;
                            case \common\models\User::STATUS_DECLINED_RADIO:
                                return 'declined radio <sup>3</sup>' ;
                                break;

                            default:
                                return $data['status'];
                        }
//                        return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');
                        },],
//            ['attribute'=>'created_at', 'format'=> 'html',
//                'value' => function($data) {
//                    return \Yii::$app->formatter->asDatetime($data['created_at'], 'dd/MM/yy HH:mm');},],
                ['attribute'=>'updated_at','format'=> 'html',
                    'value' => function($data) {
                        return \Yii::$app->formatter->asDatetime($data['updated_at'], 'dd/MM/yy HH:mm');},],

                [
                    'class' => \yii\grid\ActionColumn::class,
                    'buttons' => [
                        'delete'=>function($url,$model){
                            if ($model['status']==\common\models\User::STATUS_EMAIL_VERIFICATION){
                                $newUrl = Yii::$app->getUrlManager()->createUrl(['/user/sendactivationlink?id='.$model['id']]);
                                return \yii\helpers\Html::a( '<span class="glyphicon glyphicon-alert"></span>', $newUrl,
                                    [
                                        'title' => Yii::t('yii', 'Отправить повторную ссылку на активацию'),
                                        'data-pjax' => '0',
                                        'data-method'=>'post'
                                    ]);
                            }
                            return false;


                        },
                        'update'=>function($url,$model){
                            return false;
                        },
                        'view'=>function($url,$model){
                            return false;
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>


</div>
