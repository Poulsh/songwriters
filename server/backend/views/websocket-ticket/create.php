<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebsocketTicket */

$this->title = 'Create Websocket Ticket';
$this->params['breadcrumbs'][] = ['label' => 'Websocket Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="websocket-ticket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
