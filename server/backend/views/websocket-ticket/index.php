<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\WebsocketTicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Websocket Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="websocket-ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Websocket Ticket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [


            'id',
            'token',
            'status',
            'user_id',
            ['attribute'=>'expires','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['expires'], 'dd/MM/yy HH:mm');},],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
