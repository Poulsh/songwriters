<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\WebsocketTicket */

$this->title = 'Update Websocket Ticket: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Websocket Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="websocket-ticket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
