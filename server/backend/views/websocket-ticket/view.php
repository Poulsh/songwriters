<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\WebsocketTicket */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Websocket Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="websocket-ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'token',
            'status',
            'user_id',
            ['attribute'=>'expires','format'=> 'html',
                'value' => function($data) {
                    return \Yii::$app->formatter->asDatetime($data['expires'], 'dd/MM/yy HH:mm');},],
        ],
    ]) ?>

</div>
