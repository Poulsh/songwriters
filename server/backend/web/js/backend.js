$(document).ready(function() {



    if (document.getElementById('waveForm_mp3')) {
        var waveFormMp3Container = $("#waveForm_mp3");
        var wavesurferMp3 = WaveSurfer.create({
            container: '#waveForm_mp3',
            waveColor: '#999',
            progressColor: '#555'
        });

        wavesurferMp3.on('play', function () {
            $("#wavesurfer_play").html('Pause');
        });
        wavesurferMp3.on('pause', function () {
            $("#wavesurfer_play").html('Play');
        });
        wavesurferMp3.on('stop', function () {
            $("#wavesurfer_play").html('Play');
        });
        $("#wavesurfer_play").on("click", function () {
            wavesurferMp3.playPause();
        });
        $("#wavesurfer_stop").on("click", function () {
            wavesurferMp3.stop();
        });
        wavesurferMp3.load('../uploadedsongs/listen/'+waveFormMp3Container.data('media')+'.mp3');


        var waveFormOrigContainer = $("#waveForm_orig");
        var waveSurferOrig = WaveSurfer.create({
            container: '#waveForm_orig',
            waveColor: '#999',
            progressColor: '#555'
        });
        waveSurferOrig.on('play', function () {
            $("#wavesurfer_orig_play").html('Pause');
        });
        waveSurferOrig.on('pause', function () {
            $("#wavesurfer_orig_play").html('Play');
        });
        waveSurferOrig.on('stop', function () {
            $("#wavesurfer_orig_play").html('Play');
        });
        $("#wavesurfer_orig_play").on("click", function () {
            waveSurferOrig.playPause();
        });
        $("#wavesurfer_orig_stop").on("click", function () {
            waveSurferOrig.stop();
        });
        waveSurferOrig.load('../uploadedsongs/source/'+waveFormOrigContainer.data('media'));
    }








//    websocket

    var ws_ticket;
    var ws_path;
    var notSeenNotesQnt;
    var notSeenIndicator =  $('#notSeenNotesQnt');
    var notSeenIndicatorValue = notSeenIndicator.data('not-seen-notes-qnt');

    function setIndicatorClass(){
        if (notSeenIndicatorValue < 1){
            notSeenIndicator.removeClass('display-inline-block');
            notSeenIndicator.addClass('display-none');
        } else {
            notSeenIndicator.removeClass('display-none');
            notSeenIndicator.addClass('display-inline-block');
        }
    }


    // var notSeenIndicatorValue = notSeenIndicator.html();

    // if (notSeenNotesQnt && Number(notSeenIndicatorValue) != Number(notSeenNotesQnt)){
    //     console.log('notSeenIndicator.html()!= notSeenNotesQnt');
    // }
    //
    // console.log('notSeenIndicatorValue');console.log(notSeenIndicatorValue);


    function getData() {

        $.ajax({
            url    : '/site/user-data',
            type   : 'post',
            data   : {},
            success: function (response) {
                var res = JSON.parse(response);
                if (res.ws_ticket){
                    notSeenNotesQnt = res.adminCheckSum?res.adminCheckSum.notifyNotSeenQnt:null;
                    ws_ticket = res.ws_ticket;
                    ws_path = res.ws_path+'?ticket='+ws_ticket;
                }
                if (ws_path && ws_ticket) {
                    wsStart(ws_path);
                }
                setIndicatorClass();
            },
            error  : function () {
                console.log('getData() internal server error');
            }
        });
    }

    getData();


    function updateNotSeen() {

        $.ajax({
            url    : '/site/not-seen-notifications',
            type   : 'post',
            data   : {},
            success: function (response) {
                var res = JSON.parse(response);
                if (res.notSeenNotesQnt){
                    notSeenNotesQnt = res.notSeenNotesQnt;
                    if (notSeenNotesQnt && Number(notSeenIndicatorValue) != Number(notSeenNotesQnt)){
                        notSeenIndicatorValue = notSeenNotesQnt;
                        notSeenIndicator.html(notSeenNotesQnt);
                        
                    }
                    setIndicatorClass();

                }
            },
            error  : function () {
                console.log('getData() internal server error');
            }
        });
    }





    // function message_template(name, message, time, photo, isAuthor) {
    //     if (!isAuthor) {
    //         isAuthor = false;
    //     }
    //     var ret = "<div class='message'>message</div>";
    // }
    //
    // function scrollBottom(el, duration ) {
    //     if (!duration) {
    //         duration = 500;
    //     }
    //     var top = el.prop("scrollHeight");
    //     el.animate({
    //         scrollTop: top
    //     }, duration);
    // }

    var socket = null; // объявлять чат нужно здесь, чтобы можно было создавать новый объект чата при переподключении
    function wsStart(websocketServerLocation) {
        socket = new WebSocket(websocketServerLocation);
        // var message_text = '';

        socket.onmessage = function(e) {

            updateNotSeen();

            var response = JSON.parse(e.data);

            console.log('WS socket.onmessage -> response'); console.log(response);

            // $('#response').text('');
            //
            // var response = JSON.parse(e.data);
            // if (response.type && response.type == 'support') {
            //     var chat_div = $('#chat');
            //     message_text = message_template(response.from, response.message, response.date, response.photo, response.support);
            //     chat_div.append(message_text);
            //     scrollBottom(chat_div, 500)
            // } else if (response.message) {
            //     $('#response').text(response.message);
            // }
        };

        socket.onerror = function(e) {
            console.error('Ошибка подключения websocket');
            // $('#response').text('Возникла ошибка.');
            socket.close();
        };

        socket.onclose = function() {
            console.log('Попытка подключения websocket...');
            // $('#response').text('Попытка подключения...');
            setTimeout(function() {
                wsStart(websocketServerLocation)
            }, 5000);
        };

        socket.onopen = function(e) {
            // socket.send( JSON.stringify({'helloMessageOnOpenSocket' : 'register', 'session_id':'$session_id'}) );
            console.log('Websocket онлайн');
            // $('#response').text("Вы онлайн");
        };

        // $('#btnSend').on('click', function() {
        //     var message_input = $('#message');
        //     if (message_input.val()) {
        //         var message_send = JSON.stringify({
        //             'action' : 'support',
        //             'message' : message_input.val(),
        //             'receiver' : '$receiver',
        //             'session_id': '$session_id',
        //             'support': true
        //         });
        //         if (socket.readyState === chat.OPEN) {
        //             // Если соедение не открыто
        //             socket.send(message_send);
        //         } else {
        //             console.warn('WS NOT CONNECTED');
        //         }
        //         message_input.val('');
        //     } else {
        //         $('#response').text('Введите сообщение');
        //     }
        // });

        // $('#message').on('keypress', function(event) {
        //     // отправка при нажатии CTRL+Enter
        //     if ((event.ctrlKey) && ((event.keyCode == 0xA) || event.keyCode == 0xD)) {
        //         $('#btnSend').click();
        //     }
        // });
    }

    if (ws_path && ws_ticket) {
        wsStart(ws_path);
    }


    // scrollBottom($('#chat'));

});