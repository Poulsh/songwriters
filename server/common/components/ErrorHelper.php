<?php


namespace common\components;


use yii\base\Component;


class ErrorHelper extends Component
{
    public static function toString($array)
    {
        $text='';
        foreach ($array as $key => $item) {
            $itemText = '';
            foreach ($item as $i) {
                $itemText.= $i.'; ';
            }
            $text.=$key.' - '.$itemText.'<br>';
        }
        return $text;
    }
}