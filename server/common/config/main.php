<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            ],
        'helpers' => [
            'class' => 'common\components\Helpers'
        ],
        'errorHelper' => [
            'class' => 'common\components\ErrorHelper'
        ],
//        'ffmpeg' => [
//            'class' => '\rbtphp\ffmpeg\Ffmpeg',
//            'path' => '/usr/local/bin/ffmpeg'
//            ],
    ],
];
