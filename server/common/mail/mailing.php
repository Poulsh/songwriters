<?php



?>

<table class="container" style="border-collapse:collapse;font-family:'Roboto',sans-serif;color:#414141;margin:0;padding:0;background-color:#f9fafc;width:100%!important;height:100%!important;font-size:16px;line-height:1.4;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;" >
    <tbody>
    <tr>
        <td class="wrapper" align="center" style="border-collapse:collapse;display:block;padding-top:16px;padding-bottom:16px;padding-right:16px;padding-left:16px;" >
            <table style="margin-right:0;margin-left:0;width:80%;max-width:600px!important;border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                <tbody>
                <tr>
                    <td align="center" class="topLogo" style="border-collapse:collapse;padding-top:24px;padding-bottom:0;padding-right:24px;padding-left:24px;" >
                        <a class="link" href="https://<?= Yii::$app->params['domain'] ?>" target="_blank"  >
                            <img class="logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAABQCAYAAAA3ICPMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEzxJREFUeNrsXf9V47oS1t2z/29uBWvOK2BDBTEVECogqQCoIEkFQAUJFRAqwKmAbAH34K3g5VbwnsWOQGs0siTLjp183zk+/Aq2LM3MNzOSRkIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADUxl/oAgDoFnbn/xkUX4b0o/69xA/6nY6h4XcxsJXNMfzuX9VU+vl3Q5/+yTB6IBAAAJojh6FGCvLr9+JK6M/pgbxmTpfEpkRG24JodpAEEAgAAHaSSDWCSDSicLqN5t3L738yBvoPw10Y59yjnbY2laOab6VIqG7Uowhlo70ryAUEAgBHQxQJGVJ5/SBjPPQwniod9E4IfUwDlYhIEYtOOD6R1TuZFNcvEAsIBAAOJapQRDF0MIq6h60IwitCOODITH0deRBMTn0qI7EMpAICAYCuGrqBRhIjYU/XbMm4/VTfF4Zti14MjmJ0ctEjG45UMkUq6PcjJ5BCiFLxedWJQtbXEB/oBWEoskgFn4bKNC94C4PVql2QY/K9IvrblQgFtuKQCYQUd1Jcl8Itd6wr8lNxrY85JQA0ShhbzRiBLLoZsai0IhclKkLZIEI5IAIhBZ4V13WE262KawEiASpkTnmu5wYPFp7r4YyxmqcyOQa55nxmmEPpIYHQID8KvyWNLrgpBOIOQwposjYm73Rckrdc80wRXRx2pDksRZs61iQDyGT0gUCKAZ0UX25FM7tm36KRQhCmGNajJ41zIo1BKcJQnieMxfHKR8oQinQiHkAmHSUQijyeHcgjE583TqnNWC6bl0AiIA1RIgxEGIBPlAoy6SCBvAj76paHYrBWjh6EnHSfWD52UdxrjeE9aMUfanIgSSMXLeW36dkzeq58zr1p3oQ+JyNufS8I9i90V6YSIhJFKF62CQTS3MBIJV8yfw6auyAieWQiErkW/wTDe3AKrlbtXZG3qDzF1qIMasOrQe5Oy20gGX023OYME/W9kLVUi2zfshvkLCAqIXxt6TlXzO+nocwuFbAY5DNhToslkrTgNRyMMutRp4wsF2J/cxkTxmm5FFplWqDfoChxTddUS5O+FN/LcV7ACWiBQEqlqXVkdQ289PiK+98w0c05eQxAfz1AqbQzMsxPFK3uO/3DzcElGLWDJpQymVwWX28pIjlaO9NGBMLNe2wiDeyqGMiZQYFTiH0viSOhiHVAMnLasTmDjEitjCeM3tGRyVt6nohEFsW8O7b5ra97VsRYkINZ3pQ4kMYI+creEIdeoqKzeWZKnd6V5G2FdOnRkslKc3wkmbylvo6FSPZJIMOIJLIR5l3tclBBIP1RxruetPOmMBT3JF87LBMGyOG505yho5CJxldhWVaiSJa+6JgXrO9cNeGtPpKvd1Hc1/T+xqWB2kanTxFbjEk7bXWJKvkw0N7tlzCsaNLKcCd0facoYUsr7BLDo1aehxvNG7qHVO65SjcY2vqpX7XlnPp8R+64zFxvg+yniem9qK8/tdExUkvE59Rw0KFMnLyV26PNSSU+7S7VrDIhp77NIunwM93zl7CU0S/pQS1dY+Tqk7zUkasK2yC0sW/VYW4jAuFeaNyVlVI0MFfiY7le1ee3wm/yLGWiJl2Yr8VH7t+EmQqPRcBkMgnvTPD7Z1Ltszm9353FAXigrz+Y6G+nPDJHozireY+UuYd8F2XkLjly1vpoafmMySDIz44UoTJtKMM0BvOKsbsyGXDm81JGnhzlk+u3uSabtrp1c4tRvRKORVKLzwuS7fuaZGIau4X2PlV6UNY1lzp7Jrl6lxdfuWrZNtXCl5ZCOy6cW0qPjYR0H8QxJI/l2XWACENq+ysNcJ02yOe+io+NadbuJMH3ei4p80uF0uiQAn8r38/BAHCTxyOPbrhkfn/uOSYmrD3G4UVULL6QxqC4HovrvzRuS8Ev7a0dLdIE7SsZ8MTxX8cx5JOI/UV4FD0lnVL9MvR8pGz3s9RJMrqx+3NOfemiB7quXdfU70q5YuSsrm0ai4bROIEQ7m1sT4O0bOOFS0b1WdRbrZWQwF/XEOjHAOMjP/9ICl71jCUp8yDw/W4rHISMIgUXT9DHa3z7vYdzMfIkuPf/I1lwHQdTGqIR54bk8zqCfM5rPD/x1KkXUX9Js5SHl5gkQnowC/z3W/r/EBvjrd+hpGMY+0dyQHqdwlJLbW3hrGJ7tYohEx+VUrMGlHMi+J3xf6Q1yDjqx3ByAuY7CXxVIVjbCg9uQMJ5UkFQk4p33GipqGGg8mcGL0l6z8OqCWYtR27zSl1TMZ9u7yA/lSQZ0Bf6GHFnjewcjPcg0tjJlMx3zxpxXk4HtdmmUzstE6F0alBhE+T9ziKMCadr6syXfzUnhDPa0jb99NDxILlqwDZdSyesqfqAba7Cmgq3Yopqsm5MHaorS+2JZArplxYhlxsTjcvwaHBnjKJKEsk9anCZDr+RkdofE8cVOVt2x71lTkCldW5MuV3HHLHJyx8zRr1qNUpV1DmqIhBtkj8kfZVUGDtfZ+msNAam+aMbTo4p4uK8VtmuBcnIzjLmKWMANx658WEFOZY/c2txhBacXqjlr4ysygg0jeBEDjzln5uvkDruWjbHVa52JTm+tdgmaR/uAmyT79h3j0Boxc4pKYdvbjSla6ZNtoXW8V9aBP3MNjlNkdSa7mEyfEsSMN814CvBTIzT+8ndrz8Z4eJ23HPvaS0foz3vXrjnsdfM837USD3pBDN1kA8TfDerWo1dS7hljIBs24VN3snQZpRSvbUYQB+dsTpVmtFNA3VKtmVOmQdOvmNmIVzk/0zOczE6fhsQFeUkVysH2zQIHHtlm24Z5y9k7CvxpU3NkI0vrlPyoupstBlTR73ShJuTt0yKlYQIuvYOO1p+vGU8nesAgZ5WPZtC562LB29ZWrt29ULIyzpz8cSp7VsPw+4TgQwcJoJHFmJzJnEpm/skD80bNxnxM1flJ1m5Y+TTZx5AjumJlJkK+eScjIWrM0Vtzj3uHZ08DBmTHRMV+bRJPu+06rmks8M6Y0+2acrIve/Yd49AtBedi9+5+xtRf6NfSp7/i4Oh4Yo6htRYumAE7Mpj4te3mOSTJYWjg1sSO/UcJ+V9uuCBSbElFqVxXTRRtRrLOM/gMaZdOUNmFks+5WZHRrcmjpPTzk6Vxcj76tTCcD3sgTyqZP/SR64c+5CzTRcBtokjv0nsFa9f9qUpxJZ3VHZdRSV1dm++TTxyK060DVhlBM2rkEfAMb2LYbwLyEly7Rw4vGfT5RUyz/QSRwwrnyiFjGHiSGiCIZquHEBmes+8Ru56ERj17TzIw0YUqadOSYM7L12rGLIZeJ91YP+ptJWT82VZSBJqm9R8SWjbu08g5XQJCYskkr8pdbIQH6cTenlxzJK7sYfn7Ip7z5SKjn8b6s7UJ3qJOYaMxzvybKspvWmLZFJPQmu1X1xB0XPoQgBfA1gV0fkeerW16OK8iT0dbTm5jBwlDp587tGHTdimVeDY949ADJFJRoQivaC/KUK58TAKE8PejB+eSuZqNE1Csk+FGdU0qHWwdjXwTKSUWyK7scf75j2sTxWd+C0GcBi57ba+Vvu8ZIr52nP+oAvYWDIesfAjts6SHuVNj/3XPowgGQN53Wk1earKJMhVB/oqraSml2BTnrRO6B67uyzGpA1luzZFD4ZJwLGFgOR9JgbP6c7R8PbxOOPvnMdYs9qBSR4GtDdgF0k/d4YKxSbDNSTnQd/r1fUz63c+ehaIxEICdZAb7h3Vuf3aNy0joZfh2YoU69ZCJFfiIw85ZDq4KQHrkiebtTQ2azIOA0ObVg6h9EYjgHIaMi0bPcv8x0b0D5xiXzf0vGFkuVgIvjihyfjqe71U3amnPS+h9omuhhEdlaQhnd2Y7EHMYy6+iB6DJphsS03bKI3yUwBVgj8qCbBxB7IyHpbUy9ghHN910AgdPCj1HLpEX1WieKsxJstv9HXeJLLz0PnnfTkEwSUSMc5FHJkgdgFPjLdWRexrh/uMKn5uLdoCWH2ci99L9KcibFWl2kv1GlLDC2gXjaawqJpkOYTK9HIPsUiEdmFOGLbNMdR7jUCGpfTTuQPxqF21ZeLRl9ymjgR2aP0ZRT0bdupW4neaOaFxOqevPnMHchXXN9rTAhwbgTBIG7rvLweFaaKC6qgH49xauXyZW6UzCYaGcV9bZCAz3CcvhdvvBRq5NJjo5wS6rT/Pet7+XJEJOZXqwLaRI6HIlVtPTRRVdQQ3pxNz4j8z6EQMnf3GjEm0vmw6hbVhIpPxHgRh6yEcdY3zPifWs4be0wds+onZ78CdpGYig0sLCW17fBY1pytDcUCgPV9yE+0FLdG/IHKxjdvlPpvcdgQXUWcbl52mCYRj6fMGnvWN+X1e+ipKHm0SnAPgveDOLUtseS7Ith/k3JH0OKOaWpSjz+mrfF9GYM+EsqZKACcWOUj22MRR0148Zy/qHlYn+AKXvSEQrpMnDXhWpqhmp3m2G4//q/NM27P25smKFvemMLvSh9oenjIeOONi8PSGRIYjR+LqC7IWna2YjslbsUvDNfCUmR1FI3uVXYOD2LgRtujseY22jz3lrHsEQkLBKfVjrMJeliq7a4eOu6rx6EsPD7yvhiiN2I6JYZx2FRvJMoa4y+3q4+5zXVdyxjCNa0bJ/zNcz5FTLc+GaxhoL7IODUsrRrjC0Q61kVdtOLdtLOO9t4SlL3UjESqDzB3C8lBSUJNhT0KOpCWGNxnXvRoyyxGz3oZIO6QmBKZ00iyAbF3v0yXDE1tXbgN1o/EI2ZLKCXVYmkgJJ4GGeOY5TnUc7ZXpTyJgIymlvjjbFNW5bZxASMBWFSSyDDBuCR36wh2ctDYIN6ugPkRWcXznosOGaOnxjhOfzxvGnatU7GvMMsf79Gn+I7FErjlD/ktP/bCd/xA7Qt7G8J7JkRs0QSDid6XuxKMtS2acVrEPZaqwGzPX84402/RY5VD3KQKRqDr3Q3aQXnDNmEOVnSM7k4jj1RJiGs++IELhlMfpYCrtmFKToGdNHBsZgDvBH4azdHjHeR3y8DRU68DUjgth7RuczI8snuiNJZ3hlPYlQ8mlkZqoPfXAkLyz0aZoaWa7P823zOkQuWcfw0p98VKVbaBnLAV/sFcjDiLJOXfe+tIlS1Jhm7a0yTMqWtkHQhv9LkT1mehDXejp+FrvCE7YzzKYkmcxNAi8HKhLEtj34x+1ev1Xgp8T8D6wqeH+XjCpD7WA4V5oZ4RoG75mIt6ql42wL1JwXXb7JOw59U5OntNeFo4MfilvVkW/tMR1bSlMOCYnQDopDzoRaBO+54I/z97ngDAfrEhuBozRXtMY/rFcW5O5S4te6WdilI+SVvXR7lyHhLINM2rzpuRoKR3n5P+moehDj0JSRtZlu88Ntmmg9eG4bdvU9pnoZxReJQ09Rnbqhc3DIuM6tZBZqoTZg8C8jhxtqb9l5eIfjDFRKbilwzvW2YC5Fvb8/YPHfWYVBNNVbBmDMKP0xLuhFHTWttx5TYZhwhhBSS7XAQ7WTRPzc5pOPTLtnah38Wzz+8osIhuTgbyyeO42IrkWfvML06azC9SPZ5bosY5tamRetu0z0eVLnDbkMcp7nrp0FH3mRMRZjrdtcoBq9vdU8PNPrn06rfH8qvRT5iE3ed377An3NcYuVrpkR47VqkFZU7ISa4Nd+Uhdzul0cUbzGrq+a4M8dBIhRyKGTOdN26bWiylS1c4L6qR1JEG7oJ2tO892qIOqdoGCtegqeRgMkff51DROdQ0Cpwi+q9W4+2y7FPkZ+n8VSuKUsz6tYUzU6p6TNuaI6F3rGj+jXllWF7q8Vy4+TjndecrcadvzmmSbzoiQ8xp9eNq0bfq6R8WSg5Npm8tGojQHUjGwsmMe6nYQpXpW1Iaqgm9qnbpMmaz7UjZDGiItr35p8dpyUsh7zSgPLJ91wQOTKvA1Mk9MSifrQf9Pi/7fiOpD0Ljo64xSOOr/0wrjsTcZLbV3Ij5qXlUZeNnmTYWxlgZ1qclkLhzndKgflB4oXU9K47EjuyLHarVvx0Q5H7RYwKUYpRr3VVvj/lcXFc5SIiRvY1CZ5wfXWWJKEni/i610ik/bmPsY20MrsmYG4f6rzfeP8e40WT3oQDvSCLKQGBwBbxll7rOL7blazoDJQu9j+l+5UdJkWJssSsnI1a4J799Hd4+WQIBOkblpSaMU2BP0DtBBeW2dQI4ZX9EFB6lEz4yHHLKE0xQ9bNHLAACAQA4TJqPvvRSXUi2J4U9P6GIAAL6gCw4SxrNPAspDm/Ze2ApkAgAAAgF6Dm6D3q1LaQkq5/DIRDKrHh/aBABARCCFdZhYCXtpCfl3VVpClTJRqztUKQzupMUFuhcAAAmswjpQ0NLCl5i3FB3fNAkAWIXVLpDCOlBoZWNipJtAHgAAgECOkETkfo1VjdvI/z0BeQAAUAZSWMcT2quSMXKOQ6a3Eku0IclClcLI0XsAAIBAABOxpPTtFqurAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADg//F2AAmarAAIaH1uoAAAAASUVORK5CYII=
" alt="Songwriters.pro" style="border-width:0;border-style:none;height:auto;line-height:100%;outline-style:none;text-decoration:none;width:150px;" >
                        </a>
                    </td>
                </tr>

                <tr >
                    <td align="center" class="page-content" style="border-collapse:collapse;background-color:#fff;background-image:none;background-repeat:repeat;background-position:top left;background-attachment:scroll;border-width:1px;border-style:solid;border-color:#f1f1f1;padding-top:20px;padding-bottom:20px;padding-right:10px;padding-left:10px;" >
                        <table class="text-center" style="border-collapse:collapse;padding:0;" >
                            <tbody>

                            <tr>
                                <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:18px;line-height:1.2;font-weight:bold;text-align:left;" >
                                    <?= $head ?>
                                </td>
                            </tr>

                            <tr>
                                <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:14px;text-align:left;" >
                                    <?= $text ?>
                                </td>
                            </tr>

                            <?php if (isset($regLink) && $regLink!=null) : ?>
                                <tr>
                                    <td class="content-item" align="left" style="border-collapse:collapse;padding-top:24px;padding-bottom:24px;padding-right:10px;padding-left:10px;" >
                                        <table class="button" style="border-collapse:collapse;padding:0;" >
                                            <tbody>
                                            <tr>
                                                <td style="border-collapse:collapse;padding:0;border-radius:5px;background-color:#ff4c00;" >
                                                    <a href="<?= $regLink ?>" target="_blank" style="border-radius:5px;background-color:#ff4c00;color:#ffffff;text-decoration:none;padding-top:5px;padding-bottom:5px;padding-right:20px;padding-left:20px;display:inline-block;border-width:1px;border-style:solid;border-color:#ff4c00;font-size:14px;" ><?= $regLinkName ?></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endif; ?>




                            <tr>

                                <td   style="border-collapse:collapse;padding-top:24px;padding-bottom:24px;padding-right:10px;padding-left:10px;" >
                                    <table class="button" style="border-collapse:collapse;padding:0;" >
                                        <tbody>
                                        <tr>
                                            <td class="content-item" style="border-collapse:collapse;padding:0;" ><a href="<?= $songLink ?>" target="_blank"
                                                ><img align="center" src="<?= $songImageLink ?>" width="200" style="max-width: 990px; padding-bottom: 0; display: inline !important; vertical-align: top;" class="mcnImage"  alt="banner "></a>
                                            </td>
                                            <td>
                                                <table class="button" style="border-collapse:collapse;padding:0;" >
                                                    <tbody>
                                                        <tr>
                                                            <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:18px;line-height:1.2;font-weight:bold;text-align:left;" >
                                                                <?= $songLinkName ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:24px;padding-right:10px;padding-left:10px;font-size:14px;text-align:left;" >
                                                                Трек, который мы хотим представить вашему вниманию сегодня – <a href="<?= $songLink ?>" target="_blank"  ><?= $songLinkName ?></a> – красивая поп-композиция на французском языке, которая наверняка понравится многим вашим радиослушателям.
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:10px;padding-left:10px;" >
                                                                <table class="button" style="border-collapse:collapse;padding:0;" >
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="border-collapse:collapse;padding:0;border-radius:5px;background-color:#ff4c00;" >
                                                                            <a href="<?= $songLinkName ?>" target="_blank" style="border-radius:5px;background-color:#ff4c00;color:#ffffff;text-decoration:none;padding-top:5px;padding-bottom:5px;padding-right:20px;padding-left:20px;display:inline-block;border-width:1px;border-style:solid;border-color:#ff4c00;font-size:14px;" >Послушать</a>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>



                                        </tr>

                                        </tbody>
                                    </table>
                                </td>



                            </tr>

                            <tr>
                                <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:2px;padding-right:10px;padding-left:10px;font-size:14px;text-align:left;" >
                                    Наша почта: <a href="mailto:content@songwriters.pro" target="_blank"  >content@songwriters.pro</a>
                                </td>
                            </tr>
                            <tr>
                                <td class="content-item" style="border-collapse:collapse;padding-top:2px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:14px;font-weight:bold;text-align:left;" >
                                    Желаем вам прекрасного эфира!
                                </td>
                            </tr>
                            <tr>
                                <td class="content-item" style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;text-align:left;font-size:10px;" >
                                    Просьба регистрироваться с корпоративной почты.
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>



                <tr >
                    <td align="center" class="footer" style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <table style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                            <tbody>
                            <tr>
                                <td style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:12px;text-align:center;" >© <?= date('Y') ?> <?= Yii::$app->params['mainName'] ?><br>
                                    <?= isset(Yii::$app->params['phone'])?Yii::$app->params['phone'].'<br>':null ?>
                                    <?= isset(Yii::$app->params['address'])?Yii::$app->params['address'].'<br>':null ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>


                <tr >
                    <td align="center" class="footer" style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                        <table style="border-collapse:collapse;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
                            <tbody>
                            <tr>
                                <td style="border-collapse:collapse;padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:10px;font-size:12px;text-align:center;" >
                                    <a class="link" href="mailto:<?= Yii::$app->params['feedbackEmail']; ?>" target="_blank" style="color:#1C7098;" >Связаться с нами</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                </tbody>
            </table>
        </td>
    </tr>




    </tbody>
</table>
