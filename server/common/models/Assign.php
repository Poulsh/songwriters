<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "assign".
 *
 * @property int $id
 * @property string $type
 * @property int $artist_id
 * @property int $song_id
 * @property int $radio_id
 * @property int $media_id
 * @property int $playlist_id
 * @property int $tender_id
 * @property int $other_id
 * @property string $data
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class Assign extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const TYPE_MUSIC_STYLE = 'music_style';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'assign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_id', 'song_id', 'radio_id', 'media_id', 'playlist_id', 'tender_id', 'other_id', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['type', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'artist_id' => 'Artist ID',
            'song_id' => 'Song ID',
            'radio_id' => 'Radio ID',
            'media_id' => 'Media ID',
            'playlist_id' => 'Playlist ID',
            'tender_id' => 'Tender ID',
            'other_id' => 'Other ID',
            'data' => 'Data',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            ['class' => \yii\behaviors\TimestampBehavior::class],
        ];
    }


    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'type',
            'artist_id',
            'song_id',
            'radio_id',
            'media_id',
            'playlist_id',
            'tender_id',
            'other_id',
            'data',
            'status',
            'created_at',
            'updated_at',
        ];
    }

    public static function create($attributes)
    {
        $oModel = new self;
        $oModel->attributes = $attributes;
        $oModel->status = self::STATUS_ACTIVE;
        $oModel->save();
        return $oModel;
    }
}
