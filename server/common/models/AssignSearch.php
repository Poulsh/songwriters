<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Assign;

/**
 * AssignSearch represents the model behind the search form of `common\models\Assign`.
 */
class AssignSearch extends Assign
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'id',
                'artist_id',
                'song_id',
                'radio_id',
                'media_id',
                'playlist_id',
                'tender_id',
                'other_id',
                'created_at',
                'updated_at'], 'integer'],
            [['type', 'data', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Assign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'artist_id' => $this->artist_id,
            'song_id' => $this->song_id,
            'radio_id' => $this->radio_id,
            'media_id' => $this->media_id,
            'playlist_id' => $this->playlist_id,
            'tender_id' => $this->tender_id,
            'other_id' => $this->other_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
