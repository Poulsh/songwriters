<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $text
 * @property int $object_type
 * @property int $object_id
 * @property int $author_type
 * @property int $author_id
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Comment extends \yii\db\ActiveRecord
{

    const OBJECT_TYPE_SONG = 100;

    const AUTHOR_TYPE_RADIO = 100;
    const AUTHOR_TYPE_ARTIST = 101;

    const STATUS_ACTIVE = 100;


    public $radio_id;
    public $artist_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }


    public function behaviors()
    {
        return [
            ['class' => yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_type', 'object_id', 'author_type', 'author_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string', 'max' => 510],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'object_type' => 'Object Type',
            'object_id' => 'Object ID',
            'author_type' => 'Author Type',
            'author_id' => 'Author ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'text',
            'object_type',
            'object_id',
            'author_type',
            'author_id',
            'status',
            'author' => function ($q) {
                return $q->author;
            },
            'created_at',
            'updated_at',
        ];
    }


    /**
     * Создание комментрия от Радио по треку с отправкой оповещения артисту
     *
     * @param $radioId integer
     * @param $songId integer
     * @param $text string
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     *
     */

    public static function createSongCommentByRadio($radioId,$songId,$text){
        $song = Song::findOne($songId);

        if (!$song)  throw new NotFoundHttpException('Song not found');

        $model = new Comment();
        $model->object_type = Comment::OBJECT_TYPE_SONG;
        $model->object_id = $songId;
        $model->author_type = Comment::AUTHOR_TYPE_RADIO;
        $model->author_id = $radioId;
        $model->text = $text;
        $model->status = Comment::STATUS_ACTIVE;

        if ($model->save()) {
            $radio = Radio::findOne($radioId);
            // получить язык пользователя
            $aLang = User::getLanguageByUserID($song->artist['user_id']);
            if (!$aLang){
                $aLang = User::USER_LANG_DEFAULT;
            }


            switch ($aLang){
                case User::USER_LANG_RU:
                    $head = 'Новый комментарий';
                    break;
                case User::USER_LANG_EN:
                    $head = 'New comment';
                    break;
                default:
                    $head = 'Новый комментарий';
            }


            $notifyText = Comment::songNewCommentMessage($song->name,($aLang==User::USER_LANG_RU?'радио ':'radio ').'"'.$radio->name.'"', $aLang);


            $frontLink='/artist-my-song-view/'.$song->name_hash;



            $notification = Notification::create([
                'artist_id' => $song->artist_id,
                'type' => Notification::TYPE_SONG_NEW_COMMENT,
                'subject_id' => $song->id,
                'head' => $head,
                'text' => $notifyText,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => $frontLink,
            ],false,true, $song->artist['user_id']);


            return['success'=>  \Yii::$app->language=='ru-RU'?'комментарий успешно добавлен':'comment added'];
        } else {
            return ['error'=>(\Yii::$app->language=='ru-RU'?'ошибка добавления комментария':'comment adding failed'),'debug'=>Json::encode($model->errors)];
        }

    }

//    https://forum.yiiframework.com/t/conditional-relation/83383/7

//    public static function find()
//    {
//        $activeQuery = parent::find();
//        $activeQuery->addSelect([
//            'IF(author_type=' . self::AUTHOR_TYPE_RADIO . ', author_id, null) AS radio_id',
//            'IF(author_type=' . self::AUTHOR_TYPE_ARTIST . ', author_id, null) AS artist_id'
//        ]);
//        return $activeQuery;
//    }


    public function getAuthor()
    {
//        if ($this->author_type == Comment::AUTHOR_TYPE_RADIO) {
//            return $this->getRadio();
//        } elseif ($this->author_type == Comment::AUTHOR_TYPE_ARTIST){
//            return $this->getArtist();
//        }

        if ($this->author_type == Comment::AUTHOR_TYPE_RADIO) {
            return $this->hasOne(Radio::class,['id'=>'author_id'])
                ->asArray()
                ->select(['name','image','id','main_info','country','city']);
        }
    }

//    public function getRadio()
//    {
//        return $this->hasOne(Radio::class,['id'=>'radio_id'])
//            ->asArray()
//            ->select(['name','image','id','country','city']);
//    }
//
//    public function getArtist()
//    {
//        return $this->hasOne(Artist::class,['id'=>'artist_id'])
//            ->asArray()
//            ->select(['name','image','id','country','city']);
//    }


    public static function songNewCommentMessage($songName, $username, $lang){
        switch ($lang){
            case User::USER_LANG_RU:
                return 'Пользователь '.$username.' прокомментировал трек "'.$songName.'" .'.PHP_EOL;
            case User::USER_LANG_EN:
                return 'User '.$username.' commented song "'.$songName.'" .'.PHP_EOL;
            default:
                return 'Пользователь '.$username.' прокомментировал трек "'.$songName.'" .'.PHP_EOL;
        }
    }
}
