<?php

namespace common\models;

use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * This is the model class for table "imagefiles".
 *
 * @property int $id
 * @property string $name
 */
class Imagefiles extends \yii\db\ActiveRecord
{
    const TYPE_ARTIST_AVATAR = 'artist_avatar';
    const TYPE_RADIO_AVATAR = 'radio_avatar';
    const TYPE_MEDIA_AVATAR = 'media_avatar';
    const TYPE_SONG_ARTWORK = 'song_artwork';

    const REPLACE_NOIMAGE_RADIO = 'noimage_radio.jpg';
    const REPLACE_NOIMAGE_ARTIST = 'noimage_artist.jpg';
    const REPLACE_NOIMAGE_SONG = 'noimage_song.jpg';


//    public static $artworkFormats=[ 250, 750 ];

    public static $avatarFormats = [
        'sm'=>80,
        'md'=>440,
        'lg'=>1000
    ];
    public static $artworkFormats = [
        'sm'=>250,
        'md'=>750,
//        'lg'=>1960
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imagefiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 524],
//            [['name'], 'unique',  'message' => 'Файл "{value}" уже существует, измени имя загружаемого файла или назначь существующий'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }



    public function beforeDelete()
    {
        $file = Yii::$app->basePath . '/web/img/' . $this->name;
        if (file_exists($file)) {
            unlink($file);
        }
        if (strpos($this->name,'_sm.') === false &&
            strpos($this->name,'_md.')  === false &&
            strpos($this->name,'_lg.')  === false ) {
            $nameArr = explode(".", $this->name);
            $childSmName = $nameArr[0].'_sm.'.$nameArr[1];
            $small = self::findOne(['name'=>$childSmName]);
            if ($small) {
                $small->delete();
            }
            $childMdName = $nameArr[0].'_md.'.$nameArr[1];
            $medium = self::findOne(['name'=>$childMdName]);
            if ($medium) {
                $medium->delete();
            }
            $childLgName = $nameArr[0].'_lg.'.$nameArr[1];
            $large = self::findOne(['name'=>$childLgName]);
            if ($large) {
                $large->delete();
            }

        }

        return true;
    }


    public function addNew($name)
    {
        if (self::findOne(['name'=>$name])) {
            return true;
        }
        $this->name = $name;
        if ($this->validate()) {
            if ($this->save()) {
                return true;
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка сохранения imagefile');
                return false;
            }
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка валидации - '.$this->errors['name'][0]);
            return false;
        }
    }



    /**
     * Upload images for  model with autofill corresponding model property
     */
    public static function uploadImage($type,$objectId)
    {
        $uploadmodel = new UploadImgForm();
        if ($type == self::TYPE_ARTIST_AVATAR) {
            $oName = 'artist';
            $model = Artist::findOne($objectId);
            $toModelProperty = 'image';
            $formats = Imagefiles::$avatarFormats;
        }
        if ($type == self::TYPE_SONG_ARTWORK) {
            $oName = 'song';
            $model = Song::findOne($objectId);
            $toModelProperty = 'image';
            $formats = Imagefiles::$artworkFormats;
        }
        if ($type == self::TYPE_RADIO_AVATAR) {
            $oName = 'radio';
            $model = Radio::findOne($objectId);
            $toModelProperty = 'image';
            $formats = Imagefiles::$avatarFormats;
        }

        if ($type == self::TYPE_MEDIA_AVATAR) {
            $oName = 'media';
            $model = Media::findOne($objectId);
            $toModelProperty = 'image';
            $formats = Imagefiles::$avatarFormats;
        }

        $uploadmodel->imageFile = UploadedFile::getInstanceByName('image');

        $time = time();
        $fileName = $oName.$model->id.'-'.$toModelProperty;
        $extension = $uploadmodel->imageFile->extension;
        $filenameOrig = $fileName.'.'.$extension;
        if ($uploadmodel->upload($filenameOrig)) {


            // создаем нужные форматы картинок
            $webRoot = \Yii::getAlias('@webroot');


            $originalFile =$webRoot.'/img/'.$filenameOrig;

            if (!file_exists($originalFile)) {
                throw new NotFoundHttpException('Не найден оргинал. Not found original');
            }

            try {
                $origImg =Image::getImagine()->open( $originalFile);
            } catch (\Imagine\Exception\Exception $e) {
                throw new BadRequestHttpException('изображение повреждено или неправильного формата');
            }

            // dirty fix bug of horizontal big images
            $origData = $origImg->getSize();
            $origW = $origData->getWidth();
            $origH = $origData->getHeight();
            $autoRotate =  \yii\imagine\Image::autorotate($origImg);
            $autoRotateData = $autoRotate->getSize();
            $autoRotateW = $autoRotateData->getWidth();
            if ($origW != $autoRotateW) {
                unlink($originalFile);
                $autoRotate->save($originalFile);
                $origW = $autoRotateW;
                $origH = $autoRotateData->getHeight();
            }



            foreach ($formats as $key => $format) {
                $width = $format;
                $height = $format;

                if ($origW < $format || $origH < $format) {
                    if ($origW < $origH) {
                        $height = $origW;
                        $width = $origW;
                    } else if ($origW > $origH){
                        $height = $origH;
                        $width = $origH;
                    }
                }


                $filenameThumb = $fileName.'_'.$key.'.'.$extension;
                Image::thumbnail( $webRoot.'/img/'.$filenameOrig, $width, $height, ManipulatorInterface::THUMBNAIL_OUTBOUND)
                    ->save( $webRoot.'/img/'.$filenameThumb, ['quality' => 85]);
                // 'gaussian-blur'=>0.05
                // 'sampling-factor'=> '4:2:0'
                $imageFile = new Imagefiles();
                $imageFile->addNew($filenameThumb);
            }

            //аргументу с названием $toModelProperty присваиваем значение имени файла
            $model->$toModelProperty = $filenameOrig;
            $model->save();

            return $model;
        }
    }
}
