<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "language".
 *
 * @property int $id
 * @property string $name_orig
 * @property string $name_ru
 * @property string $name_eng
 * @property string $name_fr
 * @property string $name_ger
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property boolean $main
 * @property int $main_sort
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'language';
    }

    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::class,
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_ru'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'main_sort'], 'integer'],
            [['main'], 'boolean'],
            [['name_orig', 'name_ru', 'name_eng', 'name_fr', 'name_ger'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_orig' => 'Name Orig',
            'name_ru' => 'Name Ru',
            'name_eng' => 'Name Eng',
            'name_fr' => 'Name Fr',
            'name_ger' => 'Name Ger',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
