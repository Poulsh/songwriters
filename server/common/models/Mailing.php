<?php

namespace common\models;

use Yii;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;


class Mailing extends \yii\db\ActiveRecord
{





    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }


    public function behaviors()
    {
        return [
            ['class' =>  yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['to_id', 'from_id', 'dialog_id', 'attachment_id', 'author_id', 'created_at', 'updated_at'], 'integer'],
            [['from_to_type', 'attachment_type', 'status'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 5000],
        ];
    }




    public static function sendEmail($email)
    {
        $head = 'Мы возрождаемся, чтобы продолжать рассылать вам отличную музыку!';
        $subject = 'Radio news';
        $text = 'После быстрой бесплатной и легкой регистрации вам будет доступна наша растущая коллекция музыки, а отобранные композиции мы будем вам присылать.';
        $link = 'https://songwriters.pro/signup';
        $linkName = 'Зарегистрироваться';
        $songLink ='https://songwriters.pro/song/1606048209BNrKCrChUV';
        $songLinkName = 'Alex Savanin - Broke (feat. Patricia Edwards)';
        $songImageLink = 'https://api.songwriters.pro/img/song32-image_md.jpg';

        Yii::$app->mailer->htmlLayout = "layouts/mailing";

        $mailer = Yii::$app->mailer->compose('mailing', [
            'head'=>$head,
            'text'=>$text,
            'regLink'=>$link,
            'regLinkName'=>$linkName,
            'songLink'=>$songLink,
            'songLinkName'=>$songLinkName,
            'songImageLink'=>$songImageLink,
        ]);
        return $mailer->setFrom([Yii::$app->params['noreplyEmail']=>Yii::$app->params['noreplyName']])
            ->setTo($email)
            ->setSubject($subject)
            ->send();

    }

}
