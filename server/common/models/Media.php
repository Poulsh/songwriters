<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property int $user_id
 * @property int $page_id
 * @property string $name
 * @property string $country
 * @property string $city
 * @property string $hrurl
 * @property string $main_info
 * @property string $comment
 * @property string $image
 * @property string $image_alt
 * @property string $status
 * @property string $view
 * @property int $created_at
 * @property int $updated_at
 */
class Media extends \yii\db\ActiveRecord
{

    const SCENARIO_REGISTER = 'register';
    const SCENARIO_EDIT = 'edit';
    const STATUS_EMAIL_VERIFICATION = 'email_verification';
    const STATUS_ACTIVE = 'active';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
//                'createdAtAttribute' => 'date',
//                'updatedAtAttribute' => false,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'page_id', 'created_at', 'updated_at'], 'integer'],
            [['main_info', 'comment'], 'string'],
            [['name', 'country', 'city', 'hrurl', 'image', 'image_alt', 'status', 'view'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'page_id' => 'Page ID',
            'name' => 'Name',
            'country' => 'Country',
            'city' => 'City',
            'hrurl' => 'Hrurl',
            'main_info' => 'Main Info',
            'comment' => 'Comment',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'status' => 'Status',
            'view' => 'View',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REGISTER] = ['user_id','name', 'country', 'city'];
        $scenarios[self::SCENARIO_EDIT] = ['user_id','name', 'country', 'city', 'main_info' ];
        return $scenarios;

    }

    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'user_id',
            'page_id',
            'name',
            'country' ,
            'city',
            'hrurl',
            'main_info',
            'comment',
            'image',
            'image_alt',
            'status',
            'view',
            'created_at',
            'updated_at',
        ];
    }

    public static function create($attributes, $scenario = self::SCENARIO_REGISTER)
    {
        $oModel = new self;
        $oModel->setScenario($scenario);
        $oModel->attributes = $attributes;
        $oModel->status = self::STATUS_EMAIL_VERIFICATION;
        $oModel->save();
        return $oModel;
    }

    public function addMusicStyle($arrayStyles)
    {
        if ($arrayStyles && is_array($arrayStyles)) {
            foreach ($arrayStyles as $style) {
                Assign::create([
                    'type'=>Assign::TYPE_MUSIC_STYLE,
                    'media_id'=>$this->id,
                    'other_id'=>$style['id'],
                ]);
            }
        }

    }

    public  function getStyles()
    {
        return $this->hasMany(Menu::class,['id'=>'other_id'])
            ->viaTable('assign',['media_id'=>'id'],function($query){
                $query->andWhere([
                    'type'=>Assign::TYPE_MUSIC_STYLE,
                    'status'=>Assign::STATUS_ACTIVE
                ]);
            });
    }

    /**
     * Редактирование
     *
     * @param $id
     * @param $attributes
     *
     * @return static
     * @throws NotFoundHttpException
     */
    public static function edit($id, $attributes)
    {
        $oModel = self::findOne((int)$id);
        if (!$oModel) {
            throw new NotFoundHttpException();
        }
        $oModel->setScenario(self::SCENARIO_EDIT);
        $oModel->attributes = $attributes;
        $oModel->save();
        return $oModel;
    }


    public function beforeDelete()
    {
        \Yii::$app->authManager->revoke(\Yii::$app->authManager->getRole(User::ROLE_M), $this->user_id);
        return true;
    }
}
