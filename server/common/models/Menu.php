<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $url
 * @property string $description
 */

use creocoder\nestedsets\NestedSetsBehavior;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class Menu extends \yii\db\ActiveRecord
{
    public $sub;

    const ROOT_MUSIC_STYLES_URL = 'styles';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::class,
                 'treeAttribute' => 'tree',
            ],
        ];
    }

//    public function beforeSave($insert)
//    {
//        if (parent::beforeSave($insert)) {
//
//            $parent = self::find()->andWhere(['id'=>$this->sub])->one();
//            $this->prependTo($parent);
//            return true;
//        } else {
//            return false;
//        }
//    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new MenuQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['tree', 'lft', 'rgt', 'depth', 'sub'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['url'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 1000],
            [['tree', 'lft', 'rgt', 'depth'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tree' => 'Tree',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'depth' => 'Depth',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
        ];
    }

    public static function tree($rootUrl = null)
    {
        if ($rootUrl) {
            $root = Menu::find()->where(['url'=>$rootUrl])->one();
            if (!$root) {
                Throw new NotFoundHttpException;
            }
            $tree = self::find()
                ->where(['tree'=>$root->tree])
                ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                ->all();
        } else {
            $tree = self::find()
                ->orderBy(['tree'=> SORT_ASC, 'lft'=> SORT_ASC])
                ->all();
        }

        $newArr =[];
        $i = 0;
        foreach ($tree as $cat) {
            $newArr[$i] = $cat->attributes;
            $newArr[$i]['name'] = str_repeat('-', $cat['depth']).$cat['name'];
            $i++;
        }
        return $newArr;
    }

//    public static function filterTreeQnt($id_count_array)
//    {
//
//        $menuItems = self::find()
//            ->where(['id'=>array_keys($id_count_array)])
//            ->indexBy('id')
//            ->select('id,url,tree,lft,name,depth')
//            ->all();
//
//        $arrIndexById = ArrayHelper::toArray($menuItems);
//
//        foreach ($menuItems as $mItem) {
//
//            $parents = $mItem->parents()->asArray()->select('id,url,tree,lft,name,depth')->all();
//
//            $arrIndexById[$mItem['id']]['qnt']=$id_count_array[$mItem['id']];
//
//            foreach ($parents as $parent) {
//                if ($parent['url'] != self::ROOT_MI_URL && $parent['url'] != self::ROOT_SALE_URL ) {
//                    if (!isset($arrIndexById[$parent['id']])){
//                        $arrIndexById[$parent['id']] =  $parent;
//                        $arrIndexById[$parent['id']]['qnt'] = $id_count_array[$mItem['id']];
//                    } else {
//                        $arrIndexById[$parent['id']]['qnt'] += $id_count_array[$mItem['id']];
//                    }
//                }
//            }
//        }
//
//        ArrayHelper::multisort($arrIndexById, ['tree', 'lft'], [SORT_ASC, SORT_ASC]);
//
//        $newArr =[];
//        $i = 0;
//        foreach ($arrIndexById as $cat) {
//
//            if ($cat instanceof self) {
//                $newArr[$i] = $cat->attributes;
//            } else {
//                $newArr[$i] = $cat;
//            }
//
//            $newArr[$i]['name'] = str_repeat(' ', $cat['depth']).$cat['name'];
//
//            $i++;
//        }
//        return $newArr;
//    }

    public static function filterTree($id_array)
    {

        $menuItems = self::find()
            ->where(['id'=>array_keys($id_array)])
            ->indexBy('id')
            ->select('id,url,tree,lft,name,depth')
            ->all();

        $arrIndexById = ArrayHelper::toArray($menuItems);

        foreach ($menuItems as $mItem) {

            $parents = $mItem->parents()->asArray()->select('id,url,tree,lft,name,depth')->all();

            foreach ($parents as $parent) {
//                if ($parent['url'] != self::ROOT_MI_URL && $parent['url'] != self::ROOT_SALE_URL ) {
                    if (!isset($arrIndexById[$parent['id']])){
                        $arrIndexById[$parent['id']] =  $parent;
                    }
//                }
            }
        }

        ArrayHelper::multisort($arrIndexById, ['tree', 'lft'], [SORT_ASC, SORT_ASC]);

        $newArr =[];
        $i = 0;
        foreach ($arrIndexById as $cat) {

            if ($cat instanceof self) {
                $newArr[$i] = $cat->attributes;
            } else {
                $newArr[$i] = $cat;
            }

            $newArr[$i]['name'] = str_repeat('- ', $cat['depth']).$cat['name'];

            $i++;
        }
        return $newArr;
    }
}
