<?php

namespace common\models;

use function Couchbase\defaultDecoder;
use Yii;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property int $to_id
 * @property int $from_id
 * @property string $from_to_type
 * @property int $dialog_id
 * @property string $text
 * @property string $attachment_type
 * @property int $attachment_id
 * @property string $status
 * @property int $author_id
 * @property int $created_at
 * @property int $updated_at
 */
class Message extends \yii\db\ActiveRecord
{

    const STATUS_NOT_SEEN = 'not_seen';
    const STATUS_SEEN = 'seen';

    const SCENARIO_CREATE = 'create';
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_INDEX = 'index';
    const SCENARIO_VIEW = 'view';
    const SCENARIO_STATUS = 'status';

    const FROM_TO_ARTIST_TO_SYSTEM = 'artist_to_system';
    const FROM_TO_SYSTEM_TO_ARTIST = 'system_to_artist';





    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    public function behaviors()
    {
        return [
            ['class' =>  yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['to_id', 'from_id', 'dialog_id', 'attachment_id', 'author_id', 'created_at', 'updated_at'], 'integer'],
            [['from_to_type', 'attachment_type', 'status'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 5000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to_id' => 'To ID',
            'from_id' => 'From ID',
            'from_to_type' => 'From To Type',
            'dialog_id' => 'Dialog ID',
            'text' => 'Text',
            'attachment_type' => 'Attachment Type',
            'attachment_id' => 'Attachment ID',
            'status' => 'Status',
            'author_id' => 'Author ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->author_id && $this->author_id !==0) {
                    $this->author_id = Yii::$app->user->id;
                }

                if ($this->dialog) {
                    $this->dialog->updated_at=time();
                    $this->dialog->save();
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {

        return
            [
                'id',
                'from_to_type',
                'to_id' ,
                'from_id',
                'dialog_id',
                'text',
                'attachment_type',
                'attachment_id',
                'status',
                'author_id',
                'author' => function ($q) {
                    return $q->author?$q->author->toArray($q->author->scenarios()[User::SCENARIO_COMMON]):null;
                },
                'created_at',
                'updated_at',
            ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = [
            'id',
            'from_to_type',
            'to_id' ,
            'from_id',
            'dialog_id',
            'text',
            'attachment_type',
            'attachment_id',
            'status',
            'author_id',
            'created_at',
            'updated_at',
        ];

        $scenarios[self::SCENARIO_INDEX] = [
            'id',
            'from_to_type',
            'to_id' ,
            'from_id',
            'dialog_id',
            'text',
            'status',
            'author_id',
            'author',
            'created_at',
            'updated_at',

        ];
        $scenarios[self::SCENARIO_VIEW] = [
            'id',
            'from_to_type',
            'to_id' ,
            'from_id',
            'dialog_id',
            'text',
            'attachment_type',
            'attachment_id',
            'status',
            'author_id',
            'author',
            'created_at',
            'updated_at',
        ];

        $scenarios[self::SCENARIO_STATUS] = [
            'id',
            'status',
        ];

        return $scenarios;
    }


    public  function getAuthor()
    {
        return $this->hasOne(User::class,['id'=>'author_id']);
    }


    public  function getDialog()
    {
        return $this->hasOne(MessageDialog::class,['id'=>'dialog_id']);
    }


    /**
     * Создание с созданием диалога
     * @param $attributes
     * @return array
     */
    public static function create($attr, $mailToAdmin = false)
    {
        $oModel = new self();
        $oModel->setScenario(self::SCENARIO_CREATE);

        $oModel->load($attr);

        $fromName = '';
        $link='';
        $linkName = '';
        $notificationType = '';

        $artistId = null;
        if ($oModel->from_to_type == self::FROM_TO_ARTIST_TO_SYSTEM) {
            $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
            if (!$artist) return ['error'=> 'нет назначенного артиста'];
            $oModel->from_id = $artist->id;
            $artistId = $artist->id;
            $fromName = 'артиста "'.$artist->name.'"';
        }


        $dialog = new MessageDialog();
        $dialog->setScenario(MessageDialog::SCENARIO_CREATE);
        $request = $attr['Message'];
        $dialog->attributes = $request;



        if ($dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION) {
            $dialog->artist_id = $artistId ;
            $dialog->type = MessageDialog::TYPE_RADIO_ITEM;
            $link .='/radio-admin/view?id='.$dialog->subject_id;
            $linkName = 'К позиции в радио секции'; // админу всегда на русском
            $notificationType = Notification::TYPE_ADMIN_NEW_MESSAGE_IN_RADIO_SECTION;
        }
        else if ($dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION) {
            $dialog->artist_id = $artistId ;
            $dialog->type = MessageDialog::TYPE_PREMIUM_ITEM;
            $link .='/premium/view?id='.$dialog->subject_id;
            $linkName = 'К позиции в секции Selected tracks'; // админу всегда на русском
            $notificationType = Notification::TYPE_ADMIN_NEW_MESSAGE_IN_PREMIUM_SECTION;

        }



        $dialog->status = MessageDialog::STATUS_ACTIVE;

        if (!$dialog->save())  return ['error'=> \Yii::$app->language=='ru-RU'?'ошибка сохранения диалога':'Failed to save dialogue'];


        if ($dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION ||
            $dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION) {
            $item = PlaylistItem::findOne($dialog->subject_id);
            $item->dialog_id = $dialog->id;
            $item->save();
        }


        $oModel->dialog_id = $dialog->id;
        $oModel->status = self::STATUS_NOT_SEEN;
        $oModel->author_id = Yii::$app->user->id;


        if ($oModel->save()){

            $notification = Notification::create([
                'type' => $notificationType,
                'subject_id' => 'new_message_'.$oModel->id,
                'head' => 'Новое сообщение от '.$fromName, // админу всегда на русском
                'text' => '<small>'.$notificationType.'<br> Текст сообщения:</small><br>'. $oModel->text,
                'call2action_name' => $linkName,
                'call2action_link' => $link,
            ],$mailToAdmin);

        } else {
            return ['error'=>\Yii::$app->language=='ru-RU'?'Ошибка сохранения сообщения':'Failed to save the message'];
        }

        if (!$oModel->errors) {
            return [
                'success'=>\Yii::$app->language=='ru-RU'?'Сообщение отправлено':'message sent',
                'dialog'=>$dialog,
            ];
        } else {
            return ['error'=>(\Yii::$app->language=='ru-RU'?'Ошибка отправки':'Failed to send the message'),'errors'=>$oModel->errors];
        }

    }



    public static function addReplyToDialog($id,$attr){
        $dialog = MessageDialog::findOne($id);
        if (!$dialog) throw new NotFoundHttpException;
        $message = new Message();
        $message->load($attr);

        if (!$message->to_id && !$message->from_id) { // если не определен адресат
            if ($dialog->subject_type == MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION ||
                $dialog->subject_type == MessageDialog::SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION) {
                $message->from_id = $dialog->artist_id;
            }
        }

        $message->dialog_id = $dialog->id;
        $message->status = self::STATUS_NOT_SEEN;
        $message->author_id = Yii::$app->user->id;
        $message->save();

        if (!$message->errors) {
            $fromName = '';
            $link='';
            $linkName = 'незапилено';
            $notificationType = '';

            if ($dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION) {

                $link .='/radio-admin/view?id='.$dialog->subject_id;
                $linkName = 'К позиции в радио секции'; // админу на русском
                $notificationType = Notification::TYPE_ADMIN_NEW_MESSAGE_IN_RADIO_SECTION;
                $sender = Artist::findOne(['id'=>$dialog->artist_id]);
                $fromName = 'артиста "'.$sender->name.'"';
            }
            else if ($dialog->subject_type==MessageDialog::SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION) {

                $link .='/premium/view?id='.$dialog->subject_id;
                $linkName = 'К позиции в секции Selected tracks'; // админу на русском
                $notificationType = Notification::TYPE_ADMIN_NEW_MESSAGE_IN_PREMIUM_SECTION;
                $sender = Artist::findOne(['id'=>$dialog->artist_id]);
                $fromName = 'артиста "'.$sender->name.'"';
            }
            $notification = Notification::create([
                'type' => $notificationType,
                'subject_id' => 'new_message_'.$message->id,
                'head' => 'Новое сообщение от '.$fromName, // админу на русском
                'text' => '<small>'.$notificationType.'<br> Текст сообщения:</small><br>'. $message->text,
                'call2action_name' => $linkName,
                'call2action_link' => $link,
            ],true);

            return ['success'=>\Yii::$app->language=='ru-RU'?'Сообщение отправлено':'Message sent'];
        } else {
            return ['error'=>(\Yii::$app->language=='ru-RU'?'Ошибка отправки':'Failed to send the message'),'errors'=>$message->errors];
        }
    }


    public function sendSystemMessageToArtistForRadioItem($id){
        if ($this->from_to_type!=self::FROM_TO_SYSTEM_TO_ARTIST) {
            throw new BadRequestHttpException('from_to_type invalid');
        }
        $item = PlaylistItem::findOne($id);
        $song = $item->song;
        $aLang = User::getLanguageByUserID($item->song->artist['user_id']);

        if (!$this->dialog_id) {
            $dialog = MessageDialog::find()->where(['id'=>$item->dialog_id])->one();
            if (!$dialog) {
                $dialog = new MessageDialog();
                $dialog->setScenario(MessageDialog::SCENARIO_CREATE);
                $dialog->type = MessageDialog::TYPE_RADIO_ITEM;
                $dialog->artist_id = $this->to_id;
                $dialog->subject_type = MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION;
                $dialog->subject_id = $id;
                $dialog->name = (
                    $aLang==User::USER_LANG_RU ?
                        'Позиция в радио секции: (#'.$id.') ' :
                        'Position in radio section: (#'.$id.') '
                    ).$song->name;
                $dialog->status = MessageDialog::STATUS_ACTIVE;

                if (!$dialog->save())  return ['error'=>'ошибка сохранения диалога']; // админу на русском
                $this->dialog_id = $dialog->id;

                $item->dialog_id = $dialog->id;
                $item->save();
            } else {
                $this->dialog_id = $dialog->id;
            }


        }
        $this->status = self::STATUS_NOT_SEEN;
        $res = $this->save();

        if ($res) {
            switch ($aLang){
                case User::USER_LANG_RU:
                    $head = 'Новое сообщение в радио секции';
                    $text = 'Новое сообщение от Songwriters по треку "'.$song->name.'"'.'<br><small> Текст сообщения:</small><br>'. $this->text;
                    break;
                case User::USER_LANG_EN:
                    $head = 'New message in radio section';
                    $text = 'New message from Songwriters about song "'.$song->name.'"'.'<br><small> Message text:</small><br>'. $this->text;
                    break;
                default:
                    $head = 'Новое сообщение в радио секции';
                    $text = 'Новое сообщение от Songwriters по треку "'.$song->name.'"'.'<br><small> Текст сообщения:</small><br>'. $this->text;
            }



            $artist = Artist::findOne($this->to_id);
            $userId = $artist->user_id;
            Notification::sendWebsocketEvent($userId,
                Notification::EVENT_RADIO_SECTION_ITEM_NEW_MESSAGE,[
                    'itemId'=>$item->id,

                ]);

            $notification = Notification::create([
                'artist_id' => $this->to_id,
                'type' => Notification::TYPE_SONG_IN_RADIO_SECTION_NEW_COMMENT,
                'subject_id' => 'new_comment_to_radio_item_'.$item->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => '/artist-radio/'.$item->id,
            ],false,true,$userId);
        }

        return $res;

//        echo '<pre>'.print_r($this->attributes,true).'</pre>';
//        die;

    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);



    }

    public function sendSystemMessageToArtistForPremiumItem($id){
        if ($this->from_to_type!=self::FROM_TO_SYSTEM_TO_ARTIST) {
            throw new BadRequestHttpException('from_to_type invalid');
        }
        $item = PlaylistItem::findOne($id);
        $frontLink = '';
        if ($item->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            $sectionName = 'Selected RU';
            $frontLink = '/artist-selected/'.$item->id;

        } else if ($item->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            $sectionName = 'Selected World';
            $frontLink = '/artist-selected/'.$item->id;
        }

        $song = $item->song;
        $aLang = User::getLanguageByUserID($item->song->artist['user_id']);

        if (!$this->dialog_id) {
            $dialog = MessageDialog::find()->where(['id'=>$item->dialog_id])->one();
            if (!$dialog) {
                $dialog = new MessageDialog();
                $dialog->setScenario(MessageDialog::SCENARIO_CREATE);
                $dialog->type = MessageDialog::TYPE_PREMIUM_ITEM;
                $dialog->artist_id = $this->to_id;
                $dialog->subject_type = MessageDialog::SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION;
                $dialog->subject_id = $id;

                switch ($aLang){
                    case User::USER_LANG_RU:
                        $dialog->name = 'Позиция в премиум секции: (#'.$id.') '.$song->name;
                        break;
                    case User::USER_LANG_EN:
                        $dialog->name = 'Position in premium section: (#'.$id.') '.$song->name;
                        break;
                    default:
                        $dialog->name = 'Позиция в премиум секции: (#'.$id.') '.$song->name;
                }

                $dialog->status = MessageDialog::STATUS_ACTIVE;

                if (!$dialog->save())  return ['error'=>'ошибка сохранения диалога'];
                $this->dialog_id = $dialog->id;

                $item->dialog_id = $dialog->id;
                $item->save();
            } else {
                $this->dialog_id = $dialog->id;
            }
        }
        $this->status = self::STATUS_NOT_SEEN;
        $res = $this->save();

        if ($res) {
            switch ($aLang){
                case User::USER_LANG_RU:
                    $head = 'Новое сообщение в секции '.$sectionName;
                    $text = 'Сообщение от Songwriters по треку '.$song->name.'<br><small> Текст сообщения:</small><br>'. $this->text;
                    break;
                case User::USER_LANG_EN:
                    $head = 'New message in section '.$sectionName;
                    $text = 'Message from Songwriters about song '.$song->name.'<br><small> Message text:</small><br>'. $this->text;
                    break;
                default:
                    $head = 'Новое сообщение в секции '.$sectionName;
                    $text = 'Сообщение от Songwriters по треку '.$song->name.'<br><small> Текст сообщения:</small><br>'. $this->text;
            }


            $artist = Artist::findOne($this->to_id);
            $userId = $artist->user_id;
            Notification::sendWebsocketEvent($userId,
                Notification::EVENT_PREMIUM_SECTION_ITEM_NEW_MESSAGE,[
                    'itemId'=>$item->id,
                ]);


            $notification = Notification::create([
                'artist_id' => $this->to_id,
                'type' => Notification::TYPE_SONG_IN_PREMIUM_SECTION_NEW_COMMENT,
                'subject_id' => 'new_comment_to_premium_item_'.$item->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => $frontLink,
            ],false,true,$userId);
        }

        return $res;
    }
}
