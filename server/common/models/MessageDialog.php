<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message_dialog".
 *
 * @property int $id
 * @property string $type
 * @property int $artist_id
 * @property int $radio_id
 * @property int $media_id
 * @property string $subject_type
 * @property string $subject_id
 * @property string $name
 * @property string $status
 * @property int $author_id
 * @property int $created_at
 * @property int $updated_at
 */
class MessageDialog extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const SUBJECT_TYPE_ITEM_IN_RADIO_SECTION = 'item_in_radio_section';
    const SUBJECT_TYPE_ITEM_IN_PREMIUM_SECTION = 'item_in_premium_section';


    const TYPE_RADIO_ITEM = 'radio_item';
    const TYPE_PREMIUM_ITEM = 'premium_item';


    const SCENARIO_CREATE = 'create';
    const SCENARIO_INDEX = 'index';
    const SCENARIO_VIEW = 'view';
    const SCENARIO_CHECKSUM = 'check_sum';



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message_dialog';
    }

    public function behaviors()
    {
        return [
            ['class' =>  yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_id', 'radio_id', 'media_id',
                'author_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'subject_type', 'subject_id', 'name', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'artist_id' => 'Artist ID',
            'radio_id' => 'Radio ID',
            'media_id' => 'Media ID',
            'subject_type' => 'Subject Type',
            'subject_id' => 'Subject ID',
            'name' => 'Name',
            'status' => 'Status',
            'author_id' => 'Author ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }



    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->author_id && $this->author_id !== 0) {
                    $this->author_id = Yii::$app->user->id;
                }

            }
            if (!$this->name ) {

                if ($this->subject_type == self::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION) {
                    $_model = PlaylistItem::findOne(['id'=>$this->subject_id]);
                    $song = $_model->song->toArray();
                    $this->name = 'Позиция в радио секции: (#'.$this->subject_id.') '.
                        $song['name'].' '.$song['artist_name'];
                }

            }

            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        $children = $this->messages;
        if (count($children)>0) {
            foreach ($children as $child) {
                $child->delete();
            }
        } else {
            Yii::$app->session->setFlash('success', 'нет сообщений');
        }
        return true;
    }
    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {
//
//        'id' => 'ID',
//            'type' => 'Type',
//            'artist_id' => 'Artist ID',
//            'radio_id' => 'Radio ID',
//            'media_id' => 'Media ID',
//            'subject_type' => 'Subject Type',
//            'subject_id' => 'Subject ID',
//            'name' => 'Name',
//            'status' => 'Status',
//            'author_id' => 'Author ID',
//            'created_at' => 'Created At',
//            'updated_at' => 'Updated At',

        return
            [
                'id',
                'type',
                'artist_id',
                'radio_id',
                'media_id',
                'name',

                'subject_type',
                'subject_id',

                'messages'=> function ($q) {
                    return $q->messages;
                },
//                'last_message'=> function ($q) {
//                    $_messages = $q->messages;
//                    if ($_messages) {
//                        $model = $_messages[count($_messages)-1];
//                        return $model->toArray($model->scenarios()[Message::SCENARIO_DIALOG_LAST_MESSAGE]);
//                    }
//                },
                'messagesQnt'=> function ($q) {
                    return count($q->messages);
                },
                'notSeenQnt'=> function ($q) {
                    $res=0;
                    foreach ($q->messages as $message) {
                        if ($message->author_id!=Yii::$app->user->id &&
                            $message->status == Message::STATUS_NOT_SEEN) {
                            $res++;
                        }
                    };
                    return $res;
                },

                'author_id',
                'author'=> function ($q) {
                    return $q->author?$q->author->toArray($q->author->scenarios()[User::SCENARIO_COMMON]):null;
                },
                'status',
                'created_at',
                'updated_at',
            ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = [
            'id',
            'type',
            'artist_id',
            'radio_id',
            'media_id',
            'name',
            'subject_type',
            'subject_id',
            'author_id',
            'status',
            'created_at',
            'updated_at',
        ];

        $scenarios[self::SCENARIO_INDEX] = [
            'id',
            'type',
            'artist_id',
            'radio_id',
            'media_id',
            'name',
            'subject_type',
            'subject_id',
            'author_id',
            'status',
            'created_at',
            'updated_at',

            'messagesQnt',
            'notSeenQnt',

        ];

        $scenarios[self::SCENARIO_VIEW] = [
            'id',
            'type',
            'artist_id',
            'radio_id',
            'media_id',
            'name',
            'subject_type',
            'subject_id',
            'author_id',
            'status',
            'created_at',
            'updated_at',

            'messagesQnt',
            'notSeenQnt',

            'author',


        ];


        return $scenarios;
    }
    public  function getAuthor()
    {
        return $this->hasOne(User::class,['id'=>'author_id']);
    }

    public function getMessages()
    {
        return $this->hasMany(Message::class,['dialog_id'=>'id']);
    }



    public static function getArtistDialogBySubjectAndMarkAsSeen($subjectType,$subjectId){
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);

        $oModel =  MessageDialog::findOne([
            'subject_type'=>$subjectType,
            'subject_id'=>$subjectId,
            'artist_id'=>$artist->id,
        ]);
        if (!$oModel) {return false;}
        foreach ($oModel->messages as $message) {
            if ($message->author_id != Yii::$app->user->id && $message->status == Message::STATUS_NOT_SEEN) {
                $message->status = Message::STATUS_SEEN;
                $message->save();
            }
        }
        return $oModel;
    }

    public static function getArtistDialogAndMarkAsSeen($id){
        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        $oModel =  MessageDialog::findOne([
            'id'=>$id,
            'artist_id'=>$artist->id,
        ]);
        if (!$oModel) {return false;}
        foreach ($oModel->messages as $message) {
            if ($message->author_id != Yii::$app->user->id && $message->status == Message::STATUS_NOT_SEEN) {
                $message->status = Message::STATUS_SEEN;
                $message->save();
            }
        }
        return $oModel;
    }


//    public static function getArtistPremiumsecBySubjectAndMarkAsSeen($subjectType,$subjectId){
//        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
//
//        $oModel =  MessageDialog::findOne([
//            'subject_type'=>$subjectType,
//            'subject_id'=>$subjectId,
//            'artist_id'=>$artist->id,
//        ]);
//        if (!$oModel) {return false;}
//        foreach ($oModel->messages as $message) {
//            if ($message->author_id != Yii::$app->user->id && $message->status == Message::STATUS_NOT_SEEN) {
//                $message->status = Message::STATUS_SEEN;
//                $message->save();
//            }
//        }
//        return $oModel;
//    }

}
