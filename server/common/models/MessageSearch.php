<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Message;

/**
 * MessageSearch represents the model behind the search form of `common\models\Message`.
 */
class MessageSearch extends Message
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'to_id', 'from_id', 'dialog_id', 'attachment_id', 'author_id', 'created_at', 'updated_at'], 'integer'],
            [['from_to_type', 'text', 'attachment_type', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'to_id' => $this->to_id,
            'from_id' => $this->from_id,
            'dialog_id' => $this->dialog_id,
            'attachment_id' => $this->attachment_id,
            'author_id' => $this->author_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'from_to_type', $this->from_to_type])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'attachment_type', $this->attachment_type])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
