<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $artist_id
 * @property int $radio_id
 * @property int $media_id
 * @property int $user_id
 * @property string $type
 * @property string $subject_id
 * @property string $head
 * @property string $text
 * @property string $call2action_name
 * @property string $call2action_link
 * @property string $status
 * @property int $created_at
 * @property int $updated_at
 */
class Notification extends \yii\db\ActiveRecord
{

    const STATUS_NOT_SEEN = 'not_seen';
    const STATUS_CLOSED = 'closed';
    const STATUS_SUBJECT_DONE = 'subject_done';



    const TYPE_SONG_IN_RADIO_SECTION_NEW_COMMENT = 'song_in_radio_section_new_comment';
    const TYPE_SONG_IN_RADIO_SECTION_CHANGE_STATUS = 'song_in_radio_section_change_status';

    const TYPE_SONG_NEW_COMMENT = 'song_new_comment';

    const TYPE_SONG_IN_PREMIUM_SECTION_CHANGE_STATUS = 'song_in_premium_section_change_status';
    const TYPE_SONG_IN_PREMIUM_SECTION_NEW_COMMENT = 'song_in_premium_section_new_comment';

    const TYPE_ADMIN= 'admin';
    const TYPE_ADMIN_NEW_RADIO_SECTION_SONG = 'admin_new_radio_section_song';
    const TYPE_ADMIN_NEW_SELECTED_RU = 'admin_new_selected_ru';
    const TYPE_ADMIN_NEW_SELECTED_WORLD = 'admin_new_selected_world';
    const TYPE_ADMIN_REMOVED_RADIO_SECTION_SONG = 'admin_removed_radio_section_song';
    const TYPE_ADMIN_NEW_RADIO = 'admin_new_radio';
    const TYPE_ADMIN_NEW_MESSAGE_IN_RADIO_SECTION = 'admin_new_message_in_radio_section';
    const TYPE_ADMIN_NEW_MESSAGE_IN_PREMIUM_SECTION = 'admin_new_message_in_premium_section';


    public static $typesOfAdminNotifications = [
        self::TYPE_ADMIN,
        self::TYPE_ADMIN_NEW_RADIO_SECTION_SONG,
        self::TYPE_ADMIN_NEW_SELECTED_RU,
        self::TYPE_ADMIN_NEW_SELECTED_WORLD,
        self::TYPE_ADMIN_REMOVED_RADIO_SECTION_SONG,
        self::TYPE_ADMIN_NEW_RADIO,
        self::TYPE_ADMIN_NEW_MESSAGE_IN_RADIO_SECTION,
        self::TYPE_ADMIN_NEW_MESSAGE_IN_PREMIUM_SECTION,

    ];


    const TCP_PATH = 'tcp://127.0.0.1:1234';

    const EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE = 'radioSectionItemStatusChange';
    const EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE = 'premiumSectionItemStatusChange';

    const EVENT_RADIO_SECTION_ITEM_NEW_MESSAGE = 'radio_section_item_new_message';
    const EVENT_PREMIUM_SECTION_ITEM_NEW_MESSAGE = 'premium_section_item_new_message';


    const EVENT_USER_CHECK_SUM = 'userCheckSum';

    const CALL_2_ACTION_NO_BUTTON = 'click_on_item';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }


    public function behaviors()
    {
        return [
            ['class' =>  yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['artist_id', 'radio_id', 'media_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'subject_id', 'head', 'call2action_name', 'call2action_link', 'status'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 5000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'artist_id' => 'Artist ID',
            'radio_id' => 'Radio ID',
            'media_id' => 'Media ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'subject_id' => 'Subject ID',
            'head' => 'Head',
            'text' => 'Text',
            'call2action_name' => 'Call2action Name',
            'call2action_link' => 'Call2action Link',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->status ) {
                    $this->status = self::STATUS_NOT_SEEN;
                }
            }
            return true;
        }
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);


        if (in_array($this->type,self::$typesOfAdminNotifications)) {  // оповещение администратору
            if (isset(Yii::$app->params['swAdminEmail'])) {
                $admin = User::findOne(['email'=>Yii::$app->params['swAdminEmail']]);
                Notification::sendWebsocketEvent($admin->id,$this->type,[
                    'type'=>$this->type,
                    'subject_id'=>$this->subject_id,
                ]);
            }


        } else {   // оповещение пользователю

            if ($this->status != self::STATUS_CLOSED && $this->status != self::STATUS_SUBJECT_DONE) {
                $userId = $this->user_id;
                if (!$userId) {
                    if ($this->artist_id) {
                        $userId = Artist::findOne($this->artist_id)->user_id;
                    }
                    elseif ($this->radio_id) {
                        $userId = Radio::findOne($this->radio_id)->user_id;
                    }
                    elseif ($this->media_id) {
                        $userId = Media::findOne($this->media_id)->user_id;
                    }
                }

                Notification::sendWebsocketEvent($userId,self::EVENT_USER_CHECK_SUM,[
                    'userCheckSum'=>User::checkSum($userId),
                ]);

            }
        }


    }


    public static function notSeenYetToUser($id)
    {
        $provider =  NotificationSearch::getNotSeenToUser($id);
        return $provider;
    }



    public function getArtist()
    {
        return $this->hasOne(Artist::class,['id'=>'artist_id']);
    }

    public function getRadio()
    {
        return $this->hasOne(Radio::class,['id'=>'radio_id']);
    }

    public function getMedia()
    {
        return $this->hasOne(Media::class,['id'=>'media_id']);
    }

    /**
     * Создание
     * @param $attributes
     * @return Notification
     */
    public static function create($attr, $mailToAdmin=null, $mailToUser=null,$userId=null)
    {
        $oModel = new self();
//        $oModel->setScenario(self::SCENARIO_CREATE);
        $oModel->attributes = $attr;
        $oModel->subject_id = isset($attr['subject_id'])?strval($attr['subject_id']):null;
        $oModel->status = self::STATUS_NOT_SEEN;
        if ($oModel->save()){
            if ($mailToAdmin && isset(Yii::$app->params['swAdminEmail'])) {
                $user = User::findOne(['email'=>Yii::$app->params['swAdminEmail']]);
                $user->sendEmailNotification(
                    $attr['head'],
                    $attr['text'],
                    Yii::$app->params['swAdminDomain'].$attr['call2action_link'],
                    $attr['call2action_name']
                );
            }
            if ($mailToUser) {
                $user = User::findOne(['id'=>$userId]);
                $lang = User::getLanguageByUserID($userId);
                if ($attr['call2action_name']==Notification::CALL_2_ACTION_NO_BUTTON) {
                    $attr['call2action_name'] = $lang=='ru'?'Перейти':'Go';
                }
                $user->sendEmailNotification(
                    $attr['head'],
                    $attr['text'],
                    Yii::$app->params['verificationDomain'].($lang=='ru'?'/ru/#':'/en/#').$attr['call2action_link'],
                    $attr['call2action_name']
                );
            }
        } else {
            throw new  BadRequestHttpException( Json::encode($oModel->errors));
        }
    }


    public static function setStatus($id, $status)
    {
        $model = self::findOne($id);
        if (!$model) {throw new NotFoundHttpException;}
        if ($status != self::STATUS_NOT_SEEN &&
            $status != self::STATUS_SUBJECT_DONE &&
            $status != self::STATUS_CLOSED ) {
            if (!$model) {throw new NotFoundHttpException('статус неизвестен');}
        }
        $model->status = $status;
        if ($status==self::STATUS_SUBJECT_DONE) {
            $notes = self::find()->where(['subject_id'=>$model->subject_id])->all();
            foreach ($notes as $note) {
                if ($note->id != $id) {
                    $note->status = self::STATUS_SUBJECT_DONE;
                    $note->save();
                }
            }
        }
        return $model->save();
    }

    public static function sendWebsocketEvent($userId, $event, $data)
    {
        error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
        $fp = stream_socket_client(Notification::TCP_PATH, $errno, $errstr);
        if ($fp) {
            fwrite($fp, Json::encode(
                    [
                        'user_id' => $userId,
                        'message' => [
                            'event'=>$event,
                            'data'=> $data
                        ]
                    ])  . "\n");
            fclose($fp);
        }

    }



    public static function markAsSeenAll($id)
    {
        $query = Notification::find();
        $i=0;

        $artist = Artist::findOne(['user_id'=>$id]);
        $radio = Radio::findOne(['user_id'=>$id]);
        $media = Media::findOne(['user_id'=>$id]);

        if ($artist ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['artist_id'=> $artist->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }
        if ($radio ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['radio_id' => $radio->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }

        if ( $media) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['media_id' => $media->id],
                    ],
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }

        $models = $query->all();
        foreach ($models as $model) {
            $model->status = Notification::STATUS_CLOSED;
            $model->save();
            $i++;
        }

        if ($i>0) {
            return ['success'=>'Отмечено прочитанными '.$i.' сообщений'];
        } else {
            return ['error'=>'нет непрочитанных оповещений'];
        }

    }

    public static function notSeenToAdminQnt(){
        return  Notification::find()
            ->where([
                'type'=> Notification::$typesOfAdminNotifications,
                'status'=> Notification::STATUS_NOT_SEEN,
            ])
            ->count();
    }

    public static function setAllAdminSeen(){
        $notifications = Notification::find()
            ->where([
                'type'=> Notification::$typesOfAdminNotifications,
                'status'=> Notification::STATUS_NOT_SEEN,
            ])
            ->all();
        $count=0;
        foreach ($notifications as $notification) {
            $notification->status = Notification::STATUS_CLOSED;
            $notification->save();
            $count++;
        }
        if ($count>0) {
            Yii::$app->session->addFlash('success', 'Отмечено просмотренными '.$count.' оповещений');
        }
    }
}
