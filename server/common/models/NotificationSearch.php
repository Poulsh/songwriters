<?php

namespace common\models;

use common\models\Notification;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NotificationSearch represents the model behind the search form of `common\models\Notification`.
 */
class NotificationSearch extends Notification
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'artist_id', 'radio_id', 'media_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'subject_id', 'head', 'text', 'call2action_name', 'call2action_link', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'artist_id' => $this->artist_id,
            'radio_id' => $this->radio_id,
            'media_id' => $this->media_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'subject_id', $this->subject_id])
            ->andFilterWhere(['like', 'head', $this->head])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'call2action_name', $this->call2action_name])
            ->andFilterWhere(['like', 'call2action_link', $this->call2action_link])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }





    public static function getNotSeenToUser($id)
    {
        $query = Notification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=> [
                'pageSize' => 100,
            ],
            'sort' =>[
                'defaultOrder'=> [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $artist = Artist::findOne(['user_id'=>$id]);
        $radio = Radio::findOne(['user_id'=>$id]);
        $media = Media::findOne(['user_id'=>$id]);

        if ($artist ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['artist_id'=> $artist->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }
        if ($radio ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['radio_id' => $radio->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }

        if ( $media) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['media_id' => $media->id],
                    ],
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }

        return $dataProvider->getModels();
    }



    public function adminNotification($params)
    {
        $query = Notification::find();

        // add conditions that should always apply here
        $query->where(['type' => Notification::$typesOfAdminNotifications]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'subject_id', $this->subject_id])
            ->andFilterWhere(['like', 'head', $this->head])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'call2action_name', $this->call2action_name])
            ->andFilterWhere(['like', 'call2action_link', $this->call2action_link])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
