<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "playlist".
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $object_type
 * @property int $object_id
 * @property int $author_id
 * @property string $status
 * @property string $view
 * @property int $created_at
 * @property int $updated_at
 */
class Playlist extends \yii\db\ActiveRecord
{

    const TYPE_BLACKLIST = 'blacklist';
    const TYPE_PLAYLIST = 'playlist';
    const TYPE_RADIO_SECTION = 'radio_section';
    const TYPE_PREMIUM_SECTION = 'premium_section';

    const TYPE_PREMIUM_COMMON = 'premium_common';
    const TYPE_RADIO_COMMON = 'radio_common';


    const OBJECT_TYPE_RADIO = 'radio';
    const OBJECT_TYPE_ARTIST = 'artist';
    const STATUS_ACTIVE = 'active';





    const SCENARIO_INDEX = 'index';
    const SCENARIO_VIEW = 'view';
    const SCENARIO_ARTIST_HOME = 'artist_home';



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlist';
    }

    public function behaviors()
    {
        return [
            ['class' =>  yii\behaviors\TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['object_id', 'author_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'name', 'object_type', 'status', 'view'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'object_type' => 'Object Type',
            'object_id' => 'Object ID',
            'author_id' => 'Author ID',
            'status' => 'Status',
            'view' => 'View',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function fields()
    {
        return [
            'id' ,
            'type',
            'name',
            'object_type',
            'object_id',
            'author_id',
            'status',
            'items'=> function ($q) {
                if ($q->items) {
                    return $q->items;
                }
            },
            'items_artist_home'=> function ($q) {
                return $q->itemsArtistHome;
            },
            'items_qnt'=> function ($q) {
                if ($q->items) {
                    return count($q->items);
                }
            },
            'view',
            'created_at',
            'updated_at',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_INDEX] = ['id','name','items_qnt'];
        $scenarios[self::SCENARIO_VIEW] = ['id','name','items'];
        $scenarios[self::SCENARIO_ARTIST_HOME] = ['id','name','items_artist_home'];
        return $scenarios;

    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->author_id ) {
                    $this->author_id = Yii::$app->user->id;
                }
                if (!$this->status ) {
                    $this->status = static::STATUS_ACTIVE;
                }
            }
            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        if ($this->items){
            foreach ($this->items as $item) {
                $item->delete();
            }
        }
        return true;
    }


    public  function getItems()
    {
        return $this->hasMany(PlaylistItem::class,['playlist_id'=>'id'])
            ->orderBy(['playlist_item.order_num'=>SORT_ASC]);
    }

    public  function getItemsArtistHome()
    {
      $arr = [];
      $items = $this->items;
        if (count($items)>0) {
            foreach ($items as $item) {
                $arr[] = $item->toArray([
                    'id',
                    'song_name',
                    'status',
                    'service_type',
                    'messages_qnt',
                ]);
            }
        }
        return $arr;
    }
//     например
    public  function getItemsCut()
    {
        return PlaylistItem::find()
            ->andWhere(['playlist_id'=>$this->id])
            ->joinWith([
                'song' => function ($q) {
                    $q->select([
                          Song::tableName().'.id',
                          Song::tableName().'.name',
                          Song::tableName().'.radio_status',
                          Song::tableName().'.premium_ru_status',
                          Song::tableName().'.premium_world_status',
                    ]);
                },
                'dialog' => function ($q) {
                    $q
                        ->select([
                            MessageDialog::tableName().'.id',
                        ])
                        ->joinWith([
                            'messages' => function ($q) {
                                $q
//                                    ->andWhere(['!=', Message::tableName().'.author_id', Yii::$app->user->id])
//                                    ->andWhere([Message::tableName().'.status'=> Message::STATUS_NOT_SEEN])
                                    ->select([
                                        Message::tableName().'.id',
                                        Message::tableName().'.dialog_id',
                                        Message::tableName().'.author_id',
                                        Message::tableName().'.status',
                                        ])
                                ;
                            }
                        ])
                    ;
                },
            ])
            ->orderBy(['playlist_item.order_num'=>SORT_ASC])
            ->select([
                PlaylistItem::tableName().'.id',
                PlaylistItem::tableName().'.song_id',
                PlaylistItem::tableName().'.dialog_id',
                PlaylistItem::tableName().'.playlist_id',
                PlaylistItem::tableName().'.order_num',
                PlaylistItem::tableName().'.status',
            ])
            ->asArray()
            ->all();
    }

    public static function createPlaylist($name)
    {
        $playlist = new self;
        $playlist->name = $name;
        $playlist->type = static::TYPE_PLAYLIST;
        $playlist->object_type = static::OBJECT_TYPE_RADIO;
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$radio) throw new NotFoundHttpException();
        $playlist->object_id = $radio->id;


        if ($playlist->save()) {
            return [
                'success'=>Yii::t('api', 'Плейлист создан'),
                'playlist'=>$playlist
                ];
        } else {
            return [
                'error'=>Yii::t('api', 'Ошибка создания плейлиста'),
                'message'=>$playlist->errors
            ];
        }
    }

    public function addSong($songId)
    {
        $song = Song::find()->where(['id'=>$songId])->one();
        if ($song) {

            $item = new PlaylistItem();
            $item->playlist_id = $this->id;
            $item->song_id = $songId;

            if ($this->items) {
                $item->order_num = count($this->items)+1;
            } else {
                $item->order_num = 1;
            }
            if ($item->save()) {
                return['success'=>Yii::t('api', 'Трек {songName} добавлен в плейлист {playlistName}', [
                    'songName' => $song->name,
                    'playlistName' => $this->name,
                ])];
            } else {
                return['error'=>Yii::t('api', 'Ошибка сохранения в плейлисте - {error}', [
                    'error' => Json::encode($item->errors),
                ])];
            }

        } else {
            return['error'=>Yii::t('api', 'Ошибка добавления трека - песня не найдена ')];
        }
    }


    public function addToRadioSection($songId)
    {
        $song = Song::find()->where(['id'=>$songId])->one();
        if ($song) {
            if (PlaylistItem::find()->where(['playlist_id'=>$this->id,'song_id'=>$songId])->one()) {
                return['error'=>Yii::t('api', 'Ошибка, трек {songName} уже есть в радио секции', [
                    'songName' => $song->name,
                ])];
            }

            $item = new PlaylistItem();
            $item->playlist_id = $this->id;
            $item->song_id = $songId;
            $item->status = PlaylistItem::STATUS_INIT;
            $item->type = PlaylistItem::TYPE_RADIO;

            if ($this->items) {
                $item->order_num = count($this->items)+1;
            } else {
                $item->order_num = 1;
            }
            if ($item->save()) {
                $head = 'Новый трек в радио секции'; // админу не переводим
                $text = 'Новый трек '.$song->name.' добавлен артистом в радио секцию';
                $notification = Notification::create([
                    'type' => Notification::TYPE_ADMIN_NEW_RADIO_SECTION_SONG,
                    'subject_id' => 'new_radio_song_'.$songId,
                    'head' => $head,
                    'text' => $text,
                    'call2action_name' => 'Перейти к треку',
                    'call2action_link' => '/song/view?id='.$songId,
                ]);

                return['success'=>Yii::t('api', 'Трек {songName} добавлен в радио секцию', [
                    'songName' => $song->name,
                ])];
            } else {
                return['error'=>Yii::t('api', 'Ошибка - {error}', [
                    'error' => Json::encode($item->errors),
                ])];
            }

        } else {
            return['error'=>Yii::t('api', 'Ошибка добавления трека - песня не найдена ')];
        }
    }



    public function removeItem($id){
        $item=PlaylistItem::findOne([
            'id'=>$id,
            'playlist_id'=>$this->id
        ]);
        if (!$item) return['error'=>Yii::t('api', 'Ошибка удаления, нет такого трека в плейлисте')];
        $item->delete();
        return ['success'=>Yii::t('api', 'Удалено')];
    }



    public static  function moveSongToPlaylist($songId,$toPlaylistId,$fromPlaylistId)
    {
        $radio = Radio::findOne(['user_id'=>Yii::$app->user->id]);

        $toPlaylist = Playlist::find()
            ->where([
                'id'=>$toPlaylistId,
                'object_type'=>Playlist::OBJECT_TYPE_RADIO,
                'object_id'=>$radio->id,
            ])->one();
        $fromPlaylist = Playlist::find()
            ->where([
                'id'=>$fromPlaylistId,
                'object_type'=>Playlist::OBJECT_TYPE_RADIO,
                'object_id'=>$radio->id,
            ])->one();
        if (!$toPlaylist || !$fromPlaylist) return ['error'=>Yii::t('api', 'Плейлист не найден')];

        $song = Song::findOne($songId);
        if (!$song ) return ['error'=>Yii::t('api', 'Трек не найден')];

        $resTo = $toPlaylist->addSong($songId);

        $itemToRemove = PlaylistItem::findOne([
            'playlist_id'=>$fromPlaylistId,
            'song_id'=>$songId
        ]);

        $resFrom = $fromPlaylist->removeItem($itemToRemove->id);

        if (isset($resTo['success']) || isset($resFrom['success'])) {
            return['success'=>Yii::t('api', 'Трек {songName} перемещен в плейлист {playlistName}', [
                'songName' => $song->name,
                'playlistName' => $toPlaylist->name,
            ])];
        } else {
            return['error'=>Yii::t('api', 'Ошибка перемещения')];
        }
    }

    public function removeFromRadioSection($id){
        $item=PlaylistItem::findOne([
            'id'=>$id,
            'playlist_id'=>$this->id
        ]);
        $song = $item->song;
        if (!$item || !$song) return['error'=>Yii::t('api', 'Ошибка удаления, нет такого трека в радио секции')];
        $item->delete();
        $head = 'Трек удален из радио секции'; // админу не переводим
        $text = 'Трек '.$song->name.' удален артистом из радио секции';
        $notification = Notification::create([
            'type' => Notification::TYPE_ADMIN_REMOVED_RADIO_SECTION_SONG,
            'subject_id' => 'removed_radio_song_'.$song->id,
            'head' => $head,
            'text' => $text,
            'call2action_name' => 'Перейти к треку',
            'call2action_link' => '/song/view?id='.$song->id,
        ]);
        return ['success'=>'удалено'];
    }


//    public static function allBySearch($aParams, $scenario=null)
//    {
//        return (new PlaylistSearch())->apiSearch(['SongSearch' => $aParams],$scenario);
//    }



    public function addToRadioAllTracks($songId)
    {
        $song = Song::findOne(['id'=>$songId]);
        if ($song) {
            if (PlaylistItem::find()->where(['playlist_id'=>$this->id,'song_id'=>$songId])->one()) {
                return[
                    'error'=>Yii::t('api', 'Ошибка, трек {songName} уже есть в радио директории,<br> уточните его статус', [
                        'songName' => $song->name,
                    ]),
                    'redirect'=>true,
                    ];
            }

            $item = new PlaylistItem();
            $item->playlist_id = $this->id;
            $item->song_id = $songId;
            $item->status = PlaylistItem::STATUS_INIT;

            if ($this->items) {
                $item->order_num = count($this->items)+1;
            } else {
                $item->order_num = 1;
            }
            if ($item->save()) {
                $aHead = 'Новый трек в радио секции'; // админу не переводим
                $aText = 'Новый трек "'.$song->name.'" отправлен артистом в радио секцию All Tracks';
                $adminNotification = Notification::create([
                    'type' => Notification::TYPE_ADMIN_NEW_RADIO_SECTION_SONG,
                    'subject_id' => 'new_radio_song_'.$songId,
                    'head' => $aHead,
                    'text' => $aText,
                    'call2action_name' => 'Перейти к позиции',
                    'call2action_link' => '/radio-admin/view?id='.$item->id,
                ],true);

                $lang = User::getLanguageByUserID($song->artist->user_id);
                if($lang=='ru'){
                    $uHead = 'Трек в радио секции ожидает оплаты';
                } else {
                    $uHead = 'The track in radio section is awaiting the payment';
                }
                $uText = PlaylistItem::radioAllTracksPaymentMessage($song->name,PlaylistItem::PRICE_RADIO_ALL_TRACKS,$item->id, $lang);


                $userNotification = Notification::create([
                    'type' => Notification::TYPE_SONG_IN_RADIO_SECTION_CHANGE_STATUS,
                    'artist_id'=>$song->artist_id,
                    'subject_id' => 'new_radio_song_'.$songId,
                    'head' => $uHead,
                    'text' => $uText,
                    'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                    'call2action_link' => '/artist-radio/'.$item->id,
                ],false,true,$song->artist['user_id']);

                return[
                    'success'=>Yii::t('api', 'Трек {songName} отправлен в радио секцию', [
                        'songName' => $song->name,
                    ]),
                    'id'=>$item->id
                ];
            } else {
                return['error'=>Yii::t('api', 'Ошибка - {error}', [
                    'error' => Json::encode($item->errors),
                ])];
            }

        } else {
            return['error'=>Yii::t('api', 'Ошибка добавления трека - песня не найдена ')];
        }
    }

    public function addToArtistRadioSelectedRu($songId)
    {
        $song = Song::findOne(['id'=>$songId]);
        if ($song) {
            if (PlaylistItem::find()->where(['playlist_id'=>$this->id,'song_id'=>$songId])->one()) {
                return[
                    'error'=>Yii::t('api', 'Ошибка, трек {songName} уже есть в директории,<br> уточните его статус', [
                        'songName' => $song->name,
                    ]),
                    'redirect'=>true,
                    ];
            }

            $item = new PlaylistItem();
            $item->playlist_id = $this->id;
            $item->song_id = $songId;
            $item->service_type = PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU;
            $item->status = PlaylistItem::STATUS_INIT;

            if ($this->items) {
                $item->order_num = count($this->items)+1;
            } else {
                $item->order_num = 1;
            }
            if ($item->save()) {
                $head = 'Новый трек в selected СНГ'; // отправка админу, не переводим
                $text = 'Новый трек '.$song->name.' отправлен артистом в радио секцию Selected Tracks (СНГ)';
                $notification = Notification::create([
                    'type' => Notification::TYPE_ADMIN_NEW_SELECTED_RU,
                    'subject_id' => 'new_selected_ru_song_'.$songId,
                    'head' => $head,
                    'text' => $text,
                    'call2action_name' => 'Перейти к позиции',
                    'call2action_link' => '/premium/view?id='.$item->id,
                ],true);

                return[
                    'success'=>Yii::t('api', 'Трек {songName} отправлен на согласование качества в секцию Selected Tracks (СНГ)', [
                        'songName' => $song->name,
                    ]),
                    'id'=>$item->id
                ];
            } else {
                return['error'=>Yii::t('api', 'Ошибка - {error}', [
                    'error' => Json::encode($item->errors),
                ])];
            }

        } else {
            return['error'=>Yii::t('api', 'Ошибка добавления трека - песня не найдена ')];
        }
    }

    public function addToArtistRadioSelectedWorld($songId)
    {
        $song = Song::findOne(['id'=>$songId]);
        if ($song) {
            if (PlaylistItem::find()->where(['playlist_id'=>$this->id,'song_id'=>$songId])->one()) {
                return[
                    'error'=>Yii::t('api', 'Ошибка, трек {songName} уже есть в директории,<br> уточните его статус', [
                        'songName' => $song->name,
                    ]),
                    'redirect'=>true,
                    ];
            }

            $item = new PlaylistItem();
            $item->playlist_id = $this->id;
            $item->song_id = $songId;
            $item->service_type = PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD;
            $item->status = PlaylistItem::STATUS_INIT;

            if ($this->items) {
                $item->order_num = count($this->items)+1;
            } else {
                $item->order_num = 1;
            }
            if ($item->save()) {
                $head = 'Новый трек в selected World'; // админу - не переводим
                $text = 'Новый трек '.$song->name.' отправлен артистом в радио секцию Selected Tracks (World)';
                $notification = Notification::create([
                    'type' => Notification::TYPE_ADMIN_NEW_SELECTED_WORLD,
                    'subject_id' => 'new_selected_world_song_'.$songId,
                    'head' => $head,
                    'text' => $text,
                    'call2action_name' => 'Перейти к позиции',
                    'call2action_link' => '/premium/view?id='.$item->id,
                ],true);

                return['success'=>Yii::t('api', 'Трек {songName} отправлен на согласование качества в секцию Selected Tracks (World)', [
                    'songName' => $song->name,
                ])];
            } else {
                return['error'=>Yii::t('api', 'Ошибка - {error}', [
                    'error' => Json::encode($item->errors),
                ])];
            }

        } else {
            return['error'=>Yii::t('api', 'Ошибка добавления трека - песня не найдена ')];
        }
    }
}
