<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "playlist_item".
 *
 * @property int $id
 * @property int $playlist_id
 * @property int $order_num
 * @property string $status
 * @property int $song_id
 * @property int $rating
 * @property int $plays
 * @property int $downloads
 * @property int $created_at
 * @property int $updated_at
 * @property int $rel_id
 * @property string $rel_type
 * @property string $dialog_id
 * @property int $till_to
 * @property string $service_type

 */
class PlaylistItem extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INIT = 'init';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';
    const STATUS_EXPIRED = 'expired';

    const SERVICE_TYPE_RADIO_SELECTED_RU = 'radio_selected_ru';
    const SERVICE_TYPE_RADIO_SELECTED_WORLD = 'radio_selected_world';
    const SERVICE_TYPE_RADIO = 'radio';

    const DEFAULT_RADIO_PERIOD = 86400*365; // one year

    const PAYMENT_INFO_RU = null;
    const PAYMENT_INFO_ENG = null;
//    const PAYMENT_INFO = 'Карта хххх хххх хххх хххх, '.PHP_EOL.'платеж необходимо отправить с комментарием #';
    const PRICE_RADIO_ALL_TRACKS = '1490';
    const PRICE_RADIO_SELECTED_RU = '3990';
    const PRICE_RADIO_SELECTED_WORLD = '5990';




    const REL_TYPE_ITEM_IN_ARTIST_PREMIUM_SECTION = 'item_in_artist_premium_section';
    const REL_TYPE_RADIO_PREMIUM_ITEM = 'radio_premium_item';
    const REL_TYPE_ITEM_IN_ARTIST_RADIO_SECTION = 'item_in_artist_radio_section';
    const REL_TYPE_RADIO_ITEM = 'radio_item';



    const SCENARIO_INDEX = 'index';
    const SCENARIO_VIEW = 'view';
    const SCENARIO_CHECKSUM_ACTIVE_ITEMS = 'checksum_active_items';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'playlist_item';
    }

    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class,],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['playlist_id'], 'required'],
            [['playlist_id', 'order_num', 'song_id',
                'rating', 'plays', 'downloads', 'created_at', 'updated_at', 'rel_id', 'dialog_id','till_to'], 'integer'],
            [['status','rel_type','service_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_num' => 'Order Num',
            'status' => 'Status',
            'song_id' => 'Song ID',
            'rating' => 'Rating',
            'plays' => 'Plays',
            'downloads' => 'Downloads',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'rel_type' => 'Relation Type',
            'rel_id' => 'Relation ID',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'playlist_id',
            'order_num',
            'status',
            'song_id',
            'rating',
            'plays',
            'downloads',
            'song_name' => function ($q) {
                if ($q->song) {
                    return $q->song->name;
                }
            },
            'song'=> function ($q) {
                if ($q->song) {
                    return $q->song;
                }
            },
            'messages_qnt'=> function($q){
                $dialog = $q->dialog;
                if ($dialog) {
                    $arr=[];
                    $dialArr = $dialog->toArray($dialog->scenarios()[MessageDialog::SCENARIO_INDEX]);
                    $arr['qnt'] = $dialArr['messagesQnt'];
                    $arr['not_seen'] = $dialArr['notSeenQnt'];
                    return $arr;
                }
            },
            'till_to',
            'service_type',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_INDEX] = ['id','song_name','order_num','service_type'];
        $scenarios[self::SCENARIO_VIEW] = ['id','order_num','song_name','song','service_type'];
        $scenarios[self::SCENARIO_CHECKSUM_ACTIVE_ITEMS] = ['id','status','messages_qnt','service_type'];
        return $scenarios;

    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                if (!$this->status ) {
                    $this->status = static::STATUS_ACTIVE;
                }
            }
            return true;
        }
        return false;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }
        if ($this->dialog) {
            $this->dialog->delete();
        }
        return true;
    }

    public  function getSong()
    {
        return $this->hasOne(Song::class,['id'=>'song_id']);
    }


    /**
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     */
    public function activateForRadio(){

        $tillTo = time()+self::DEFAULT_RADIO_PERIOD;

        $this->service_type = self::SERVICE_TYPE_RADIO;
        $this->status = self::STATUS_ACTIVE;
        $this->till_to = $tillTo;
        if (!$this->save()) throw new BadRequestHttpException('Ошибка сохранения объекта');

        $song = $this->song;
        $song->radio_status = Song::STATUS_ACTIVE;
        if (!$song->save()) {
            throw new BadRequestHttpException('Ошибка сохранения статуса трека');
        }

        $radioPlaylist = Playlist::find()->where(['type'=>Playlist::TYPE_RADIO_COMMON])->one();
        if (!$radioPlaylist)  throw new NotFoundHttpException('Не найден радио плейлист, обратитесь к администратору');

        $existed = PlaylistItem::findOne([
            'playlist_id'=>$radioPlaylist->id,
            'song_id'=>$this->song_id,
        ]);
        if ($existed) {
            $item = $existed;
        } else {
            $item = new PlaylistItem();
            $item->playlist_id = $radioPlaylist->id;
            $item->service_type = PlaylistItem::SERVICE_TYPE_RADIO;
            $item->status = PlaylistItem::STATUS_ACTIVE;
            $item->song_id = $this->song_id;
            $item->rel_type = PlaylistItem::REL_TYPE_ITEM_IN_ARTIST_RADIO_SECTION;
            $item->rel_id = $this->id;
        }

        if ($radioPlaylist->items) {
            $item->order_num = count($radioPlaylist->items)+1;
        } else {
            $item->order_num = 1;
        }

        $item->till_to = $tillTo;

        if ($item->save()) {
            $this->rel_type = PlaylistItem::REL_TYPE_RADIO_ITEM;
            $this->rel_id = $item->id;
            $this->save();

            Yii::$app->session->addFlash('success', 'Трек успешно активирован в директории All Tracks на 1 год.');
            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);

            $lang = User::getLanguageByUserID($this->song->artist->user_id);
            if($lang=='ru'){
                $head = 'Активирован трек в директории All Tracks';
                $text = 'Трек "'.$this->song->name.'" активирован в директории All Tracks на 1 год';
            } else {
                $head = 'The track was activated in section All Tracks';
                $text = 'The track "'.$this->song->name.'" was activated to section All Tracks for 1 year';
            }



            $notification = Notification::create([
                'artist_id' => $this->song->artist_id,
                'type' => Notification::TYPE_SONG_IN_RADIO_SECTION_CHANGE_STATUS,
                'subject_id' => 'change_radio_status_of_item_'.$this->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => '/artist-radio/'.$this->id,
            ],false,true,$song->artist['user_id']);
            return true ;
        } else {
            Yii::$app->session->addFlash('error', 'Не получилось'); // админу
            return false;
        }


    }


    public function deactivateRadio(){

        $this->status = self::STATUS_EXPIRED;
        if (!$this->save()) {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения объекта');
            return false;
        }

        $song = $this->song;
        $song->radio_status = Song::STATUS_EXPIRED;
        if (!$song->save()) {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения трека');
            return false;
        }

        $radioItem = PlaylistItem::findOne($this->rel_id);
        if (!$radioItem)  throw new NotFoundHttpException('Не найден объект в радио плейлисте');
        $radioItem->status = PlaylistItem::STATUS_EXPIRED;
        if (!$radioItem->save()) {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения объекта в радио плейлисте');
            return false;
        }

        $this->status = self::STATUS_EXPIRED;
        if ($this->save()) {
            Yii::$app->session->addFlash('success', 'Операция выполнена, трек деактивирован для радио');
            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_RADIO_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);
        } else {
            Yii::$app->session->addFlash('error', 'Не получилось');
        }

        $lang = User::getLanguageByUserID($this->song->artist->user_id);
        if($lang=='ru'){
            $head = 'Трек деактивирован в директории All Tracks';
            $text = 'Трек "'.$this->song->name.'" деактивирован в директории All Tracks';
        } else {
            $head = 'The track was deactivated in section All Tracks';
            $text = 'The track "'.$this->song->name.'" was deactivated in section All Tracks';
        }


        $notification = Notification::create([
            'artist_id' => $this->song->artist_id,
            'type' => Notification::TYPE_SONG_IN_RADIO_SECTION_CHANGE_STATUS,
            'subject_id' => 'change_radio_status_of_item_'.$this->id,
            'head' => $head,
            'text' => $text,
            'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
            'call2action_link' => '/artist-radio/'.$this->id,
        ],false,true,$song->artist['user_id']);

    }

    public function getDialog()
    {
        return $this->hasOne(MessageDialog::class,['id'=>'dialog_id']);
    }

    public function getPlaylist()
    {
        return $this->hasOne(Playlist::class,['id'=>'playlist_id']);
    }

    public function getRadioSectionDialog()
    {
        return $this->hasOne(MessageDialog::class,['subject_id'=>'id'])
            ->andWhere(['subject_type'=>MessageDialog::SUBJECT_TYPE_ITEM_IN_RADIO_SECTION]);
    }


    /**
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function setPremiumExpired(){

        $playlist = $this->playlist;
        if ($playlist->type!=Playlist::TYPE_PREMIUM_SECTION ) {
            Yii::$app->session->addFlash('error', 'Не тот объект, необходим объект в примиум секции артиста');
            return false;
        }

        $commonItem = PlaylistItem::findOne($this->rel_id);
        if (!$commonItem)  throw new NotFoundHttpException('Не найден объект в премиум плейлисте');

        $commonItem->status = PlaylistItem::STATUS_EXPIRED;
        if (!$commonItem->save()) {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения объекта в премиум плейлисте');
            return false;
        }

        $song = $this->song;
        $sectionName = '';
        $frontLink = '/artist-selected/'.$this->id;


        if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            $sectionName = 'Selected RU';
            $song->premium_ru_status = Song::STATUS_EXPIRED;

        }
        else if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            $sectionName = 'Selected World';
            $song->premium_world_status = Song::STATUS_EXPIRED;
        }
        if (!$song->save()) throw new BadRequestHttpException('Ошибка сохранения статуса трека');
        $this->status =  PlaylistItem::STATUS_EXPIRED;

        if ($this->save()) {
            Yii::$app->session->addFlash('success', 'Операция выполнена, трек деактивирован в директории Selected Tracks');
            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения объекта в премиум плейлисте');
            return false;
        }

        $lang = User::getLanguageByUserID($this->song->artist->user_id);
        if($lang=='ru'){
            $head = 'Деактивирован трек в директории '.$sectionName;
            $text = 'Трек "'.$this->song->name.'" деактивирован в директории '.$sectionName;
        } else {
            $head = 'The track was deactivated for section '.$sectionName;
            $text = 'The track "'.$this->song->name.'" was deactivated in section '.$sectionName;
        }



        $notification = Notification::create([
            'artist_id' => $this->song->artist_id,
            'type' => Notification::TYPE_SONG_IN_PREMIUM_SECTION_CHANGE_STATUS,
            'subject_id' => 'change_premium_status_of_item_'.$this->id,
            'head' => $head,
            'text' => $text,
            'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
            'call2action_link' => $frontLink,
        ],false,true,$song->artist['user_id']);

    }



    public function activateForPremium(){

        $tillTo = time()+self::DEFAULT_RADIO_PERIOD;
        $playlist = $this->playlist;
        if ($playlist->type!=Playlist::TYPE_PREMIUM_SECTION ) {
            Yii::$app->session->addFlash('error', 'Активируется не тот объект, необходимо активировать объект в примиум секции артиста');
            return false;
        }

        $this->status = self::STATUS_ACTIVE;
        $this->till_to = $tillTo;
        if (!$this->save()) throw new BadRequestHttpException('Ошибка сохранения объекта');

        $song = $this->song;
        $sectionName = '';
        $commonPlaylist = null;

        if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            $sectionName = 'Selected RU';
            $song->premium_ru_status = Song::STATUS_ACTIVE;
        }
        else if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            $sectionName = 'Selected World';
            $song->premium_world_status = Song::STATUS_ACTIVE;
        }
        $commonPlaylist = Playlist::findOne(['type'=>Playlist::TYPE_PREMIUM_COMMON]);
        if (!$song->save()) throw new BadRequestHttpException('Ошибка сохранения статуса трека');

        if (!$commonPlaylist)  throw new NotFoundHttpException('Не найден common плейлист, обратитесь к администратору');
        $existed = PlaylistItem::findOne([
            'playlist_id'=>$commonPlaylist->id,
            'song_id'=>$this->song_id,
        ]);
        if ($existed) {
            $item = $existed;
        } else {
            $item = new PlaylistItem();
            $item->playlist_id = $commonPlaylist->id;
            $item->song_id = $this->song_id;
            $item->service_type = $this->service_type;
            $item->rel_type = PlaylistItem::REL_TYPE_ITEM_IN_ARTIST_PREMIUM_SECTION;
            $item->rel_id = $this->id;
        }
        $item->status = PlaylistItem::STATUS_ACTIVE;

        if ($commonPlaylist->items) {
            $item->order_num = count($commonPlaylist->items)+1;
        } else {
            $item->order_num = 1;
        }

        $item->till_to = $tillTo;

        if ($item->save()) {
            $this->rel_type = PlaylistItem::REL_TYPE_RADIO_PREMIUM_ITEM;
            $this->rel_id = $item->id;
            $this->save();

            Yii::$app->session->addFlash('success', 'Трек успешно активирован в директории '.$sectionName.' на 1 год.');
            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);
            $lang = User::getLanguageByUserID($this->song->artist->user_id);
            if($lang=='ru'){
                $head = 'Активирован трек в директории '.$sectionName;
                $text = 'Трек "'.$this->song->name.'" активирован в директории '.$sectionName.' на 1 год';
            } else {
                $head = 'The track was activated for section '.$sectionName;
                $text = 'The track "'.$this->song->name.'" was activated in section '.$sectionName;
            }

            $notification = Notification::create([
                'artist_id' => $this->song->artist_id,
                'type' => Notification::TYPE_SONG_IN_PREMIUM_SECTION_CHANGE_STATUS,
                'subject_id' => 'change_status_of_item_'.$this->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => '/artist-selected/'.$this->id,
            ],false,true,$song->artist['user_id']);
            return true ;
        } else {
            Yii::$app->session->addFlash('error', 'Не получилось');
            return false;
        }
    }




    public function radioMoveToTop(){
        $radioPlaylist = Playlist::find()->where(['type'=>Playlist::TYPE_RADIO_COMMON])->one();
        if (!$radioPlaylist) {
            throw new NotFoundHttpException('Не найден радио плейлист, обратитесь к администратору');
        }
        $items = self::find()->where(['playlist_id'=>$radioPlaylist->id])->orderBy(['order_num'=>SORT_DESC])->all();
       echo '<pre>'; print_r($items,true);  echo '</pre>'; die;
    }



    public function approvePremium(){
        $this->status = PlaylistItem::STATUS_APPROVED;
        $this->save();
        $song = $this->song;
        $price = '';
        $sectionName = '';
        $frontLink = '';
        if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            $sectionName = 'Selected RU';
            $frontLink = '/artist-selected/'.$this->id;
            $song->premium_ru_status = Song::STATUS_APPROVED;
            $song->save();
            $price = self::PRICE_RADIO_SELECTED_RU;

        } else if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            $sectionName = 'Selected World';
            $frontLink = '/artist-selected/'.$this->id;
            $song->premium_world_status = Song::STATUS_APPROVED;
            $song->save();
            $price = self::PRICE_RADIO_SELECTED_WORLD;
        }

        if ($this->save()) {
            Yii::$app->session->addFlash('success', 'Трек успешно согласован в директорию '.$sectionName);

            $lang = User::getLanguageByUserID($this->song->artist->user_id);
            if ($lang=='ru'){
                $head = 'Трек согласован в '.$sectionName;
            } else {
                $head = 'The track is approved to '.$sectionName;
            }

            $text = PlaylistItem::radioSelectedPaymentMessage($song->name,$sectionName,$price,$this->id, $lang);

            $notification = Notification::create([
                'artist_id' => $song->artist_id,
                'type' => Notification::TYPE_SONG_IN_PREMIUM_SECTION_CHANGE_STATUS,
                'subject_id' => 'change_premium_status_of_item_'.$this->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => $frontLink,
            ],false,true,$song->artist['user_id']);

            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);
        } else {
            Yii::$app->session->addFlash('error', 'Не получилось');
        }
    }

    public function declinePremium(){
        $this->status = PlaylistItem::STATUS_DECLINED;
        $this->save();
        $song = $this->song;

        $sectionName = '';
        $frontLink = '';
        if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_RU) {
            $sectionName = 'Selected RU';
            $frontLink = '/artist-selected/'.$this->id;
            $song->premium_ru_status = Song::STATUS_DECLINED;
            $song->save();
        } else if ($this->service_type == PlaylistItem::SERVICE_TYPE_RADIO_SELECTED_WORLD) {
            $sectionName = 'Selected World';
            $frontLink = '/artist-selected/'.$this->id;
            $song->premium_world_status = Song::STATUS_DECLINED;
            $song->save();
        }

        if ($this->save()) {
            Yii::$app->session->addFlash('success', 'Трек отклонен для размещения в директории '.$sectionName);

            $lang = User::getLanguageByUserID($this->song->artist->user_id);
            if ($lang=='ru'){
                $head = 'Трек отклонен в '.$sectionName;
                $text = 'Трек "'.$song->name.'"  отклонен для размещения в директорию '.$sectionName;
            } else {
                $head = 'The track was declined for '.$sectionName;
                $text = 'The track "'.$song->name.'"  was declined for placing in section '.$sectionName;
            }


            $notification = Notification::create([
                'artist_id' => $song->artist_id,
                'type' => Notification::TYPE_SONG_IN_PREMIUM_SECTION_CHANGE_STATUS,
                'subject_id' => 'change_premium_status_of_item_'.$this->id,
                'head' => $head,
                'text' => $text,
                'call2action_name' => Notification::CALL_2_ACTION_NO_BUTTON,
                'call2action_link' => $frontLink,
            ],false,true,$song->artist['user_id']);

            Notification::sendWebsocketEvent($this->song->artist->user_id,
                Notification::EVENT_PREMIUM_SECTION_ITEM_STATUS_CHANGE,[
                    'itemId'=>$this->id,
                    'status'=>$this->status
                ]);
        } else {
            Yii::$app->session->addFlash('error', 'Не получилось');
        }
    }

    public static function radioSelectedPaymentMessage($songName,$sectionName, $sum, $comment, $lang){
        if($lang=='ru'){
            return 'Трек "'.$songName.'" согласован в директорию '.$sectionName.' и ожидает оплаты.'.PHP_EOL
                . 'Сумма: ' . $sum . ' руб.'.PHP_EOL
                . self::PAYMENT_INFO_RU . $comment . '.';
        } else {
            return 'The track "'.$songName.'" is approved to '.$sectionName.' and awaiting the payment.'.PHP_EOL
                . 'Sum: ' . $sum . ' rub.'.PHP_EOL
                . self::PAYMENT_INFO_ENG . $comment . '.';
        }

    }

    public static function radioAllTracksPaymentMessage($songName, $sum, $comment,  $lang){
        if($lang=='ru') {
            return 'Трек "' . $songName . '" в директории All Tracks ожидает оплаты.' . PHP_EOL
                . 'Сумма: ' . $sum . ' руб.' . PHP_EOL
                . self::PAYMENT_INFO_RU . $comment . '.';
        } else {
            return 'The track "' . $songName . '" in All Tracks is awaiting the payment.' . PHP_EOL
                . 'Sum: ' . $sum . ' rub.'.PHP_EOL
                . self::PAYMENT_INFO_ENG . $comment . '.';
        }
    }
}
