<?php

namespace common\models;

use common\models\Notification;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;

/**
 * This is the search class for table "PlaylistItem".
 *
 * @property int $id
 * @property string $songName
 * @property string $songArtist
 *
 *  @property string $name;
 * @property string $artist_name;
 * @property int $language_id;
 * @property int $music_style_id;
 * @property int $is_vocal;
 * @property int $is_not_vocal;
 * @property int $is_adult;
 * @property int $is_not_adult;
 * @property string $playlistType;
 * @property string $service_type;
 */
class PlaylistItemSearch extends PlaylistItem
{
    public $song;
    public $songName;
    public $playlistType;
    public $songArtist;
    public $artistId;

    public $name;
    public $artist_name;
    public $language_id;
    public $music_style_id;
    public $is_vocal;
    public $is_not_vocal;
    public $is_adult;
    public $is_not_adult;

    public $premiumId;
    public $tmp;
    public $service_type;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'playlist_id', 'song_id','premiumId', ], 'integer'],
            [['status',
                'song',
                'songName',
                'songArtist',
                'name',
                'artist_name',
                'language_id',
                'music_style_id',
                'is_vocal',
                'is_not_vocal',
                'is_adult',
                'is_not_adult',
                'playlistType',
                'service_type',
                ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function backendRadioSearch($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);
        $query->joinWith(['playlist']);

        $query->where([
            'playlist.type' => [
                Playlist::TYPE_RADIO_SECTION
            ],
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);

        if ($this->status) {
            $query->andFilterWhere(['playlist_item.status' => $this->status]);
        }


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;
        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function backendRadioPlaylist($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);
        $query->joinWith(['playlist']);

        $query->where([
            'playlist.type' => [
                Playlist::TYPE_RADIO_COMMON
            ],
            'playlist_item.status' => [
                PlaylistItem::STATUS_ACTIVE,
            ],
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;

        return $dataProvider;
    }


    public function backendPremiumSearch($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song','playlist']);

        $query->where([
            'playlist.type' => [
                Playlist::TYPE_PREMIUM_SECTION,
            ],
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->playlistType) {
            $query->andFilterWhere(['playlist.type' => $this->playlistType]);
        }
        if ($this->service_type) {
            $query->andFilterWhere(['playlist_item.service_type' => $this->service_type]);
        }


        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);

        if ($this->status) {
            $query->andFilterWhere(['playlist_item.status' => $this->status]);
        }

        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;
        return $dataProvider;
    }



    public function backendPremiumRuPlaylist($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);

        $premiumPlaylist = Playlist::find()->where(['type'=>Playlist::TYPE_PREMIUM_COMMON])->one();

        $query->where([
            'playlist_item.playlist_id' => $premiumPlaylist->id,
            'playlist_item.status' => [
                PlaylistItem::STATUS_ACTIVE,
            ],
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;

        return $dataProvider;
    }

    public function backendPremiumWorldPlaylist($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);

        $premiumPlaylist = Playlist::find()->where(['type'=>Playlist::TYPE_PREMIUM_WORLD_COMMON])->one();

        $query->where([
            'playlist_item.playlist_id' => $premiumPlaylist->id,
            'playlist_item.status' => [
                PlaylistItem::STATUS_ACTIVE,
            ],
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }


        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;

        return $dataProvider;
    }



    /*
     * Выборка для Radio секции AllTracks
     * */
    public function apiSearchInAllTracksPlaylist($params)
    {
//        $microStart = microtime(true);

        $query = PlaylistItem::find();
        $query->joinWith(['song']);

        $radioPlaylist = Playlist::findOne(['type'=>Playlist::TYPE_RADIO_COMMON]);

        $query->where([
            'playlist_item.playlist_id' => $radioPlaylist->id,
            'playlist_item.status' => [
                PlaylistItem::STATUS_ACTIVE,
            ],
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => Yii::$app->request->post('pageSize') ?
                    Yii::$app->request->post('pageSize') : 50,
                'pageSizeLimit' => [1, 100]
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_num' => SORT_DESC,
                ]
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $this->errors;
        }

        if (!($this->load($params) && $this->validate())) {
            return [
                'error'=>'Ошибка загрузки запроса '.Json::encode($this->errors),
            ];
        }


        $query->andFilterWhere(['like', 'song.name', $this->name])
            ->andFilterWhere(['like', 'song.artist_name', $this->artist_name])
        ;


        if ($this->name) {
            $query->andFilterWhere(['like', 'song.name', $this->name]);
        }
        if ($this->artist_name) {
            $query->andFilterWhere(['like', 'song.artist_name', $this->artist_name]);
        }

        if ($this->language_id) {
            foreach ($this->language_id as $la) {
                $query->andFilterWhere(['like', 'song.language', $la]);
            }
        }
        if ($this->music_style_id) {
            $query->andFilterWhere(['song.music_style1_id' => $this->music_style_id])
                ->orFilterWhere(['song.music_style2_id' => $this->music_style_id])
                ->orFilterWhere(['song.music_style3_id' => $this->music_style_id]);
        }

        if ($this->is_vocal) $query->andFilterWhere(['song.vocal'=> 1]);
        if ($this->is_not_vocal) {
            $query->andWhere([ 'or',
                ['song.vocal'=> 0],
                ['song.vocal'=> null]
            ]);
        }

        if ($this->is_adult) $query->andFilterWhere(['song.adult_text'=> 1]);
        if ($this->is_not_adult) {
            $query->andWhere([ 'or',
                ['song.adult_text'=> 0],
                ['song.adult_text'=> null]
            ]);
        }

        $models = $dataProvider->getModels();

        $oModels = [];
        $music_styles = [];

        foreach( $models as $model) {
            $arrayModel = $model->toArray([
                'id',
                'playlist_id',
                'order_num',
                'song_name',
                'status',
                'song',
            ]);

            $oModels[] = $arrayModel;
        }

        // getting values for filter
        $arrAllItemsQnt = $query
            ->select([
                PlaylistItem::tableName().'.id',
                PlaylistItem::tableName().'.song_id',
                PlaylistItem::tableName().'.playlist_id',
                PlaylistItem::tableName().'.status',
            ])
            ->joinWith(['song'=>function($q){
                $q->select([
                    Song::tableName().'.id',
                    Song::tableName().'.language',
                    Song::tableName().'.music_style1_id',
                    Song::tableName().'.music_style2_id',
                    Song::tableName().'.music_style3_id',
                ]);
            }])
            ->asArray()
            ->all();


        $allLangs = Language::find()
            ->select(['id','name_ru','name_orig', 'name_eng'])
            ->indexBy('id')
            ->asArray()
            ->all();
        $resLangArr = [];

        if (count($arrAllItemsQnt)>0) {
            foreach ($arrAllItemsQnt as $item){


                if ($item['song']['music_style1_id'] && !isset( $music_styles[$item['song']['music_style1_id']])) {
                    $music_styles[$item['song']['music_style1_id']] = true;
                }
                if ($item['song']['music_style2_id'] && !isset( $music_styles[$item['song']['music_style2_id']])) {
                    $music_styles[$item['song']['music_style2_id']] = true;
                }
                if ($item['song']['music_style3_id'] && !isset( $music_styles[$item['song']['music_style3_id']])) {
                    $music_styles[$item['song']['music_style3_id']] = true;
                }

                // Languages
                $decoded = Json::decode($item['song']['language']);
                if ( is_array($decoded)) {
                    foreach ($decoded as $id){
                        if (!isset($resLangArr[$id])) {
                            $resLangArr[$id]['id'] = $id;
                            $resLangArr[$id]['name'] =  \Yii::$app->language=='ru-RU'?$allLangs[$id]['name_ru']:$allLangs[$id]['name_eng']; // or    name_orig
                        }
                    }
                }
            }
        }


        $result = [
            'feed'=>$oModels,
            'filter'=>[
                'languages'=>array_values($resLangArr),
                'music_styles'=>\common\models\Menu::filterTree($music_styles),
            ],
            'pagination'=>[
                'total'=>$dataProvider->totalCount,
                'pageSize'=>$dataProvider->pagination->pageSize,
                'currentPage'=>$dataProvider->pagination->page +1,
            ],
        ];
        return $result;
    }


    /*
     * Выборка Selected Tracks для Radio
     * */
    public function apiSearchInSelectedTracksPlaylist($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);
        $premiumPlaylist = Playlist::findOne(['type'=>Playlist::TYPE_PREMIUM_COMMON]);
        $query->where([
            'playlist_item.playlist_id' => $premiumPlaylist->id,
            'playlist_item.status' => [
                PlaylistItem::STATUS_ACTIVE,
            ],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => Yii::$app->request->post('pageSize') ?
                    Yii::$app->request->post('pageSize'): 50,
                'pageSizeLimit' => [1, 100]
            ],
            'sort' => [
                'defaultOrder' => [
                    'order_num' => SORT_DESC,
                ]
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $this->errors;
        }

        if (!($this->load($params) && $this->validate())) {
            return [
                'error'=>'Ошибка загрузки запроса '.Json::encode($this->errors),
            ];
        }


        $query->andFilterWhere(['like', 'song.name', $this->name])
            ->andFilterWhere(['like', 'song.artist_name', $this->artist_name])
        ;


        if ($this->name) {
            $query->andFilterWhere(['like', 'song.name', $this->name]);
        }
        if ($this->artist_name) {
            $query->andFilterWhere(['like', 'song.artist_name', $this->artist_name]);
        }

        if ($this->language_id) {
            foreach ($this->language_id as $la) {
                $query->andFilterWhere(['like', 'song.language', $la]);
            }
        }
        if ($this->music_style_id) {
            $query->andFilterWhere(['song.music_style1_id' => $this->music_style_id])
                ->orFilterWhere(['song.music_style2_id' => $this->music_style_id])
                ->orFilterWhere(['song.music_style3_id' => $this->music_style_id]);
        }

        if ($this->is_vocal) $query->andFilterWhere(['song.vocal'=> 1]);
        if ($this->is_not_vocal) {
            $query->andWhere([ 'or',
                ['song.vocal'=> 0],
                ['song.vocal'=> null]
            ]);
        }

        if ($this->is_adult) $query->andFilterWhere(['song.adult_text'=> 1]);
        if ($this->is_not_adult) {
            $query->andWhere([ 'or',
                ['song.adult_text'=> 0],
                ['song.adult_text'=> null]
            ]);
        }

        $models = $dataProvider->getModels();

        $oModels = [];
        $music_styles = [];

        foreach( $models as $model) {
            $arrayModel = $model->toArray([
                'id',
                'playlist_id',
                'order_num',
                'song_name',
                'status',
                'song',
            ]);
            $oModels[] = $arrayModel;
        }

        // getting values for filter
        $arrAllItemsQnt = $query
            ->select([
                PlaylistItem::tableName().'.id',
                PlaylistItem::tableName().'.song_id',
                PlaylistItem::tableName().'.playlist_id',
                PlaylistItem::tableName().'.status',
            ])
             ->joinWith(['song'=>function($q){
                $q->select([
                    Song::tableName().'.id',
                    Song::tableName().'.language',
                    Song::tableName().'.music_style1_id',
                    Song::tableName().'.music_style2_id',
                    Song::tableName().'.music_style3_id',
                ]);
             }])
            ->asArray()
            ->all();


        $allLangs = Language::find()
            ->select(['id','name_ru','name_orig','name_eng'])
            ->indexBy('id')
            ->asArray()
            ->all();
        $resLangArr = [];
        if (count($arrAllItemsQnt)>0) {
            foreach ($arrAllItemsQnt as $item){

                if ($item['song']['music_style1_id'] && !isset( $music_styles[$item['song']['music_style1_id']])) {
                    $music_styles[$item['song']['music_style1_id']] = true;
                }
                if ($item['song']['music_style2_id'] && !isset( $music_styles[$item['song']['music_style2_id']])) {
                    $music_styles[$item['song']['music_style2_id']] = true;
                }
                if ($item['song']['music_style3_id'] && !isset( $music_styles[$item['song']['music_style3_id']])) {
                    $music_styles[$item['song']['music_style3_id']] = true;
                }

                // Languages
                $decoded = Json::decode($item['song']['language']);
                if ( is_array($decoded)) {
                    foreach ($decoded as $id){
                        if (!isset($resLangArr[$id])) {
                            $resLangArr[$id]['id'] = $id;
                            $resLangArr[$id]['name'] = \Yii::$app->language=='ru-RU'?$allLangs[$id]['name_ru']:$allLangs[$id]['name_eng']; // or    name_orig
                        }
                    }
                }
            }
        }



        $result = [
            'feed'=>$oModels,
            'filter'=>[
                'languages'=>array_values($resLangArr),
                'music_styles'=>\common\models\Menu::filterTree($music_styles),
            ],
            'pagination'=>[
                'total'=>$dataProvider->totalCount,
                'pageSize'=>$dataProvider->pagination->pageSize,
                'currentPage'=>$dataProvider->pagination->page +1,
            ],

        ];
        return $result;
    }











    public function apiSearch($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);

        $query->where([
            'playlist_item.status' => [PlaylistItem::STATUS_ACTIVE],
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'playlist_item.status' => [
//                PlaylistItem::RADIO_SECTION_STATUS_INIT,
//                PlaylistItem::RADIO_SECTION_STATUS_ACTIVE,
//            ],
//        ]);


        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;

        return $dataProvider;
    }






//     not used
    public function apiAllTracksByRadioSearch($params)
    {
        $query = PlaylistItem::find();
        $query->joinWith(['song']);

        $query->where([
            'playlist_item.service_type' => [PlaylistItem::SERVICE_TYPE_RADIO],
            'playlist_item.status' => [PlaylistItem::STATUS_ACTIVE],
        ]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->sort->attributes['songName'] = [
            'asc' => ['song.name' => SORT_ASC],
            'desc' => ['song.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['songArtist'] = [
            'asc' => ['song.artist_name' => SORT_ASC],
            'desc' => ['song.artist_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['id'] = [
            'asc' => ['playlist_item.id' => SORT_ASC],
            'desc' => ['playlist_item.id' => SORT_DESC],
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

//        $query->andFilterWhere([
//            'playlist_item.status' => [
//                PlaylistItem::RADIO_SECTION_STATUS_INIT,
//                PlaylistItem::RADIO_SECTION_STATUS_ACTIVE,
//            ],
//        ]);


        $query->andFilterWhere([
            'playlist_item.id' => $this->id,
        ]);


        $query->andFilterWhere(['like', 'song.name', $this->songName])
            ->andFilterWhere(['like', 'song.artist_name', $this->songArtist])
        ;

        return $dataProvider;
    }

}
