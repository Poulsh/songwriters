<?php

namespace common\models;

use Yii;
use yii\base\ErrorException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;


/**
 * This is the model class for table "radio".
 *
 * @property int $id
 * @property int $user_id
 * @property int $page_id
 * @property string $name
 * @property string $country
 * @property string $city
 * @property string $hrurl
 * @property string $main_info
 * @property string $comment
 * @property string $image
 * @property string $image_alt
 * @property string $status
 * @property string $view
 * @property int $created_at
 * @property int $updated_at
 */
class Radio extends \yii\db\ActiveRecord
{
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_EDIT = 'edit';
    const STATUS_EMAIL_VERIFICATION = 'email_verification';
    const STATUS_ON_CHECK_BY_ADMIN = 'on_check_by_admin';
    const STATUS_ACTIVE = 'active';
    const STATUS_DECLINED = 'declined';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'radio';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
//                'createdAtAttribute' => 'date',
//                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'page_id', 'created_at', 'updated_at'], 'integer'],
            [['main_info', 'comment'], 'string'],
            [['name', 'country', 'city', 'hrurl', 'image', 'image_alt', 'status', 'view'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'page_id' => 'Page ID',
            'name' => 'Name',
            'country' => 'Country',
            'city' => 'City',
            'hrurl' => 'Hrurl',
            'main_info' => 'Main Info',
            'comment' => 'Comment',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'status' => 'Status',
            'view' => 'View',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REGISTER] = ['user_id','name', 'country', 'city'];
        $scenarios[self::SCENARIO_EDIT] = ['user_id','name', 'country', 'city', 'main_info' ];

        return $scenarios;

    }

    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'user_id',
            'page_id',
            'name',
            'country' ,
            'city',
            'hrurl',
            'main_info',
            'comment',
            'image' => function ($q) {
                return $q->image?$q->image:Imagefiles::REPLACE_NOIMAGE_RADIO;
            },
            'image_alt',
            'status',
            'view',
            'created_at',
            'updated_at',
        ];
    }

    public static function create($attributes, $scenario = self::SCENARIO_REGISTER)
    {
        $oModel = new self;
        $oModel->setScenario($scenario);
        $oModel->attributes = $attributes;
        $oModel->status = self::STATUS_EMAIL_VERIFICATION;
        $oModel->save();
        return $oModel;
    }

    public function addMusicStyle($arrayStyles)
    {
        if ($arrayStyles && is_array($arrayStyles)) {
            foreach ($arrayStyles as $style) {
                Assign::create([
                    'type'=>Assign::TYPE_MUSIC_STYLE,
                    'radio_id'=>$this->id,
                    'other_id'=>$style['id'],
                ]);
            }
        }
    }

    public  function getStyles()
    {
        return $this->hasMany(Menu::class,['id'=>'other_id'])
            ->viaTable('assign',['radio_id'=>'id'],function($query){
                $query->andWhere([
                    'type'=>Assign::TYPE_MUSIC_STYLE,
                    'status'=>Assign::STATUS_ACTIVE
                ]);
            });
    }

    public  function getUser()
    {
        return $this->hasOne(User::class,['id'=>'user_id']);
    }

    /**
     * Редактирование
     *
     * @param $id
     * @param $attributes
     *
     * @return static
     * @throws NotFoundHttpException
     */
    public static function edit($id, $attributes)
    {
        $oModel = self::findOne((int)$id);
        if (!$oModel) {
            throw new NotFoundHttpException();
        }
        $oModel->setScenario(self::SCENARIO_EDIT);
        $oModel->attributes = $attributes;
        $oModel->save();
        return $oModel;
    }

    public static function getFeed($params)
    {
        $feed = Song::find()->all();

        return $feed;
    }


    public function confirmAccess(){
        if ($this->status == Radio::STATUS_ACTIVE) {
            Yii::$app->session->addFlash('error', 'Уже активен');
            return false;
        }
        $oUser = User::findOne(['id'=>$this->user_id]);
        $oUser->status = User::STATUS_ACTIVE;
        if ($oUser->save()) {
           $this->status = Radio::STATUS_ACTIVE;
           $this->save();


            if (\Yii::$app->authManager-> getAssignment(User::ROLE_R,$oUser->id)){
                Yii::$app->session->addFlash('error', 'Пользователь уже имеет права радио');
                return false;
            } else {
                \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(User::ROLE_R), $oUser->id);
            }

            if ($oUser->sendEmailNotification(
                'Подтверждение доступа',
                'Доступ радиостанции успешно подтвержден',
                'https://'.Yii::$app->params['domain'].'/login',
                'Войти'
            )) {
                Yii::$app->session->addFlash('success', 'Операция выполнена, доступ радиостанции успешно подтвержден. Оповещение отправлено');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка отправки email оповещения');
            }

        } else {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения пользователя');
        }
    }

    public function declineAccess(){
        if ($this->status == Radio::STATUS_DECLINED) {
            Yii::$app->session->addFlash('error', 'Уже отклонен');
            return false;
        }
        $oUser = User::findOne(['id'=>$this->user_id]);
        $oUser->status = User::STATUS_DECLINED_RADIO;
        if ($oUser->save()) {
            $this->status = Radio::STATUS_DECLINED;
            $this->save();
            $auth = Yii::$app->authManager;
            if ($auth->getAssignment(User::ROLE_R,$oUser->id)){
                $item = $auth->getRole(User::ROLE_R);
                $item = $item ? : $auth->getPermission(User::ROLE_R);

                if ($auth->revoke($item,$oUser->id)) {
                    Yii::$app->session->addFlash('success', 'Роль '.User::ROLE_R.' удалена у пользователя '.$oUser->id);
                } else {
                    Yii::$app->session->addFlash('error', 'Ошибка удаления роли '.User::ROLE_R.' у пользователя '.$oUser->id);
                };

            }
            if ($oUser->sendEmailNotification(
                'Отклонение доступа',
                'Доступ радиостанции отклонен',
                null,
                null
            ))  {
                Yii::$app->session->addFlash('success', 'Операция выполнена, доступ радиостанции успешно отклонен, оповещение пользователю отправлено');
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка отправки email оповещения');
            }
        } else {
            Yii::$app->session->addFlash('error', 'Ошибка сохранения пользователя '.Json::encode($oUser->errors));
        }
    }

    public function beforeDelete()
    {
        \Yii::$app->authManager->revoke(\Yii::$app->authManager->getRole(User::ROLE_R), $this->user_id);
        return true;
    }
}
