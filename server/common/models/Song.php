<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;


/**
 * This is the model class for table "song".
 *
 * @property int $id
 * @property int $page_id
 * @property string $hrurl
 * @property string $name
 * @property string $name_hash
 * @property string $artist_name
 * @property int $artist_id
 * @property int $uploader_user_id
 * @property string $rightholders
 * @property int $music_style1_id
 * @property int $music_style2_id
 * @property int $music_style3_id
 * @property int $vocal
 * @property string $language
 * @property string $label
 * @property string $isrc
 * @property int $release_date
 * @property string $author_comment
 * @property int $hide_reviews
 * @property int $adult_text
 * @property int $publication_agreed
 * @property string $path_to_file
 * @property string $info
 * @property string $peaks
 * @property string $image
 * @property string $image_alt
 * @property string $background_image
 * @property string $status
 * @property string $radio_status
 * @property string $premium_ru_status
 * @property string $premium_world_status
 * @property string $review_status
 * @property string $view
 * @property int $created_at
 * @property int $updated_at
 */
class Song extends \yii\db\ActiveRecord
{
    public $languages_array;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_EDIT = 'edit';
    const SCENARIO_VIEW = 'view';
    const SCENARIO_INDEX = 'index';
    const SCENARIO_SEARCH = 'search';
    const SCENARIO_UNDONE_LIST = 'undone_list';


    const STATUS_UNDONE = 'undone';
    const STATUS_INIT = 'init';
    const STATUS_ACTIVE = 'active';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';
    const STATUS_EXPIRED = 'expired';

    const STATUS_UPLOADED = 'uploaded';

    const RADIO_STATUS_INIT = 'init';
    const RADIO_STATUS_DECLINED = 'declined';
    const RADIO_STATUS_APPROVED = 'approved';

    const SERVICE_TYPE_RADIO = 'radio';
    const SERVICE_TYPE_PREMIUM_RU = 'premium_ru';
    const SERVICE_TYPE_PREMIUM_WORLD = 'premium_world';


    const SERVICE_URL_CODE_ALL_TRACKS = 100;
    const SERVICE_URL_CODE_SELECTED_RU = 101;
    const SERVICE_URL_CODE_SELECTED_WORLD = 102;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'song';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'artist_id', 'uploader_user_id', 'music_style1_id', 'music_style2_id', 'music_style3_id', 'vocal', 'release_date', 'hide_reviews', 'adult_text', 'created_at', 'updated_at', 'publication_agreed'], 'integer'],
            [['name', 'artist_name'], 'required'],
            [['rightholders', 'author_comment', 'info', 'image','peaks'], 'string'],
            [['hrurl',
                'name',
                'name_hash',
                'artist_name',
                'isrc',
                'image_alt',
                'background_image',
                'status',
                'view',
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
                'review_status',
            ], 'string', 'max' => 255],
            [['language', 'label', 'path_to_file'], 'string', 'max' => 510],
            [['languages_array'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'hrurl' => 'Hrurl',
            'name' => 'Name',
            'name_hash' => 'Name Hash',
            'artist_name' => 'Artist Name',
            'artist_id' => 'Artist ID',
            'uploader_user_id' => 'Uploader User ID',
            'rightholders' => 'Rightholders',
            'music_style1_id' => 'Music Style1 ID',
            'music_style2_id' => 'Music Style2 ID',
            'music_style3_id' => 'Music Style3 ID',
            'vocal' => 'Vocal',
            'language' => 'Language',
            'label' => 'Label',
            'isrc' => 'Isrc',
            'release_date' => 'Release Date',
            'author_comment' => 'Author Comment',
            'hide_reviews' => 'Hide Reviews',
            'adult_text' => 'Adult Text',
            'path_to_file' => 'Path To File',
            'info' => 'Info',
            'peaks' => 'Peaks',
            'image' => 'Image',
            'image_alt' => 'Image Alt',
            'background_image' => 'Background Image',
            'status' => 'Status',
            'radio_status' => 'Radio Status',
            'view' => 'View',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'publication_agreed' => 'Согласен с публикацией',
        ];
    }


    public function beforeDelete()
    {
        $source =  Yii::$app->basePath . '/web/uploadedsongs/source/' . $this->path_to_file;
        if (file_exists($source)) {
            unlink($source);
        }
        $listen = Yii::$app->basePath . '/web/uploadedsongs/listen/' . $this->name_hash .'.mp3';
        if (file_exists($listen)) {
            unlink($listen);
        }
        if ($this->image) {
            $image = Imagefiles::findOne(['name'=>$this->image]);
            if ($image) {
                $image->delete();
            }
        }
        if ($this->playlistItems) {
            foreach ($this->playlistItems as $item) {
                $item->delete();
            }
        }
        return true;
    }

    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {

        return
            [
                'id',
                'page_id',
                'hrurl',
                'name' ,
                'name_hash' ,
                'artist_name' ,
                'artist_id' ,
                'uploader_user_id',
                'rightholders',
                'music_style1_id' ,
                'music_style2_id' ,
                'music_style3_id',
                'music_style1' => function ($q) {
                    return Menu::findOne($q->music_style1_id);
                },
                'music_style2' => function ($q) {
                    return Menu::findOne($q->music_style2_id);
                },
                'music_style3' => function ($q) {
                    return Menu::findOne($q->music_style3_id);
                },

                'vocal' ,
                'language' ,
                'languages' => function ($q) {
                    $id_array = json_decode($q->language);
                    $str='';
                    if ($id_array) {
                        foreach ($id_array as $id) {
                            $language = Language::findOne($id);
                            $langName = \Yii::$app->language=='ru-RU'?$language->name_ru:$language->name_eng;
                            $str.= $str ? (', '.$langName) : $langName;
                        }
                    }
                    return $str;
                },
                'lang_obj'  => function ($q) {
                    $id_array = json_decode($q->language);
                    $arr=[];
                    if ($id_array) {
                        foreach ($id_array as $id) {
                            $arr[] = Language::findOne($id);
                        }
                    }
                    return $arr;
                },
                'label',
                'isrc' ,
                'release_date',
                'author_comment' ,
                'hide_reviews' ,
                'adult_text',
                'path_to_file' ,
                'info' ,
                'peaks' ,
                'image' => function ($q) {
                    return $q->image?$q->image:Imagefiles::REPLACE_NOIMAGE_SONG;
                },
                'image_alt',
                'background_image',
                'status',
                'view',
                'created_at',
                'updated_at' ,
                'publication_agreed' ,
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
                'comments' => function ($q) {
                    return $q->comments;
                },


            ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = [
            'id',
            'page_id',
            'hrurl',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'artist_id' ,
            'uploader_user_id',
            'rightholders',
            'music_style1_id' ,
            'music_style2_id' ,
            'music_style3_id',
            'vocal' ,
            'language' ,
            'label',
            'isrc' ,
            'release_date',
            'author_comment' ,
            'hide_reviews' ,
            'adult_text',
            'path_to_file' ,
            'info' ,
            'image',
            'image_alt',
            'background_image',
            'status',
            'radio_status',
            'view',
            'publication_agreed',
        ];
        $scenarios[self::SCENARIO_EDIT] = [
            'id',
            'page_id',
            'hrurl',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'artist_id' ,
            'uploader_user_id',
            'rightholders',
            'music_style1_id' ,
            'music_style2_id' ,
            'music_style3_id',
            'vocal' ,
            'language' ,
            'label',
            'isrc' ,
            'release_date',
            'author_comment' ,
            'hide_reviews' ,
            'adult_text',
            'path_to_file' ,
            'info' ,
            'image',
            'image_alt',
            'background_image',
            'status',
            'radio_status',
            'view',
            'publication_agreed',
            'radio_status',
            'premium_ru_status',
            'premium_world_status',
        ];
        $scenarios[self::SCENARIO_VIEW] = [
            'id',
            'page_id',
            'hrurl',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'artist_id',
            'uploader_user_id',
            'rightholders',
            'music_style1_id' ,
            'music_style2_id' ,
            'music_style3_id',
            'music_style1' ,
            'music_style2' ,
            'music_style3' ,

            'vocal' ,
            'language' ,
            'languages' ,
            'lang_obj' ,
            'label',
            'isrc' ,
            'release_date',
            'author_comment' ,
            'hide_reviews' ,
            'adult_text',
            'path_to_file' ,
            'info' ,
            'image',
            'image_alt',
            'background_image',
            'status',
            'radio_status',
            'view',
            'peaks',
            'publication_agreed',
            'radio_status',
            'premium_ru_status',
            'premium_world_status',

        ];
        $scenarios[self::SCENARIO_INDEX] = [
            'id',
            'hrurl',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'artist_id' ,
            'music_style1_id' ,
            'music_style2_id' ,
            'music_style3_id',
            'music_style1' ,
            'music_style2' ,
            'music_style3' ,
            'vocal' ,
            'language' ,
            'languages' ,
            'lang_obj' ,
            'label',
            'isrc' ,
            'release_date',
            'author_comment' ,
            'hide_reviews' ,
            'adult_text',
            'path_to_file' ,
            'info' ,
            'image',
            'image_alt',
            'status',
            'radio_status',
            'peaks' ,
            'publication_agreed' ,
        ];
        $scenarios[self::SCENARIO_SEARCH] = [
            'id',
            'hrurl',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'artist_id' ,
            'music_style1_id' ,
            'music_style2_id' ,
            'music_style3_id',
            'music_style1' ,
            'music_style2' ,
            'music_style3' ,
            'vocal' ,
            'language' ,
            'languages' ,
            'lang_obj' ,
            'label',
            'isrc' ,
            'release_date',
            'author_comment' ,
            'hide_reviews' ,
            'adult_text',
            'path_to_file' ,
            'info' ,
            'image',
            'image_alt',
            'status',
            'radio_status',
            'peaks' ,
            'publication_agreed' ,
        ];
        $scenarios[self::SCENARIO_UNDONE_LIST] = [
            'id',
            'name' ,
            'name_hash' ,
            'artist_name' ,
            'radio_status',
            'premium_ru_status',
            'premium_world_status',

        ];


        return $scenarios;
    }


    public function addNew($name)
    {
        $this->name = $name;
        $this->uploader_user_id = Yii::$app->user->id;
        if ($this->validate()) {
            if ($this->save()) {
                return true;
            } else {
                Yii::$app->session->addFlash('error', 'Ошибка сохранения song');
                return false;
            }
        } else {

            $errorsText = '';
            foreach ($this->errors as $field => $errors) {
                $errTxt ='';
                foreach ($errors as $error) {
                    $errTxt .=   $error.'! ';
                }
                $errorsText .= ' '. PHP_EOL . $field .' - (' . $errTxt.'), ';
            }
            Yii::$app->session->setFlash('error', 'Ошибка валидации: '. $errorsText);
            return false;
        }
    }



    public static function upload($songFile,$artistId,$artistName,$songName,$rightHolders,$musicStyle)
    {
        $uploadModel = new UploadSongForm();
            $uploadModel->artistName = $artistName;
            $uploadModel->artistId = $artistId;
            $uploadModel->songName = $songName;
            $uploadModel->rightHolders = $rightHolders;
            $uploadModel->musicStyle1 = $musicStyle;
            $uploadModel->songFile = $songFile;
            if ($uploadModel->upload()) {
                Yii::$app->session->setFlash('success', 'аудиофайл загружен успешно');
                return true;
            } else {
                return false;
            }
    }

    public static function uploadSongByApi(
        $songFile,
        $artistId,
        $artistName,
        $songName,
        $rightHolders,
        $musicStyle1,
        $serviceType,
        $musicStyle2 = null,
        $musicStyle3 = null,
        $vocal = null,
        $language = null,
        $label = null,
        $isrc = null,
        $releaseDate = null,
        $comments = null,
        $hideReviews = null,
        $adultText = null,
        $publicationAgreed = null
    ){
        $uploadModel = new UploadSongForm();
        $uploadModel->songFile = $songFile;

        $song = new Song();
        $song->setScenario(Song::SCENARIO_CREATE);
        $song->name = $songName;
        $song->artist_id = $artistId;
        $song->artist_name = $artistName;
        $song->rightholders = $rightHolders;
        $song->music_style1_id = $musicStyle1;
        $song->music_style2_id = $musicStyle2;
        $song->music_style3_id = $musicStyle3;
        $song->vocal = $vocal;
        $song->label = $label;
        $song->isrc = $isrc;
        $song->release_date = $releaseDate;
        $song->author_comment = $comments;
        $song->hide_reviews = $hideReviews;
        $song->adult_text = $adultText;
        $song->publication_agreed = $publicationAgreed;
        $song->language = $language;




        $random = Yii::$app->security->generateRandomString(10);
        $random = str_replace('_','', str_replace('-','',$random));
        $song->name_hash = time().$random;
        $song->uploader_user_id = Yii::$app->user->id;
        $song->path_to_file = $song->name_hash .'.' . $uploadModel->songFile->extension;
        if ($serviceType == self::SERVICE_TYPE_RADIO) {
            $song->radio_status = self::STATUS_UNDONE;
            $song->status = self::STATUS_UPLOADED;
        }
        else if ($serviceType == self::SERVICE_TYPE_PREMIUM_RU) {
            $song->premium_ru_status = self::STATUS_UNDONE;
            $song->status = self::STATUS_UPLOADED;
        }
        else if ($serviceType == self::SERVICE_TYPE_PREMIUM_WORLD) {
            $song->premium_world_status = self::STATUS_UNDONE;
            $song->status = self::STATUS_UPLOADED;
        }
        if ($uploadModel->validate() && $song->save()) {

            if ($uploadModel->songFile->saveAs(Yii::$app->basePath . '/web/uploadedsongs/source/' . $song->path_to_file)) {

                $input_file =  Yii::$app->basePath . '/web/uploadedsongs/source/' . $song->path_to_file;
                $output_file = Yii::$app->basePath . '/web/uploadedsongs/listen/' . $song->name_hash .'.mp3';
                $cmd = Yii::$app->params['ffMpegPath'] .' -y -i '
                    .$input_file
                    .' -codec:a libmp3lame -b:a 128k '
                    .$output_file;
                // -b:a 320k     -q:a 0  (245)  -q:a 2  (190)   -q:a 5  (130)   -b:a 128k
                exec($cmd,$results);
            }

            return $song->toArray($song->scenarios()[Song::SCENARIO_VIEW]);
        } else {
            return $song;
        }

    }


    public static function addPeaks($hash,$peaks){
        $model = self::findOne(['name_hash'=>$hash]);
        if (!$model) return ['error'=>'song not found'];

        $model->peaks = $peaks;
        if($model->save()){
            return ['succeed'=>true];
        };

    }


    public static function allByDataProvider($aParams, $scenario=null)
    {
        return (new SongSearch())->apiSearch(['SongSearch' => $aParams],$scenario);
    }



    public function addToRadioSectionOfArtist($artistId)
    {
        $radioSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_RADIO_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artistId,
        ])->one();
        if (!$radioSection) {
            $radioSection = new Playlist();
            $radioSection->name = 'Радио';
            $radioSection->type = Playlist::TYPE_RADIO_SECTION;
            $radioSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $radioSection->object_id = $artistId;
            $radioSection->save();
        }
        $res = $radioSection->addToRadioSection($this->id);
        if (isset($res['success'])) {
            return['success'=>'трек '.$this->name.' добавлен в радио раздел.'];
        } else {
            if (isset($res['error'])) {
                if (isset($res['redirect'])) {
                    return['error'=>$res['error'],'redirect'=>$res['redirect']];
                } else {
                    return['error'=>$res['error'], 'asdf'=>'asdfasdf'];
                }
            } else {
                return['error'=>'Ошибка добавления трека в радио раздел'];
            }
        }
    }

    public  function getPlaylistItems()
    {
        return $this->hasMany(PlaylistItem::class,['song_id'=>'id']);
    }


    public  function getArtist()
    {
        return $this->hasOne(Artist::class,['id'=>'artist_id']);
    }

    public  function submitToRadioAllTracks()
    {
        $this->radio_status = Song::STATUS_INIT;
        $this->save();

        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) {
            return ['error'=>'не найден доступ артиста'];
        }
        $artistId = $artist->id;
        $radioSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_RADIO_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artistId,
        ])->one();
        if (!$radioSection) {
            $radioSection = new Playlist();
            $radioSection->name = 'Радио';
            $radioSection->type = Playlist::TYPE_RADIO_SECTION;
            $radioSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $radioSection->object_id = $artistId;
            $radioSection->save();
        }
        $res = $radioSection->addToRadioAllTracks($this->id);  // добавление в персональный радио раздел артиста
        if (isset($res['success'])) {
            return[
                'success'=>'трек '.$this->name.' добавлен в радио раздел.',
                'id'=>$res['id'],
            ];
        } else {
            if (isset($res['error'])) {
                if (isset($res['redirect'])) {
                    return['error'=>$res['error'],'redirect'=>$res['redirect']];
                } else {
                    return['error'=>$res['error']];
                }
            } else {
                return['error'=>'Ошибка добавления трека в радио раздел'];
            }
        }
    }


    public  function submitToRadioSelectedRu()
    {
        $this->premium_ru_status = Song::STATUS_INIT;
        $this->save();

        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) {
            return ['error'=>'не найден доступ артиста'];
        }
        $artistId = $artist->id;

        $artistSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_PREMIUM_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artistId,
        ])->one();
        if (!$artistSection) {
            $artistSection = new Playlist();
            $artistSection->name = 'Selected RU';
            $artistSection->type = Playlist::TYPE_PREMIUM_SECTION;
            $artistSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $artistSection->object_id = $artistId;
            $artistSection->save();
        }
        $res = $artistSection->addToArtistRadioSelectedRu($this->id);  // добавление в персональный радио раздел артиста
        if (isset($res['success'])) {
            return[
                'success'=>'трек '.$this->name.' отправлен на согласование в раздел "Selected Tracks (СНГ)".',
                'id'=>$res['id'],
            ];
        } else {
            if (isset($res['error'])) {
                if (isset($res['redirect'])) {
                    return['error'=>$res['error'],'redirect'=>$res['redirect']];
                } else {
                    return['error'=>$res['error']];
                }
            } else {
                return['error'=>'Ошибка отправки трека в раздел "Selected Tracks (СНГ)"'];
            }
        }
    }

    public  function submitToRadioSelectedWorld()
    {
        $this->premium_world_status = Song::STATUS_INIT;
        $this->save();

        $artist = Artist::findOne(['user_id'=>Yii::$app->user->id]);
        if (!$artist) {
            return ['error'=>'не найден доступ артиста'];
        }
        $artistId = $artist->id;

        $artistSection = Playlist::find()->where([
            'type'=>Playlist::TYPE_PREMIUM_SECTION,
            'object_type'=>Playlist::OBJECT_TYPE_ARTIST,
            'object_id'=>$artistId,
        ])->one();
        if (!$artistSection) {
            $artistSection = new Playlist();
            $artistSection->name = 'Премиум';
            $artistSection->type = Playlist::TYPE_PREMIUM_SECTION;
            $artistSection->object_type = Playlist::OBJECT_TYPE_ARTIST;
            $artistSection->object_id = $artistId;
            $artistSection->save();
        }
        $res = $artistSection->addToArtistRadioSelectedWorld($this->id);  // добавление в персональный радио раздел артиста
        if (isset($res['success'])) {
            return[
                'success'=>'трек '.$this->name.' отправлен на согласование в раздел "Selected Tracks (World)".',
                'id'=>$res['id'],
            ];
        } else {
            if (isset($res['error'])) {
                if (isset($res['redirect'])) {
                    return['error'=>$res['error'],'redirect'=>$res['redirect']];
                } else {
                    return['error'=>$res['error']];
                }
            } else {
                return['error'=>'Ошибка отправки трека в раздел "Selected Tracks (СНГ)"'];
            }
        }
    }

    public static function findAllTracksOptionsToArtist($artistId)
    {
        return Song::find()
            ->where(
                ['AND',
                    ['artist_id'=>$artistId],
                    ['OR',
                        ['radio_status' => null],
                        ['AND',
                            ['NOT', ['radio_status' => Song::STATUS_ACTIVE]],
                            ['NOT', ['radio_status' => Song::STATUS_APPROVED]],
                            ['NOT', ['radio_status' => Song::STATUS_INIT]],
                        ]
                    ],
                    ['OR',
                        ['premium_ru_status' => null],
                        ['premium_ru_status' => Song::STATUS_ACTIVE],
                        ['premium_ru_status' => Song::STATUS_EXPIRED],
                        ['premium_ru_status' => Song::STATUS_APPROVED],
                        ['premium_world_status' => null],
                        ['premium_world_status' => Song::STATUS_ACTIVE],
                        ['premium_world_status' => Song::STATUS_EXPIRED],
                        ['premium_world_status' => Song::STATUS_APPROVED],
                    ],
                ]
            )
            ->select([
                'id',
                'name',
                'name_hash',
                'artist_name',
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
            ])
            ->asArray()
            ->all();
    }


    public static function findSelectedRuOptionsToArtist($artistId)
    {
        return Song::find()
            ->where(
                ['AND',
                    ['artist_id'=>$artistId],
                    ['OR',
                        ['premium_ru_status' => null],
                        ['AND',
                            ['NOT', ['premium_ru_status' => Song::STATUS_ACTIVE]],
                            ['NOT', ['premium_ru_status' => Song::STATUS_APPROVED]],
                            ['NOT', ['premium_ru_status' => Song::STATUS_INIT]],
                        ]

                    ],
                    ['OR',
                        ['premium_world_status' => null],
                        ['AND',
                            ['NOT', ['premium_world_status' => Song::STATUS_ACTIVE]],
                            ['NOT', ['premium_world_status' => Song::STATUS_APPROVED]],
                            ['NOT', ['premium_world_status' => Song::STATUS_INIT]],
                        ]
                    ],
                    ['OR',
                        ['radio_status' => null],
                        ['radio_status' => Song::STATUS_ACTIVE],
                        ['radio_status' => Song::STATUS_EXPIRED],
                        ['radio_status' => Song::STATUS_UNDONE],
                        ['radio_status' => Song::STATUS_INIT],

                    ],
                ]
            )
            ->select([
                'id',
                'name',
                'name_hash',
                'artist_name',
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
            ])
            ->asArray()
            ->all();
    }


    public static function findSelectedWorldOptionsToArtist($artistId)
    {
        return Song::find()
            ->where(
                ['AND',
                    ['artist_id'=>$artistId],
                    ['OR',
                        ['premium_world_status' => null],
                        ['AND',
                            ['NOT', ['premium_world_status' => Song::STATUS_ACTIVE]],
                            ['NOT', ['premium_world_status' => Song::STATUS_APPROVED]],
                            ['NOT', ['premium_world_status' => Song::STATUS_INIT]],
                        ]
                    ],
                    ['OR',
                        ['radio_status' => null],
                        ['radio_status' => Song::STATUS_ACTIVE],
                        ['radio_status' => Song::STATUS_EXPIRED],
                        ['radio_status' => Song::STATUS_APPROVED],
                        ['premium_ru_status' => null],
                        ['premium_ru_status' => Song::STATUS_ACTIVE],
                        ['premium_ru_status' => Song::STATUS_EXPIRED],
                        ['premium_ru_status' => Song::STATUS_APPROVED],
                    ],
                ]
            )
            ->select([
                'id',
                'name',
                'name_hash',
                'artist_name',
                'radio_status',
                'premium_ru_status',
                'premium_world_status',
            ])
            ->asArray()
            ->all();
    }


    public  function getComments()
    {
        return $this->hasMany(Comment::class,['object_id'=>'id'])->andWhere(['object_type'=>Comment::OBJECT_TYPE_SONG]);
    }
}
