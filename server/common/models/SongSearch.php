<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\UploadedFile;


/**
 * This is the model class for table "song".
 *
 * @property int $id
 * @property int $page_id
 * @property string $hrurl
 * @property string $name
 * @property string $name_hash
 * @property string $artist_name
 * @property int $artist_id
 * @property int $uploader_user_id
 * @property string $rightholders
 * @property int $music_style1_id
 * @property int $music_style2_id
 * @property int $music_style3_id
 * @property int $vocal
 * @property string $language
 * @property string $label
 * @property string $isrc
 * @property int $release_date
 * @property string $author_comment
 * @property int $hide_reviews
 * @property int $adult_text
 * @property string $path_to_file
 * @property string $info
 * @property string $peaks
 * @property string $image
 * @property string $image_alt
 * @property string $background_image
 * @property string $status
 * @property string $view
 * @property int $created_at
 * @property int $updated_at
 *
 * @property string $language_id
 * @property string $music_style_id
 * @property string $is_vocal
 * @property string $is_not_vocal
 * @property string $is_adult
 * @property string $is_not_adult
 */
class SongSearch extends Song
{

    public $language_id;
    public $music_style_id;
    public $is_vocal;
    public $is_not_vocal;
    public $is_adult;
    public $is_not_adult;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id', 'artist_id', 'uploader_user_id', 'music_style1_id', 'music_style2_id', 'music_style3_id', 'vocal', 'release_date', 'hide_reviews', 'adult_text', 'created_at', 'updated_at'], 'integer'],
            [['hrurl', 'name', 'name_hash', 'artist_name', 'isrc', 'image_alt', 'background_image', 'status', 'view','language', 'label', 'path_to_file','rightholders', 'author_comment', 'info', 'image','peaks'], 'string'],

            [['languages_array','language_id','music_style_id','is_vocal','is_not_vocal','is_adult','is_not_adult'], 'safe'],
        ];
    }



    public function scenarios()
    {
        return Song::scenarios();
    }



    public function apiSearch($params, $scenario=null)
    {
        $microStart = microtime(true);

        $query = Song::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSizeLimit' => [1, 999999]],
        ]);


//        $premiumPlaylistIds = Playlist::find()
//            ->where(['type'=>Playlist::TYPE_PREMIUM])
//            ->select('id')
//            ->column();


        $this->load($params);
        if (!$this->validate()) {
            return $this->errors;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'artist_id' => $this->artist_id,
//                'music_style1_id' => $this->music_style1_id,
//                'music_style2_id' => $this->music_style2_id,
//                'music_style3_id' => $this->music_style3_id,
//                'vocal' => $this->vocal,
//                'adult_text' => $this->adult_text,
//                'created_at' => $this->created_at,
//                'updated_at' => $this->updated_at,

        ]);

        if ($this->name) {
            $query->andFilterWhere(['like', 'name', $this->name]);
        }
        if ($this->artist_name) {
            $query->andFilterWhere(['like', 'artist_name', $this->artist_name]);
        }

        if ($this->language_id) {
            $langArr = explode(',',$this->language_id);
            foreach ($langArr as $la) {
                $query->andFilterWhere(['like', 'language', $la]);
            }
        }
        if ($this->music_style_id) {
            $msarr = explode(',',$this->music_style_id);
            $query->andFilterWhere(['music_style1_id' => $msarr])
                ->orFilterWhere(['music_style2_id' => $msarr])
                ->orFilterWhere(['music_style3_id' => $msarr]);
        }

        if ($this->is_vocal) $query->andFilterWhere(['vocal'=> 1]);
        if ($this->is_not_vocal) {
            $query->andWhere([ 'or',
                ['vocal'=> 0],
                ['vocal'=> null]
            ]);
        }

        if ($this->is_adult) $query->andFilterWhere(['adult_text'=> 1]);
        if ($this->is_not_adult) {
            $query->andWhere([ 'or',
                ['adult_text'=> 0],
                ['adult_text'=> null]
            ]);
        }
//            ->orFilterWhere(['vocal'=> null]);



//            $_search = null;
//            if ($this->search) {
//                $_search = strtolower($this->search);
//                $query->andFilterWhere(['like', 'textsearch', $_search]);
//            }
//
//            if ($this->shop_id==='0'||$this->shop_id===0) {
//                $this->is_personal = true;
//            }
//
//            if ($this->is_personal) {
//                $query->andWhere(['is', 'shop_id', null]);
//            } else {
//                $query->andFilterWhere(['shop_id' => $this->shop_id]);
//            }
//
//
//            if (isset($this->sort)) {
//                if ($this->sort == 'price') {
//                    $query->orderBy(['current_price_int_value'=>SORT_DESC]);
//                }
//                if ($this->sort == '-price') {
//                    $query->orderBy(['current_price_int_value'=>SORT_ASC]);
//                }
//                if ($this->sort == 'name') {
//                    $query->orderBy(['textsearch'=>SORT_DESC]);
//                }
//                if ($this->sort == '-name') {
//                    $query->orderBy(['textsearch'=>SORT_ASC]);
//                }
//                if ($this->sort == 'date') {
//                    $query->orderBy(['updated_at'=>SORT_DESC]);
//                }
//                if ($this->sort == '-date') {
//                    $query->orderBy(['updated_at'=>SORT_ASC]);
//                }
//
//                if ($this->sort == 'popular') {
////                $query->with('follow');
////                $query->joinWith('follow', true);
////                $query->orderBy(['followers'=>SORT_DESC]);
//                }
//                if ($this->sort == '-popular') {
////                $query->joinWith('follow', true);
////                $query->orderBy(['followers'=>SORT_ASC]);
//                }
//            }
//
            $models = $dataProvider->getModels();

            $oModels = [];
            $languages = [];
            $music_styles = [];
//            $conditions = [];
//            $years = [];
//            $countries = [];
//            $modelScenario = $scenario?$scenario:ProductItem::SCENARIO_SEARCH;
//
            foreach( $models as $model) {
                $model->scenario = Song::SCENARIO_SEARCH;
                $arrayModel = $model->toArray(Song::scenarios()[Song::SCENARIO_SEARCH]);

//                // $languages
                if (isset($arrayModel['lang_obj'])) {
                    foreach ($arrayModel['lang_obj'] as $lang) {
                        if (!isset( $languages[$lang['id']])) {
                            $languages[$lang['id']]['id'] = $lang['id'];
                            $languages[$lang['id']]['name'] = $lang['name_ru'];
                        }
                    }
                }

//                // $music_styles
                if ($arrayModel['music_style1_id'] && !isset( $music_styles[$arrayModel['music_style1_id']])) {
                        $music_styles[$arrayModel['music_style1_id']] = true;
                }
                if ($arrayModel['music_style2_id'] && !isset( $music_styles[$arrayModel['music_style2_id']])) {
                        $music_styles[$arrayModel['music_style2_id']] = true;
                }
                if ($arrayModel['music_style3_id'] && !isset( $music_styles[$arrayModel['music_style3_id']])) {
                        $music_styles[$arrayModel['music_style3_id']] = true;
                }

//
                $oModels[] = $arrayModel;
            }
//
////         сортировка
//            if (isset($this->sort)) {
//                if ($this->sort == 'popular') {
//                    ArrayHelper::multisort($oModels,'followers_count',SORT_ASC);
//                }
//                if ($this->sort == '-popular') {
//                    ArrayHelper::multisort($oModels,'followers_count',SORT_DESC);
//                }
//            }
//
//
//
            $result = [
                'feed'=>$oModels,
                'filter'=>[
                    'languages'=>array_values($languages),
                    'music_styles'=>\common\models\Menu::filterTree($music_styles),

//                    'conditions'=>array_values($conditions),
//                    'countries'=>array_values($countries),
//                    'years'=>array_values($years),
//                    'sort'=>$this->sort,

                ],
            ];
//
//            $cash->set($cacheKey, Json::encode($result),1800);
//            $result['updated'] = true;
//            $startIndex  = 0;
//        }
//
//        $result['items'] = array_slice($result['allItems'], $startIndex,$limit);
//        $isLastApiPage = false;
//        if (($startIndex+$limit) >= count($result['allItems'])) {
//            $isLastApiPage = true;
//        }
//        unset($result['allItems']);
//
//        $microEnd = microtime(true);
//        $duration = round(($microEnd - $microStart)*1000);
//        $stat = new Stat();
//        $stat->task = Stat::TASK_SEARCH_PRODUCT_ITEM;
//        $stat->duration = $duration;
//        $stat->data = $cacheKey;
//        $stat->save();
//        $result['duration'] = $duration;
//        $result['startIndex']  = $startIndex;
//        $result['limit']  = $limit;
//        $result['isLastApiPage']  = $isLastApiPage;
//
        $result['debug'] = [
            '$msarr'=>isset($msarr)?$msarr:null,
        ];
//
        return $result;
    }




}
