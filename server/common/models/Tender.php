<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tender".
 *
 * @property int $id
 * @property string $hrurl
 * @property int $author_user_id
 * @property string $name
 * @property string $description
 * @property string $conditions
 * @property string $music_mood
 * @property int $music_style1_id
 * @property int $music_style2_id
 * @property int $music_style3_id
 * @property string $language
 * @property int $only_vocal
 * @property int $end_date
 * @property string $reward
 * @property string $location
 * @property int $accept_demo
 * @property int $adult_text
 * @property int $tracks_from_author
 * @property string $example1
 * @property string $example2
 * @property string $example3
 * @property string $image
 * @property int $image_from_avatar
 * @property string $background_image
 * @property string $imagelink
 * @property string $imagelink_alt
 * @property string $promolink
 * @property string $promoname
 * @property string $stylekey
 * @property string $view
 * @property string $layout
 * @property int $created_at
 * @property int $updated_at
 */
class Tender extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_user_id', 'music_style1_id', 'music_style2_id', 'music_style3_id', 'only_vocal', 'end_date', 'accept_demo', 'adult_text', 'tracks_from_author', 'image_from_avatar', 'created_at', 'updated_at'], 'integer'],
            [['description', 'conditions', 'reward', 'image', 'background_image'], 'string'],
            [['music_style1_id'], 'required'],
            [['hrurl', 'name', 'imagelink', 'imagelink_alt', 'promolink', 'promoname', 'stylekey', 'view', 'layout'], 'string', 'max' => 255],
            [['music_mood', 'language', 'location', 'example1', 'example2', 'example3'], 'string', 'max' => 510],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hrurl' => 'Hrurl',
            'author_user_id' => 'Author User ID',
            'name' => 'Name',
            'description' => 'Description',
            'conditions' => 'Conditions',
            'music_mood' => 'Music Mood',
            'music_style1_id' => 'Music Style1 ID',
            'music_style2_id' => 'Music Style2 ID',
            'music_style3_id' => 'Music Style3 ID',
            'language' => 'Language',
            'only_vocal' => 'Only Vocal',
            'end_date' => 'End Date',
            'reward' => 'Reward',
            'location' => 'Location',
            'accept_demo' => 'Accept Demo',
            'adult_text' => 'Adult Text',
            'tracks_from_author' => 'Tracks From Author',
            'example1' => 'Example1',
            'example2' => 'Example2',
            'example3' => 'Example3',
            'image' => 'Image',
            'image_from_avatar' => 'Image From Avatar',
            'background_image' => 'Background Image',
            'imagelink' => 'Imagelink',
            'imagelink_alt' => 'Imagelink Alt',
            'promolink' => 'Promolink',
            'promoname' => 'Promoname',
            'stylekey' => 'Stylekey',
            'view' => 'View',
            'layout' => 'Layout',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
