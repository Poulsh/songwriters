<?php

namespace common\models;

use backend\controllers\SongController;
use common\models\Song;
use yii\base\Action;
use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class UploadSongForm  extends Model
{
    /**
     * @var UploadedFile
     */
    public $songFile;

    /**
     * @var string
     */
    public $toModelId;

    /**
     * @var string
     */
    public $toModelProperty;

    /**
     * @var string
     */
    public $songName;

    /**
     * @var string
     */
    public $artistName;

    /**
     * @var string
     */
    public $rightHolders;

    /**
     * @var number
     */
    public $musicStyle1;

    /**
     * @var number
     */
    public $artistId;



    public function rules()
    {
        return [
            [
                ['songFile'], 'file',
                'skipOnEmpty' => false,
                'extensions' => 'wav, wave, mp3, aif, aiff',
                'checkExtensionByMimeType' => false,
                ],
        ];
    }

    public function upload()
    {
        $song = new Song();
        $song->artist_id = $this->artistId;
        $song->artist_name = $this->artistName;
        $song->rightholders = $this->rightHolders;
        $song->music_style1_id = $this->musicStyle1;
        $song->name_hash = time().Yii::$app->security->generateRandomString(10);
        $song->status = 'uploaded';

//        $fileName = $this->songFile->baseName .'.' . $this->songFile->extension;
        $song->path_to_file = $song->name_hash .'.' . $this->songFile->extension;

        if ($this->validate() && $song->addNew($this->songName)) {

            if ($this->songFile->saveAs(Yii::$app->basePath . '/web/uploadedsongs/source/' . $song->path_to_file)) {

//                $args = array('type' => 'audio',
//                    'input_file' => Yii::$app->basePath . '/web/uploadedsongs/' . $song->path_to_file,
//                    'output_file' =>  Yii::$app->basePath . '/web/uploadedsongs/' . $song->name_hash .'.mp3',
//                );
//                 Yii::$app->ffmpeg->ffmpeg($args);

                $input_file =  Yii::$app->basePath . '/web/uploadedsongs/source/' . $song->path_to_file;
                $output_file = Yii::$app->basePath . '/web/uploadedsongs/listen/' . $song->name_hash .'.mp3';
                $cmd = Yii::$app->params['ffMpegPath'] .' -y -i '
                    .$input_file
                    .' -codec:a libmp3lame -b:a 128k '
                    .$output_file;
                // -b:a 320k     -q:a 0  (245)  -q:a 2  (190)   -q:a 5  (130)   -b:a 128k
                exec($cmd,$results);
//                return $song;
                return true;
            } else {
//                return $song;
                return false;
            }
        }
    }


    public function change($fileNameWithExtension, $fileNameWithoutExtension)
    {
        if ($this->validate()) {

//             todo delete old files???

            if ($this->songFile->saveAs(Yii::$app->basePath . '/web/uploadedsongs/source/' . $fileNameWithExtension)) {
                $input_file =  Yii::$app->basePath . '/web/uploadedsongs/source/' . $fileNameWithExtension;


                $output_file = Yii::$app->basePath . '/web/uploadedsongs/listen/' . $fileNameWithoutExtension .'.mp3';
                $cmd = Yii::$app->params['ffMpegPath'] .' -y -i '
                    .$input_file
                    .' -codec:a libmp3lame -b:a 128k '
                    .$output_file;
                // -b:a 320k     -q:a 0  (245)  -q:a 2  (190)   -q:a 5  (130)   -b:a 128k
                exec($cmd,$results);
                return true;
            } else {
                return false;
            }
        }
    }







}