<?php
namespace common\models;

use Yii;
use yii\base\ErrorException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $access_token
 * @property string $verification_code
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_EMAIL_VERIFICATION = 1;
    const STATUS_ACTIVE = 10;
    const STATUS_ON_CHECK_BY_ADMIN = 2;
    const STATUS_DECLINED_RADIO = 3;




    const SCENARIO_COMMON = 'common';
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_PROFILE = 'profile';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_VERIFICATION = 'verification';


    const ROLE_ADM = 'admin';
    const ROLE_ART = 'artist';
    const ROLE_R = 'radio';
    const ROLE_M = 'media';

    const USER_LANG_RU = 'ru';
    const USER_LANG_EN = 'en';
    const USER_LANG_DEFAULT = 'ru';


    public static $aRoles = [self::ROLE_ADM, self::ROLE_ART, self::ROLE_R, self::ROLE_M];

    public $roles = [];



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
//        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [
                self::STATUS_ACTIVE,
                self::STATUS_EMAIL_VERIFICATION,
                self::STATUS_DELETED,
                self::STATUS_ON_CHECK_BY_ADMIN,
                self::STATUS_DECLINED_RADIO
            ]],
            ['language', 'string', 'max' => 5]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_LOGIN] = ['email', 'password', 'first_name', 'last_name'];
        $scenarios[self::SCENARIO_COMMON] = ['email', 'first_name', 'last_name'];
        $scenarios[self::SCENARIO_REGISTER] = ['email', 'password', 'first_name', 'last_name'];
        $scenarios[self::SCENARIO_PROFILE] = ['email', 'password', 'first_name', 'last_name', 'language'];
        $scenarios[self::SCENARIO_CREATE] = ['email', 'password', 'first_name', 'last_name', 'roles', 'language'];
        $scenarios[self::SCENARIO_VERIFICATION] = ['email', 'verification_code', 'first_name', 'last_name','roles'];
        return $scenarios;

    }

    /**
     * Безопасные поля
     *
     * @return array
     */
    public function fields()
    {

        $fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'language',
            'roles' => function ($q) {
                return $q->getRoles();
            },
//            'about',
//            'birthday_at'=>function($q){
//                return $q->birthday_at == '0000-00-00 00:00:00' ? null : date('m-d-Y', strtotime($q->birthday_at));
//            },
//            'avatar' => function ($q) {
//                return $q->avatar ? $q->avatar : 'avatar.gif';
//            },
//            'url_folder' => function ($q) {
//                return Url::to('uploads/', true);
//            }
        ];



        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        return static::findOne([
            'access_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password = null)
    {
        if ($password) {
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        } else {
            if (!empty($this->password)) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            }
        }

    }

    /**
     * Set language to the model
     *
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    public function setAccessToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function setVerificationCode()
    {
        $this->verification_code = Yii::$app->security->generateRandomString();
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return $this->hasMany(RolesItem::class, ['name' => 'item_name'])
            ->viaTable('auth_assignment', ['user_id' => 'id'])
            ->select('name')->andWhere('type = 1')->column();
    }

    public function getArtist()
    {
        return $this->hasOne(Artist::class,['user_id'=>'id']);
    }

    public function getRadio()
    {
        return $this->hasOne(Radio::class,['user_id'=>'id']);
    }

    public function getMedia()
    {
        return $this->hasOne(Media::class,['user_id'=>'id']);
    }


    /**
     *
     * Создать пользователя
     *
     * @param $attributes
     *
     * @return User
     */
    public static function create($attributes, $scenario = 'register')
    {
        $oUser = new self;
        $oUser->setScenario($scenario);
        $oUser->attributes = $attributes;
        $oUser->generateAuthKey();
        $oUser->setPassword();
        $oUser->setAccessToken();
        $oUser->setVerificationCode();
        $oUser->status = self::STATUS_EMAIL_VERIFICATION;
        if ($oUser->save()) {
            $oUser->sendEmailVerification();
        }
        return $oUser;
    }



    public function sendEmailVerification()
    {
        Yii::$app->mailer->htmlLayout = "layouts/montserrat";
        $mailer = Yii::$app->mailer->compose('sign-up-verification', [
            'name' => $this->first_name,
            'verification_code'=>$this->verification_code,
        ]);
        return $mailer->setFrom([Yii::$app->params['noreplyEmail']=>Yii::$app->params['noreplyName']])
            ->setTo($this->email)
            ->setSubject((\Yii::$app->language=='ru-RU'?
                                        '★ Подтверждение регистрации на ':
                                        '★ Sign in confirmation ').Yii::$app->params['mainName'])
            ->send();

    }

    /**
     * Верификация аккаунта
     *
     * @param $token
     *
     * @return array
     * @throws BadRequestHttpException
     * @throws ErrorException
     */
    public static function verification($token)
    {

        $oUser = self::find()->andWhere(['verification_code' => $token])->one();
        if (!$oUser) {
            throw new BadRequestHttpException('Ссылка уже подтверждена или утратила свою силу.');
        }
        $oUser->setScenario(self::SCENARIO_VERIFICATION);
        $oUser->verification_code = null;
        if ($oUser->radio) {
            $radio = $oUser->radio;
            $oUser->status = self::STATUS_ON_CHECK_BY_ADMIN;
            if ($oUser->save()) {
                $radio->status = Radio::STATUS_ON_CHECK_BY_ADMIN;
                $radio->save();

                $head = 'Новый радио доступ требует подтверждения ';
                $text = 'Пользователь '.$oUser->first_name.' '.$oUser->last_name.' ожидает подтверждения доступа для радио '.$radio->name;
                $notification = Notification::create([
                    'type' => Notification::TYPE_ADMIN_NEW_RADIO,
                    'subject_id' => 'new_radio_'.$radio->id,
                    'head' => $head,
                    'text' => $text,
                    'call2action_name' => 'Перейти к радио',
                    'call2action_link' => '/radio/view?id='.$radio->id,
                ]);
                return [
                    'onRadioConfirmation'=>true,
                    'success'=> \Yii::t('api', 'EMAIL_CONFIRMED_BUT_ADMIN_NOT_YET')];

            } else {
                throw new ErrorException( \Yii::t('api', 'EMAIL_VERIFICATION_ERROR'));
            }


        } else {
            $oUser->status = self::STATUS_ACTIVE;
            if ($oUser->save()) {
                if ($oUser->artist) {
                    $oUser->artist->status = Artist::STATUS_ACTIVE;
                    $oUser->artist->save();
                    \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(self::ROLE_ART), $oUser->id);

                }
                if ($oUser->media) {
                    $oUser->media->status = Media::STATUS_ACTIVE;
                    $oUser->media->save();
                    \Yii::$app->authManager->assign(\Yii::$app->authManager->getRole(self::ROLE_M), $oUser->id);
                }
                $oUser->setScenario(self::SCENARIO_COMMON);
                return $oUser;
            } else {
                throw new ErrorException('Ошибка верификации аккаунта. Обратитесь в службу поддержки.');
            }
        }

    }


    public static function adminCheckSum(){
        $res=[];

        $res['notifyNotSeenQnt'] = Notification::notSeenToAdminQnt();
        return $res;
    }


    public static function checkSum($id,$attr=[]){

        $query = Notification::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $artist = Artist::findOne(['user_id'=>$id]);
        $radio = Radio::findOne(['user_id'=>$id]);
        $media = Media::findOne(['user_id'=>$id]);

        if ($artist ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['artist_id'=> $artist->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }
        if ($radio ) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['radio_id' => $radio->id],
                    ],
                    ['AND',
                        ['status'=> Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }

        if ( $media) {
            $query->orWhere(
                ['OR',
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['media_id' => $media->id],
                    ],
                    ['AND',
                        ['status' => Notification::STATUS_NOT_SEEN],
                        ['user_id'=> $id],
                    ],
                ]
            );
        }
        $res['notifyQnt'] = $dataProvider->getTotalCount();


        if (isset($attr['activeItems'])) {
            $activeItems = [];
            foreach ( $attr['activeItems'] as $key => $value) {
                $item = PlaylistItem::find()
                    ->where(['id'=>$key])
                    ->one();
                $item = $item->toArray($item->scenarios()[PlaylistItem::SCENARIO_CHECKSUM_ACTIVE_ITEMS]);
                $activeItems[$key]['id'] = $key;
                $activeItems[$key]['status'] = $item['status'];
                $activeItems[$key]['messagesQnt'] = $item['messages_qnt']['qnt'];
                $activeItems[$key]['messagesQntNotSeenToBuyer'] = $item['messages_qnt']['not_seen'];
            }
            $res['activeItems'] = $activeItems;
        }
        return $res;
    }


    /**
     * Reset password
     */
    public static function passwordReset($email)
    {
        $user = self::findByEmail($email);
        if (!$user) {
            return ['success'=> \Yii::t('api', 'Пользователь с email {email} не найден', [
                'email' => $email,
            ])];
        }
        $user->scenario = self::SCENARIO_VERIFICATION;
        $user->generatePasswordResetToken();
        $user->save();

        if ( User::sendEmailToPasswordReset($email,$user->first_name,$user->password_reset_token)) {
            return[
                'success'=> \Yii::t('api', 'LINK_TO_PASS_SENT', [
                    'email' => $email,
                ])];
        } else {
            return['success'=> \Yii::t('api','SYSTEM_ERROR')];
        }

    }


    /**
     *  Изменение пароля по токену
     */
    public static function changePasswordByToken($token,$password)
    {
        $user = self::findByPasswordResetToken($token);
        if (!$user) return ['success'=> \Yii::t('api', 'Ссылка недействительна')];
        $user->scenario = self::SCENARIO_REGISTER;
        $user->removePasswordResetToken();
        $user->setPassword($password);


        if ($user->save()) {
            return['success'=>\Yii::t('api', 'Пароль изменен, можете авторизоваться')];
        } else {
            return['success'=>\Yii::t('api', 'Ошибка в работе системы, обратитесь к администратору')];
        }

    }


    public static function sendEmailToPasswordReset($email, $first_name, $verificationCode)
    {
        Yii::$app->mailer->htmlLayout = "layouts/montserrat";
        $mailer = Yii::$app->mailer->compose('user-password-reset', [
            'name' => $first_name,
            'verification_code'=>$verificationCode,
        ]);
        return $mailer->setFrom([Yii::$app->params['noreplyEmail']=>Yii::$app->params['noreplyName']])
            ->setTo($email)
            ->setSubject( \Yii::$app->language=='ru-RU'?'Сброс пароля':'Password reset')
            ->send();
    }


    public function sendEmailNotification($subject,$text,$link,$linkName)
    {
        Yii::$app->mailer->htmlLayout = "layouts/montserrat";
        $mailer = Yii::$app->mailer->compose('notification', [
            'name' => $this->first_name,
            'text'=>$text,
            'link'=>$link,
            'linkName'=>$linkName,
        ]);
        return $mailer->setFrom([Yii::$app->params['noreplyEmail']=>Yii::$app->params['noreplyName']])
            ->setTo($this->email)
            ->setSubject($subject)
            ->send();

    }


    /**
     * Finds current language of user
     *
     * @param integer $id
     * @return string
     */
    public static function getLanguageByUserID($id)
    {
        $user = User::findOne($id);
        return $user->language;

    }


}
