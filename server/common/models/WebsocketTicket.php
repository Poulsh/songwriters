<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "websocket_ticket".
 *
 * @property int $id
 * @property string $token
 * @property string $status
 * @property int $user_id
 * @property int $expires

 */
class WebsocketTicket extends \yii\db\ActiveRecord
{
    const EXPIRE_PERIOD = 3600*24*30;
    const STATUS_ACTIVE = 'active';
    const STATUS_USED = 'used';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'websocket_ticket';
    }




    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'expires'], 'integer'],
            [['token', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'status' => 'Status',
            'user_id' => 'User ID',
            'expires' => 'Expires',

        ];
    }

    public static function create()
    {
        $now = time();
        $existedValid =  WebsocketTicket::find()
            ->where(['user_id'=>Yii::$app->user->id])
            ->andWhere(['status'=>self::STATUS_ACTIVE])
            ->andWhere(['>=', 'expires', $now])
            ->one();
        if ($existedValid) {
            return $existedValid->token;
        } else {
            $model = new self();
            $model->token = Yii::$app->security->generateRandomString();
            $model->user_id = Yii::$app->user->id;
            $model->expires = time() + self::EXPIRE_PERIOD;
            $model->status = self::STATUS_ACTIVE;
            if ($model->save()) {
                return $model->token;
            } else {
                return false;
            }
        }

    }

    public static function validateTicket($token)
    {
        $model = WebsocketTicket::find()
            ->where(['token'=>$token])
            ->andWhere(['status'=>self::STATUS_ACTIVE])
            ->andWhere(['>=', 'expires', time()])
            ->one();
        if ($model) {
            $model->status = self::STATUS_USED;
            $model->save();
            return $model->user_id;
        } else {
            return false;
        }
    }
}
