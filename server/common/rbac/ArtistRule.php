<?php
namespace common\rbac;

use yii\rbac\Rule;

class ArtistRule extends Rule
{
    public $name = 'Artist';
    public $description = 'Доступ артиста';

    public function execute($user, $item, $params)
    {
        return true;
    }
}