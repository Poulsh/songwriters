<?php
namespace common\rbac;

use yii\rbac\Rule;

class MediaRule extends Rule
{
    public $name = 'Media';
    public $description = 'Доступ медиа работника';


    public function execute($user, $item, $params)
    {
        return true;
    }
}