<?php
namespace common\rbac;

use yii\rbac\Rule;

class RadioRule extends Rule
{
    public $name = 'Radio';
    public $description = 'Доступ радио';


    public function execute($user, $item, $params)
    {
        return true;
    }
}