<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace console\controllers;

use common\models\RolesAssignment;

use common\models\User;
use common\rbac\AdminRule;
use common\rbac\ArtistRule;
use common\rbac\CreatorRule;
use common\rbac\MediaRule;
use common\rbac\RadioRule;
use common\rbac\StatRule;
use common\components\ErrorHelper;
use yii\console\Controller;
use yii\console\ExitCode;

class RbacController extends Controller {

    private $creatorRoleName = 'creator';
    private $adminRoleName = 'admin';
    private $artistRoleName = 'artist';
    private $radioRoleName = 'radio';
    private $mediaRoleName = 'media';

    private $creatorPermName = 'creatorPermission';
    private $adminPermName = 'adminPermission';
    private $artistPermName = 'artistPermission';
    private $radioPermName = 'radioPermission';
    private $mediaPermName = 'mediaPermission';


    public function actionIndex() {
        $oldAssigns = RolesAssignment::find()->all();
//        $oldAssigns = null;
        $auth = \Yii::$app->authManager;
        $auth->removeAll();
//        создание ролей
        $admin = $auth->createRole( 'Admin' );
        $stat = $auth->createRole( 'Stat' );
        $creator = $auth->createRole( 'Creator' );

        $auth->add( $admin );
        $auth->add( $stat );
        $auth->add( $creator );

        // правило
        $statRule = new StatRule();
        $auth->add( $statRule );
        // Разрешение
        $statPermission = $auth->createPermission('statPermission');
        $statPermission->description = 'Доступ к статистике - просмотр и изменение';
        $statPermission->ruleName = $statRule->name;
        $auth->add( $statPermission );

        $adminRule = new AdminRule();
        $auth->add( $adminRule );
        $adminPermission = $auth->createPermission('adminPermission');
        $adminPermission->description = 'Доступ к админке';
        $adminPermission->ruleName = $adminRule->name;
        $auth->add( $adminPermission );

        $creatorRule = new CreatorRule();
        $auth->add( $creatorRule );
        $creatorPermission = $auth->createPermission('creatorPermission');
        $creatorPermission->description = 'Доступ ко всему';
        $creatorPermission->ruleName = $creatorRule->name;
        $auth->add( $creatorPermission );


//        наследование
        $auth->addChild( $creator, $creatorPermission );
        $auth->addChild( $creator, $admin );
        $auth->addChild( $admin, $adminPermission );
        $auth->addChild( $admin, $stat );
        $auth->addChild( $stat, $statPermission );



        $auth->assign($admin,1);
        $auth->assign($admin,2);
        $auth->assign($stat,5);
        $auth->assign($creator,6);

//        if (is_array($oldAssigns)) {
//            foreach ($oldAssigns as $oldAssign) {
//                $auth->assign($auth->createRole($oldAssign['item_name']),$oldAssign['user_id']);
//            }
//        }

    }

    // php yii rbac/set
    public function actionSet()
    {

        $oldAssigns = RolesAssignment::find()->all();

        $auth = \Yii::$app->authManager;
        $auth->removeAll();
        $creatorRole = $this->addRole($this->creatorRoleName);
        $adminRole = $this->addRole($this->adminRoleName,'администратор');
        $artistRole = $this->addRole($this->artistRoleName,'артист');
        $radioRole = $this->addRole($this->radioRoleName,'радио');
        $mediaRole = $this->addRole($this->mediaRoleName,'медиа работник');

        $this->addPermission($this->creatorPermName, CreatorRule::class);
        $this->addPermission($this->adminPermName, AdminRule::class);
        $this->addPermission($this->artistPermName, ArtistRule::class);
        $this->addPermission($this->radioPermName, RadioRule::class);
        $this->addPermission($this->mediaPermName, MediaRule::class);

        $creatorPermission = $auth->getPermission($this->creatorPermName);
        $adminPermission = $auth->getPermission($this->adminPermName);
        $artistPermission = $auth->getPermission($this->artistPermName);
        $radioPermission = $auth->getPermission($this->radioPermName);
        $mediaPermission = $auth->getPermission($this->mediaPermName);


        $auth->addChild( $creatorRole, $creatorPermission );
        $auth->addChild( $adminRole, $adminPermission );
        $auth->addChild( $artistRole, $artistPermission );
        $auth->addChild( $radioRole, $radioPermission );
        $auth->addChild( $mediaRole, $mediaPermission );


        $auth->addChild( $creatorRole, $adminRole );
        $auth->addChild( $adminRole, $artistRole );
        $auth->addChild( $adminRole, $radioRole );
        $auth->addChild( $adminRole, $mediaRole );


//        $auth->assign($creatorRole,1);
//        $auth->assign($adminRole,7);
//        $auth->assign($artistRole,9);
//        $auth->assign($radioRole,11);
//        $auth->assign($mediaRole,10);
    }

    private function addRole($name, $description=null)
    {
        $auth = \Yii::$app->authManager;
        $role = $auth->createRole ( $name );
        $role->description = $description;
        $auth->add( $role );
        return $role;
    }


    private function addPermission($permissionName, $ruleClass)
    {

        $rule = new $ruleClass;
        $auth = \Yii::$app->authManager;

        $auth->add( $rule );
        $permission = $auth->createPermission($permissionName);
        $permission->description = $rule->description;
        $permission->ruleName = $rule->name;
        $res = $auth->add( $permission );
        return $res;
    }



    //  php yii rbac/add-user firstName lastName q@g.com passwww
    public function actionAddUser($firstName,$lastName,$email,$password)
    {
        $user = new User();
        $user->first_name = $firstName;
        $user->last_name = $lastName;
        $user->email = $email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->setAccessToken();
        if ($user->save()) {
            echo 'Пользователь создан, id: '.$user->id.PHP_EOL;
            return ExitCode::OK;
        } else {
            echo "Houston, we have a problem!\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    public function actionAssignRole ($roleName, $userId)
        // php yii rbac/assign-role admin/radio/media id
    {

        $user = User::find()->where(['id'=>$userId])->one();
        if (!$user) {
            echo "Нет такого пользователя!\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }
        $assign = new RolesAssignment();
        if (
            $roleName != $this->adminRoleName and
            $roleName != $this->artistRoleName and
            $roleName != $this->radioRoleName and
            $roleName != $this->mediaRoleName and
            $roleName != $this->creatorRoleName
        ) {
            echo "not valid role!\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }

        $assign->item_name = $roleName;
        $assign->user_id = $userId;
        if ($assign->save()) {
            echo 'Пользователю '.$user->first_name.' '.$user->last_name.' назначена роль: '.$roleName.PHP_EOL;
            return ExitCode::OK;
        } else {
            echo "Houston, we have a problem!\n";
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

    public function actionPrintRoles ($user_id)
        // php yii rbac/print-roles   id
    {
        echo  json_encode(\ array_keys(\Yii::$app->authManager->getRolesByUser($user_id))).PHP_EOL ;
    }


}



