<?php
/**
 * китайский вариант
 * https://www.gowhich.com/blog/921
 *
 */

namespace console\controllers;


use common\models\Notification;
use common\models\User;
use common\models\WebsocketTicket;
use yii\console\Controller;
use yii\console\ExitCode;
use Workerman\Worker;
use yii\helpers\Console;
use yii\helpers\Json;

class WorkermanWebSocketController extends Controller
{

//    ws://0.0.0.0:2346
//    private $port = '2346';
//    private $wsIp = '0.0.0.0'; // ; //'0.0.0.0'
//    private $wsPort = '2346';


    public $send;


    private $tcpPath = Notification::TCP_PATH;


    public $daemon = false;
    public $gracefully = true;

//    static $activeUsers=[];
    static $connections=[];


    public function options($actionID)
    {
        return ['send', 'daemon', 'gracefully'];
    }

    public function optionAliases()
    {
        return [
            's' => 'send',
            'd' => 'daemon',
            'g' => 'gracefully',
        ];
    }

    public function actionIndex($action)
        // php yii workerman-web-socket start
        // php yii workerman-web-socket start -d        // старт демоном
        // php yii workerman-web-socket status -g
        // php yii workerman-web-socket connections      // -s
        // php yii workerman-web-socket stop

    {
        if ('start' == $action) {
            try {
                $this->start();
            } catch (\Exception $e) {
                $this->stderr($e->getMessage() . "\n", Console::FG_RED);
            }
        } else if ('stop' == $action) {
            $this->stop();
        } else if ('restart' == $action) {
            $this->restart();
        } else if ('reload' == $action) {
            $this->reload();
        } else if ('status' == $action) {
            $this->status();
        } else if ('connections' == $action) {
            $this->connections();
        }
    }

    public function initWorker()
    {
        $wsConnections=[];

        if (isset(\Yii::$app->params['ssl']) && \Yii::$app->params['ssl']==true) {
            $wsWorker = new Worker(\Yii::$app->params['wsPath'],
                    ['ssl' =>
                        [
                            'local_cert'  =>  __DIR__.'/'. \Yii::$app->params['ws_local_cert'],
                            'local_pk'    =>  __DIR__.'/'. \Yii::$app->params['ws_local_pk'],
                            'verify_peer' => false,
                        ]
                    ]
                );

            $wsWorker->transport = 'ssl';
        } else {
            $wsWorker = new Worker(\Yii::$app->params['wsPath']);
        }

//        $wsWorker->count = 4;

        $wsWorker->onWorkerStart = function() use (&$wsConnections) {

            $local_tcp_worker = new Worker($this->tcpPath);

            $local_tcp_worker->onMessage = function($connection, $data) use (&$wsConnections) {
                $data1 = json_decode($data);
                $user_id = $data1->user_id;
                $message = $data1->message;


//                echo 'tsp message:  user_id:'.$user_id.', message:'.Json::encode($message). PHP_EOL;

                if (isset($wsConnections[$user_id])) {
                    $_connection = $wsConnections[$user_id];

                    $_connection->send(Json::encode($message));

//                    $sendRes = $_connection->send(Json::encode($message));
//                    if ($sendRes) {
//                        echo 'message sent to ws connection'.PHP_EOL;
//                    } else {
//                        echo 'message sending to ws connection failed'.PHP_EOL;
//                    }
                } else {
                    echo 'ws connection of user '.$user_id.' is not connected now'.PHP_EOL;
                }


            };
            $local_tcp_worker->listen();
        };




        // при новом WS соединении
        $wsWorker->onConnect = function ($connection) use (&$wsConnections) {

            $connection->onWebSocketConnect = function($connection, $header) use (&$wsConnections){

                if (isset($_GET['ticket'])) {
                    $user_id = $this->accept($_GET['ticket']);
                    if ($user_id) {
                        $wsConnections[$user_id] = $connection;

                        echo 'WS connection user_id '.$user_id.' stored'.PHP_EOL;

                    } else {
//                         закрываем соединение - к сожалению сабж не проходит, todo timeout
                        $data=['event'=>'message','data'=> 'reject_auth',];
                        $connection->send(Json::encode($data));
                        $connection->close('reject_auth');

                        echo "New WS connection rejected \n";

                    }
                }
            };

        };

        // при сообщении от клиента
        $wsWorker->onMessage = function ($connection, $data) use (&$wsConnections) {

            $data=['event'=>'message','data'=> 'echo_message_from_server','income_data_to_server'=>$data];
            $connection->send(Json::encode($data));

            echo '$wsWorker->onMessage'.PHP_EOL;
        };

        $wsWorker->onClose = function ($connection) use (&$wsConnections) {
            $savedConnection = array_search($connection, $wsConnections);
            unset($wsConnections[$savedConnection]);

            echo "WS connection closed\n";
        };
    }


    /**
     *  websocket start
     */
    public function start()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'start';
        if ($this->daemon) {
            $argv[2] = '-d';
        }
        Worker::runAll();
    }

    /**
     *  websocket stop
     */
    public function stop()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'stop';
        if ($this->gracefully) {
            $argv[2] = '-g';
        }
        Worker::runAll();
    }

    /**
     *  websocket restart
     */
    public function restart()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'restart';
        if ($this->daemon) {
            $argv[2] = '-d';
        }
        if ($this->gracefully) {
            $argv[2] = '-g';
        }
        Worker::runAll();
    }


    /**
     *  websocket reload
     */
    public function reload()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'reload';
        if ($this->gracefully) {
            $argv[2] = '-g';
        }
        Worker::runAll();
    }


    /**
     *  websocket status
     */
    public function status()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'status';
        if ($this->daemon) {
            $argv[2] = '-d';
        }
        Worker::runAll();
    }


    /**
     *  websocket connections
     */
    public function connections()
    {
        $this->initWorker();
        global $argv;
        $argv[0] = $argv[1];
        $argv[1] = 'connections';
        Worker::runAll();
    }


    private function accept($token){
        return WebsocketTicket::validateTicket($token);
    }


}
