<?php

use yii\db\Migration;

/**
 * Handles adding access_token to table `user`.
 */
class m180829_194547_add_access_token_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('user', true) === null) {
            $this->createTable('user', [
                'id' => $this->primaryKey(),
                'username' => $this->string(255)->notNull(),
                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string(255)->notNull(),
                'password_reset_token' => $this->string(255),
                'email' => $this->string(255)->notNull(),
                'status' => $this->integer()->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


        $this->addColumn('user', 'access_token', $this->string(32));
        $this->addColumn('user', 'verification_code', $this->string(32));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'access_token');
        $this->dropColumn('user', 'verification_code');
    }
}
