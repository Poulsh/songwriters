<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `username_column_to_user`.
 */
class m180830_083713_drop_username_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'username');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('user', 'username', $this->string(255)->notNull());

    }
}
