<?php

use yii\db\Migration;

/**
 * Handles adding peaks to table `song`.
 */
class m190803_070030_add_peaks_column_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('song', true) === null) {
            $this->createTable('song', [
                'id' => $this->primaryKey(),
                'page_id' => $this->integer(),
                'hrurl' => $this->string(255),
                'name' => $this->string(255)->notNull(),
                'name_hash' => $this->string(255),
                'artist_name' => $this->string(255)->notNull(),
                'artist_id' => $this->integer(),
                'uploader_user_id' => $this->integer(),
                'rightholders' => $this->text(),
                'music_style1_id' => $this->integer(),
                'music_style2_id' => $this->integer(),
                'music_style3_id' => $this->integer(),
                'vocal' => $this->integer(1),
                'language' => $this->string(510),
                'label' => $this->string(510),
                'isrc' => $this->string(510),
                'release_date' => $this->integer(),
                'comments' => $this->text(),
                'hide_reviews' => $this->integer(1),
                'adult_text' => $this->integer(1),
                'path_to_file' => $this->string(510),
                'info' => $this->text(),
                'image' => $this->text(),
                'image_alt' => $this->string(255),
                'background_image' => $this->string(255),
                'status' => $this->string(255),
                'view' => $this->string(255),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }
        $this->addColumn('song', 'peaks', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('song', 'peaks');
    }
}
