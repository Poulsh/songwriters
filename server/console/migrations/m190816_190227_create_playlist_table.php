<?php

use yii\db\Migration;

/**
 * Handles the creation of table `playlist`.
 */
class m190816_190227_create_playlist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('playlist', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'name' => $this->string(),
            'object_type' => $this->string(),
            'object_id' => $this->integer(),
            'author_id' => $this->integer(),
            'status' => $this->string(),
            'view' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('playlist');
    }
}
