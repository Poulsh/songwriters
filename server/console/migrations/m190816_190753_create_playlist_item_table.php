<?php

use yii\db\Migration;

/**
 * Handles the creation of table `playlist_item`.
 */
class m190816_190753_create_playlist_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('playlist_item', [
            'id' => $this->primaryKey(),
            'playlist_id' => $this->integer()->notNull(),
            'order_num' => $this->integer(),
            'status' => $this->string(),
            'song_id' => $this->integer(),
            'rating' => $this->integer(),
            'plays' => $this->integer(),
            'downloads' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('playlist_item');
    }
}
