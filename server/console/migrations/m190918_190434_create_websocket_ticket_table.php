<?php

use yii\db\Migration;

/**
 * Handles the creation of table `websocket_ticket`.
 */
class m190918_190434_create_websocket_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('websocket_ticket', [
            'id' => $this->primaryKey(),
            'token' => $this->string(),
            'status' => $this->string(),
            'user_id' => $this->integer(),
            'expires' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('websocket_ticket');
    }
}
