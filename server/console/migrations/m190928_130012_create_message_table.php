<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message`.
 */
class m190928_130012_create_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'to_id' => $this->integer(),
            'from_id' => $this->integer(),
            'from_to_type' => $this->string(),

            'dialog_id' => $this->integer(),


            'text' => $this->string(5000),
            'attachment_type' => $this->string(),
            'attachment_id' => $this->integer(),
            'status' => $this->string(),
            'author_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message');
    }
}
