<?php

use yii\db\Migration;

/**
 * Handles the creation of table `message_dialog`.
 */
class m190928_130052_create_message_dialog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message_dialog', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'artist_id' => $this->integer(),
            'radio_id' => $this->integer(),
            'media_id' => $this->integer(),

            'subject_type' => $this->string(),
            'subject_id' => $this->string(),

            'name' => $this->string(),
            'status' => $this->string(),
            'author_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('message_dialog');
    }
}
