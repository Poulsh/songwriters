<?php

use yii\db\Migration;

/**
 * Handles adding radio_status to table `song`.
 */
class m190928_142642_add_radio_status_column_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('song', 'radio_status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('song', 'radio_status');
    }
}
