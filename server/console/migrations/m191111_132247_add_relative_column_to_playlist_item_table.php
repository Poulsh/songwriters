<?php

use yii\db\Migration;

/**
 * Handles adding relative to table `playlist_item`.
 */
class m191111_132247_add_relative_column_to_playlist_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('playlist_item', 'rel_id', $this->integer());
        $this->addColumn('playlist_item', 'rel_type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('playlist_item', 'rel_id');
        $this->dropColumn('playlist_item', 'rel_type');
    }
}
