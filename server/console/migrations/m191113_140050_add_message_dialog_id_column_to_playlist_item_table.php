<?php

use yii\db\Migration;

/**
 * Handles adding message_dialog_id to table `playlist_item`.
 */
class m191113_140050_add_message_dialog_id_column_to_playlist_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('playlist_item', 'dialog_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('playlist_item', 'dialog_id');
    }
}
