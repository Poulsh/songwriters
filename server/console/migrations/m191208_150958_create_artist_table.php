<?php

use yii\db\Migration;

/**
 * Handles the creation of table `artist`.
 */
class m191208_150958_create_artist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('artist', true) === null) {
            $this->createTable('artist', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'page_id' => $this->integer(),
                'name' => $this->string(),
                'country' => $this->string(),
                'city' => $this->string(),
                'hrurl' => $this->string(),
                'main_info' => $this->text(),
                'comment' => $this->text(),
                'image' => $this->string(),
                'image_alt' => $this->string(),
                'status' => $this->string(),
                'view' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('artist');
    }
}
