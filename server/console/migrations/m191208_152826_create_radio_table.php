<?php

use yii\db\Migration;

/**
 * Handles the creation of table `radio`.
 */
class m191208_152826_create_radio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('radio', true) === null) {
            $this->createTable('radio', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'page_id' => $this->integer(),
                'name' => $this->string(),
                'country' => $this->string(),
                'city' => $this->string(),
                'hrurl' => $this->string(),
                'main_info' => $this->text(),
                'comment' => $this->text(),
                'image' => $this->string(),
                'image_alt' => $this->string(),
                'status' => $this->string(),
                'view' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('radio');
    }
}
