<?php

use yii\db\Migration;

/**
 * Handles the creation of table `media`.
 */
class m191208_152943_create_media_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('media', true) === null) {
            $this->createTable('media', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'page_id' => $this->integer(),
                'name' => $this->string(),
                'country' => $this->string(),
                'city' => $this->string(),
                'hrurl' => $this->string(),
                'main_info' => $this->text(),
                'comment' => $this->text(),
                'image' => $this->string(),
                'image_alt' => $this->string(),
                'status' => $this->string(),
                'view' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('media');
    }
}
