<?php

use yii\db\Migration;

/**
 * Handles the creation of table `assign`.
 */
class m191208_153354_create_assign_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('assign', true) === null) {
            $this->createTable('assign', [
                'id' => $this->primaryKey(),
                'type' => $this->string(),
                'artist_id' => $this->integer(),
                'song_id' => $this->integer(),
                'radio_id' => $this->integer(),
                'media_id' => $this->integer(),
                'playlist_id' => $this->integer(),
                'tender_id' => $this->integer(),
                'other_id' => $this->integer(),
                'data' => $this->text(),
                'status' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('assign');
    }
}
