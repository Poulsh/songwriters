<?php

use yii\db\Migration;

/**
 * Handles the creation of table `imagefiles`.
 */
class m191208_153850_create_imagefiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('imagefiles', true) === null) {
            $this->createTable('imagefiles', [
                'id' => $this->primaryKey(),
                'name' => $this->string(510)->notNull(),
                'cloudname' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('imagefiles');
    }
}
