<?php

use yii\db\Migration;

/**
 * Handles the creation of table `language`.
 */
class m191208_154317_create_language_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('language', true) === null) {
            $this->createTable('language', [
                'id' => $this->primaryKey(),
                'name_orig' => $this->string(),
                'name_ru' => $this->string()->notNull(),
                'name_eng' => $this->string(),
                'name_fr' => $this->string(),
                'name_ger' => $this->string(),
                'description' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('language');
    }
}
