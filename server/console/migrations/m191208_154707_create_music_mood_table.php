<?php

use yii\db\Migration;

/**
 * Handles the creation of table `music_mood`.
 */
class m191208_154707_create_music_mood_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('music_mood', true) === null) {
            $this->createTable('music_mood', [
                'id' => $this->primaryKey(),
                'name' => $this->string(),
                'description' => $this->text(),
                'comment' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }



    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('music_mood');
    }
}
