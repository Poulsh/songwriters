<?php

use yii\db\Migration;

/**
 * Handles the creation of table `music_style`.
 */
class m191208_155104_create_music_style_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('music_style', true) === null) {
            $this->createTable('music_style', [
                'id' => $this->primaryKey(),
                'parent_id' => $this->integer(),
                'name' => $this->string()->notNull(),
                'description' => $this->text(),
                'comment' => $this->string(),
                'near_to' => $this->text(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('music_style');
    }
}
