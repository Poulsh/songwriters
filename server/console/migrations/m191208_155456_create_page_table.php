<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page`.
 */
class m191208_155456_create_page_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('page', true) === null) {
            $this->createTable('page', [
                'id' => $this->primaryKey(),
                'hrurl' => $this->string(),
                'seo_logo' => $this->text(),
                'title' => $this->string(),
                'description' => $this->text(),
                'keywords' => $this->text(),
                'pagehead' => $this->string(),
                'pagedescription' => $this->text(),
                'text' => $this->text(),
                'image' => $this->text(),
                'background_image' => $this->text(),
                'imagelink' => $this->string(),
                'imagelink_alt' => $this->string(),
                'sendtopage' => $this->string(),
                'promolink' => $this->string(),
                'promoname' => $this->string(),
                'stylekey' => $this->string(),
                'view' => $this->string(),
                'layout' => $this->string(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('page');
    }
}
