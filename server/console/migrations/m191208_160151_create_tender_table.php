<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tender`.
 */
class m191208_160151_create_tender_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema('tender', true) === null) {
            $this->createTable('tender', [
                'id' => $this->primaryKey(),
                'hrurl' => $this->string(255),
                'author_user_id' => $this->integer(),
                'name' => $this->string(255)->notNull(),
                'description' => $this->text(),
                'conditions' => $this->text(),
                'music_mood' => $this->string(510),
                'music_style1_id' => $this->integer(),
                'music_style2_id' => $this->integer(),
                'music_style3_id' => $this->integer(),
                'language' => $this->string(510),
                'only_vocal' => $this->integer(1),
                'end_date' => $this->integer(),
                'reward' => $this->text(),
                'location' => $this->string(510),
                'accept_demo' => $this->integer(1),
                'adult_text' => $this->integer(1),
                'tracks_from_author' => $this->integer(2),
                'example1' => $this->string(510),
                'example2' => $this->string(510),
                'example3' => $this->string(510),
                'image' => $this->text(),
                'image_from_avatar' => $this->integer(1),
                'background_image' => $this->text(),
                'imagelink' => $this->string(255),
                'imagelink_alt' => $this->string(255),
                'promolink' => $this->string(255),
                'promoname' => $this->string(255),
                'stylekey' => $this->string(255),
                'view' => $this->string(255),
                'layout' => $this->string(255),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);
        }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tender');
    }
}
