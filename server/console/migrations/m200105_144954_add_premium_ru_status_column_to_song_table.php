<?php

use yii\db\Migration;

/**
 * Handles adding premium_ru_status to table `song`.
 */
class m200105_144954_add_premium_ru_status_column_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('song', 'premium_ru_status', $this->string());
        $this->addColumn('song', 'premium_world_status', $this->string());
        $this->addColumn('song', 'review_status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('song', 'premium_ru_status');
        $this->dropColumn('song', 'premium_world_status');
        $this->dropColumn('song', 'review_status');
    }
}
