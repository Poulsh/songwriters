<?php

use yii\db\Migration;

/**
 * Class m200110_123233_alter_rightholders_column_to_song_table
 */
class m200110_123233_alter_rightholders_column_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('song', 'music_style1_id', $this->integer());
        $this->alterColumn('song', 'rightholders', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('song', 'music_style1_id', $this->integer()->notNull());
        $this->alterColumn('song', 'rightholders', $this->text()->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200110_123233_alter_rightholders_column_to_song_table cannot be reverted.\n";

        return false;
    }
    */
}
