<?php

use yii\db\Migration;

/**
 * Handles adding till_to to table `playlist_item`.
 */
class m200123_121940_add_till_to_column_to_playlist_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('playlist_item', 'till_to', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('playlist_item', 'till_to');
    }
}
