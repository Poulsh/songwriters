<?php

use yii\db\Migration;

/**
 * Handles adding type to table `playlist_item`.
 */
class m200125_204208_add_type_column_to_playlist_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('playlist_item', 'service_type', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('playlist_item', 'service_type');
    }
}
