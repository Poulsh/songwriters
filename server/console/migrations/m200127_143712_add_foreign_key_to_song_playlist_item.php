<?php

use yii\db\Migration;

/**
 * Class m200127_143712_add_foreign_key_to_song_playlist_item
 */
class m200127_143712_add_foreign_key_to_song_playlist_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_playlist_song',
            'playlist_item','song_id',
            'song','id',
            'CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey( 'fk_playlist_song','playlist_item');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200127_143712_add_foreign_key_to_song_playlist_item cannot be reverted.\n";

        return false;
    }
    */
}
