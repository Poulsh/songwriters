<?php

use yii\db\Migration;

/**
 * Class m200127_153429_add_foreign_key_to_dialog_message
 */
class m200127_153429_add_foreign_key_to_dialog_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_dialog_message',
            'message','dialog_id',
            'message_dialog','id',
            'CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey( 'fk_dialog_message','message');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200127_153429_add_foreign_key_to_dialog_message cannot be reverted.\n";

        return false;
    }
    */
}
