<?php

use yii\db\Migration;

/**
 * Handles adding agreed_with_publication to table `song`.
 */
class m200528_142630_add_agreed_with_publication_column_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('song', 'publication_agreed', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('song', 'publication_agreed');
    }
}
