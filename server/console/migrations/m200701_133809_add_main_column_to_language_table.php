<?php

use yii\db\Migration;

/**
 * Handles adding main to table `language`.
 */
class m200701_133809_add_main_column_to_language_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('language', 'main', $this->boolean());
        $this->addColumn('language', 'main_sort', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('language', 'main');
        $this->dropColumn('language', 'main_sort');
    }
}
