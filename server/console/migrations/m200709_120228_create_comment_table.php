<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m200709_120228_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'text' => $this->string(510),
            'object_type' => $this->integer(),
            'object_id' => $this->integer(),
            'author_type' => $this->integer(),
            'author_id' => $this->integer(),
            'status' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }
}
