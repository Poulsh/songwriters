<?php

use yii\db\Migration;

/**
 * Class m200709_145331_delete_column_comments_to_song_table
 */
class m200709_145331_delete_column_comments_to_song_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('song', 'comments');
        $this->addColumn('song', 'author_comment', $this->string(10000));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('song', 'author_comment');
        $this->addColumn('song', 'comments', $this->string(10000));
    }


}
