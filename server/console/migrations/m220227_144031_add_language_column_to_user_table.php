<?php

use yii\db\Migration;

/**
 * Handles adding language to table `user`.
 */
class m220227_144031_add_language_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'language', $this->string(5));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'language');
    }
}
